$(document).ready(function() {
/* ------------ START PEUGEOT 301 -------------- */	
    var images = [
        {
            image: 'images/cars/peugeot/301/peugeot_301_big_1.jpg',
            thumb: 'images/cars/peugeot/301/peugeot_301_small_1.jpg',
            link:  'images/cars/peugeot/301/peugeot_301_big_1.jpg'
        },
		{
            image: 'images/cars/peugeot/301/peugeot_301_big_2.jpg',
            thumb: 'images/cars/peugeot/301/peugeot_301_small_2.jpg',
            link:  'images/cars/peugeot/301/peugeot_301_big_2.jpg'
        },
		{
            image: 'images/cars/peugeot/301/peugeot_301_big_3.jpg',
            thumb: 'images/cars/peugeot/301/peugeot_301_small_3.jpg',
            link:  'images/cars/peugeot/301/peugeot_301_big_3.jpg'
        },
		{
            image: 'images/cars/peugeot/301/peugeot_301_big_4.jpg',
            thumb: 'images/cars/peugeot/301/peugeot_301_small_4.jpg',
            link:  'images/cars/peugeot/301/peugeot_301_big_4.jpg'
        },
		{
            image: 'images/cars/peugeot/301/peugeot_301_big_5.jpg',
            thumb: 'images/cars/peugeot/301/peugeot_301_small_5.jpg',
            link:  'images/cars/peugeot/301/peugeot_301_big_5.jpg'
        }
    ];
/* ------------ END PEUGEOT 301 -------------- */	

	var gallery = $("#gallery");
    
    Galleria.loadTheme('galleria/themes/simplecoding/galleria.simplecoding.js');
    
   
        $("#gallery").galleria({
            data_source: images,
            width: 600,
            height: 500,
            clicknext: true,
			showCounter: false,
			autoplay: 3000
			
        });
        return false;
		
	

});
