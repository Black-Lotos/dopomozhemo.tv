$(document).ready(function() {
/* ------------ START PEUGEOT 4008 -------------- */	
    var images = [
        {
            image: 'images/cars/peugeot/4008/peugeot_4008_big_1.jpg',
            thumb: 'images/cars/peugeot/4008/peugeot_4008_small_1.jpg',
            link:  'images/cars/peugeot/4008/peugeot_4008_big_1.jpg'
        },
		{
            image: 'images/cars/peugeot/4008/peugeot_4008_big_2.jpg',
            thumb: 'images/cars/peugeot/4008/peugeot_4008_small_2.jpg',
            link:  'images/cars/peugeot/4008/peugeot_4008_big_2.jpg'
        },
		{
            image: 'images/cars/peugeot/4008/peugeot_4008_big_3.jpg',
            thumb: 'images/cars/peugeot/4008/peugeot_4008_small_3.jpg',
            link:  'images/cars/peugeot/4008/peugeot_4008_big_3.jpg'
        },
		{
            image: 'images/cars/peugeot/4008/peugeot_4008_big_4.jpg',
            thumb: 'images/cars/peugeot/4008/peugeot_4008_small_4.jpg',
            link:  'images/cars/peugeot/4008/peugeot_4008_big_4.jpg'
        },
		{
            image: 'images/cars/peugeot/4008/peugeot_4008_big_5.jpg',
            thumb: 'images/cars/peugeot/4008/peugeot_4008_small_5.jpg',
            link:  'images/cars/peugeot/4008/peugeot_4008_big_5.jpg'
        }
    ];
/* ------------ END PEUGEOT 4008 -------------- */	

	var gallery = $("#gallery");
    
    Galleria.loadTheme('galleria/themes/simplecoding/galleria.simplecoding.js');
    
   
        $("#gallery").galleria({
            data_source: images,
            width: 600,
            height: 500,
            clicknext: true,
			showCounter: false,
			autoplay: 3000
			
        });
        return false;
		
	

});
