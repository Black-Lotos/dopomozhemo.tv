$(document).ready(function() {
/* ------------ START PEUGEOT RCZ -------------- */	
    var images = [
        {
            image: 'images/cars/peugeot/partner/peugeot_partner_big_1.jpg',
            thumb: 'images/cars/peugeot/partner/peugeot_partner_small_1.jpg',
            link:  'images/cars/peugeot/partner/peugeot_partner_big_1.jpg'
        },
		{
            image: 'images/cars/peugeot/partner/peugeot_partner_big_2.jpg',
            thumb: 'images/cars/peugeot/partner/peugeot_partner_small_2.jpg',
            link:  'images/cars/peugeot/partner/peugeot_partner_big_2.jpg'
        },
		{
            image: 'images/cars/peugeot/partner/peugeot_partner_big_3.jpg',
            thumb: 'images/cars/peugeot/partner/peugeot_partner_small_3.jpg',
            link:  'images/cars/peugeot/partner/peugeot_partner_big_3.jpg'
        },
		{
            image: 'images/cars/peugeot/partner/peugeot_partner_big_4.jpg',
            thumb: 'images/cars/peugeot/partner/peugeot_partner_small_4.jpg',
            link:  'images/cars/peugeot/partner/Peugeot_partner_big_4.jpg'
        },
		{
            image: 'images/cars/peugeot/partner/peugeot_partner_big_5.jpg',
            thumb: 'images/cars/peugeot/partner/peugeot_partner_small_5.jpg',
            link:  'images/cars/peugeot/partner/Peugeot_partner_big_5.jpg'
        }
    ];
/* ------------ END PEUGEOT RCZ -------------- */	

	var gallery = $("#gallery");
    
    Galleria.loadTheme('galleria/themes/simplecoding/galleria.simplecoding.js');
    
   
        $("#gallery").galleria({
            data_source: images,
            width: 600,
            height: 500,
            clicknext: true,
			showCounter: false,
			autoplay: 3000
			
        });
        return false;
		
	

});
