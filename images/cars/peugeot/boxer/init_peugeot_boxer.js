$(document).ready(function() {
/* ------------ START PEUGEOT RCZ -------------- */	
    var images = [
        {
            image: 'images/cars/peugeot/boxer/peugeot_boxer_big_1.jpg',
            thumb: 'images/cars/peugeot/boxer/peugeot_boxer_small_1.jpg',
            link:  'images/cars/peugeot/boxer/peugeot_boxer_big_1.jpg'
        },
		{
            image: 'images/cars/peugeot/boxer/peugeot_boxer_big_2.jpg',
            thumb: 'images/cars/peugeot/boxer/peugeot_boxer_small_2.jpg',
            link:  'images/cars/peugeot/boxer/peugeot_boxer_big_2.jpg'
        }
		
    ];
/* ------------ END PEUGEOT RCZ -------------- */	

	var gallery = $("#gallery");
    
    Galleria.loadTheme('galleria/themes/simplecoding/galleria.simplecoding.js');
    
   
        $("#gallery").galleria({
            data_source: images,
            width: 600,
            height: 500,
            clicknext: true,
			showCounter: false,
			autoplay: 3000
			
        });
        return false;
		
	

});
