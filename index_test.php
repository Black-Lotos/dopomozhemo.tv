<?php
/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     05.01.2015 17:33:05                          */
/*==============================================================*/
/*==============================================================*/
/* Table: tb_answer                                             */
/*==============================================================*/
create table tb_answer
(
   answer_id            int(11) not null auto_increment,
   question_id          int(11) not null,
   user_id              int(11),
   answer_message       text,
   answer_date          datetime not null,
   primary key (answer_id, question_id)
);

/*==============================================================*/
/* Table: tb_ask_to_contact                                     */
/*==============================================================*/
create table tb_ask_to_contact
(
   ask_to_contact_id    int(11) not null auto_increment,
   user_id              int(11),
   ask_to_contact_title varchar(250),
   ask_to_contact_text  varchar(250),
   ask_to_contact_name  varchar(100),
   ask_to_contact_phone varchar(50),
   ask_to_contact_email varchar(250),
   ask_to_contact_date  datetime not null,
   ask_to_contact_status tinyint not null default 0,
   ask_to_contact_resolved tinyint not null default 0,
   primary key (ask_to_contact_id)
);

/*==============================================================*/
/* Table: tb_blogs                                              */
/*==============================================================*/
create table tb_blogs
(
   blogs_id             int(11) not null auto_increment,
   cblogs_id            int(11) not null,
   language_id          int(11) not null,
   images_id            int(11),
   blogs_title          varchar(100),
   blogs_text           varchar(500),
   blogs_status         tinyint not null default 0,
   blogs_main           tinyint not null default 0,
   blogs_top            tinyint not null default 0,
   blogs_owner          int,
   primary key (blogs_id, cblogs_id, language_id)
);

/*==============================================================*/
/* Table: tb_blogs_category                                     */
/*==============================================================*/
create table tb_blogs_category
(
   cblogs_id            int(11) not null auto_increment,
   language_id          int(11) not null,
   cblogs_title         varchar(100),
   cblogs_text          varchar(500),
   cblogs_status        tinyint not null default 0,
   primary key (cblogs_id, language_id)
);

/*==============================================================*/
/* Table: tb_category_pictures                                  */
/*==============================================================*/
create table tb_category_pictures
(
   cpicture_id          int(11) not null auto_increment,
   language_id          int(11) not null,
   cpicture_title       varchar(250),
   cpicture_text        text,
   cpicture_status      tinyint not null default 0,
   primary key (cpicture_id, language_id)
);

/*==============================================================*/
/* Table: tb_comments                                           */
/*==============================================================*/
create table tb_comments
(
   comments_id          int(11) not null auto_increment,
   user_id              int(11) not null,
   tb__language_id      int(11),
   cblogs_id            int(11),
   blogs_id             int(11),
   tb__language_id2     int(11),
   nc_id                int(11),
   news_id              int(11),
   proposal_id          int(11),
   comments_rate        int default 0,
   comments_text        text,
   comments_created     datetime not null,
   comments_status      tinyint not null default 0,
   comments_reply_id    int default 0,
   comments_owner_id    int default 0,
   images_id            int(11),
   language_id          int(11) not null,
   primary key (comments_id, user_id)
);

/*==============================================================*/
/* Table: tb_comments_rate                                      */
/*==============================================================*/
create table tb_comments_rate
(
   comments_id          int(11) not null,
   user_id              int(11) not null,
   cr_ip_address        char(15),
   cr_date              datetime not null,
   cr_rate              int not null,
   primary key (comments_id, user_id)
);

/*==============================================================*/
/* Table: tb_comments_view                                      */
/*==============================================================*/
create table tb_comments_view
(
   comments_id          int(11) not null,
   user_id              int(11) not null,
   cv_date              datetime not null,
   primary key (comments_id, user_id)
);

/*==============================================================*/
/* Table: tb_content                                            */
/*==============================================================*/
create table tb_content
(
   content_id           int(11) not null auto_increment,
   ccontent_id          int(11) not null,
   language_id          int(11) not null,
   content_title        varchar(250),
   content_text         text,
   content_status       tinyint not null default 0,
   content_status_top   tinyint not null default 0,
   content_status_main  tinyint not null default 0,
   content_creates      datetime not null,
   primary key (content_id, ccontent_id, language_id)
);

/*==============================================================*/
/* Table: tb_content_category                                   */
/*==============================================================*/
create table tb_content_category
(
   ccontent_id          int(11) not null auto_increment,
   language_id          int(11) not null,
   ccontent_title       varchar(250),
   ccontent_text        text,
   ccontent_status      tinyint not null default 0,
   primary key (ccontent_id, language_id)
);

/*==============================================================*/
/* Table: tb_footerlink                                         */
/*==============================================================*/
create table tb_footerlink
(
   footerlink_id        int(11) not null auto_increment,
   cfooterlink_id       int(11) not null,
   language_id          int(11) not null,
   footerlink_title     varchar(250),
   footerlink_text      text,
   footerlink_url       varchar(250),
   footerlink_status    tinyint not null default 0,
   primary key (footerlink_id, cfooterlink_id, language_id)
);

/*==============================================================*/
/* Table: tb_footerlink_category                                */
/*==============================================================*/
create table tb_footerlink_category
(
   cfooterlink_id       int(11) not null auto_increment,
   language_id          int(11) not null,
   cfooterlink_title    varchar(250),
   cfooterlink_text     text,
   cfooterlink_status   tinyint not null default 0,
   primary key (cfooterlink_id, language_id)
);

/*==============================================================*/
/* Table: tb_images                                             */
/*==============================================================*/
create table tb_images
(
   images_id            int(11) not null auto_increment,
   img_title            varchar(250),
   img_description      text,
   img_weight           int not null default 0,
   img_created          datetime not null,
   img_hash             varchar(100),
   img_image            varchar(250),
   img_folder           varchar(250),
   img_width            int,
   img_height           int,
   img_url              varchar(500),
   img_status           tinyint not null default 0,
   img_main             tinyint not null default 0,
   primary key (images_id)
);

/*==============================================================*/
/* Table: tb_language                                           */
/*==============================================================*/
create table tb_language
(
   language_id          int(11) not null auto_increment,
   lan_title            varchar(250),
   lan_code             varchar(5) not null,
   lan_weight           int not null default 0,
   lan_status           tinyint not null default 0,
   primary key (language_id)
);

/*==============================================================*/
/* Table: tb_menu                                               */
/*==============================================================*/
create table tb_menu
(
   menu_id              int(11) not null auto_increment,
   menu_category_id     int(11) not null,
   language_id          int(11) not null,
   mi_name              varchar(250) not null,
   mi_url               varchar(250) not null,
   mi_access_page       varchar(500),
   mi_title             varchar(250) not null,
   mi_type              int(11) not null,
   mi_weight            int not null default 0,
   mi_description       text,
   mi_auto              tinyint not null default 0,
   mi_class             varchar(250),
   mi_function          varchar(250),
   mi_filename          varchar(250),
   mi_updated           tinyint not null default 0,
   mi_page_arguments    varchar(500),
   mi_page_callback     varchar(500),
   mi_status            tinyint not null default 0,
   mi_module            varchar(100),
   primary key (menu_id, menu_category_id, language_id, mi_name, mi_url, mi_title)
);

/*==============================================================*/
/* Table: tb_menu_category                                      */
/*==============================================================*/
create table tb_menu_category
(
   menu_category_id     int(11) not null auto_increment,
   language_id          int(11) not null,
   mc_name              varchar(250),
   mc_title             varchar(250),
   mc_description       text,
   mc_status            tinyint not null default 0,
   mc_weight            int not null default 0,
   primary key (menu_category_id, language_id)
);

/*==============================================================*/
/* Table: tb_metadata                                           */
/*==============================================================*/
create table tb_metadata
(
   metadata_id          int(11) not null auto_increment,
   ss_id                int(11) not null,
   metadata_name        varchar(100) not null,
   metadata_url         varchar(500) not null,
   metadata_keywords    varchar(250) not null,
   metadata_type        varchar(100) not null,
   primary key (metadata_id)
);

/*==============================================================*/
/* Table: tb_news                                               */
/*==============================================================*/
create table tb_news
(
   news_id              int(11) not null auto_increment,
   nc_id                int(11) not null,
   language_id          int(11) not null,
   images_id            int(11),
   news_title           varchar(250) not null,
   news_text            text not null,
   news_status          tinyint not null default 0,
   news_status_top      tinyint not null default 0,
   news_status_main     tinyint not null default 0,
   news_creates         datetime not null,
   primary key (news_id, nc_id, language_id)
);

/*==============================================================*/
/* Table: tb_news_category                                      */
/*==============================================================*/
create table tb_news_category
(
   nc_id                int(11) not null auto_increment,
   language_id          int(11) not null,
   nc_title             varchar(250),
   nc_text              text,
   nc_status            tinyint not null default 0,
   primary key (nc_id, language_id)
);

/*==============================================================*/
/* Table: tb_pictures                                           */
/*==============================================================*/
create table tb_pictures
(
   picture_id           int(11) not null auto_increment,
   cpicture_id          int(11) not null,
   language_id          int(11) not null,
   picture_title        varchar(250),
   picture_text         text,
   picture_status       tinyint not null default 0,
   picture_code         varchar(250),
   picture_width        int,
   picture_height       int,
   primary key (picture_id, cpicture_id, language_id)
);

/*==============================================================*/
/* Table: tb_professional                                       */
/*==============================================================*/
create table tb_professional
(
   proffessional_id     int not null,
   p_category_id        int(11) not null,
   primary key (proffessional_id)
);

/*==============================================================*/
/* Table: tb_professional_category                              */
/*==============================================================*/
create table tb_professional_category
(
   p_category_id        int(11) not null auto_increment,
   primary key (p_category_id)
);

/*==============================================================*/
/* Table: tb_proposal                                           */
/*==============================================================*/
create table tb_proposal
(
   proposal_id          int(11) not null auto_increment,
   user_id              int(11) not null,
   proposal_title       varchar(250),
   proposal_text        text,
   proposal_name        varchar(100),
   proposal_phone       varchar(50),
   proposal_email       varchar(250),
   proposal_date        datetime not null,
   proposal_status      tinyint not null default 0,
   proposal_resolved    tinyint not null default 0,
   primary key (proposal_id)
);

/*==============================================================*/
/* Table: tb_question                                           */
/*==============================================================*/
create table tb_question
(
   question_id          int(11) not null auto_increment,
   user_id              int(11) not null,
   question_date        datetime not null,
   question_message     text,
   question_name        varchar(100),
   question_email       varchar(100),
   question_phone       varchar(50),
   primary key (question_id, user_id)
);

/*==============================================================*/
/* Table: tb_register_mails                                     */
/*==============================================================*/
create table tb_register_mails
(
   rm_id                int(11) not null auto_increment,
   rm_name              varchar(250) not null,
   rm_text              text not null,
   rm_subject           varchar(250) not null,
   rm_type              varchar(250) not null,
   primary key (rm_id)
);

/*==============================================================*/
/* Table: tb_register_mails_settings                            */
/*==============================================================*/
create table tb_register_mails_settings
(
   rms_id               int(11) not null auto_increment,
   rm_id                int(11) not null,
   rms_slug             varchar(250) not null,
   rms_email            text not null,
   rms_status           tinyint not null default 0,
   primary key (rms_id, rm_id)
);

/*==============================================================*/
/* Table: tb_site_settings                                      */
/*==============================================================*/
create table tb_site_settings
(
   ss_id                int(11) not null auto_increment,
   ss_title             varchar(100) not null,
   ss_charset           varchar(100) not null,
   primary key (ss_id)
);

/*==============================================================*/
/* Table: tb_user                                               */
/*==============================================================*/
create table tb_user
(
   user_id              int(11) not null auto_increment,
   user_login           varchar(250) character set utf8,
   user_email           varchar(250),
   user_pass            varchar(250),
   user_name            varchar(250) character set utf8,
   user_active          tinyint not null default 0,
   user_reg_date        datetime not null,
   user_last_login      datetime not null,
   user_avatar_id       int(11) not null,
   user_is_online       tinyint not null default 0,
   user_auth_key        varchar(250) character set utf8 not null,
   user_recovery_key    varchar(250) character set utf8,
   user_phone           varchar(50),
   primary key (user_id)
);

/*==============================================================*/
/* Table: tb_user_logs                                          */
/*==============================================================*/
create table tb_user_logs
(
   user_logs_id         int(11) not null auto_increment,
   user_id              int(11) not null,
   logs_type            tinyint not null default 0,
   logs_date            datetime not null,
   primary key (user_logs_id, user_id)
);

/*==============================================================*/
/* Table: tb_user_role                                          */
/*==============================================================*/
create table tb_user_role
(
   role_id              int(11) not null auto_increment,
   role_title           varchar(250) character set utf8,
   role_weight          int not null default 0,
   primary key (role_id)
);

/*==============================================================*/
/* Table: tb_user_roles_id                                      */
/*==============================================================*/
create table tb_user_roles_id
(
   role_id              int(11) not null,
   user_id              int(11) not null,
   primary key (role_id, user_id)
);

/*==============================================================*/
/* Table: tb_user_rules                                         */
/*==============================================================*/
create table tb_user_rules
(
   rules_id             int(11) not null auto_increment,
   ur_source            varchar(250) character set utf8,
   ur_title             varchar(250) character set utf8,
   ur_module            varchar(250) character set utf8,
   ur_weight            int not null default 0,
   primary key (rules_id)
);

/*==============================================================*/
/* Table: tb_user_rules_roles_id                                */
/*==============================================================*/
create table tb_user_rules_roles_id
(
   rules_id             int(11) not null,
   user_id              int(11) not null,
   primary key (rules_id, user_id)
);
