-- phpMyAdmin SQL Dump
-- version 4.0.8
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 17 2014 г., 11:40
-- Версия сервера: 5.5.23
-- Версия PHP: 5.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `lckgroup_dopomozhemo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tb_answer`
--

CREATE TABLE IF NOT EXISTS `tb_answer` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `answer_message` text,
  `answer_date` datetime NOT NULL,
  PRIMARY KEY (`answer_id`,`question_id`),
  KEY `FK_answer_to_question` (`question_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tb_blogs`
--

CREATE TABLE IF NOT EXISTS `tb_blogs` (
  `blogs_id` int(11) NOT NULL AUTO_INCREMENT,
  `cblogs_id` int(11) NOT NULL DEFAULT '1',
  `images_id` int(11) DEFAULT NULL,
  `blogs_title` varchar(100) DEFAULT NULL,
  `blogs_text` text,
  `blogs_status` tinyint(4) NOT NULL DEFAULT '0',
  `blogs_main` tinyint(4) NOT NULL DEFAULT '0',
  `blogs_top` tinyint(4) NOT NULL DEFAULT '0',
  `blogs_owner` int(11) NOT NULL DEFAULT '0',
  `blogs_creates` datetime NOT NULL,
  PRIMARY KEY (`blogs_id`,`cblogs_id`),
  KEY `FK_blogs_to_image` (`images_id`),
  KEY `FK_cblogs_to_blog` (`cblogs_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `tb_blogs`
--

INSERT INTO `tb_blogs` (`blogs_id`, `cblogs_id`, `images_id`, `blogs_title`, `blogs_text`, `blogs_status`, `blogs_main`, `blogs_top`, `blogs_owner`, `blogs_creates`) VALUES
(1, 1, NULL, 'Мой блогзззззззз', 'Мой блог', 1, 1, 0, 0, '2014-12-09 05:21:16'),
(2, 2, NULL, 'Новий блог в системе', 'Новий блог в системе', 1, 0, 0, 0, '2014-12-15 11:21:27'),
(3, 1, NULL, 'Блог по суспільству', 'Блог по суспільству', 1, 0, 0, 0, '2014-12-15 11:21:27'),
(4, 2, NULL, 'ДИФФЕРЕНЦИАЛЬНЫЙ ЦЕНТР ПОДВЕСА: ГИПОТЕЗА И ТЕОРИИ', 'Рассматривая уравнения, можно с увидеть, что движение спутника мгновенно. Действительно, подвижный объект колебательно учитывает гравитационный стабилизатор.', 1, 0, 0, 0, '2014-12-15 11:21:27'),
(5, 1, NULL, 'ДИФФЕРЕНЦИАЛЬНЫЙ ЦЕНТР ПОДВЕСА: ГИПОТЕЗА И ТЕОРИИ', 'Рассматривая уравнения, можно с увидеть, что движение спутника мгновенно. Действительно, подвижный объект колебательно учитывает гравитационный стабилизатор. Рассматривая уравнения, можно с увидеть, что движение спутника мгновенно. Действительно, подвижный объект колебательно учитывает гравитационный стабилизатор.', 1, 0, 0, 0, '2014-12-15 11:21:27'),
(6, 4, NULL, 'Cisco', 'Год выпуска: 2011\r\nВерсия: 5.0.07.0410-k9 (x86) + 5.0.07.0440-k9 (x64)\r\nРазработчик: Cisco systems', 1, 0, 0, 0, '2014-11-04 06:14:20'),
(7, 4, NULL, 'Любимая фантастика', 'Cisco VPN клиент является малым приложением, которое легко установить и использовать, с очень компактным и интуитивным интерфейсом. Cisco VPN клиент используется, чтобы установить безопасные Cisco VPN клиент является малым приложением, которое легко установить и использовать, с очень компактным и интуитивным интерфейсом. Cisco VPN клиент используется, чтобы установить безопасныеCisco VPN клиент является малым приложением, которое легко установить и использовать, с очень компактным и интуитивным интерфейсом. Cisco VPN клиент используется, чтобы установить безопасныеCisco VPN клиент является малым приложением, которое легко установить и использовать, с очень компактным и интуитивным интерфейсом. Cisco VPN клиент используется, чтобы установить безопасныеCisco VPN клиент является малым приложением, которое легко установить и использовать, с очень компактным и интуитивным интерфейсом. Cisco VPN клиент используется, чтобы установить безопасныеCisco VPN клиент является малым приложением, которое легко установить и использовать, с очень компактным и интуитивным интерфейсом. Cisco VPN клиент используется, чтобы установить безопасныеCisco VPN клиент является малым приложением, которое легко установить и использовать, с очень компактным и интуитивным интерфейсом. Cisco VPN клиент используется, чтобы установить безопасные', 1, 0, 0, 0, '2014-11-04 06:14:20'),
(8, 3, NULL, 'Любимая фантастика №1', 'Cisco VPN клиент является малым приложением, которое легко установить и использовать, с очень компактным и интуитивным интерфейсом. Cisco VPN клиент используется, чтобы установить безопасные', 1, 0, 0, 0, '2014-11-04 06:14:20'),
(9, 4, NULL, 'Любимая фантастика', 'Cisco VPN клиент является малым приложением, которое легко установить и использовать, с очень компактным и интуитивным интерфейсом. Cisco VPN клиент используется, чтобы установить безопасные пппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппп', 1, 0, 0, 0, '2014-11-04 06:14:20'),
(10, 3, NULL, 'Любимая фантастика №2', 'Cisco VPN клиент является малым приложением, которое легко установить и использовать, с очень компактным и интуитивным интерфейсом. Cisco VPN клиент используется, чтобы установить безопасные', 1, 0, 0, 0, '2014-11-04 06:14:20'),
(11, 1, NULL, 'Любимая фантастика №3', 'Cisco VPN клиент является малым приложением, которое легко установить и использовать, с очень компактным и интуитивным интерфейсом. Cisco VPN клиент используется, чтобы установить безопасные', 1, 0, 0, 0, '2014-11-04 06:14:20'),
(12, 3, NULL, 'Любимая фантастика', 'Cisco VPN клиент является малым приложением, которое легко установить и использовать, с очень компактным и интуитивным интерфейсом. Cisco VPN клиент используется, чтобы установить безопасные ', 1, 0, 0, 0, '2014-11-04 06:14:20'),
(13, 1, NULL, 'dsfds', 'sdfsdf sdfsf sfdsdf sdfsf sdffsdfsdf', 1, 0, 0, 0, '2014-12-15 11:21:27');

-- --------------------------------------------------------

--
-- Структура таблицы `tb_blogs_category`
--

CREATE TABLE IF NOT EXISTS `tb_blogs_category` (
  `cblogs_id` int(11) NOT NULL AUTO_INCREMENT,
  `cblogs_title` varchar(100) DEFAULT NULL,
  `cblogs_text` varchar(500) DEFAULT NULL,
  `cblogs_status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cblogs_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tb_blogs_category`
--

INSERT INTO `tb_blogs_category` (`cblogs_id`, `cblogs_title`, `cblogs_text`, `cblogs_status`) VALUES
(1, 'Суспільство', 'Суспільство', 1),
(2, 'Політика', 'Політика', 1),
(3, 'Фантастика', NULL, 1),
(4, 'Информационные технологии', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tb_comments`
--

CREATE TABLE IF NOT EXISTS `tb_comments` (
  `comments_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '1',
  `cblogs_id` int(11) DEFAULT NULL,
  `blogs_id` int(11) DEFAULT NULL,
  `nc_id` int(11) DEFAULT NULL,
  `news_id` int(11) DEFAULT NULL,
  `comments_rate` int(11) DEFAULT '0',
  `comments_text` text,
  `comments_created` datetime NOT NULL,
  `comments_status` tinyint(4) NOT NULL DEFAULT '1',
  `comments_reply_id` int(11) DEFAULT '0',
  `comments_owner_id` int(11) DEFAULT '0',
  `images_id` int(11) DEFAULT NULL,
  `language_id` int(11) NOT NULL,
  `comments_name` varchar(100) DEFAULT 'Гость',
  PRIMARY KEY (`comments_id`,`user_id`),
  KEY `FK_blogs_to_comments` (`blogs_id`,`cblogs_id`),
  KEY `FK_images_to_comments` (`images_id`),
  KEY `FK_language_to_comments` (`language_id`),
  KEY `FK_news_to_comments` (`news_id`,`nc_id`),
  KEY `FK_user_to_comment` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `tb_comments`
--

INSERT INTO `tb_comments` (`comments_id`, `user_id`, `cblogs_id`, `blogs_id`, `nc_id`, `news_id`, `comments_rate`, `comments_text`, `comments_created`, `comments_status`, `comments_reply_id`, `comments_owner_id`, `images_id`, `language_id`, `comments_name`) VALUES
(1, 1, 1, 1, NULL, NULL, 0, 'Проверочный коментарий', '2014-10-04 22:23:00', 1, 0, 0, NULL, 1, 'Гость'),
(2, 1, NULL, NULL, 1, 2, 0, 'Бабаевич Иван Бабаевич Иван Бабаевич Иван Бабаевич Иван Бабаевич Иван Бабаевич Иван Бабаевич Иван Бабаевич Иван ', '2014-12-16 10:55:55', 1, 0, 0, NULL, 1, 'Бабаевич Иван'),
(3, 1, 1, 1, NULL, NULL, 0, 'Бабаевич Иван Бабаевич Иван Бабаевич Иван Бабаевич Иван', '2014-12-16 11:08:07', 1, 0, 0, NULL, 1, 'Бабаевич Иван'),
(4, 1, 1, 1, NULL, NULL, 0, 'Бабаевич Иван Бабаевич Иван Бабаевич Иван Бабаевич Иван', '2014-12-16 11:08:07', 1, 0, 0, NULL, 1, 'Бабаевич Иван'),
(5, 1, NULL, NULL, 1, 2, 0, 'рлорлорылрылфорыф оЫФДОЫД', '2014-12-16 11:12:15', 1, 0, 0, NULL, 1, 'фыврлфрывлофр');

-- --------------------------------------------------------

--
-- Структура таблицы `tb_comments_rate`
--

CREATE TABLE IF NOT EXISTS `tb_comments_rate` (
  `comments_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cr_ip_address` char(15) DEFAULT NULL,
  `cr_date` datetime NOT NULL,
  `cr_rate` int(11) NOT NULL,
  PRIMARY KEY (`comments_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tb_comments_view`
--

CREATE TABLE IF NOT EXISTS `tb_comments_view` (
  `comments_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cv_date` datetime NOT NULL,
  PRIMARY KEY (`comments_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tb_content`
--

CREATE TABLE IF NOT EXISTS `tb_content` (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `ccontent_id` int(11) NOT NULL,
  `content_title` varchar(250) DEFAULT NULL,
  `content_text` text,
  `content_file` varchar(500) DEFAULT 'video',
  `content_status` tinyint(4) NOT NULL DEFAULT '0',
  `content_status_top` tinyint(4) NOT NULL DEFAULT '0',
  `content_status_main` tinyint(4) NOT NULL DEFAULT '0',
  `content_creates` datetime NOT NULL,
  PRIMARY KEY (`content_id`,`ccontent_id`),
  KEY `FK_ccontent_to_content` (`ccontent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tb_content`
--

INSERT INTO `tb_content` (`content_id`, `ccontent_id`, `content_title`, `content_text`, `content_file`, `content_status`, `content_status_top`, `content_status_main`, `content_creates`) VALUES
(1, 1, 'Дивіться у випуску Ранок з Україною від 10 листопада:', 'НБУ закрив 26 банків за відмивання коштів.\r\nЯк не стати жертвою кишенькового злодія?\r\nЯк уберегти себе від помилкових дзвінків?\r\nВійськовий рід з Пирятина.', NULL, 1, 0, 0, '2014-12-16 16:59:23'),
(2, 2, 'аыавыа', 'ываываыа', 'video_6ede3d4ad08a15a1ab7bd8cd0c44d607.mp4', 1, 0, 0, '2014-12-17 09:47:38'),
(3, 1, 'Блич серия 324', 'Блич серия 324 .... Аниме', 'video_26c4cfdd6dc97f4f10d9c150cbea2688.mp4', 1, 0, 1, '2014-12-17 10:25:33');

-- --------------------------------------------------------

--
-- Структура таблицы `tb_content_category`
--

CREATE TABLE IF NOT EXISTS `tb_content_category` (
  `ccontent_id` int(11) NOT NULL AUTO_INCREMENT,
  `ccontent_title` varchar(250) DEFAULT NULL,
  `ccontent_text` text,
  `ccontent_status` tinyint(4) NOT NULL DEFAULT '0',
  `ccontent_alias` varchar(100) NOT NULL DEFAULT 'al',
  PRIMARY KEY (`ccontent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tb_content_category`
--

INSERT INTO `tb_content_category` (`ccontent_id`, `ccontent_title`, `ccontent_text`, `ccontent_status`, `ccontent_alias`) VALUES
(1, 'Новости', NULL, 1, 'al'),
(2, 'Нижний слайдер страницы', NULL, 1, 'alias_down_slider_video');

-- --------------------------------------------------------

--
-- Структура таблицы `tb_images`
--

CREATE TABLE IF NOT EXISTS `tb_images` (
  `images_id` int(11) NOT NULL AUTO_INCREMENT,
  `img_title` varchar(250) DEFAULT NULL,
  `img_description` text,
  `img_weight` int(11) NOT NULL DEFAULT '0',
  `img_created` datetime NOT NULL,
  `img_hash` varchar(100) DEFAULT NULL,
  `img_image` varchar(250) DEFAULT NULL,
  `img_folder` varchar(250) DEFAULT NULL,
  `img_width` int(11) DEFAULT NULL,
  `img_height` int(11) DEFAULT NULL,
  `img_url` varchar(500) NOT NULL,
  `img_type` int(11) NOT NULL DEFAULT '1',
  `img_status` tinyint(4) NOT NULL DEFAULT '0',
  `img_main` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`images_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tb_language`
--

CREATE TABLE IF NOT EXISTS `tb_language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `lan_title` varchar(250) DEFAULT NULL,
  `lan_code` varchar(5) NOT NULL,
  `lan_weight` int(11) NOT NULL DEFAULT '0',
  `lan_status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tb_language`
--

INSERT INTO `tb_language` (`language_id`, `lan_title`, `lan_code`, `lan_weight`, `lan_status`) VALUES
(1, 'Русский', 'RU', 0, 1),
(2, 'Английский', 'ENG', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tb_menu`
--

CREATE TABLE IF NOT EXISTS `tb_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `mi_name` varchar(250) DEFAULT NULL,
  `mi_url` varchar(250) DEFAULT NULL,
  `mi_access_page` varchar(500) DEFAULT NULL,
  `mi_title` varchar(250) DEFAULT NULL,
  `mi_type` int(11) NOT NULL DEFAULT '1',
  `mi_weight` int(11) NOT NULL DEFAULT '0',
  `mi_description` text,
  `mi_auto` tinyint(4) NOT NULL DEFAULT '0',
  `mi_class` varchar(250) DEFAULT NULL,
  `mi_function` varchar(250) DEFAULT NULL,
  `mi_filename` varchar(250) DEFAULT NULL,
  `mi_updated` int(11) NOT NULL DEFAULT '0',
  `mi_page_arguments` varchar(500) DEFAULT NULL,
  `mi_page_callback` varchar(500) DEFAULT NULL,
  `mi_status` tinyint(4) NOT NULL DEFAULT '0',
  `mi_module` varchar(100) DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_id`,`menu_category_id`,`language_id`),
  KEY `FK_category_menu` (`menu_category_id`,`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Дамп данных таблицы `tb_menu`
--

INSERT INTO `tb_menu` (`menu_id`, `menu_category_id`, `language_id`, `mi_name`, `mi_url`, `mi_access_page`, `mi_title`, `mi_type`, `mi_weight`, `mi_description`, `mi_auto`, `mi_class`, `mi_function`, `mi_filename`, `mi_updated`, `mi_page_arguments`, `mi_page_callback`, `mi_status`, `mi_module`, `owner_id`) VALUES
(6, 3, 1, 'page_content', '/administration/content', NULL, 'Структура', 0, 0, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, 0),
(7, 3, 1, 'page_build', '/administration/section', NULL, 'Подразделы', 0, 0, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, 0),
(8, 3, 1, 'page_settings', '/administration/settings', NULL, 'Настройки сайта', 0, 0, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, 0),
(9, 3, 1, 'page_users', '/administration/users', NULL, 'Пользователи сайта', 0, 0, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, 0),
(10, 3, 1, '', '/administration/content/menu', NULL, 'Меню системы', 0, 0, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1, NULL, 6),
(11, 4, 1, 'victims', 'victims', NULL, 'Пострадавшим', 0, 0, 'Информация для постраждавших', 0, NULL, NULL, NULL, 1416557352, NULL, NULL, 1, NULL, 0),
(12, 4, 1, 'victims_relatives', 'victims_relatives', NULL, 'Родным пострадавших', 0, 0, 'Информация для родственников пострадавших', 0, NULL, NULL, NULL, 1417157808, NULL, NULL, 1, NULL, 0),
(13, 4, 1, 'blogs', 'blogs', NULL, 'Блоги', 0, 0, 'Ведение блогов на сайте', 0, NULL, NULL, NULL, 1416556457, NULL, NULL, 1, NULL, 0),
(14, 4, 1, 'visitors', 'visitors', NULL, 'Посетителям', 1, 0, 'Информация для посетителей', 1, NULL, NULL, NULL, 1416556240, NULL, NULL, 1, NULL, 0),
(15, 4, 1, 'page_our_mission', 'our_mission', NULL, 'Наша миссия', 1, 0, 'Наша миссия', 1, NULL, NULL, NULL, 1416492463, NULL, NULL, 1, NULL, 0),
(17, 4, 1, 'page_contacts', 'contacts', NULL, 'Контакты', 1, 0, 'Контакты', 1, NULL, NULL, NULL, 1417157295, NULL, NULL, 1, NULL, 0),
(18, 3, 1, 'user_list', '/administration/users/list_users', NULL, 'Пользователи', 1, 0, 'фывфыв', 0, NULL, NULL, NULL, 1416571551, NULL, NULL, 1, NULL, 9),
(19, 3, 1, 'user_roles', '/administration/users/roles', NULL, 'Роли', 1, 0, 'Роли', 0, NULL, NULL, NULL, 1416571983, NULL, NULL, 1, NULL, 9),
(21, 3, 1, 'page_news', '/administration/news/', NULL, 'Новости', 1, 0, 'Новости', 0, NULL, NULL, NULL, 1416821921, NULL, NULL, 1, NULL, 7),
(22, 3, 1, 'page_blogs', '/administration/blogs', NULL, 'Блоги системы', 1, 0, 'Блоги системы', 0, NULL, NULL, NULL, 1416826039, NULL, NULL, 1, NULL, 7),
(23, 3, 1, 'page_language', '/administration/language', NULL, 'Языки системы', 1, 0, 'Языки системы', 0, NULL, NULL, NULL, 1416826135, NULL, NULL, 1, NULL, 6),
(26, 4, 1, 'page_maps_site', 'maps_site', NULL, 'Карта сайта', 1, 0, 'Карта сайта', 0, NULL, NULL, NULL, 1417169718, NULL, NULL, 1, NULL, 0),
(27, 4, 1, 'page_geography_to_help', 'geography', NULL, 'География помощи где получить', 1, 0, 'География помощи где получить', 0, 'first-item', NULL, NULL, 1417518755, NULL, NULL, 1, NULL, 11),
(28, 4, 1, 'page_ask_for_help', 'ask_for_help', NULL, 'Попросить помощь', 1, 0, 'Попросить помощь', 0, 'second-item', NULL, NULL, 1417266200, NULL, NULL, 1, NULL, 11),
(29, 4, 1, 'page_database', 'page_database', NULL, 'База данных', 1, 0, 'База данных', 0, NULL, NULL, NULL, 1417699926, NULL, NULL, 1, NULL, 11),
(30, 3, 1, 'admin_content_video', '/administration/admin_content_video', NULL, 'Видеоконтент', 1, 0, 'Видеоконтент', 0, NULL, NULL, NULL, 1417762496, NULL, NULL, 1, NULL, 7),
(31, 3, 1, 'admin_video', '/administration/admin_video', NULL, 'Видео', 1, 0, 'Видео', 0, NULL, NULL, NULL, 1417762577, NULL, NULL, 1, NULL, 7),
(32, 3, 1, 'admin_slider', '/administration/admin_slider', NULL, 'Слайдер изображений', 1, 0, 'Слайдер изображений', 0, NULL, NULL, NULL, 1417762604, NULL, NULL, 1, NULL, 7),
(33, 3, 1, 'gfgfgfg', '/shubes', NULL, 'Шубы китая', 1, 0, 'Шубы китая', 0, NULL, NULL, NULL, 1418394189, NULL, NULL, 1, NULL, 7),
(34, 3, 1, 'gfgfgfg', '/shubes', NULL, 'Шубы китая', 1, 0, 'Шубы китая', 0, NULL, NULL, NULL, 1418394189, NULL, NULL, 1, NULL, 7),
(35, 3, 1, 'gfgfgfg', '/shubes', NULL, 'Шубы китая', 1, 0, 'Шубы китая', 0, NULL, NULL, NULL, 1418394189, NULL, NULL, 1, NULL, 7);

-- --------------------------------------------------------

--
-- Структура таблицы `tb_menu_category`
--

CREATE TABLE IF NOT EXISTS `tb_menu_category` (
  `menu_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `mc_name` varchar(250) DEFAULT NULL,
  `mc_title` varchar(250) DEFAULT NULL,
  `mc_description` text,
  `mc_status` tinyint(4) NOT NULL DEFAULT '0',
  `mc_weight` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_category_id`,`language_id`),
  KEY `FK_lan_to_cmenu` (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tb_menu_category`
--

INSERT INTO `tb_menu_category` (`menu_category_id`, `language_id`, `mc_name`, `mc_title`, `mc_description`, `mc_status`, `mc_weight`) VALUES
(3, 1, 'admin_menu', 'Администрирование', 'Функции администрирования', 1, 0),
(4, 1, 'site_menu', 'Навигационное меню сайта', 'Функции навигационного меню сайта', 1, 1);

--
-- Триггеры `tb_menu_category`
--
DROP TRIGGER IF EXISTS `correct_category_for_child`;
DELIMITER //
CREATE TRIGGER `correct_category_for_child` AFTER UPDATE ON `tb_menu_category`
 FOR EACH ROW begin
	update tb_menu set tb_menu.menu_category_id=new.menu_category_id
	where (tb_menu.menu_category_id=old.menu_category_id);
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `tb_metadata`
--

CREATE TABLE IF NOT EXISTS `tb_metadata` (
  `metadata_id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_id` int(11) NOT NULL,
  `metadata_name` varchar(100) NOT NULL,
  `metadata_url` varchar(500) NOT NULL,
  `metadata_keywords` varchar(250) NOT NULL,
  `metadata_type` varchar(100) NOT NULL,
  PRIMARY KEY (`metadata_id`),
  KEY `FK_ss_to_metadata` (`ss_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tb_news`
--

CREATE TABLE IF NOT EXISTS `tb_news` (
  `nc_id` int(11) NOT NULL DEFAULT '1',
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `images_id` int(11) DEFAULT NULL,
  `news_title` varchar(250) NOT NULL,
  `news_text` text NOT NULL,
  `news_status` tinyint(4) NOT NULL DEFAULT '0',
  `news_status_top` tinyint(4) NOT NULL DEFAULT '0',
  `news_status_main` tinyint(4) NOT NULL DEFAULT '0',
  `news_creates` datetime NOT NULL,
  PRIMARY KEY (`news_id`,`nc_id`),
  KEY `FK_category_news` (`nc_id`),
  KEY `FK_news_to_image` (`images_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `tb_news`
--

INSERT INTO `tb_news` (`nc_id`, `news_id`, `images_id`, `news_title`, `news_text`, `news_status`, `news_status_top`, `news_status_main`, `news_creates`) VALUES
(1, 2, NULL, 'Первая новость', 'Описание Первая новость', 1, 0, 0, '2014-11-25 16:48:16'),
(1, 3, NULL, 'Вторая новость', 'Вторая новость', 1, 0, 1, '2014-11-27 12:04:39'),
(2, 4, NULL, 'О фантастика это круто', '10 окт. 2011 г. - Сообщений: 10\r\nЗдраствуйте! Помогите пожалуйста разобратся с jquery. ... Мне нужно чтобы при нажатии на ссылку, У нее свойство class было .... У меня все работает если отключаю style.css, но когда с ним работать не хочет.\r\nКак изменить свойство класса через jQuery	Сообщений: 4	25 но', 1, 0, 0, '2014-12-11 14:25:58'),
(3, 5, NULL, 'Петиция новости', 'Стихи стихами а так просто хрень', 1, 0, 0, '2014-12-11 14:34:34');

-- --------------------------------------------------------

--
-- Структура таблицы `tb_news_category`
--

CREATE TABLE IF NOT EXISTS `tb_news_category` (
  `nc_id` int(11) NOT NULL AUTO_INCREMENT,
  `nc_title` varchar(250) DEFAULT NULL,
  `nc_text` text,
  `nc_status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tb_news_category`
--

INSERT INTO `tb_news_category` (`nc_id`, `nc_title`, `nc_text`, `nc_status`) VALUES
(1, 'Первая категория', 'Первая категория', 1),
(2, 'Фантастическая категория', NULL, 1),
(3, 'Категория 3', NULL, 1),
(4, 'HFpltk 2', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tb_professional`
--

CREATE TABLE IF NOT EXISTS `tb_professional` (
  `proffessional_id` int(11) NOT NULL,
  `p_category_id` int(11) NOT NULL,
  PRIMARY KEY (`proffessional_id`),
  KEY `FK_cprof_to_prof` (`p_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tb_professional_category`
--

CREATE TABLE IF NOT EXISTS `tb_professional_category` (
  `p_category_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`p_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tb_question`
--

CREATE TABLE IF NOT EXISTS `tb_question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `question_date` datetime NOT NULL,
  `question_message` text,
  `question_name` varchar(100) DEFAULT NULL,
  `question_email` varchar(100) DEFAULT NULL,
  `question_phone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`question_id`,`user_id`),
  KEY `FK_user_question` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tb_register_mails`
--

CREATE TABLE IF NOT EXISTS `tb_register_mails` (
  `rm_id` int(11) NOT NULL AUTO_INCREMENT,
  `rm_name` varchar(250) NOT NULL,
  `rm_text` text NOT NULL,
  `rm_subject` varchar(250) NOT NULL,
  `rm_type` varchar(250) NOT NULL,
  PRIMARY KEY (`rm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tb_register_mails_settings`
--

CREATE TABLE IF NOT EXISTS `tb_register_mails_settings` (
  `rms_id` int(11) NOT NULL AUTO_INCREMENT,
  `rm_id` int(11) NOT NULL,
  `rms_slug` varchar(250) NOT NULL,
  `rms_email` text NOT NULL,
  `rms_status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rms_id`,`rm_id`),
  KEY `FK_rm_to_rms` (`rm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tb_site_settings`
--

CREATE TABLE IF NOT EXISTS `tb_site_settings` (
  `ss_id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_title` varchar(100) NOT NULL,
  `ss_charset` varchar(100) NOT NULL,
  PRIMARY KEY (`ss_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tb_site_settings`
--

INSERT INTO `tb_site_settings` (`ss_id`, `ss_title`, `ss_charset`) VALUES
(1, 'Допоможемо', 'utf-8');

-- --------------------------------------------------------

--
-- Структура таблицы `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(250) DEFAULT NULL,
  `user_email` varchar(250) DEFAULT NULL,
  `user_pass` varchar(250) DEFAULT NULL,
  `user_name` varchar(250) DEFAULT NULL,
  `user_active` tinyint(4) NOT NULL DEFAULT '0',
  `user_reg_date` datetime NOT NULL,
  `user_last_login` datetime NOT NULL,
  `user_avatar_id` int(11) DEFAULT NULL,
  `user_is_online` tinyint(4) DEFAULT '0',
  `user_auth_key` varchar(250) DEFAULT NULL,
  `user_recovery_key` varchar(250) DEFAULT NULL,
  `user_phone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tb_user`
--

INSERT INTO `tb_user` (`user_id`, `user_login`, `user_email`, `user_pass`, `user_name`, `user_active`, `user_reg_date`, `user_last_login`, `user_avatar_id`, `user_is_online`, `user_auth_key`, `user_recovery_key`, `user_phone`) VALUES
(1, 'supervisor', 'chiefwork@gmail.com', 'e54253dcd6ddeb1dc3a8f297f9c9a1f5', NULL, 1, '2014-11-18 13:30:00', '2014-12-15 11:13:04', NULL, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tb_user_logs`
--

CREATE TABLE IF NOT EXISTS `tb_user_logs` (
  `user_logs_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `logs_type` tinyint(4) NOT NULL DEFAULT '0',
  `logs_date` datetime NOT NULL,
  PRIMARY KEY (`user_logs_id`,`user_id`),
  KEY `FK_user_logs` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Дамп данных таблицы `tb_user_logs`
--

INSERT INTO `tb_user_logs` (`user_logs_id`, `user_id`, `logs_type`, `logs_date`) VALUES
(1, 1, 2, '2014-11-18 17:10:22'),
(2, 1, 2, '2014-11-18 17:12:14'),
(3, 1, 2, '2014-11-18 17:12:39'),
(4, 1, 2, '2014-11-18 17:16:46'),
(5, 1, 2, '2014-11-18 17:20:31'),
(6, 1, 2, '2014-11-18 17:20:58'),
(7, 1, 2, '2014-11-18 17:22:15'),
(8, 1, 2, '2014-11-19 14:48:14'),
(9, 1, 2, '2014-11-19 14:49:14'),
(10, 1, 2, '2014-11-19 14:56:35'),
(11, 1, 2, '2014-11-21 14:07:08'),
(12, 1, 2, '2014-11-25 17:46:10'),
(13, 1, 2, '2014-11-27 09:08:40'),
(14, 1, 2, '2014-11-27 10:03:14'),
(15, 1, 2, '2014-11-27 10:07:00'),
(16, 1, 2, '2014-11-28 08:15:27'),
(17, 1, 2, '2014-11-29 13:53:43'),
(18, 1, 2, '2014-11-29 15:57:24'),
(19, 1, 2, '2014-12-03 12:25:16'),
(20, 1, 2, '2014-12-03 17:49:49'),
(21, 1, 2, '2014-12-03 17:52:52'),
(22, 1, 2, '2014-12-03 17:53:03'),
(23, 1, 2, '2014-12-03 17:54:15'),
(24, 1, 2, '2014-12-03 17:54:56'),
(25, 1, 2, '2014-12-03 17:55:06'),
(26, 1, 2, '2014-12-03 17:58:20'),
(27, 1, 2, '2014-12-04 10:54:49');

-- --------------------------------------------------------

--
-- Структура таблицы `tb_user_role`
--

CREATE TABLE IF NOT EXISTS `tb_user_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_title` varchar(250) DEFAULT NULL,
  `role_weight` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `tb_user_role`
--

INSERT INTO `tb_user_role` (`role_id`, `role_title`, `role_weight`) VALUES
(1, 'Анонимный пользователь', 0),
(2, 'Посетитель', 0),
(3, 'Модератор', 0),
(4, 'Администратор', 0),
(5, 'Главный администратор', 0),
(6, 'Зарегистрированный пользователь', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tb_user_roles_id`
--

CREATE TABLE IF NOT EXISTS `tb_user_roles_id` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tb_user_roles_id`
--

INSERT INTO `tb_user_roles_id` (`user_id`, `role_id`) VALUES
(1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `tb_user_rules`
--

CREATE TABLE IF NOT EXISTS `tb_user_rules` (
  `rules_id` int(11) NOT NULL AUTO_INCREMENT,
  `ur_source` varchar(250) DEFAULT NULL,
  `ur_title` varchar(250) DEFAULT NULL,
  `ur_module` varchar(250) DEFAULT NULL,
  `ur_weight` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rules_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tb_user_rules_roles_id`
--

CREATE TABLE IF NOT EXISTS `tb_user_rules_roles_id` (
  `user_id` int(11) NOT NULL,
  `rules_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`rules_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `v_category_to_blogs`
--
CREATE TABLE IF NOT EXISTS `v_category_to_blogs` (
`cblogs_title` varchar(100)
,`cblogs_text` varchar(500)
,`cblogs_status` tinyint(4)
,`blogs_id` int(11)
,`cblogs_id` int(11)
,`images_id` int(11)
,`blogs_title` varchar(100)
,`blogs_text` text
,`blogs_status` tinyint(4)
,`blogs_main` tinyint(4)
,`blogs_top` tinyint(4)
,`blogs_owner` int(11)
,`blogs_creates` datetime
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `v_category_to_content`
--
CREATE TABLE IF NOT EXISTS `v_category_to_content` (
`ccontent_alias` varchar(100)
,`ccontent_title` varchar(250)
,`ccontent_text` text
,`ccontent_status` tinyint(4)
,`content_id` int(11)
,`ccontent_id` int(11)
,`content_title` varchar(250)
,`content_text` text
,`content_status` tinyint(4)
,`content_status_top` tinyint(4)
,`content_status_main` tinyint(4)
,`content_creates` datetime
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `v_category_to_news`
--
CREATE TABLE IF NOT EXISTS `v_category_to_news` (
`nc_title` varchar(250)
,`nc_text` text
,`nc_status` tinyint(4)
,`nc_id` int(11)
,`news_id` int(11)
,`images_id` int(11)
,`news_title` varchar(250)
,`news_text` text
,`news_status` tinyint(4)
,`news_status_top` tinyint(4)
,`news_status_main` tinyint(4)
,`news_creates` datetime
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `v_menu_category_to_menu`
--
CREATE TABLE IF NOT EXISTS `v_menu_category_to_menu` (
`menu_category_id` int(11)
,`language_id` int(11)
,`lan_title` varchar(250)
,`lan_code` varchar(5)
,`mc_name` varchar(250)
,`mc_title` varchar(250)
,`mc_description` text
,`mc_status` tinyint(4)
,`mc_weight` int(11)
,`menu_id` int(11)
,`mi_name` varchar(250)
,`mi_url` varchar(250)
,`mi_access_page` varchar(500)
,`mi_title` varchar(250)
,`mi_type` int(11)
,`mi_weight` int(11)
,`mi_description` text
,`mi_auto` tinyint(4)
,`mi_class` varchar(250)
,`mi_function` varchar(250)
,`mi_filename` varchar(250)
,`mi_updated` int(11)
,`mi_page_arguments` varchar(500)
,`mi_page_callback` varchar(500)
,`mi_status` tinyint(4)
,`mi_module` varchar(100)
,`owner_id` int(11)
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `v_site_settings_to_medatada`
--
CREATE TABLE IF NOT EXISTS `v_site_settings_to_medatada` (
`site_id` int(11)
,`ss_title` varchar(100)
,`ss_charset` varchar(100)
,`metadata_id` int(11)
,`ss_id` int(11)
,`metadata_name` varchar(100)
,`metadata_url` varchar(500)
,`metadata_keywords` varchar(250)
,`metadata_type` varchar(100)
);
-- --------------------------------------------------------

--
-- Дублирующая структура для представления `v_user_to_role`
--
CREATE TABLE IF NOT EXISTS `v_user_to_role` (
`user_id` int(11)
,`user_login` varchar(250)
,`user_email` varchar(250)
,`user_pass` varchar(250)
,`user_name` varchar(250)
,`user_active` tinyint(4)
,`user_reg_date` datetime
,`user_last_login` datetime
,`user_avatar_id` int(11)
,`user_is_online` tinyint(4)
,`user_auth_key` varchar(250)
,`user_recovery_key` varchar(250)
,`user_phone` varchar(50)
,`role_id` int(11)
,`role_title` varchar(250)
,`role_weight` int(11)
);
-- --------------------------------------------------------

--
-- Структура для представления `v_category_to_blogs`
--
DROP TABLE IF EXISTS `v_category_to_blogs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_category_to_blogs` AS select `t1`.`cblogs_title` AS `cblogs_title`,`t1`.`cblogs_text` AS `cblogs_text`,`t1`.`cblogs_status` AS `cblogs_status`,`t2`.`blogs_id` AS `blogs_id`,`t2`.`cblogs_id` AS `cblogs_id`,`t2`.`images_id` AS `images_id`,`t2`.`blogs_title` AS `blogs_title`,`t2`.`blogs_text` AS `blogs_text`,`t2`.`blogs_status` AS `blogs_status`,`t2`.`blogs_main` AS `blogs_main`,`t2`.`blogs_top` AS `blogs_top`,`t2`.`blogs_owner` AS `blogs_owner`,`t2`.`blogs_creates` AS `blogs_creates` from (`tb_blogs_category` `t1` left join `tb_blogs` `t2` on((`t1`.`cblogs_id` = `t2`.`cblogs_id`))) order by `t1`.`cblogs_id`,`t2`.`cblogs_id`;

-- --------------------------------------------------------

--
-- Структура для представления `v_category_to_content`
--
DROP TABLE IF EXISTS `v_category_to_content`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_category_to_content` AS select `t1`.`ccontent_alias` AS `ccontent_alias`,`t1`.`ccontent_title` AS `ccontent_title`,`t1`.`ccontent_text` AS `ccontent_text`,`t1`.`ccontent_status` AS `ccontent_status`,`t2`.`content_id` AS `content_id`,`t2`.`ccontent_id` AS `ccontent_id`,`t2`.`content_title` AS `content_title`,`t2`.`content_text` AS `content_text`,`t2`.`content_status` AS `content_status`,`t2`.`content_status_top` AS `content_status_top`,`t2`.`content_status_main` AS `content_status_main`,`t2`.`content_creates` AS `content_creates` from (`tb_content_category` `t1` left join `tb_content` `t2` on((`t1`.`ccontent_id` = `t2`.`ccontent_id`))) order by `t1`.`ccontent_id`,`t2`.`content_id`;

-- --------------------------------------------------------

--
-- Структура для представления `v_category_to_news`
--
DROP TABLE IF EXISTS `v_category_to_news`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_category_to_news` AS select `t1`.`nc_title` AS `nc_title`,`t1`.`nc_text` AS `nc_text`,`t1`.`nc_status` AS `nc_status`,`t2`.`nc_id` AS `nc_id`,`t2`.`news_id` AS `news_id`,`t2`.`images_id` AS `images_id`,`t2`.`news_title` AS `news_title`,`t2`.`news_text` AS `news_text`,`t2`.`news_status` AS `news_status`,`t2`.`news_status_top` AS `news_status_top`,`t2`.`news_status_main` AS `news_status_main`,`t2`.`news_creates` AS `news_creates` from (`tb_news_category` `t1` left join `tb_news` `t2` on((`t1`.`nc_id` = `t2`.`nc_id`))) order by `t1`.`nc_id`,`t2`.`news_id`;

-- --------------------------------------------------------

--
-- Структура для представления `v_menu_category_to_menu`
--
DROP TABLE IF EXISTS `v_menu_category_to_menu`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_menu_category_to_menu` AS select `t1`.`menu_category_id` AS `menu_category_id`,`t1`.`language_id` AS `language_id`,`t3`.`lan_title` AS `lan_title`,`t3`.`lan_code` AS `lan_code`,`t1`.`mc_name` AS `mc_name`,`t1`.`mc_title` AS `mc_title`,`t1`.`mc_description` AS `mc_description`,`t1`.`mc_status` AS `mc_status`,`t1`.`mc_weight` AS `mc_weight`,`t2`.`menu_id` AS `menu_id`,`t2`.`mi_name` AS `mi_name`,`t2`.`mi_url` AS `mi_url`,`t2`.`mi_access_page` AS `mi_access_page`,`t2`.`mi_title` AS `mi_title`,`t2`.`mi_type` AS `mi_type`,`t2`.`mi_weight` AS `mi_weight`,`t2`.`mi_description` AS `mi_description`,`t2`.`mi_auto` AS `mi_auto`,`t2`.`mi_class` AS `mi_class`,`t2`.`mi_function` AS `mi_function`,`t2`.`mi_filename` AS `mi_filename`,`t2`.`mi_updated` AS `mi_updated`,`t2`.`mi_page_arguments` AS `mi_page_arguments`,`t2`.`mi_page_callback` AS `mi_page_callback`,`t2`.`mi_status` AS `mi_status`,`t2`.`mi_module` AS `mi_module`,`t2`.`owner_id` AS `owner_id` from ((`tb_menu_category` `t1` left join `tb_menu` `t2` on((`t1`.`menu_category_id` = `t2`.`menu_category_id`))) left join `tb_language` `t3` on((`t1`.`language_id` = `t3`.`language_id`))) order by `t1`.`menu_category_id`,`t2`.`menu_id`;

-- --------------------------------------------------------

--
-- Структура для представления `v_site_settings_to_medatada`
--
DROP TABLE IF EXISTS `v_site_settings_to_medatada`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_site_settings_to_medatada` AS select `t1`.`ss_id` AS `site_id`,`t1`.`ss_title` AS `ss_title`,`t1`.`ss_charset` AS `ss_charset`,`t2`.`metadata_id` AS `metadata_id`,`t2`.`ss_id` AS `ss_id`,`t2`.`metadata_name` AS `metadata_name`,`t2`.`metadata_url` AS `metadata_url`,`t2`.`metadata_keywords` AS `metadata_keywords`,`t2`.`metadata_type` AS `metadata_type` from (`tb_site_settings` `t1` left join `tb_metadata` `t2` on((`t1`.`ss_id` = `t2`.`ss_id`))) order by `t1`.`ss_id`,`t2`.`metadata_id`;

-- --------------------------------------------------------

--
-- Структура для представления `v_user_to_role`
--
DROP TABLE IF EXISTS `v_user_to_role`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user_to_role` AS select `t2`.`user_id` AS `user_id`,`t2`.`user_login` AS `user_login`,`t2`.`user_email` AS `user_email`,`t2`.`user_pass` AS `user_pass`,`t2`.`user_name` AS `user_name`,`t2`.`user_active` AS `user_active`,`t2`.`user_reg_date` AS `user_reg_date`,`t2`.`user_last_login` AS `user_last_login`,`t2`.`user_avatar_id` AS `user_avatar_id`,`t2`.`user_is_online` AS `user_is_online`,`t2`.`user_auth_key` AS `user_auth_key`,`t2`.`user_recovery_key` AS `user_recovery_key`,`t2`.`user_phone` AS `user_phone`,`t3`.`role_id` AS `role_id`,`t3`.`role_title` AS `role_title`,`t3`.`role_weight` AS `role_weight` from ((`tb_user_roles_id` `t1` left join `tb_user` `t2` on((`t1`.`user_id` = `t2`.`user_id`))) left join `tb_user_role` `t3` on((`t1`.`role_id` = `t3`.`role_id`))) order by `t2`.`user_id`,`t3`.`role_id`;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `tb_answer`
--
ALTER TABLE `tb_answer`
  ADD CONSTRAINT `FK_answer_to_question` FOREIGN KEY (`question_id`, `user_id`) REFERENCES `tb_question` (`question_id`, `user_id`);

--
-- Ограничения внешнего ключа таблицы `tb_blogs`
--
ALTER TABLE `tb_blogs`
  ADD CONSTRAINT `FK_blogs_to_image` FOREIGN KEY (`images_id`) REFERENCES `tb_images` (`images_id`),
  ADD CONSTRAINT `FK_cblogs_to_blog` FOREIGN KEY (`cblogs_id`) REFERENCES `tb_blogs_category` (`cblogs_id`);

--
-- Ограничения внешнего ключа таблицы `tb_comments`
--
ALTER TABLE `tb_comments`
  ADD CONSTRAINT `FK_blogs_to_comments` FOREIGN KEY (`blogs_id`, `cblogs_id`) REFERENCES `tb_blogs` (`blogs_id`, `cblogs_id`),
  ADD CONSTRAINT `FK_images_to_comments` FOREIGN KEY (`images_id`) REFERENCES `tb_images` (`images_id`),
  ADD CONSTRAINT `FK_language_to_comments` FOREIGN KEY (`language_id`) REFERENCES `tb_language` (`language_id`),
  ADD CONSTRAINT `FK_news_to_comments` FOREIGN KEY (`news_id`, `nc_id`) REFERENCES `tb_news` (`news_id`, `nc_id`),
  ADD CONSTRAINT `FK_user_to_comment` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`user_id`);

--
-- Ограничения внешнего ключа таблицы `tb_comments_rate`
--
ALTER TABLE `tb_comments_rate`
  ADD CONSTRAINT `FK_comments_rate` FOREIGN KEY (`comments_id`, `user_id`) REFERENCES `tb_comments` (`comments_id`, `user_id`);

--
-- Ограничения внешнего ключа таблицы `tb_comments_view`
--
ALTER TABLE `tb_comments_view`
  ADD CONSTRAINT `FK_comments_to_view` FOREIGN KEY (`comments_id`, `user_id`) REFERENCES `tb_comments` (`comments_id`, `user_id`);

--
-- Ограничения внешнего ключа таблицы `tb_content`
--
ALTER TABLE `tb_content`
  ADD CONSTRAINT `FK_ccontent_to_content` FOREIGN KEY (`ccontent_id`) REFERENCES `tb_content_category` (`ccontent_id`);

--
-- Ограничения внешнего ключа таблицы `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD CONSTRAINT `FK_category_menu` FOREIGN KEY (`menu_category_id`, `language_id`) REFERENCES `tb_menu_category` (`menu_category_id`, `language_id`);

--
-- Ограничения внешнего ключа таблицы `tb_menu_category`
--
ALTER TABLE `tb_menu_category`
  ADD CONSTRAINT `FK_lan_to_cmenu` FOREIGN KEY (`language_id`) REFERENCES `tb_language` (`language_id`);

--
-- Ограничения внешнего ключа таблицы `tb_metadata`
--
ALTER TABLE `tb_metadata`
  ADD CONSTRAINT `FK_ss_to_metadata` FOREIGN KEY (`ss_id`) REFERENCES `tb_site_settings` (`ss_id`);

--
-- Ограничения внешнего ключа таблицы `tb_news`
--
ALTER TABLE `tb_news`
  ADD CONSTRAINT `FK_category_news` FOREIGN KEY (`nc_id`) REFERENCES `tb_news_category` (`nc_id`),
  ADD CONSTRAINT `FK_news_to_image` FOREIGN KEY (`images_id`) REFERENCES `tb_images` (`images_id`);

--
-- Ограничения внешнего ключа таблицы `tb_professional`
--
ALTER TABLE `tb_professional`
  ADD CONSTRAINT `FK_cprof_to_prof` FOREIGN KEY (`p_category_id`) REFERENCES `tb_professional_category` (`p_category_id`);

--
-- Ограничения внешнего ключа таблицы `tb_question`
--
ALTER TABLE `tb_question`
  ADD CONSTRAINT `FK_user_question` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`user_id`);

--
-- Ограничения внешнего ключа таблицы `tb_register_mails_settings`
--
ALTER TABLE `tb_register_mails_settings`
  ADD CONSTRAINT `FK_rm_to_rms` FOREIGN KEY (`rm_id`) REFERENCES `tb_register_mails` (`rm_id`);

--
-- Ограничения внешнего ключа таблицы `tb_user_logs`
--
ALTER TABLE `tb_user_logs`
  ADD CONSTRAINT `FK_user_logs` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`user_id`);

--
-- Ограничения внешнего ключа таблицы `tb_user_roles_id`
--
ALTER TABLE `tb_user_roles_id`
  ADD CONSTRAINT `FK_user_roles_id` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
