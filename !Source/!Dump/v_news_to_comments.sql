create view v_news_to_comments as
select t2.`user_id`, t2.`cblogs_id` as 'section_blogs', t2.`blogs_id`, t2.`nc_id` as 'section_news', t2.`news_id` as 'parent_id',  t1.`news_title` as 'parent_title', t1.`news_text` as 'parent_text', t1.`news_status` as 'parent_status', t1.`news_status_top` as 'parent_top', t1.`news_status_main` as 'parent_main', t1.`news_creates` as 'parent_date', t2.`proposal_id`,
t2.`comments_id`, t2.`comments_rate`, t2.`comments_text`, t2.`comments_created` as 'comments_date', t2.`comments_status`, t2.`comments_reply_id`, t2.`comments_owner_id`, t2.`images_id`, t2.`language_id` as 'comments_language', t2.`comments_name` from tb_news t1
right join tb_comments t2
on t1.news_id=t2.news_id
order by t2.comments_created