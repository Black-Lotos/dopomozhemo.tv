/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     18.02.2015 10:09:30                          */
/*==============================================================*/


drop procedure if exists prc_delete_before_language;

drop table if exists tb_metadata;

drop table if exists tb_site_settings;

drop table if exists tb_variables;

/*==============================================================*/
/* Table: tb_metadata                                           */
/*==============================================================*/
create table tb_metadata
(
   metadata_id          int(11) not null auto_increment,
   ss_id                int(11) not null,
   metadata_name        varchar(100) not null,
   metadata_url         varchar(500) not null,
   metadata_keywords    varchar(250) not null,
   metadata_type        varchar(100) not null,
   primary key (metadata_id)
);

/*==============================================================*/
/* Table: tb_site_settings                                      */
/*==============================================================*/
create table tb_site_settings
(
   ss_id                int(11) not null auto_increment,
   ss_title             varchar(100) not null,
   ss_charset           varchar(100) not null,
   primary key (ss_id)
);

/*==============================================================*/
/* Table: tb_variables                                          */
/*==============================================================*/
create table tb_variables
(
   variable_id          int(11) not null auto_increment,
   ss_id                int(11) not null,
   variable_name        varchar(100) not null,
   variable_value       int not null,
   variable_prefix      varchar(100),
   variable_type        int not null,
   primary key (variable_id)
);

alter table tb_metadata add constraint FK_ss_to_metadata foreign key (ss_id)
      references tb_site_settings (ss_id) on delete restrict on update restrict;

alter table tb_variables add constraint FK_site_to_variable foreign key (ss_id)
      references tb_site_settings (ss_id) on delete restrict on update restrict;


create procedure prc_delete_before_language (IN inLanguageId int)
begin
    delete from tb_comments_rate
	where comments_id = select comments_id from tb_comments where (language_id=inLanguageId);
	delete from tb_comments_view
	where comments_id = select comments_id from tb_comments where (language_id=inLanguageId);
    delete from tb_comments where (language_id=inLanguageId);
    delete from tb_menu where (language_id=inLanguageId);
  	delete from tb_menu_category where (language_id=inLanguageId);
end;

