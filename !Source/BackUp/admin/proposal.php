<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proposal extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
        //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model("Proposal_model");
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
    }
    //
    //
    public function index()
    {   
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Блоги',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        $inData['output'] = $this->Proposal_model->getOutput();
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadProposal();
        }
        echo $this->twig->render("{$inData['tabs_run']['property']['template']}", $inData);
    }
    public function load() {
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        $inData['output'] = $this->Proposal_model->getOutput($inProces);
        //
        //echo "<pre>"; var_dump($inProces); die();
        $inData['data'] = $this->Proposal_model->loadTree(
                array(
                    'category'=>array('fields'=>array('cproposal_status','cproposal_id as value', 'cproposal_title as title')),
                    'blogs'=>array('fields'=>array('proposal_status','proposal_id as value', 'proposal_title as title'))    
                )
        );
        //echo "<pre>"; var_dump($inData['data']); die();
        foreach ($inData['data'] as $outKey => $outData) {
            $outData['action'] =    "<a href='#' id='ref-сblogs-edit-{$outData['value']}' class='action-base action-edit'></a>".
                                    "<a href='#' id='ref-сblogs-delete-{$outData['value']}' class='action-base action-delete'></a>";
            $outName = "ref-сblogs-status-{$outData['value']}";                        
            $outChecked = ($outData['cproposal_status']==1)?'checked':'un-checked';
            //$outData['status'] = "<input name='{$outName}' type='checkbox' id='{$outName}' $outChecked />";
            //
            $outData['action'] =    "<a href='#' id='ref-сblogs-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                    "<a href='#' id='ref-сblogs-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                    "<a href='#' id='ref-сblogs-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>"        
                                    ;
            if (!empty($outData['sub_tree'])) {
                foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                    $outChecked = ($outSData['proposal_status']==1)?'checked':'un-checked';
                    $outSData['action'] =    "<a href='#' id='ref-blogs-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                            "<a href='#' id='ref-blogs-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                            "<a href='#' id='ref-blogs-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>"        
                                            ;
                    $outData['sub_tree'][$outSKey] = $outSData;
                }
            }
            $inData['data'][$outKey] = $outData;
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        $inCategory = $this->Proposal_model->loadCategory(array('cproposal_status'=>1,'fields'=>array('cproposal_title as title','cproposal_id as value')));
        $inData['form'] = $this->Proposal_model->getForm(
                $inCategory,
                null,
                array(),
                PROCESS_BLOGS_SECTION
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inCategory = $this->Proposal_model->loadCategory(array('cproposal_status'=>1,'fields'=>array('cproposal_title as title','cproposal_id as value')));
        $inData['form'] = $this->Proposal_model->getForm(
                $inCategory,
                null
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        if (!empty($_POST['proposal_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['proposal_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Proposal_model->loadById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inMenu = null;
            $inCategory = $this->Proposal_model->loadCategory(array('cproposal_status'=>1,'fields'=>array('cproposal_title as title','cproposal_id as value')));
            $inData['form'] = $this->Proposal_model->getForm(
                $inCategory,
                null,
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function edit_section() {
        if (!empty($_POST['cproposal_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['cproposal_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Proposal_model->loadCategoryById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inMenu = null;
            $inCategory = null;
            $inData['form'] = $this->Proposal_model->getForm(
                $inCategory,
                null,
                $inData,
                PROCESS_BLOGS_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function change_status() {
        if (!empty($_POST)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['proposal_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inProposal = $this->Proposal_model->loadById($inId);
                    $inProposal['proposal_status'] = ($inProposal['proposal_status']==0)?1:0;
                    $this->Proposal_model->save($inProposal);
                    echo $inProposal['proposal_status'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'cproposal_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inCProposal = $this->Proposal_model->loadCategoryById($inId);
                    $inCProposal['cproposal_status'] = ($inCProposal['cproposal_status']==0)?1:0;
                    $this->Proposal_model->save_category($inCProposal);
                    echo $inCProposal['cproposal_status'];
                    break;
            }
        }
        
    }
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }
            if(!empty($inDecode['proposal_status'])&&$inDecode['proposal_status']=='on') {
                $inDecode['proposal_status']=1;
            }
            if(!empty($inDecode['proposal_top'])&&$inDecode['proposal_top']=='on') {
                $inDecode['proposal_top']=1;
            }
            if(!empty($inDecode['proposal_main'])&&$inDecode['proposal_main']=='on') {
                $inDecode['proposal_main']=1;
            }
            //echo "<pre>"; var_dump($inDecode); die();
            $inDecode['user_id']=1;
            echo $this->Proposal_model->save($inDecode);
        }
    }
    public function save_category() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }
        }
        echo $this->Proposal_model->save_category($inDecode);
    }
}

