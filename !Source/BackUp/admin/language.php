<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Language extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
        //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
    }
    //
    //
    public function index()
    {   
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Языки',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadLanguages();
        }
        echo $this->twig->render($inData['tabs_run']['property']['template'], $inData);
    }
    public function list_language() {
        $inData = $this->Language_model->getOutput();
        //echo "<pre>"; var_dump($inData); die();
        $inData['data'] = $this->Language_model->loadLanguage();
        foreach ($inData['data'] as $outKey => $outData) {
            $outData['action'] =    "<a href='#' id='ref-lan-edit-{$outData['language_id']}' class='action-base action-edit'></a>".
                                    "<a href='#' id='ref-lan-delete-{$outData['language_id']}' class='action-base action-delete'></a>";
            $outName = "ref-lan-status-{$outData['language_id']}";                        
            $outChecked = ($outData['lan_status']==1)?"checked='checked'":'';
            $outData['lan_status'] = "<input name='{$outName}' type='checkbox' id='{$outName}' $outChecked />";
            $inData['data'][$outKey] = $outData;
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system.twig", $inData);
    }
    //
    public function add_language() {
        $inData = array();
        $inArg = func_get_args();
        //
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inData['form'] = $this->Language_model->getFormLanguage(
                $inSufix,
                null,
                null,
                $inMenu
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        if (!empty($_POST['language_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['language_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Language_model->loadLanguageById($inMcId);
            $inMenu = null;
            $inData['form'] = $this->Language_model->getFormLanguage(
                $inSufix,
                null,
                null,
                $inMenu,
                $inData    
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    //
    public function change_status() {
        
    }
    //
     public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value']))
                    $inDecode[$outData['name']]=$outData['value'];
            }
            //$inDecode['owner_id'] = $this->Menu_model->
            //echo "<pre>"; var_dump($inArg,$inDecode); 
            if($inDecode['lan_status']=='on') {
                $inDecode['lan_status']=1;
            }
            //echo "<pre>"; var_dump($inDecode); die();
            echo $this->Language_model->save($inDecode);
            //echo $this->inUriString; 
            //die($this->inUriString);
            //Goto_Page("/administration/content");
            
        }
    }
}

