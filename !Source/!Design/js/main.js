$(document).ready(function(){
	$('#carousel-top').owlCarousel({
		loop:true,
		nav:true,
		items:1,
		slideSpeed: 200,
		autoPlay: true
	}),
	 $('#carousel-video').owlCarousel({
		loop:true,
		margin:11,
		nav:true,
		items:3
	});
	$('#carousel-small-images').owlCarousel({
		loop:true,
		margin:11,
		nav:true,
		items:4
	});
})

