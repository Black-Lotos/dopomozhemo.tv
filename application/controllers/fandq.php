<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
•	Имя того, кто оставляет заявку
•	Телефон
•	E-mail
•	Подробное описание
•	Дата заявки
•	Ответ
•	Дата ответа
•	Статус «Опубликовано» / «Не опубликовано»
•fandq_	Статус «Решена» / «Не решена»

 *  */
class Question extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    private $inPage=1;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'site_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model("Comments_model","Comments");
        $this->load->model("Fandq_model","act_model");
    }
    //
    private function includeUp(){
        $inMenu = $this->Menu_model->load(array('name'=>'site_menu','status'=>1));
        $inData = array('title'=>'Главная страница','content'=>array('header'=>'Главная страница'),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //
        $inData['pieses']['outNews'] = $this->News_model->getOutput();
        $this->News_model->setCountRecord(3);
        $outNews = $this->News_model->load(array('news_status_main'=>1));
        $inData['pieses']['outNews']['data'] = (!$outNews)?array('news_title'=>'Новостей нет'):array('data'=>$outNews);
        //
        $inData['pieses']['outBlogs'] = $this->Blogs_model->getOutput();
        $this->Blogs_model->setCountRecord(3);
        $outBlogs = $this->Blogs_model->load(array('blogs_main'=>1));
        $inData['pieses']['outBlogs']['data'] = (!$outBlogs)?array('blogs_title'=>'Блогов нет'):array('data'=>$outBlogs);
        //
        $inData['pieses']['listFandq'] = $this->act_model->getOutput(OUTPUT_LIST);
        $this->act_model->setCountRecord(0);
        $inData['pieses']['listFandq']['data'] = $this->act_model->load(array('question_status'=>1),false,$this->inPage);
        if ($inData['pieses']['listFandq']['data']) {
            foreach ($inData['pieses']['listFandq']['data'] as $outKey => $outData) {
                $outData['fandq_count'] = $this->Comments->CountByFandqId($outData['fandq_id']);
                $inData['pieses']['listFandq']['data'][$outKey] = $outData;
            }
        } 
        //
        $inData['page']['active'] = $this->inPage;
        return $inData;
    }
    //
    public function index() {   
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inData = $this->includeUp();
        $this->load->model("Footerlink_model");
        $inData['pieses']['outLinkDown'] = $this->Footerlink_model->getOutput();
        $inData['pieses']['outLinkDown']['data'] = $this->Footerlink_model->load(array('footerlink_status'=>1));
        $inData['pieses']['outVideo'] = $this->Content_video->getOutput();
        $inData['pieses']['outVideo']['data'] = $this->Content_video->loadCategoryElemntByAlias('alias_down_slider_video',array('content_status'=>1));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function add() {
        $inData = $this->includeUp();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inCategory = "";
        $inData['pieses']['fandq']['form'] = $this->act_model->getForm(
                $inCategory,
                null
        );
        //echo "<pre>";var_dump($inData['pieses']['fandq']['form']['form_property']['action']); die();
        $inData['pieses']['fandq']['form']['form_property']['action'] = '/fandq/save';
        $inData['pieses']['fandq']['add']['form'] = $this->twig->render("administration/common/form-users.twig", array('form'=>$inData['pieses']['fandq']['form']));
        //echo "<pre>";var_dump($inData['pieses']['fandq']['add']['form']); die();
        $inData['pieses']['fandq']['add']['isRun'] = true;
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function detail($aId=null) {
        $inData = $this->includeUp(); $inId = 0;
        if ($_POST) {
            $inNewsId = filter_input(INPUT_POST, 'fandq_id', FILTER_SANITIZE_SPECIAL_CHARS);
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $inNewsId, $outMatches)>0)?(int)$outMatches[0]:0;
        } else $inId  = $aId;
        if (!empty($inId)) {
            $inSData=array('comments'=>array('title'=>getCaptionInput('caption_comments'),'counts'=>$this->Comments->CountByQuestionId($inId)));
            $inSData['comments']['data'] = $this->Comments->loadByQuestionId($inId);
            $inSData['fandq']['data'] = $this->act_model->loadById($inId);
            
        }
        $inData['sub_page'] = $this->twig->render('fandq/fandq-one-record-list.twig', $inSData);
        //echo "<pre>"; var_dump($inSData); die();
        echo $this->twig->render('fandq/fandq-page-detail.twig', $inData);
    }
    //
    public function save_comments() {
        if (!empty($_POST)) {
            $this->Comments->save($_POST);
        }
        Goto_Page('/fandq/');
    }
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            if(!empty($inDecode['question_status'])&&$inDecode['question_status']=='on') {
                $inDecode['question_status']=1;
            }
            //echo "<pre>"; var_dump($inDecode); die();
            $inDecode['user_id']=1;
            //echo $this->act_model->save($inDecode);
            Goto_Page('/fandq/');
        }
    }
}