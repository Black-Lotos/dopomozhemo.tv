<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Victims_relatives extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'site_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
    }
    //
    public function index() {   
        $inMenu = $this->Menu_model->load(array('name'=>'site_menu','status'=>1));
        $inData = array('title'=>'Главная страница','content'=>array('header'=>'Главная страница'),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        $inData['news_main'] = $this->News_model->getOutput();
        $this->News_model->setCountRecord(3);
        $outNews = $this->News_model->load(array('news_status_main'=>1));
        $inData['news_main']['data'] = (!$outNews)?array('news_title'=>'Новостей нет'):array('data'=>$outNews);
        //
        $inData['blogs_main'] = $this->Blogs_model->getOutput();
        $this->Blogs_model->setCountRecord(3);
        $outBlogs = $this->Blogs_model->load(array('blogs_main'=>1));
        $inData['blogs_main']['data'] = (!$outBlogs)?array('blogs_title'=>'Блогов нет'):array('data'=>$outBlogs);
        $this->load->model("Footerlink_model");
        $inData['pieses']['outLinkDown'] = $this->Footerlink_model->getOutput();
        $inData['pieses']['outLinkDown']['data'] = $this->Footerlink_model->load(array('footerlink_status'=>1));
        $inData['pieses']['outVideo'] = $this->Content_video->getOutput();
        $inData['pieses']['outVideo']['data'] = $this->Content_video->loadCategoryElemntByAlias('alias_down_slider_video',array('content_status'=>1));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
}