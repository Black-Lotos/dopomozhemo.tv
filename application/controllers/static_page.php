<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Static_page extends MY_Controller {
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        //var_dump($inArg);
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func(array($this, $aMethod),$inArg[0]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
    protected function afterInclude($aData = array()) {
        $inData = $aData;
        $this->load->model("Static_model");
        $this->Static_model->setCountRecord(0);
        $inData['pieses']['outVideo']['property']['isRun'] = false;
        return $inData;
    }
    //
    public function index() {   
        $inArg = func_get_args();
        $inAlias = (!empty($inArg[0]))?$inArg[0]:'page_404';
        $inData = $this->includeUp();
        $inPage = $this->Static_model->loadByAlias($inAlias,array('language_id'=>$this->inLang),true);
        if (!$inPage) {
            $inPage = $this->Static_model->loadByAlias("page_404",array('language_id'=>$this->inLang),true);
        }
        $inData['sub_page'] = $inPage->sp_text;
        echo $this->twig->render('site-master-page.twig',$inData);
    }
    //
}