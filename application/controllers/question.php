<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
•	Имя того, кто оставляет заявку
•	Телефон
•	E-mail
•	Подробное описание
•	Дата заявки
•	Ответ
•	Дата ответа
•	Статус «Опубликовано» / «Не опубликовано»
•question_	Статус «Решена» / «Не решена»

 *  */
class Question extends MY_Controller {
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
    protected function afterInclude($aData = array()) {
        $inData = $aData;
        $this->load->model('Question_model','act_model');
        $inData['pieses']['listQuestion'] = $this->act_model->getOutput(OUTPUT_LIST);
        $this->act_model->setCountRecord(0);
        $inData['pieses']['listQuestion']['data'] = $this->act_model->load(array('question_status'=>1,'language_id'=>$this->inLang),false,$this->inPage);
        return $inData;
    }
    //
    public function index() {   
        $inData = $this->includeUp();
        //var_dump($inData); die();
        $inData['pieses']['outVideo']['property']['isRun'] = false;
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function add() {
        $inData = $this->includeUp();
        $inDataAdd = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inDataAdd['language_id'] = $this->inLang; 
        setLanguage($inDataAdd,true);
        $inData['pieses']['question']['form'] = $this->act_model->getForm(
                $inDataAdd,
                PROCESS_USER_APPEND
        );
        //echo "<pre>";var_dump($inData['pieses']['question']['form']); die();
        $inData['pieses']['question']['form']['form_property']['action'] = '/question/save';
        $inData['pieses']['question']['add']['form'] = $this->twig->render("administration/common/form-users.twig", array('form'=>$inData['pieses']['question']['form']));
        //echo "<pre>";var_dump($inData['pieses']['question']['add']['form']); die();
        $inData['pieses']['question']['add']['isRun'] = true;
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function detail($aId=null) {
        $inData = $this->includeUp(); $inId = 0;
        $this->load->model("Question_model","act_model");
        if ($_POST) {
            $inNewsId = filter_input(INPUT_POST, 'question_id', FILTER_SANITIZE_SPECIAL_CHARS);
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $inNewsId, $outMatches)>0)?(int)$outMatches[0]:0;
        } else $inId  = $aId;
        if (!empty($inId)) {
            
            $inSData=array('comments'=>array('title'=>getCaptionInput('caption_comments'),'counts'=>'0'));
            //die("TYT");
            $inSData['comments']['data'] = '0';
            $inSData['question']['data'] = $this->act_model->loadById($inId);
            
        }
        $inData['sub_page'] = $this->twig->render('question/question-one-record-list.twig', $inSData);
        //pieses.listQuestion.property.isRun
        $inData['pieses']['listQuestion']['property']['isRun'] = false;        
        $inData['pieses']['outVideo']['property']['isRun'] = false;
        //echo "<pre>"; var_dump($inSData); die();
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function save_comments() {
        if (!empty($_POST)) {
            $this->Comments->save($_POST);
        }
        Goto_Page('/question/');
    }
    public function save() { 
        
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            if(!empty($inDecode['question_status'])&&$inDecode['question_status']=='on') {
                $inDecode['question_status']=1;
            }
            //echo "<pre>"; var_dump($inDecode); die();
            $inDecode['user_id']=1;
            $this->load->model("Question_model","act_model");
            echo $this->act_model->save($inDecode);
            Goto_Page('/question/');
        }
    }
}