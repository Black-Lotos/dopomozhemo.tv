<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_blogs extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    protected $inUser;
    private $inPermition;
    protected $inPage=1;
    protected $inLang=0;
    protected $inLangName='';
    protected $inFooterMenu=false;
    protected $inModule;
    protected $PropertySearch = "";
    protected $inPicPath = "picture/";
    protected $inImgPath = "uploads/";
    protected $inVideoPath = "video/";
    private $inOutRecord = OUTPUT_RECORD;

    function __construct() {
        parent::__construct();
    }
    //
    public function _remap($aMethod=null){
        //die("STEP");
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    
    public function _output($output) {
        echo $output;  
    }
    //
    protected function startUp() {
        
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->load->library('Captcha');
        $this->inUser = $this->session->userdata('user');
        $inData = array(); 
        $this->inPermition = 'start';
        if (!$this->Users_model->isLogin()) {
            //CI_goto("/administration/users/login");
        }
        
        if ($this->Users_model->getCheckRoles($this->inUser,array('Администратор','Главный администратор'))) {
            $this->inPermition = $this->inUser->user_login;
        }
        else {
            $inData['message'] = getMessage('error');
            //echo $this->twig->render("administration/administration_empty.twig", $inData);
            //die("TYT");
        }
        //$this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
    }
    //
    protected function includeUp($aMenuName,$aMenuRecursive=false) {
        $this->Menu_model->setRecursive($aMenuRecursive);
        
        $this->inMenu = $this->Menu_model->load(array('name'=>$aMenuName,'status'=>1));
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        
        $inData = array(
            'page' => array(
                'menu'=>$this->inMenu,
                'property'=>array(
                    'title'=>'Администрирование',
                    'copyright'=>"© 2015 <a href='".base_url()."'></a>. Все права защищены"
                ),
                'content'=>array(
                    'left'=>"hkdhgfdkf",
                    'right'=>'sdfsdjflsfls'
                ),
                'user'=>array (
                    'status'=>'skhfdkjshfkjsh'
                )
            )
        );
        /*array('title'=>'Главная страница','content'=>array('header'=>'Главная страница'),
        'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);*/
        $inData = $this->afterInclude($inData);
        return $inData;
    }
    //
    protected function afterInclude($aData=array()) {
        //echo "after";
        return $aData;
    }
    //
    public function index() {   
        
    }
    public function load() {
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        
        $inData['output'] = $this->Blogs_model->getOutput($inProces);
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Blogs_model->loadCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'admin/section/admin_blogs';
        //
        //echo "<pre>"; var_dump($inProces); die();
        $inData['page']['data'] = $this->Blogs_model->loadTree(array(
            'tree'=>array('fields'=>array("{$this->Blogs_model->getCategoryId()} as value", "{$this->Blogs_model->getCategoryName()} as title", "{$this->Blogs_model->getCategoryStatus()}")),
            'item'=>array('fields'=>array("{$this->Blogs_model->getSelfId()} as value", "{$this->Blogs_model->getSelfName()} as title", "{$this->Blogs_model->getStatus()}", $this->Blogs_model->getStatusMain())),        
                    ),true,$this->inPage,$this->inOutRecord);
        if($inData['page']['data']) {
            foreach ($inData['page']['data'] as $outKey => $outData) {
                $outChecked = ($outData[$this->Blogs_model->getCategoryStatus()]==1)?'checked':'un-checked';
                //$outData['status'] = "<input name='{$outName}' type='checkbox' id='{$outName}' $outChecked />";
                //
                $outData['action'] =    "<a href='#' id='ref-sblogs-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>";
                    if (empty($outData['sub_tree']))    
                        $outData['action'] .= "<a href='#' id='ref-sblogs-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>";
                        $outData['action'] .= "<a href='#' id='ref-sblogs-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";        
                if (!empty($outData['sub_tree'])) {
                    foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                        $outChecked = ($outSData[$this->Blogs_model->getStatus()]==1)?'checked':'un-checked';
                        //var_dump($outSData['news_status_main']); die();
                        $outMain = ($outSData[$this->Blogs_model->getStatusMain()]==1)?'checked':'un-checked';
                        $outSData['action'] =    "<a href='#' id='ref-blogs-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                                "<a href='#' id='ref-blogs-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                                "<a href='#' id='ref-blogs-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>"
                                                ;
                        $outData['sub_tree'][$outSKey] = $outSData;
                    }
                }
                $inData['page']['data'][$outKey] = $outData;
            }
        }
        //echo "<pre>"; var_dump($inData['page']['data']); die();
        echo $this->twig->render("administration/common/list-system-tree-new.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inData[$this->Blogs_model->getCategoryId()] = $this->Blogs_model->loadCategory(array($this->Blogs_model->getCategoryStatus()=>1,
            'fields'=>array("{$this->Blogs_model->getCategoryName()} as title","{$this->Blogs_model->getCategoryId()} as value")));
        $inData['cpicture_id'][] = array('title'=>'- без коллекции -','value'=>'0');
        /*$inData['cpicture_id'] = array_merge($inData['cpicture_id'],                
        $this->Picture_model->loadCategory(array('cpicture_status'=>1,'fields'=>array('cpicture_title as title','cpicture_id as value')))
                );*/
        //echo "<pre>"; var_dump($inData['cpicture_id']); die();
        setLanguage($inData);
        //var_dump($inData['cpicture_id']); die();
        $inData['form'] = $this->Blogs_model->getForm(
            $inData
        );
        //var_dump($inData['form']); die();
        
        echo $this->twig->render("administration/common/form-system-new.twig", $inData);
    }
    //
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        $inCategory = $this->Blogs_model->loadCategory(array($this->Blogs_model->getCategoryStaus()=>1,
            'fields'=>array("{$this->Blogs_model->getCategoryName()} as title","{$this->Blogs_model->getCategoryId()} as value")));
        setLanguage($inData);
        $inData['form'] = $this->Blogs_model->getForm(
                $inData,
                PROCESS_NEWS_SECTION_ADD
        );
        //var_dump($inData['form']); die();
        echo $this->twig->render("administration/common/form-system-new.twig", $inData);
    }
    //
    public function edit() {
        if (!empty($_POST[$this->Blogs_model->getSelfId()])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST[$this->Blogs_model->getSelfId()], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Blogs_model->loadById($inMcId);
            $inActive = $inData[$this->Blogs_model->getCategoryId()];
            $inData[$this->Blogs_model->getCategoryId()] = $this->Blogs_model->loadCategory(array($this->Blogs_model->getCategoryStatus()=>1,
                'fields'=>array("{$this->Blogs_model->getCategoryName()} as title","{$this->Blogs_model->getCategoryId()} as value")));
            $inData[$this->Blogs_model->getCategoryId()] = setCategoryActiveItem($inData[$this->Blogs_model->getCategoryId()], $inActive);
            
            $inMenu = null;
            $inCategory = $this->Blogs_model->loadCategory(array($this->Blogs_model->getCategoryStatus()=>1,
                'fields'=>array("{$this->Blogs_model->getCategoryName()} as title","{$this->Blogs_model->getCategoryId()} as value")));
            //коллекция изображений
            if(!empty($inData['cpicture_id'])) {
                $inActive = $inData['cpicture_id'];
            } else $inActive = 0;   
            $inData['cpicture_id'] = array(array('title'=>'- без коллекции -','value'=>'0'));
            //echo "<pre>"; var_dump($inData['cpicture_id']); die();
            $inData['cpicture_id'] = array_merge($inData['cpicture_id'],
                $this->Picture_model->loadCategory(array('cpicture_status'=>1,'fields'=>array('cpicture_title as title','cpicture_id as value')))
                );
            //echo "<pre>"; var_dump($inData['cpicture_id']); die();
            $inData['cpicture_id'] = setCategoryActiveItem($inData['cpicture_id'], $inActive);
            //echo "<pre>"; var_dump($inData['cpicture_id']); die();
            setLanguage($inData,true);
            $inData['form'] = $this->Blogs_model->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system-new.twig", $inData);
        }
    }
    public function delete() {
        if (!empty($_POST[$this->Blogs_model->getSelfId()])&&$_POST['process']=='delete-news') {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST[$this->Blogs_model->getSelfId()], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Blogs_model->DeleteById($inMcId);
        }
    }
    public function delete_category() {
        if (filter_input(INPUT_POST,'process')=='delete-snews' && filter_input(INPUT_POST,'nc_id')) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['nc_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Blogs_model->deleteCategoryById($inMcId);
        }
    }
        

    public function clear_image() {
        if (filter_input(INPUT_POST, $this->Blogs_model->getSelfId()) && filter_input(INPUT_POST, 'process')=='clear-image-news') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST[$this->Blogs_model->getSelfId()], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Blogs_model->loadById($inMcId);
            $inData['images_id'] = null;
            //echo json_encode($inData); die();
            //$this->Debug();
            $inRes = $this->Blogs_model->save($inData);
            $inRes["rec-no"] = '';
            $inRes["rec-name"] = "images/empty210x210.png";
            echo json_encode($inRes);
        }
    }
    public function edit_section() {
        if (!empty($_POST['nc_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['nc_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Blogs_model->loadCategoryById($inMcId);
            setLanguage($inData,true);
            $inData['form'] = $this->Blogs_model->getForm(
                $inData,
                PROCESS_NEWS_SECTION
            );
            echo $this->twig->render("administration/common/form-system-new.twig", $inData);
        }
    }
    //
    public function save() {   
        
        if (!empty($_POST)) {
            $inDecode=$_POST; $outResultMove=true;
            if(!empty($inDecode[$this->Blogs_model->getStatus()])&&$inDecode[$this->Blogs_model->getStatus()]=='on') {
                $inDecode[$this->Blogs_model->getStatus()]=1;
            }
            if(!empty($inDecode[$this->Blogs_model->getStatusTop()])&&$inDecode[$this->Blogs_model->getStatusTop()]=='on') {
                $inDecode[$this->Blogs_model->getStatusTop()]=1;
            }
            if(!empty($inDecode[$this->Blogs_model->getStatusMain()])&&$inDecode[$this->Blogs_model->getStatusMain()]=='on') {
                $inDecode[$this->Blogs_model->getStatusMain()]=1;
            }
            
            //$inDecode['images_id'] = 'uploads/empty.jpg';
            if (!empty($_FILES['images_id'])) {
                if ($_FILES['images_id']['size']>0) {
                    $inFName = $_FILES['images_id']['tmp_name'];
                    $inExt = pathinfo($_FILES['images_id']['name']); $inExt = $inExt['extension'];
                    $inGenName = 'news_'.md5(time()).".$inExt";
                    $outFName = 'uploads/'.$inGenName;
                    $inDecode['images_id'] = $inGenName;
                    $outResultMove = move_uploaded_file($inFName, $outFName);
                    if($outResultMove) {
                        $writeImages = array(
                            'images_created'=>date('Y-m-d H:i:s',time()),
                            'images_folder'=>'uploads/',
                            'images_name'=>$inGenName,
                            'images_type'=>T_IMAGE);
                        $this->load->model('Images_model','images');
                        $inDecode['images_id'] = $this->images->save($writeImages);
                        $inDecode['images_id'] = $inDecode['images_id']['rec-no'];
                        //$inDecode['images_id'] = $inDecode['images_id']['rec_no'];
                    } else {unset($inDecode['images_id']);}
                    //die();
                }
            }
            $inDecode['images_id'] = ($inDecode['images_id']==0)?null:$inDecode['images_id'];
            $this->Blogs_model->save($inDecode);
            Goto_Page('/admin/section/admin_blogs');
        }
    }
    //
    public function change_status() {
        if (!empty($_POST)) {
            //var_dump(filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)); die();
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, $this->Blogs_model->getSelfId(), FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->Blogs_model->loadById($inId);
                    $inBlogs['news_status'] = ($inBlogs['news_status']==0)?1:0;
                    $this->Blogs_model->save($inBlogs);
                    echo $inBlogs['news_status'];
                    break;
                case 'change-status-main':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, $this->Blogs_model->getSelfId(), FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->Blogs_model->loadById($inId);
                    $inBlogs['news_status_main'] = ($inBlogs['news_status_main']==0)?1:0;
                    $this->Blogs_model->save($inBlogs);
                    echo $inBlogs['news_status_main'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'nc_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    //echo $inId; die();
                    $inCBlogs = $this->Blogs_model->loadCategoryById($inId);
                    $inCBlogs['nc_status'] = ($inCBlogs['nc_status']==0)?1:0;
                    $this->Blogs_model->save_category($inCBlogs);
                    echo $inCBlogs['nc_status'];
                    break;
            }
        }
    }
    //
    public function save_category() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
        }
        if ($this->Blogs_model->save_category($inDecode)) {
            setMessage('Категория новостей записана.');
        } else  {
                    setMessage('Ошибка записи раздела новостей.','error');
                };
        Goto_Page('/admin/section/admin_blogs');
    }
    //
}

