<?php
class Admin_expert_help extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    protected $inUser;
    private $inPermition;
    protected $inPage=1;
    protected $inLang=0;
    protected $inLangName='';
    protected $inFooterMenu=false;
    protected $inModule;
    protected $PropertySearch = "";
    protected $inPicPath = "picture/";
    protected $inImgPath = "uploads/";
    protected $inVideoPath = "video/";
    private $inOutRecord = OUTPUT_RECORD;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output) {
        echo $output;  
    }
    //
    private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model("Expert_help_model");
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
    }
    //
    protected function includeUp($aMenuName,$aMenuRecursive=false) {
        $this->Menu_model->setRecursive($aMenuRecursive);
        
        $this->inMenu = $this->Menu_model->load(array('name'=>$aMenuName,'status'=>1));
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        
        $inData = array(
            'page' => array(
                'menu'=>$this->inMenu,
                'property'=>array(
                    'title'=>'Администрирование',
                    'copyright'=>"© 2015 <a href='".base_url()."'></a>. Все права защищены"
                ),
                'content'=>array(
                    'left'=>"",
                    'right'=>''
                ),
                'user'=>array (
                    'status'=>''
                )
            )
        );
        /*array('title'=>'Главная страница','content'=>array('header'=>'Главная страница'),
        'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);*/
        $inData = $this->afterInclude($inData);
        return $inData;
    }
    //
    public function index() {   
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        if (empty($inArg)) {
            $inData['output'] = $this->Expert_help_model->getOutput();
            $inData['tabs_run'] = $this->Tabs_model->loadExpertHelp($this->inPage);
        }
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель ""',
                'picture'=>array('left'=>'',
                'right'=>$this->twig->render("{$inData['tabs_run']['property']['template']}", $inData)),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function load_section() {
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        $inData['output'] = $this->Expert_help_model->getOutput($inProces);
        //echo "<pre>"; var_dump($inData['output']); die();
        $inData['data'] = $this->Expert_help_model->loadTree();
        //echo "<pre>"; var_dump($inData['data']); die();
        if (!is_array($inData['data'])) {
            $inData['data'][] = array('p_category_status'=>0,'value'=>0,'title'=>'Не существует каталога помощников. Введите каталог');
        }
        if (!empty($inData['data'])) {
            foreach ($inData['data'] as $outKey => $outData) {
                $outChecked = ($outData['p_category_status']==1)?'checked':'un-checked';
                $outData['action'] = "<a href='#' id='node-edit-{$outData['value']}' class='action-base action-edit'></a>";
                if (empty($outData['sub_tree']))
                    $outData['action'] .=   "<a href='#' id='node-delete-{$outData['value']}' class='action-base action-delete'></a>";
                $outData['action'] .= "<a href='#' id='node-status-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                if (!empty($outData['sub_tree'])) {
                    foreach ($outData['sub_tree'] as $outSKey => $outSData){
                        $outChecked = ($outSData[$this->Expert_help_model->getStatus()]==1)?'checked':'un-checked';
                        $outSData['action']  = "<a href='#' id='items-edit-{$outSData['value']}' class='action-base action-edit'></a>";
                        $outSData['action'] .= "<a href='#' id='items-delete-{$outSData['value']}' class='action-base action-delete'></a>";
                        $outSData['action'] .= "<a href='#' id='items-status-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                        $outData['sub_tree'][$outSKey] = $outSData;
                    }
                }
                $inData['data'][$outKey] = $outData;
            }
        } 
        //echo "<pre>"; var_dump($inData); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    public function load($aParam=null) {
        $inData = array();
        //var_dump($this->inPage); die();
        
        $inSelectMenu = !empty($aParam)?"/$aParam/":"";
        $incAction = (!empty($aParam))?'items':"node";
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Expert_help_model->loadCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'admin/section/admin_expert_help';
        $inData['page']['setup_page'] = $this->Expert_help_model->getOutput();
        //$this->Expert_help_model->debug();
        if(empty($aParam)) {
            $inData['page']['data'] = $this->Expert_help_model->loadTree(
                array(
                    'tree'=>array('fields'=>array("{$this->Expert_help_model->getCategoryStatus()} as status",
                            "{$this->Expert_help_model->getCategoryName()} as title","{$this->Expert_help_model->getCategoryId()} as value")),
                    'item'=>array('fields'=>array("{$this->Expert_help_model->getSelfId()} as value","{$this->Expert_help_model->getSelfName()} as title", "{$this->Expert_help_model->getStatus()} as status"))    
                ),false,$this->inPage,$this->inOutRecord);
        } else {
            //var_dump($this->Expert_help_model->getCategoryId()); die();
            
            $inData['page']['data'] = $this->Expert_help_model->loadTree(
                array(
                    'tree'=>array($this->Expert_help_model->getCategoryId()=>$aParam,
                        'fields'=>array("{$this->Expert_help_model->getCategoryStatus()} as status",
                            "{$this->Expert_help_model->getCategoryName()} as title","{$this->Expert_help_model->getCategoryId()} as value")),
                    'item'=>array($this->Expert_help_model->getCategoryId()=>$aParam,
                        'fields'=>array("{$this->Expert_help_model->getSelfId()} as value","{$this->Expert_help_model->getSelfName()} as title", "{$this->Expert_help_model->getStatus()} as status"))    
                ),false,$this->inPage,$this->inOutRecord);
        }
        //echo "<pre>"; var_dump($inData['page']['data']); die();
        if ($inData['page']['data']) {
            foreach ($inData['page']['data'] as $outKey => $outData) {
                $outChecked = ($outData['status']==1)?'checked':'un-checked';
                if (empty($aParam))
                $outData['action'] =   
                    "<a href='/{$inData['page']['link_run']}/{$outData['value']}/?page={$this->inPage}' id='ref-menu-child-{$outData['value']}' class='action-base action-child' title='Перейти к пунктам'></a>";    
                else $outData['action']='';           
                $outData['action'] .=
                    "<a href='#' id='ref-{$incAction}-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>";
                    if(empty($outData['sub_tree'])) {
                        $outData['action'] .= "<a href='#' id='ref-{$incAction}-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>";
                    };
                    $outData['action'] .= "<a href='#' id='ref-{$incAction}-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                    if (!empty($outData['sub_tree'])) {
                        foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                            $outChecked = ($outSData['status']==1)?'checked':'un-checked';
                            $outSData['action'] =   
                                "<a href='#' id='ref-items-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                "<a href='#' id='ref-items-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                "<a href='#' id='ref-items-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                            $outData['sub_tree'][$outSKey] = $outSData;
                        }
                    }
                $inData['page']['data'][$outKey] = $outData;                        
            }
        }
        echo $this->twig->render("administration/common/list-system-tree-new.twig", $inData);
        
    }
    public function add_section() {
        $inData = array();
        $inCategory = $this->Expert_help_model->loadCategory(
            array($this->Expert_help_model->getCategoryStatus()=>1,
                'fields'=>array("{$this->Expert_help_model->getCategoryName()} as title","{$this->Expert_help_model->getCategoryId()} as value")
            )
        );
        setLanguage($inData);
        $inData['form'] = $this->Expert_help_model->getForm(
            $inData,
            PROCESS_SECTION_ADD
        );
        echo $this->twig->render("administration/common/form-system-new.twig", $inData);
    }
    public function add($aParam=null) {
        $inData = array();
        $inCategory = $this->Expert_help_model->loadCategory(
            array($this->Expert_help_model->getCategoryStatus()=>1,
                'fields'=>array("{$this->Expert_help_model->getCategoryName()} as title","{$this->Expert_help_model->getCategoryId()} as value")
            )
        );
        setCategoryActiveItem($inCategory, $aParam);
        $inData[$this->Expert_help_model->getCategoryId()] = $inCategory;
        setLanguage($inData);
        $inData['form'] = $this->Expert_help_model->getForm(
            $inData
        );
        echo $this->twig->render("administration/common/form-system-new.twig", $inData);
    }
    public function edit_section() {
        if ((filter_input(INPUT_POST, 'process')=='category-edit')&&filter_input(INPUT_POST, 'category_id')) {
            $inData = array();
            $inId = filter_input(INPUT_POST, 'category_id');
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $inId, $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Expert_help_model->loadCategoryById($inId);
            setLanguage($inData,TRUE);
            $inData['form'] = $this->Expert_help_model->getForm(
                $inData,
                PROCESS_SECTION_ADD
            );
            echo $this->twig->render("administration/common/form-system-new.twig", $inData);
        }
    }
    public function edit() {
        if ((filter_input(INPUT_POST, 'process')=='items-edit')&&filter_input(INPUT_POST, 'items_id')) {
            $inData = array();
            $inId = filter_input(INPUT_POST, 'items_id');
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $inId, $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Expert_help_model->loadById($inId);
            $inActive = $inData[$this->Expert_help_model->getCategoryId()];
            $inFilterCategory = array($this->Expert_help_model->getCategoryStatus()=>1,
                'fields'=>array("{$this->Expert_help_model->getCategoryName()} as title","{$this->Expert_help_model->getCategoryId()} as value"));
            $inData[$this->Expert_help_model->getCategoryId()] = $this->Expert_help_model->loadCategory($inFilterCategory);
            $inData[$this->Expert_help_model->getCategoryId()] = setCategoryActiveItem($inData[$this->Expert_help_model->getCategoryId()], $inActive);
            setLanguage($inData,TRUE);
            //var_dump($inData); die("ddddddd");        
            $inData['form'] = $this->Expert_help_model->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system-new.twig", $inData);
        }
    }
    //
    public function edit_qap() {
        if ((filter_input(INPUT_POST, 'process')=='items-edit')&&filter_input(INPUT_POST, 'items_id')) {
            
        }
    }
    //
    public function save_section() {
        if ($_POST) {
            $this->Expert_help_model->save_category($_POST);
        }
        Goto_Page("/admin/section/admin_expert_help/");
    }
    public function save() {
        //var_dump($_POST); die("GGGGG");
        if ($_POST) {
            //$this->Expert_help_model->Debug();
            $inDecode=$_POST;
            if(!empty($inDecode["{$this->Expert_help_model->getPrefix()}_status"])&&$inDecode["{$this->Expert_help_model->getPrefix()}_status"]=='on') {
                $inDecode["{$this->Expert_help_model->getPrefix()}_status"]=1;
            }
            $this->Expert_help_model->save($inDecode);
        }
        Goto_Page("/admin/section/admin_expert_help/");
    }
    public function change_status() {
        //echo "<pre>"; var_dump($_POST); die();
        if (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST,"items_id"), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->Expert_help_model->loadById($inId);
                    $inBlogs[$this->Expert_help_model->getStatus()] = ($inBlogs[$this->Expert_help_model->getStatus()]==0)?1:0;
                    $this->Expert_help_model->save($inBlogs);
                    echo $inBlogs[$this->Expert_help_model->getStatus()];
                    break;
                case 'change-status-category':
                    $inDataId = filter_input(INPUT_POST, "category_id", FILTER_SANITIZE_SPECIAL_CHARS);
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $inDataId, $outMatches)>0)?(int)$outMatches[0]:0;
                    $inCBlogs = $this->Expert_help_model->loadCategoryById($inId);
                    //var_dump($inCBlogs);
                    $inCBlogs[$this->Expert_help_model->getCategoryStatus()] = ($inCBlogs[$this->Expert_help_model->getCategoryStatus()]==0)?1:0;
                    //echo "<pre>"; var_dump($inCBlogs); die(); 
                    $this->Expert_help_model->save_category($inCBlogs);
                    echo $inCBlogs[$this->Expert_help_model->getCategoryStatus()];
                    break;
            }
        }
        
    }
    public function question_load() {
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        
        $inData['output'] = $this->Professional_question_model->getOutput($inProces);
        
        $inData['page']['active'] = $this->inPage;
        //$inData['page']['count'] = $this->Professional_question_model->loadCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'administration/section/admin_expert_help';
        //
        
        $inData['data'] = $this->Professional_question_model->loadTree(array(
            'tree'=>array('fields'=>array("{$this->Professional_question_model->getCategoryId()} as value, {$this->Professional_question_model->getCategoryName()} as title, {$this->Professional_question_model->getCategoryStatus()}")),
            'item'=>array('fields'=>array("{$this->Professional_question_model->getSelfId()} as value, {$this->Professional_question_model->getSelfName()} as title, {$this->Professional_question_model->getStatus()}")),        
                    ),true,$this->inPage,$this->inOutRecord);
        //var_dump($inData); die();
        foreach ($inData['data'] as $outKey => $outData) {
            
            $outChecked = ($outData[$this->Professional_question_model->getCategoryStatus()]==1)?'checked':'un-checked';
            //$outData['status'] = "<input name='{$outName}' type='checkbox' id='{$outName}' $outChecked />";
            //
            $outData['action'] =    '';//"<a href='#' id='ref-news-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>";
                //if (empty($outData['sub_tree']))    
                    //$outData['action'] .= "<a href='#' id='ref-news-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>";
                    //$outData['action'] .= "<a href='#' id='ref-news-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";        
            if (!empty($outData['sub_tree'])) {
                foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                    $outChecked = ($outSData[$this->Professional_question_model->getStatus()]==1)?'checked':'un-checked';
                    //var_dump($outSData['news_status_main']); die();
                    $outSData['action'] =    "<a href='#' id='ref-newscomments-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                            "<a href='#' id='ref-comments-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                            "<a href='#' id='ref-newscomments-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>"
                                            ;
                    $outData['sub_tree'][$outSKey] = $outSData;
                }
            }
            $inData['data'][$outKey] = $outData;
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

