<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_static extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    protected $inUser;
    private $inPermition;
    protected $inPage=1;
    protected $inLang=0;
    protected $inLangName='';
    protected $inFooterMenu=false;
    protected $inModule;
    protected $PropertySearch = "";
    protected $inPicPath = "picture/";
    protected $inImgPath = "uploads/";
    protected $inVideoPath = "video/";
    private $inOutRecord = OUTPUT_RECORD;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output) {
        echo $output;  
    }
    //
    private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model("Expert_help_model");
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
    }
    //
    protected function includeUp($aMenuName,$aMenuRecursive=false) {
        $this->Menu_model->setRecursive($aMenuRecursive);
        
        $this->inMenu = $this->Menu_model->load(array('name'=>$aMenuName,'status'=>1));
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        
        $inData = array(
            'page' => array(
                'menu'=>$this->inMenu,
                'property'=>array(
                    'title'=>'Администрирование',
                    'copyright'=>"© 2015 <a href='".base_url()."'></a>. Все права защищены"
                ),
                'content'=>array(
                    'left'=>"",
                    'right'=>''
                ),
                'user'=>array (
                    'status'=>''
                )
            )
        );
        /*array('title'=>'Главная страница','content'=>array('header'=>'Главная страница'),
        'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);*/
        $inData = $this->afterInclude($inData);
        return $inData;
    }
    //
    public function index() {   
    }
    //
    public function load() {
        $this->load->model("Static_model");
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        //$this->act_model->Debug();
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Static_model->loadCountPage($this->inOutRecord);
        $inData['page']['data'] = $this->Static_model->loadTree();
        $inData['page']['setup_page'] = $this->Static_model->getOutput();
        //echo "<pre>"; var_dump($inData['page']['data']); //die();
        if (empty($inData['page']['data'])) {
            $outDefault = array('spc_title'=>'Системные страницы','spc_text'=>'Системные страницы','spc_status'=>1,'language_id'=>1);
            //$this->act_model->Debug();
            //$this->act_model->save_category($outDefault);
            $inData['page']['data'] = $this->Static_model->loadCategory(array('fields'=>array('spc_title as title, spc_id as value, spc_status')));
        }
        //var_dump($inList); die();
        //$inData['data'] = ($inList==false)?array("title"=>'Нет категорий страниц. Введите категорию страниц.',"url"=>'#'):$inList;
        if(!empty($inData['page']['data'])) {
            foreach ($inData['page']['data'] as $outKey => $outData) {
                $outChecked = ($outData['spc_status']==1)?'checked':'un-checked';
                $outData['action'] =    "<a href='#' id='ref-spc-edit-{$outData['value']}' class='action-base action-edit'></a>";
                if (empty($outData['sub_tree']))
                    $outData['action'] .= "<a href='#' id='ref-spc-delete-{$outData['value']}' class='action-base action-delete'></a>";
                $outData['action'] .= "<a href='#' id='ref-spc-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";        
                if (!empty($outData['sub_tree'])) {
                    foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                        $outChecked = ($outSData['sp_status']==1)?'checked':'un-checked';
                        $outSData['action'] =   "<a href='#' id='ref-sp-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                            "<a href='#' id='ref-sp-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                            "<a href='#' id='ref-sp-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                                            //"<a href='#' id='ref-picture-main-{$outSData['value']}' class='action-base action-main-{$outMain}' title='Отключить/Включить'></a>".
                                            //"<a href='#' id='ref-picture-video-{$outSData['value']}' class='action-base action-video' title='Код встраивания'></a>"        
                                            ;
                        $outData['sub_tree'][$outSKey] = $outSData;
                    }
                }                        
                $inData['page']['data'][$outKey] = $outData;
            }
        }
        echo $this->twig->render("administration/common/list-system-tree-new.twig", $inData);
    }
    public function add() {
        $this->load->model("Static_model");
        $inData = array();
        $inData['spc_id'] = $this->Static_model->loadCategory(array('spc_status'=>1,'fields'=>array('spc_title as title','spc_id as value')));
        setLanguage($inData);
        $inData['form'] = $this->Static_model->getForm(
                $inData
        );
        echo $this->twig->render("administration/common/form-system-new.twig", $inData);
    }
    public function add_section() {
        $inData = array();
        $this->load->model("Static_model");
        $inArg = func_get_args();
        //$inCategory = $this->act_model->loadCategory(array('cquestion_status'=>1,'fields'=>array('cquestion_title as title','cquestion_id as value')));
        setLanguage($inData);
        //die('tyta');
        $inData['form'] = $this->Static_model->getForm(
                $inData,
                PROCESS_STATIC_ADD_SECTION
        );
        echo $this->twig->render("administration/common/form-system-new.twig", $inData);
    }
    public function edit() {
        $this->load->model("Static_model");
        if (filter_input(INPUT_POST,'sp_id') && filter_input(INPUT_POST,'process')=='edit-item') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST,'sp_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            //$this->act_model->Debug();
            //var_dump($inMcId); die();
            $inData = $this->Static_model->loadById($inMcId);
            
            $inActive = $inData['spc_id'];
            $inData['spc_id'] = $this->Static_model->loadCategory(array('fields'=>array('spc_title as title','spc_id as value')));
            $inData['spc_id'] = setCategoryActiveItem($inData['spc_id'], $inActive);
            setLanguage($inData,TRUE);
            $inData['form'] = $this->Static_model->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system-new.twig", $inData);
        }
    }
    public function delete() {
        $this->load->model("Static_model");
        if (filter_input(INPUT_POST,'sp_id') && filter_input(INPUT_POST,'process')=='delete-item') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST,'sp_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            //$this->act_model->Debug();
            $inData = $this->Static_model->deleteById($inMcId);
        }
    }
    public function edit_section() {
        $this->load->model("Static_model");
        //$inData = $this->act_model->loadCategoryById(2);
        //var_dump(filter_input(INPUT_POST,'spc_id') && filter_input(INPUT_POST,'process')=='edit-category');
        if (filter_input(INPUT_POST,'spc_id') && filter_input(INPUT_POST,'process')=='edit-category') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST,'spc_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            //$this->act_model->Debug();
            $inData = $this->Static_model->loadCategoryById($inMcId);
            setLanguage($inData,TRUE);
            $inData['form'] = $this->Static_model->getForm(
                $inData,
                PROCESS_STATIC_ADD_SECTION
            );
            echo $this->twig->render("administration/common/form-system-new.twig", $inData);
        }
    }
    public function change_status() {
        $this->load->model("Static_model");
        if (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'spc_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inData = $this->Static_model->loadById($inId);
                    $inData['sp_status'] = ($inData['sp_status']==0)?1:0;
                    $this->Static_model->save($inData);
                    echo $inData['sp_status'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'spc_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inData = $this->Static_model->loadCategoryById($inId);
                    $inData['spc_status'] = ($inData['spc_status']==0)?1:0;
                    $this->Static_model->save_category($inData);
                    echo $inData['spc_status'];
                    break;
            }
        }
        
    }
    public function save() {   
        $this->load->model("Static_model");
        if(filter_input(INPUT_POST,'process')=='item-save') {
            $inData = $_POST; unset($inData['process']);
            $inData['sp_status'] = (!empty($inData['sp_status']))?1:0;
            //$inData['sp_status'] = $inStatus;
            //$this->Static_model->Debug();
            if ($this->Static_model->save($inData)) {
                setMessage('Страница записана');
            } else setMessage('Ошибка записи','error');
            //setMessage('Страница записана');
            Goto_Page('admin/section/admin_static');
        }
    }
    public function save_section() {   
        $this->load->model("Static_model");
        //var_dump($_POST); die("section-save");
        if(filter_input(INPUT_POST,'process')=='section-save') {
            $inData = $_POST; unset($inData['process']);
            $inData['spc_status'] = (!empty($inData['spc_status']))?1:0;
            //$this->Static_model->Debug();
            //echo $this->Static_model->save_category($inData);
            if ($this->Static_model->save_category($inData)) {
                setMessage('Категория записана');
            } else setMessage('Ошибка записи категории','error');
            Goto_Page('admin/section/admin_static');
        }
    }
}

