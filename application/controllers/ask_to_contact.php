<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
•	Имя того, кто оставляет заявку
•	Телефон
•	E-mail
•	Подробное описание
•	Дата заявки
•	Ответ
•	Дата ответа
•	Статус «Опубликовано» / «Не опубликовано»
•ask_to_contact_	Статус «Решена» / «Не решена»

 *  */
class Ask_to_contact extends MY_Controller {
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
    protected function afterInclude($aData = array()) {
        $inData = $aData;
        $this->load->model("Ask_to_contact_model","act_model");
        $inData['pieses']['listask_to_contact'] = $this->act_model->getOutput(OUTPUT_LIST);
        $this->act_model->setCountRecord(0);
        return $inData;
    }
    //
    public function index() {   
        $inArg = func_get_args();
        $inData = $this->includeUp();
        //echo "<pre>"; var_dump($inData);         die();
        $inCategory = "";
        $inForm = $this->act_model->getForm($inCategory, null);
        $inForm['form_property']['action'] = '/ask_to_contact/save';
        //echo "<pre>"; var_dump($inForm); die();
        //var_dump($this->inLangName);
        $inData['sub_page'] = 
            getVariableLanguageByName('caption_callme_subtitle',$this->inLangName).    
            $this->twig->render("administration/common/form-users.twig", 
                        array('form'=>$inForm));
        
        $inData['sub_page_title'] = "<h2>".getCaptionInput('caption_callme')."</h2>";
        //echo "<pre>"; var_dump(getCaptionInput('caption_callme')); die();        
       
        //echo $this->twig->render('ask-to-contact/ask-to-contact-detail.twig', $inData);
        echo $this->twig->render('site-master-page.twig',$inData);
    }
    //
    public function add() {
        $inData = $this->includeUp();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inCategory = "";
        $inData['pieses']['ask_to_contact']['form'] = $this->act_model->getForm(
                $inCategory,
                null
        );
        //echo "<pre>";var_dump($inData['pieses']['ask_to_contact']['form']['form_property']['action']); die();
        $inData['pieses']['ask_to_contact']['form']['form_property']['action'] = '/ask_to_contact/save';
        $inData['pieses']['ask_to_contact']['add']['form'] = $this->twig->render("administration/common/form-users.twig", array('form'=>$inData['pieses']['ask_to_contact']['form']));
        //echo "<pre>";var_dump($inData['pieses']['ask_to_contact']['add']['form']); die();
        $inData['pieses']['ask_to_contact']['add']['isRun'] = true;
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function detail($aId=null) {
        $inData = $this->includeUp(); $inId = 0;
        if ($_POST) {
            $inNewsId = filter_input(INPUT_POST, 'ask_to_contact_id', FILTER_SANITIZE_SPECIAL_CHARS);
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $inNewsId, $outMatches)>0)?(int)$outMatches[0]:0;
        } else $inId  = $aId;
        if (!empty($inId)) {
            $inSData=array('comments'=>array('title'=>getCaptionInput('caption_comments'),'counts'=>$this->Comments->CountByask_to_contactId($inId)));
        }
        $inData['sub_page'] = $this->twig->render('ask_to_contact/ask_to_contact-one-record-list.twig', $inSData);
        //echo "<pre>"; var_dump($inSData); die();
        echo $this->twig->render('ask_to_contact/ask_to_contact-page-detail.twig', $inData);
    }
    //
    public function save_comments() {
        if (!empty($_POST)) {
            //$this->Comments->save($_POST);
        }
        Goto_Page('/ask_to_contact/');
    }
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            //die();
            $inCategory = "";
            $inDecode=$_POST;
            if(!empty($inDecode['ask_to_contact_status'])&&$inDecode['ask_to_contact_status']=='on') {
                $inDecode['ask_to_contact_status']=1;
            }
            
            $inDecode['user_id']=1;
            $this->act_model->save($inDecode);
            
            $inDecode = array('data'=>$inDecode);
            //echo "<pre>"; var_dump($inDecode); die();
            $inSubject = iconv("utf-8", "windows-1251", "Раздел: ПОПРОСИТЬ СВЯЗАТЬСЯ");
            $inMessage = iconv("utf-8", "windows-1251",$this->twig->render('ask-to-contact/ask-to-contact-out-post.twig',$inDecode));
            //var_dump($inMessage);
            // = $this->twig->render("administration/common/form-users.twig",$inDecode);
            //var_dump($inMessage); die();
            //_email('v.shevchenko@lck-group.com', 'postmaster@dopomozhemo.eco-n-tech.com', "Раздел: ПОПРОСИСТЬ СВЯЗАТЬСЯ", $inMessage);
            $this->inPage = isset($_GET['page'])?$_GET['page']:1;
            $inData = $this->includeUp();
            $inData['sub_page'] = "<center><h2>Ваше обращение записано</h2></center>";
            $inData['sub_page'] .= "<center><a class='button-base blue whide' id='btnGoask_to_contact' href='/ask_to_contact' url='#'>Продолжить</a></center>";
            
            //$inMessage = iconv("utf-8", "windows-1251", "Зарегистрирован запрос.");
            
            _email('rybak.evgen@gmail.com', 'postmaster@dopomozhemo.eco-n-tech.com', $inSubject , $inMessage);
            echo $this->twig->render('ask-to-contact/ask-to-contact-detail.twig', $inData);
        }
    }
}