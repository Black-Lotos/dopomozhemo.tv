<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_menu extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    private $inPage=1;
    private $inOutRecord = 26;
    public function _remap($aMethod=null){
        //$this->twig->getTwig()->addExtension(new Dopomozhemo_Twig_Extension());
        $inArg = func_get_args();
        //echo "content"; print_r($inArg);
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            //var_dump($inArg);
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
    private function startUp() {
        //__init();
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
    }
    //
    public function index()
    {   
        $inData = array();
        //echo $this->twig->render('administration/administration_master.twig', $inData);
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $inData = array('title'=>'Административная панель',
            'content'=>array('left'=>'Menu','right'=>'Cgbcjr'),
            'menu'=>$inMenu);
        if (!empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadMenuItem();
        }
        //$this->load->view('administration_start', $data);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    //
    public function load_menu() {
        $inData = array();
        //var_dump($_POST); die();
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        
        if (!empty($_POST['ItemId'])) {
            
            $inMcId = $_POST['ItemId']; $inPattern = '/([0-9]+)$/';
            $inMcId = preg_match($inPattern, $inMcId, $outMatches);
            $inMcId = ($inMcId>0)?(int)$outMatches[0]:0;
            $inSubMenu = $this->Menu_model->load(array('menu_category_id'=>$inMcId, 'status'=>1));
            $inCategory = $this->Menu_model->loadCategoryById($inMcId);
            //var_dump($inSubMenu); die();
            $inData = array('title'=>'Административная панель','content'=>array('left'=>$inCategory['mc_title'],'right'=>'ffff'),'menu'=>$inMenu);
            $inData['title'] = $inCategory['mc_title'];
            $inData['tabs'] = array(
                    array('title'=>'Список пунктов','url'=>"/administration/menu/load/sub-item/$inMcId"),
                    array('title'=>'Добавить пункт','url'=>"/administration/menu/add/sub-item/{$inMcId}")
            );
        } else {
        //echo "<pre>"; var_dump($inSubMenu); die();
        //echo $this->twig->render('administration/administration_master.twig', $inData);
        //echo "<pre>"; var_dump($inCategory); die();
        }
        //echo "<pre>"; var_dump($inData); die("load_menu");
        echo $this->twig->render('/administration/menu/menu-load.twig', $inData);
        
        //$this->load->view('administration_start', $data);
    }
    public function load() {   
        $inData = array();
        $inData['output'] = $this->Menu_model->getOutput();
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Menu_model->loadCategoryCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'administration/content/admin_menu';
        $inData['data'] = $this->Menu_model->loadCategory(array('fields'=>array('mc_status','mc_title as title','menu_category_id as value')));
        foreach ($inData['data'] as $outKey => $outData) {
            $outChecked = ($outData['mc_status']==1)?'checked':'un-checked';
            $outData['action'] =   
                "<a href='/administration/content/admin_menu?page={$this->inPage}&select_menu={$outData['value']}' id='ref-menu-child-{$outData['value']}' class='action-base action-child' title='Перейти к пунктам'></a>".    
                "<a href='#' id='ref-menu-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                "<a href='#' id='ref-menu-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>".
                "<a href='#' id='ref-menu-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
            $inData['data'][$outKey] = $outData;                        
        }
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    public function load_menu_item() {
        $inData = array();
        $inSelectMenu = filter_input(INPUT_GET, 'select_menu');
        if (!preg_match("/\d/",$inSelectMenu)) {
            echo $this->load(); die();
        }
        $inData['output'] = $this->Menu_model->getOutput();
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Menu_model->loadCountPage($this->inOutRecord,array('menu_category_id'=>$inSelectMenu));
        $inData['page']['link_run'] = 'administration/content/admin_menu';
        $inData['page']['output_advanced'] = (!empty($inSelectMenu)?"&select_menu=".$inSelectMenu:'');
        $inData['data'] = $this->Menu_model->load(array('menu_category_id'=>$inSelectMenu,'fields'=>array('mc_weight','mc_status','mi_weight','mi_status','menu_id','owner_id','mi_url','mi_title_ru as title','menu_id as value')));
        //$aFilter=array(), $aUnion=true, $aRender=true, $aPage=0, $aCountRec=12, $aDebug=false
        //echo "<pre>"; var_dump($inData['data']); die();
        
        if ($inData['data']) {
            foreach ($inData['data'] as $outKey => $outData) {
                $inInc = $this->Menu_model->load(array('menu_category_id'=>$inSelectMenu,'owner_id'=>0,'fields'=>array('max(mi_weight) as max_weight_mi')),true,false);
                $inInc = (object)$inInc[0];
                //var_dump((int)$inInc->max_weight_mi); die();
                $outChecked = ($outData['mi_status']==1)?'checked':'un-checked';
                $outPos = (object)array(
                    'ToTop'=>($outData['mi_weight']>0)?'':'-disable',
                    'ToDec'=>($outData['mi_weight']>0)?'':'-disable',
                    'ToBottom'=>($outData['mi_weight']==0)?'':'-disable',
                    'ToInc'=>($outData['mi_weight']>=0 && $outData['mi_weight']<(int)$inInc->max_weight_mi)?'':'-disable');
                $outData['action'] =   
                    //"<a href='#' id='ref-mi-to-top-{$outData['value']}' class='action-base action-to-top{$outPos->ToTop}' title='В верх'></a>".
                    //"<a href='#' id='ref-mi-to-dec-{$outData['value']}' class='action-base action-to-dec{$outPos->ToDec}' title='Поднять'></a>".       
                    //"<a href='#' id='ref-mi-to-bottom-{$outData['value']}' class='action-base action-to-bottom{$outPos->ToBottom}' title='В конец'></a>".                
                    //"<a href='#' id='ref-mi-to-inc-{$outData['value']}' class='action-base action-to-inc{$outPos->ToInc}' title='Ниже'></a>".            
                    "<a href='#' id='ref-mi-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                    "<a href='#' id='ref-mi-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>".
                    "<a href='#' id='ref-mi-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                    if (!empty($outData['sub_tree'])) {
                        foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                            $inInc = $this->Menu_model->load(array('menu_category_id'=>$inSelectMenu,'owner_id'=>$outData['owner_id'],'fields'=>array('max(mi_weight) as max_weight_mi')),true,false);
                            $inInc = (object)$inInc[0];
                            $outChecked = ($outSData['mi_status']==1)?'checked':'un-checked';
                            $outPos = (object)array(
                                'ToTop'=>($outSData['mi_weight']>0)?'':'-disable',
                                'ToDec'=>($outSData['mi_weight']>0)?'':'-disable',
                                'ToBottom'=>($outSData['mi_weight']==0)?'':'-disable',
                                'ToInc'=>($outSData['mi_weight']>=0 && $outSData['mi_weight']<(int)$inInc->max_weight_mi)?'':'-disable');
                            $outSData['action'] =   
                                //"<a href='#' id='ref-mi-to-top-{$outSData['value']}' class='action-base action-to-top{$outPos->ToTop}' title='В верх'></a>".
                                //"<a href='#' id='ref-mi-to-dec-{$outSData['value']}' class='action-base action-to-dec{$outPos->ToDec}' title='Поднять'></a>".               
                                //"<a href='#' id='ref-mi-to-bottom-{$outSData['value']}' class='action-base action-to-bottom{$outPos->ToBottom}' title='В конец'></a>".        
                                //"<a href='#' id='ref-mi-to-inc-{$outSData['value']}' class='action-base action-to-inc{$outPos->ToInc}' title='Ниже'></a>".                
                                "<a href='#' id='ref-mi-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                "<a href='#' id='ref-mi-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                "<a href='#' id='ref-mi-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                                                    //"<a href='#' id='ref-picture-main-{$outSData['value']}' class='action-base action-main-{$outMain}' title='Отключить/Включить'></a>".
                                                    ;
                            $outData['sub_tree'][$outSKey] = $outSData;
                        }
                }
                $inData['data'][$outKey] = $outData;                        
            }
        }
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add()
    {   
        $inData = array();
        $inData['language_id'] = $this->Language_model->loadLanguage(array('status'=>1,'fields'=>array('lan_title as title','language_id as value')));
        $inData['form'] = $this->Menu_model->getForm(
                $inData,
                PROCESS_MENU_ADD_SECTION
        );
        
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add_menu_item() {
        $inData = array();
        $inSelectMenu = filter_input(INPUT_GET, 'select_menu');
        if (!preg_match("/\d/",$inSelectMenu)) {
            //echo $this->load(); die("ddd");
        }
        $inRoot[] = $this->Menu_model->loadCategoryById($inSelectMenu,array('status'=>1,'fields'=>array('mc_title as title','menu_category_id as value')));
        $inCategory = $this->Menu_model->load(array('menu_category_id'=>$inSelectMenu,'status'=>1,'fields'=>array('mi_title_ru as title','mi_url','menu_id','owner_id','menu_id as value')),false);
        $inItemNo   = $this->Menu_model->getMaxItem(array('menu_category_id'=>$inSelectMenu,'fields'=>array('mi_weight'))); $inItemNo = (int)$inItemNo['max_item']+1;
        $inData['language_id'] = $this->Language_model->loadLanguage(array('status'=>1,'fields'=>array('lan_title as title','language_id as value')));
        if (!empty($inRoot) && !empty($inCategory)) {
            $inData['owner_id'] = array_merge($inRoot,$inCategory);
        }
        $inData['menu_category_id'] = $inSelectMenu;
        $inData['mi_weight'] = $inItemNo;
        $inData['form'] = $this->Menu_model->getForm(
                $inData
        );
        //echo "<pre>"; var_dump($inData['form']); die();
        //echo "<pre>"; var_dump($inData); //die();
        //$inSufix = empty($inArg[0])?'start':$inArg[0];
        //echo $this->twig->render("administration/menu/menu-add{$inSufix}.twig", $inData);
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function edit_section()
    {   
        if (filter_input(INPUT_POST, 'menu_id')&&filter_input(INPUT_POST, 'process')=='menu-edit') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST, 'menu_id'), $outMatches))>0?(int)$outMatches[0]:0;
            //var_dump($inMcId); die();
            $inData = $this->Menu_model->loadCategoryById($inMcId);
            //$inData = array();
            $inData['language_id'] = $this->Language_model->loadLanguage(array('status'=>1,'fields'=>array('lan_title as title','language_id as value')));
            $inData['form'] = $this->Menu_model->getForm(
                $inData,
                PROCESS_MENU_ADD_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    //
    public function delete_section()
    {   
        if (filter_input(INPUT_POST, 'menu_id')&&filter_input(INPUT_POST, 'process')=='menu-edit') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST, 'menu_id'), $outMatches))>0?(int)$outMatches[0]:0;
            //var_dump($inMcId); die();
            $inData = $this->Menu_model->DeleteCategoryById($inMcId);
            //$inData = array();
        }
    }
    //
    public function edit() {   
        $outMatches = array();
        if (filter_input(INPUT_POST, 'menu_item_id')&&filter_input(INPUT_POST, 'process')=='menu-item-edit') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST, 'menu_item_id'), $outMatches))>0?(int)$outMatches[0]:0;
            //var_dump($inMcId); die();
            $inData = $this->Menu_model->loadById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            //$inData = $inData[0];
            $inRoot[] = $this->Menu_model->loadCategoryById($inData['menu_category_id'],array('status'=>1,'fields'=>array('mc_title as title','menu_category_id as value')));
            $inCategory = $this->Menu_model->load(array('menu_category_id'=>$inData['menu_category_id'],'status'=>1,'fields'=>array('mi_title_ru as title','mi_url','menu_id','owner_id','menu_id as value')));
            $inData['language_id'] = $this->Language_model->loadLanguage(array('status'=>1,'fields'=>array('lan_title as title','language_id as value')));
            if (!empty($inRoot) && !empty($inCategory)) {
                $inData['owner_id'] = array_merge($inRoot,$inCategory);
            }
            $inData['form'] = $this->Menu_model->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    //
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                $inDecode[$outData['name']]=$outData['value'];
            }
            $inDecode['owner_id'] = ($inDecode['owner_id']==$inDecode['menu_category_id'])?0:$inDecode['owner_id'];
            $inDecode['mi_status'] = ($inDecode['mi_status']=='on')?1:0;
            //$this->Menu_model->Debug();
            echo $this->Menu_model->save($inDecode);
        }
    }
    public function save_category() {   
        $inArg = func_get_args();
        
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                $inDecode[$outData['name']]=$outData['value'];
            }
            
            //$inDecode['owner_id'] = ($inDecode['owner_id']==$inDecode['menu_category_id'])?0:$inDecode['owner_id'];
            $inDecode['mc_status'] = ($inDecode['mc_status']=='on')?1:0;
            //var_dump($inDecode); die();
            //$this->Menu_model->Debug();
            $this->Menu_model->save_category($inDecode);
        }
    }
    public function change_weight() {
        if (filter_input(INPUT_POST, 'process')&&filter_input(INPUT_POST, 'menu_item_id')) {
            $inSplit = filter_input(INPUT_POST, 'process');
            $inPattern = "/(([A-Za-z]+)\-([0-9]+)$)/";
            $inProcAndId = preg_match($inPattern, filter_input(INPUT_POST, 'menu_item_id'),$outMatches)>0?$outMatches[0]:false;
            $inParse = preg_split("/\-/", $inProcAndId);
            $outGo = 'DO-'.mb_strtoupper($inParse[0]);
            $outId = (int)$inParse[1];
            $inItem = $this->Menu_model->load(array('menu_id'=>$outId,'fields'=>array('menu_id','mi_weight','owner_id','menu_category_id','mi_title_ru')),true,false);
            $inItem = ($inItem)?$inItem[0]:false;
            if ($inItem) {
                $inItems = $this->Menu_model->load(array('owner_id'=>$inItem['owner_id'],'menu_category_id'=>$inItem['menu_category_id'],'fields'=>array('menu_id','mi_weight','owner_id','menu_category_id','mi_title_ru')),true,false);
            }
            
            switch ($outGo) {
                case 'DO-INC' :
                    $inItem['mi_weight'] =  (int)$inItem['mi_weight']+1;
                    $inStep = 1;
                    //var_dump($inItem); die();
                    break;
                case 'DO-DEC' :
                    $inItem['mi_weight'] =  (int)$inItem['mi_weight']-1;
                    $inStep = -1;
                    //var_dump($inItem); die();
                    break;
            }
            foreach ($inItems as $inKey => $inWeight) {
               if($inWeight['menu_id']==$inItem['menu_id'])  {
                   $inWeight = $inItem;
               }
               if ($inWeight['mi_weight']==$inItem['mi_weight']&&$inWeight['menu_id']!=$inItem['menu_id']) {
                   $inWeight['mi_weight'] = (int)$inWeight['mi_weight']+$inStep;
               }
               $inItems[$inKey] = $inWeight;
               $this->Menu_model->save($inWeight);
            }
            //echo "<pre>"; var_dump($inItems); die();
        };
    }
    public function ChangeStatus() {
        //var_dump($_POST); die();
        if ((filter_input(INPUT_POST, 'process')=='menu-item-check')&&filter_input(INPUT_POST, 'menu_item_id')) {
            $inPattern = '/([0-9]+)$/';
            $inItemId = (preg_match($inPattern, filter_input(INPUT_POST, 'menu_item_id'), $outMatches))>0?(int)$outMatches[0]:0;
            //var_dump($inItemId); die();
            //$this->Menu_model->Debug();
            $outRes = $this->Menu_model->changeStatus($inItemId,'mi_status');
            echo $outRes;
        }
        if ((filter_input(INPUT_POST, 'process')=='menu-check')&&filter_input(INPUT_POST, 'menu_item_id')) {
            $inPattern = '/([0-9]+)$/';
            $inItemId = (preg_match($inPattern, filter_input(INPUT_POST, 'menu_item_id'), $outMatches))>0?(int)$outMatches[0]:0;
            //var_dump($inItemId); die();
            //$this->Menu_model->Debug();
            $outRes = $this->Menu_model->changeStatus($inItemId,'mc_status',true);
            echo $outRes;
        }
    }
}

