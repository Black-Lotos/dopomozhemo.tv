<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
        //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model('News_model');
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
    }
    //
    //
    public function index()
    {   
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Новости',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadNews();
        }
        echo $this->twig->render("{$inData['tabs_run']['property']['template']}", $inData);
    }
    public function load() {
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        $inData['output'] = $this->News_model->getOutput($inProces);
        //
        //echo "<pre>"; var_dump($inProces); die();
        $inData['data'] = $this->News_model->loadTree(
                array(
                    'category'=>array('fields'=>array('nc_status','nc_id as value', 'nc_title as title')),
                    'news'=>array('fields'=>array('news_status','news_status_main','news_id as value', 'news_title as title'))    
                )
        );
        //echo "<pre>"; var_dump($inData['data']); die();
        foreach ($inData['data'] as $outKey => $outData) {
            $outData['action'] =    "<a href='#' id='ref-сnews-edit-{$outData['value']}' class='action-base action-edit'></a>".
                                    "<a href='#' id='ref-сnews-delete-{$outData['value']}' class='action-base action-delete'></a>";
            $outName = "ref-сnews-status-{$outData['value']}";                        
            $outChecked = ($outData['nc_status']==1)?'checked':'un-checked';
            //$outData['status'] = "<input name='{$outName}' type='checkbox' id='{$outName}' $outChecked />";
            //
            $outData['action'] =    "<a href='#' id='ref-cnews-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                    "<a href='#' id='ref-cnews-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                    "<a href='#' id='ref-cnews-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>"        
                                    ;
            if (!empty($outData['sub_tree'])) {
                foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                    $outChecked = ($outSData['news_status']==1)?'checked':'un-checked';
                    $outMain = ($outSData['news_status_main']==1)?'checked':'un-checked';
                    $outSData['action'] =    "<a href='#' id='ref-news-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                            "<a href='#' id='ref-news-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                            "<a href='#' id='ref-news-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>".
                                            "<a href='#' id='ref-news-main-{$outSData['value']}' class='action-base action-main-{$outMain}' title='На главной(включить отключить)'></a>"                                                                    
                                            ;
                    $outData['sub_tree'][$outSKey] = $outSData;
                }
            }
            $inData['data'][$outKey] = $outData;
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inCategory = $this->News_model->loadCategory(array('nc_status'=>1,'fields'=>array('nc_title as title','nc_id as value')));
        $inData['form'] = $this->News_model->getForm(
                $inCategory,
                null
        );
        //var_dump($inData['form']); die();
        
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        $inCategory = $this->News_model->loadCategory(array('nc_status'=>1,'fields'=>array('nc_title as title','nc_id as value')));
        
        $inData['form'] = $this->News_model->getForm(
                $inCategory,
                null,
                array(),
                PROCESS_NEWS_SECTION
        );
        //var_dump($inData['form']); die();
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function edit() {
        if (!empty($_POST['news_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['news_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->News_model->loadById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inMenu = null;
            $inCategory = $this->News_model->loadCategory(array('nc_status'=>1,'fields'=>array('nc_title as title','nc_id as value')));
            $inData['form'] = $this->News_model->getForm(
                $inCategory,
                null,
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function delete() {
        if (!empty($_POST['news_id'])&&$_POST['process']=='delete-new') {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['news_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $this->News_model->delete($inMcId);
        }
    }
    public function edit_section() {
        if (!empty($_POST['nc_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['nc_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->News_model->loadCategoryById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inMenu = null;
            $inCategory = null;
            $inData['form'] = $this->News_model->getForm(
                $inCategory,
                null,
                $inData,
                PROCESS_NEWS_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    //
    public function save() {   
        
        if (!empty($_POST)) {
            $inDecode=$_POST; $outResultMove=true;
            if(!empty($inDecode['news_status'])&&$inDecode['news_status']=='on') {
                $inDecode['news_status']=1;
            }
            if(!empty($inDecode['news_status_top'])&&$inDecode['news_status_top']=='on') {
                $inDecode['news_status_top']=1;
            }
            if(!empty($inDecode['news_status_main'])&&$inDecode['news_status_main']=='on') {
                $inDecode['news_status_main']=1;
            }
            
            //$inDecode['images_id'] = 'uploads/empty.jpg';
            
            if ($_FILES['images_id']['size']>0) {
                $inFName = $_FILES['images_id']['tmp_name'];
                $inExt = pathinfo($_FILES['images_id']['name']); $inExt = $inExt['extension'];
                $inGenName = 'news_'.md5(time()).".$inExt";
                $outFName = 'uploads/'.$inGenName;
                $inDecode['images_id'] = $inGenName;
                $outResultMove = move_uploaded_file($inFName, $outFName);
                if($outResultMove) {
                    $writeImages = array(
                        'images_created'=>date('Y-m-d H:i:s',time()),
                        'images_folder'=>'uploads/',
                        'images_name'=>$inGenName,
                        'images_type'=>T_IMAGE);
                    $this->load->model('Images_model','images');
                    $inDecode['images_id'] = $this->images->save($writeImages);
                    $inDecode['images_id'] = $inDecode['images_id']['rec-no'];
                    //$inDecode['images_id'] = $inDecode['images_id']['rec_no'];
                } else {unset($inDecode['images_id']);}
                //die();
            }
            $this->News_model->save($inDecode);
            Goto_Page('/administration/section');
        }
    }
    //
    public function change_status() {
        if (!empty($_POST)) {
            //var_dump(filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)); die();
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'news_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->News_model->loadById($inId);
                    $inBlogs['news_status'] = ($inBlogs['news_status']==0)?1:0;
                    $this->News_model->save($inBlogs);
                    echo $inBlogs['news_status'];
                    break;
                case 'change-status-main':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'news_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->News_model->loadById($inId);
                    $inBlogs['news_status_main'] = ($inBlogs['news_status_main']==0)?1:0;
                    $this->News_model->save($inBlogs);
                    echo $inBlogs['news_status_main'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'nc_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    //echo $inId; die();
                    $inCBlogs = $this->News_model->loadCategoryById($inId);
                    $inCBlogs['nc_status'] = ($inCBlogs['nc_status']==0)?1:0;
                    $this->News_model->save_category($inCBlogs);
                    echo $inCBlogs['nc_status'];
                    break;
            }
        }
    }
    //
    public function save_category() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }
        }
        echo $this->News_model->save_category($inDecode);
    }
}

