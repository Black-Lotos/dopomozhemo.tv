<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_blogs extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    private $inOutRecord = 15;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
        //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model('Blogs_comments_model');
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
        $this->lang->load('form_items','ru');
    }
    //
    //
    public function index()
    {   
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Блоги',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        $inData['output'] = $this->Blogs_model->getOutput();
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadBlogs();
        }
        $inData['content']['right'] = $this->twig->render("{$inData['tabs_run']['property']['template']}", $inData);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function load() {
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        $inData['output'] = $this->Blogs_model->getOutput($inProces);
        //
        //echo "<pre>"; var_dump($inProces); die();
        $inData['data'] = $this->Blogs_model->loadTree(
                array(
                    'category'=>array('fields'=>array('cblogs_status','cblogs_id as value', 'cblogs_title as title')),
                    'blogs'=>array('fields'=>array('blogs_status','blogs_main','blogs_id as value', 'blogs_title as title'))    
                )
        );
        //echo "<pre>"; var_dump($inData['data']); die();
        foreach ($inData['data'] as $outKey => $outData) {
            
            $outName = "ref-cblogs-status-{$outData['value']}";                        
            $outChecked = ($outData['cblogs_status']==1)?'checked':'un-checked';
            //$outData['status'] = "<input name='{$outName}' type='checkbox' id='{$outName}' $outChecked />";
            //
            $outData['action'] =    "<a href='#' id='ref-cblogs-edit-{$outData['value']}' class='action-base action-edit'></a>";
                if (empty($outData['sub_tree']))
                    $outData['action'] .= "<a href='#' id='ref-cblogs-delete-{$outData['value']}' class='action-base action-delete'></a>";
                    $outData['action'] .= "<a href='#' id='ref-cblogs-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
            if (!empty($outData['sub_tree'])) {
                foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                    $outChecked = ($outSData['blogs_status']==1)?'checked':'un-checked';
                    $outMain = ($outSData['blogs_main']==1)?'checked':'un-checked';
                    $outSData['action'] =    "<a href='#' id='ref-blogs-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                            "<a href='#' id='ref-blogs-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                            "<a href='#' id='ref-blogs-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>".
                                            "<a href='#' id='ref-blogs-main-{$outSData['value']}' class='action-base action-main-{$outMain}' title='На главной(включить отключить)'></a>"                
                                            ;
                    $outData['sub_tree'][$outSKey] = $outSData;
                }
            }
            $inData['data'][$outKey] = $outData;
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        $inCategory = $this->Blogs_model->loadCategory(array('cblogs_status'=>1,'fields'=>array('cblogs_title as title','cblogs_id as value')));
        setLanguage($inData);
        $inData['form'] = $this->Blogs_model->getForm(
            $inData,
            PROCESS_BLOGS_SECTION_ADD
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inData['cblogs_id'] = $this->Blogs_model->loadCategory(array('cblogs_status'=>1,'fields'=>array('cblogs_title as title','cblogs_id as value')));
        setLanguage($inData);
        $inData['form'] = $this->Blogs_model->getForm(
                $inData
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        if (!empty($_POST['blogs_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['blogs_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Blogs_model->loadById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inActive = $inData['cblogs_id'];
            $inData['cblogs_id'] = $this->Blogs_model->loadCategory(array('cblogs_status'=>1,'fields'=>array('cblogs_title as title','cblogs_id as value')));
            $inData['cblogs_id'] = setCategoryActiveItem($inData['cblogs_id'], $inActive);
            //
            setLanguage($inData,true);
            $inData['form'] = $this->Blogs_model->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function delete() {
        if (!empty($_POST['blogs_id'])&&$_POST['process']=='delete-blogs') {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['blogs_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Blogs_model->DeleteById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
        }
    }
    public function delete_section() {
        if (!empty($_POST['cblogs_id'])&&$_POST['process']=='delete-cblogs') {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['cblogs_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Blogs_model->deleteCategoryById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
        }
    }
    public function edit_section() {
        //echo "<pre>"; var_dump(); die();
        if (filter_input(INPUT_POST, 'process')=='edit-cblogs' && filter_input(INPUT_POST, 'cblogs_id')) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['cblogs_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            
            $inData = $this->Blogs_model->loadCategoryById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            setLanguage($inData,true);
            $inData['form'] = $this->Blogs_model->getForm(
                $inData,
                PROCESS_BLOGS_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function change_status() {
        if (!empty($_POST)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['blogs_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->Blogs_model->loadById($inId);
                    $inBlogs['blogs_status'] = ($inBlogs['blogs_status']==0)?1:0;
                    $this->Blogs_model->save($inBlogs);
                    echo $inBlogs['blogs_status'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'cblogs_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inCBlogs = $this->Blogs_model->loadCategoryById($inId);
                    $inCBlogs['cblogs_status'] = ($inCBlogs['cblogs_status']==0)?1:0;
                    $this->Blogs_model->save_category($inCBlogs);
                    echo $inCBlogs['cblogs_status'];
                    break;
                case 'change-main':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['blogs_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->Blogs_model->loadById($inId);
                    $inBlogs['blogs_main'] = ($inBlogs['blogs_main']==0)?1:0;
                    $this->Blogs_model->save($inBlogs);
                    echo $inBlogs['blogs_main'];
                    break;
            }
        }
        
    }
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            if(!empty($inDecode['blogs_status'])&&$inDecode['blogs_status']=='on') {
                $inDecode['blogs_status']=1;
            }
            if(!empty($inDecode['blogs_top'])&&$inDecode['blogs_top']=='on') {
                $inDecode['blogs_top']=1;
            }
            if(!empty($inDecode['blogs_main'])&&$inDecode['blogs_main']=='on') {
                $inDecode['blogs_main']=1;
            }
            //var_dump($inDecode); die();
            /*if ($_FILES['images_id']['size']>0) {
                
                $inFName = $_FILES['images_id']['tmp_name'];
                $inExt = pathinfo($_FILES['images_id']['name']); $inExt = $inExt['extension'];
                $inGenName = 'blogs_'.md5(time()).".$inExt";
                $outFName = 'uploads/'.$inGenName;
                $inDecode['images_id'] = $inGenName;
                
                $outResultMove = move_uploaded_file($inFName, $outFName);
                if($outResultMove) {
                    $writeImages = array(
                        'images_created'=>date('Y-m-d H:i:s',time()),
                        'images_folder'=>'uploads/',
                        'images_name'=>$inGenName,
                        'images_type'=>T_IMAGE);
                    $this->load->model('Images_model','images');
                    $inDecode['images_id'] = $this->images->save($writeImages);
                    $inDecode['images_id'] = $inDecode['images_id']['rec-no'];
                    //$inDecode['images_id'] = $inDecode['images_id']['rec_no'];
                }
            } else {unset($inDecode['images_id']);}*/
            
            $this->Blogs_model->save($inDecode);
            //Goto_Page('/administration/section/admin_blogs');
        }
    }
    public function save_category() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }
        }
        echo $this->Blogs_model->save_category($inDecode);
    }
    //
    public function comments_load() {
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        
        $inData['output'] = $this->Blogs_comments_model->getOutput($inProces);
        
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Blogs_comments_model->loadCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'administration/section/admin_blogs';
        //
        
        $inData['data'] = $this->Blogs_comments_model->loadTree(array(
            'tree'=>array('fields'=>array("{$this->Blogs_comments_model->getCategoryId()} as value, {$this->Blogs_comments_model->getCategoryName()} as title, {$this->Blogs_comments_model->getCategoryStatus()}")),
            'item'=>array('fields'=>array("{$this->Blogs_comments_model->getSelfId()} as value, {$this->Blogs_comments_model->getSelfName()} as title, {$this->Blogs_comments_model->getStatus()}")),        
                    ),true,$this->inPage,$this->inOutRecord);
        
        foreach ($inData['data'] as $outKey => $outData) {
            
            $outChecked = ($outData['blogs_status']==1)?'checked':'un-checked';
            //$outData['status'] = "<input name='{$outName}' type='checkbox' id='{$outName}' $outChecked />";
            //
            $outData['action'] =    '';//"<a href='#' id='ref-news-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>";
                //if (empty($outData['sub_tree']))    
                    //$outData['action'] .= "<a href='#' id='ref-news-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>";
                    //$outData['action'] .= "<a href='#' id='ref-news-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";        
            if (!empty($outData['sub_tree'])) {
                foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                    $outChecked = ($outSData['comments_status']==1)?'checked':'un-checked';
                    //var_dump($outSData['news_status_main']); die();
                    $outSData['action'] =    "<a href='#' id='ref-newscomments-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                            "<a href='#' id='ref-comments-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                            "<a href='#' id='ref-newscomments-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>"
                                            ;
                    $outData['sub_tree'][$outSKey] = $outSData;
                }
            }
            $inData['data'][$outKey] = $outData;
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
}

