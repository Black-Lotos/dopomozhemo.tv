<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_asktocontact extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    private $inPage=1;
    private $inOutRecord = 26;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
        //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model("Ask_to_contact_model","act_model");
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
        $this->lang->load('form_items','ru');
    }
    //
    //
    public function index()
    {   
        //die('TYT');
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inData['output'] = $this->act_model->getOutput();
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadAsktocontact($this->inPage);
        }
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData['page']['active'] = $this->inPage;
        $inData = array('title'=>'Административная панель Блоги',
            'content'=>array(
                'left'=>'dasd',
                'right'=>$this->twig->render("{$inData['tabs_run']['property']['template']}", $inData)),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function load() {
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->act_model->loadCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'administration/section/admin_asktocontact';
        $inData['output'] = $this->act_model->getOutput($inProces);
        //
        //echo "<pre>"; var_dump($inProces); die();
        $inData['data'] = $this->act_model->load(array('fields'=>array('ask_to_contact_status','ask_to_contact_id as value', 'ask_to_contact_title as title')),false,$this->inPage,$this->inOutRecord);
        //echo "<pre>"; var_dump($inData['data']); die();
        if(!empty($inData['data'])) {
            foreach ($inData['data'] as $outKey => $outData) {
                $outChecked = ($outData['ask_to_contact_status']==1)?'checked':'un-checked';
                $outData['action'] =    "<a href='#' id='ref-ask_to_contact-edit-{$outData['value']}' class='action-base action-edit'></a>".
                                        "<a href='#' id='ref-ask_to_contact-delete-{$outData['value']}' class='action-base action-delete'></a>".
                                        "<a href='#' id='ref-ask_to_contact-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";        
                $inData['data'][$outKey] = $outData;
            }
        } else {
            $inData['data'][] = array('title'=>"Вопросов нет. Подайте вопрос.",
                'action'=> "<a href='#' id='ref-proposal-edit-0' class='action-base action-edit'></a>".
                "<a href='#' id='ref-proposal-delete-0' class='action-base action-delete'></a>".
                "<a href='#' id='ref-proposal-check-0' class='action-base action-checked' title='Активировать/Деактивировать'></a>");
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        $inCategory = $this->act_model->loadCategory(array('cask_to_contact_status'=>1,'fields'=>array('cask_to_contact_title as title','cask_to_contact_id as value')));
        $inData['form'] = $this->act_model->getForm(
                $inCategory,
                null,
                array(),
                PROCESS_BLOGS_SECTION
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inCategory = "";//$this->act_model->loadCategory(array('cask_to_contact_status'=>1,'fields'=>array('cask_to_contact_title as title','cask_to_contact_id as value')));
        $inData['form'] = $this->act_model->getForm(
                $inCategory,
                null
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        if (!empty($_POST['ask_to_contact_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['ask_to_contact_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->act_model->loadById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inMenu = null;
            $inCategory = ""; //$this->act_model->loadCategory(array('cask_to_contact_status'=>1,'fields'=>array('cask_to_contact_title as title','cask_to_contact_id as value')));
            $inData['form'] = $this->act_model->getForm(
                $inCategory,
                null,
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function edit_section() {
        if (!empty($_POST['cask_to_contact_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['cask_to_contact_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->act_model->loadCategoryById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inMenu = null;
            $inCategory = null;
            $inData['form'] = $this->act_model->getForm(
                $inCategory,
                null,
                $inData,
                PROCESS_BLOGS_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function change_status() {
        if (!empty($_POST)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['ask_to_contact_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inAsktocontact = $this->act_model->loadById($inId);
                    $inAsktocontact['ask_to_contact_status'] = ($inAsktocontact['ask_to_contact_status']==0)?1:0;
                    $this->act_model->save($inAsktocontact);
                    echo $inAsktocontact['ask_to_contact_status'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'cask_to_contact_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inCAsktocontact = $this->act_model->loadCategoryById($inId);
                    $inCAsktocontact['cask_to_contact_status'] = ($inCAsktocontact['cask_to_contact_status']==0)?1:0;
                    $this->act_model->save_category($inCAsktocontact);
                    echo $inCAsktocontact['cask_to_contact_status'];
                    break;
            }
        }
        
    }
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            /*foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }*/
            if(!empty($inDecode['ask_to_contact_status'])&&$inDecode['ask_to_contact_status']=='on') {
                $inDecode['ask_to_contact_status']=1;
            }
            if(!empty($inDecode['ask_to_contact_top'])&&$inDecode['ask_to_contact_top']=='on') {
                $inDecode['ask_to_contact_top']=1;
            }
            if(!empty($inDecode['ask_to_contact_main'])&&$inDecode['ask_to_contact_main']=='on') {
                $inDecode['ask_to_contact_main']=1;
            }
            //echo "<pre>"; var_dump($inDecode); die();
            $inDecode['user_id']=1;
            echo $this->act_model->save($inDecode);
        }
    }
    public function save_category() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }
        }
        echo $this->act_model->save_category($inDecode);
    }
    //
    public function delete() {
        if (filter_input(INPUT_POST,'ask_to_contact_id') && (filter_input(INPUT_POST,'process')=='delete-asktocontact')) {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,'ask_to_contact_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->act_model->DeleteById($inId);
        }
        /*if (!empty(filter_input(INPUT_POST,'ask_to_contact_id'))&&filter_input(INPUT_POST,'process')=='delete-asktocontact') {
            
            
            $inData = $this->act_model->DeleteById($inId);
        }*/
    }
}

