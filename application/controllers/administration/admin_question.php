<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_question extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    private $inPage=1;
    private $inOutRecord = 26;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
        //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model("Question_model");
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
        $this->lang->load('form_items','ru');
    }
    //
    //
    public function index(){   
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inData['output'] = $this->Question_model->getOutput();
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadQuestion($this->inPage);
        }
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Блоги',
            'content'=>array(
                'left'=>'',
                'right'=>$this->twig->render("{$inData['tabs_run']['property']['template']}", $inData)),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function load() {
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        $inData['output'] = $this->Question_model->getOutput($inProces);
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Question_model->loadCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'administration/section/admin_question';
        //echo "<pre>"; var_dump($inProces); die();
        $inData['data'] = $this->Question_model->load(array('fields'=>array('question_status','question_id as value', 'question_title as title')),false,$this->inPage,$this->inOutRecord);
        
        //echo "<pre>"; var_dump($inData['data']); die();
        if(!empty($inData['data'])) {
            foreach ($inData['data'] as $outKey => $outData) {
                $outChecked = ($outData['question_status']==1)?'checked':'un-checked';
                $outData['action'] =    "<a href='#' id='ref-question-edit-{$outData['value']}' class='action-base action-edit'></a>".
                                        "<a href='#' id='ref-question-delete-{$outData['value']}' class='action-base action-delete'></a>".
                                        "<a href='#' id='ref-question-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";        
                $inData['data'][$outKey] = $outData;
            }
        } else {
            $inData['data'][] = array('title'=>"Вопросов нет. Подайте вопрос.",
                'action'=> "<a href='#' id='ref-proposal-edit-0' class='action-base action-edit'></a>".
                "<a href='#' id='ref-proposal-delete-0' class='action-base action-delete'></a>".
                "<a href='#' id='ref-proposal-check-0' class='action-base action-checked' title='Активировать/Деактивировать'></a>");
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        $inCategory = $this->Question_model->loadCategory(array('cquestion_status'=>1,'fields'=>array('cquestion_title as title','cquestion_id as value')));
        setLanguage($inData);
        $inData['form'] = $this->Question_model->getForm(
                $inData,
                PROCESS_BLOGS_SECTION
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        setLanguage($inData);
        $inData['form'] = $this->Question_model->getForm(
                $inData
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        if (!empty($_POST['question_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['question_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Question_model->loadById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            setLanguage($inData,TRUE);
            $inData['form'] = $this->Question_model->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function edit_section() {
        if (!empty($_POST['cquestion_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['cquestion_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Question_model->loadCategoryById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            setLanguage($inData,TRUE);
            $inData['form'] = $this->Question_model->getForm(
                $inData,
                PROCESS_BLOGS_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function change_status() {
        if (!empty($_POST)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['question_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inQuestion = $this->Question_model->loadById($inId);
                    $inQuestion['question_status'] = ($inQuestion['question_status']==0)?1:0;
                    $this->Question_model->save($inQuestion);
                    echo $inQuestion['question_status'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'cquestion_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inCQuestion = $this->Question_model->loadCategoryById($inId);
                    $inCQuestion['cquestion_status'] = ($inCQuestion['cquestion_status']==0)?1:0;
                    $this->Question_model->save_category($inCQuestion);
                    echo $inCQuestion['cquestion_status'];
                    break;
            }
        }
        
    }
    public function delete() {
        if (filter_input(INPUT_POST,'question_id') && (filter_input(INPUT_POST,'process')=='delete-question')) {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,'question_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $this->Question_model->DeleteById($inId);
            Goto_Page('/administration/section/admin_question');
        }
    }
    
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            /*foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }*/
            if(!empty($inDecode['question_status'])&&$inDecode['question_status']=='on') {
                $inDecode['question_status']=1;
            }
            if(!empty($inDecode['question_top'])&&$inDecode['question_top']=='on') {
                $inDecode['question_top']=1;
            }
            if(!empty($inDecode['question_main'])&&$inDecode['question_main']=='on') {
                $inDecode['question_main']=1;
            }
            if(!empty($inDecode['question_resolved'])&&$inDecode['question_resolved']=='on') {
                $inDecode['question_resolved']=1;
            } else $inDecode['question_resolved']=0;
            //$inDecode['question_resolved'] = (!empty(trim($inDecode['question_text'])));
            //echo "<pre>"; var_dump($inDecode); die();
            $inDecode['user_id']=1;
            $this->Question_model->save($inDecode);
            Goto_Page('/administration/section/admin_question');
        }
    }
    public function save_category() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }
        }
        echo $this->Question_model->save_category($inDecode);
    }
}

