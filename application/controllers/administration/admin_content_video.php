<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_content_video extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    private $inPage=1;
    private $inOutRecord = 15;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
        //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
        
    }
    //
    public function clear_image() {
        if (filter_input(INPUT_POST, 'content_id') && filter_input(INPUT_POST, 'process')=='clear-image-content') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['content_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Content_video->loadById($inMcId);
            $inData['images_id'] = null;
            //var_dump(inData); die();
            $inRes = $this->Content_video->save($inData);
            print_r ($inRes);
        }
    }
    //
    public function index() {
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Новости',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        if (empty($inArg)) {
            $inData['output'] = $this->Content_video->getOutput();
            $inData['tabs_run'] = $this->Tabs_model->loadContentVideo($this->inPage);
        }
        $inData['content']['right'] = $this->twig->render("{$inData['tabs_run']['property']['template']}", $inData);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function load() {
        $inArg = func_get_args();
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inProces = empty($inArg)?null:$inArg[0];
        $inData['output'] = $this->Content_video->getOutput($inProces);
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Content_video->loadCountPage($this->inOutRecord);
        //echo "<pre>"; var_dump($inData); die();
        $inData['page']['link_run'] = 'administration/section/admin_content_video/';        
        
        $inData['data'] = $this->Content_video->loadTree(array(),false,$this->inPage,$this->inOutRecord);
        
        foreach ($inData['data'] as $outKey => $outData) {
            $outName = "ref-ccontent-status-{$outData['value']}";                        
            $outChecked = ($outData['ccontent_status']==1)?'checked':'un-checked';
            //$outData['status'] = "<input name='{$outName}' type='checkbox' id='{$outName}' $outChecked />";
            //
            $outData['action'] =    "<a href='#' id='ref-ccontent-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>";
                if(empty($outData['sub_tree']))
                    $outData['action'] .= "<a href='#' id='ref-ccontent-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>";
                $outData['action'] .= "<a href='#' id='ref-ccontent-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
            if (!empty($outData['sub_tree'])) {
                foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                    $outChecked = ($outSData['content_status']==1)?'checked':'un-checked';
                    $outMain = ($outSData['content_status_main']==1)?'checked':'un-checked';
                    $outSData['action'] =   "<a href='#' id='ref-content-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                            "<a href='#' id='ref-content-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                            "<a href='#' id='ref-content-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>".
                                            "<a href='#' id='ref-content-main-{$outSData['value']}' class='action-base action-main-{$outMain}' title='Отключить/Включить'></a>".
                                            "<a href='#' id='ref-content-video-{$outSData['value']}' class='action-base action-video' title='Код встраивания'></a>"        
                                            ;
                    $outData['sub_tree'][$outSKey] = $outSData;
                }
            }
            $inData['data'][$outKey] = $outData;
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inData['ccontent_id'] = $this->Content_video->loadCategory(array('ccontent_status'=>1,'fields'=>array('ccontent_title as title','ccontent_id as value')));
        
        //$inData['picture_id'] = $this->Picture_model->load(array('picture_status'=>1,'fields'=>array('picture_title as title','picture_id as value')));
        setLanguage($inData);
        
        $inData['form'] = $this->Content_video->getForm(
            $inData
        );
        //
        //var_dump($inData['form']); die('!!!!!!!!');
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        if (!empty($_POST['content_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['content_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Content_video->loadById($inMcId);
            $inActive = $inData['ccontent_id'];
            $inData['ccontent_id'] = $this->Content_video->loadCategory(array('ccontent_status'=>1,'fields'=>array('ccontent_title as title','ccontent_id as value')));
            $inData['ccontent_id'] = setCategoryActiveItem($inData['ccontent_id'], $inActive);
            /*$tmpLink = simplexml_load_string($inData['content_link']);
            $inData['content_link'] = $inData['content_code'] = (string)$tmpLink->iframe['src'];
            $inData['content_width'] = (int)$tmpLink->iframe['width'];
            $inData['content_height'] = (int)$tmpLink->iframe['height'];*/
            //echo "<pre>"; var_dump($tmpLink->iframe); die();
            /*$inActiveP = $inData['picture_id'];
            $inData['picture_id'] = $this->Picture_model->load(array('picture_status'=>1,'fields'=>array('picture_title as title','picture_id as value')));
            $inData['picture_id'] = setActiveItem($inData['picture_id'], $inActiveP);
             * 
             */
            //
            //var_dump($inData); die();
            setLanguage($inData,true);
            
            $inData['form'] = $this->Content_video->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    //
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        $inCategory = $this->Content_video->loadCategory(array('ccontent_status'=>1,'fields'=>array('ccontent_title as title','ccontent_id as value')));
        setLanguage($inData);
        $inData['form'] = $this->Content_video->getForm(
                $inData,
                PROCESS_CONTENT_SECTION
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function edit_section() {
        if (filter_input(INPUT_POST, 'ccontent_id') && filter_input(INPUT_POST, 'process')=='edit-category') {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST, 'ccontent_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Content_video->loadCategoryById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            setLanguage($inData,true);
            $inData['form'] = $this->Content_video->getForm(
                $inData,
                PROCESS_CONTENT_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function content_code() {
        $inProcess = filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS);
        if ($inProcess=='content-code') {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $_POST['content_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            $outObj = (object)$this->Content_video->loadById($inId);
            echo $outObj->content_code;
        }
    }
    public function change_status() {
        if (!empty($_POST)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['content_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->Content_video->loadById($inId);
                    $inBlogs['content_status'] = ($inBlogs['content_status']==0)?1:0;
                    $this->Content_video->save($inBlogs);
                    echo $inBlogs['content_status'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'ccontent_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inCBlogs = $this->Content_video->loadCategoryById($inId);
                    $inCBlogs['ccontent_status'] = ($inCBlogs['ccontent_status']==0)?1:0;
                    $this->Content_video->save_category($inCBlogs);
                    echo $inCBlogs['ccontent_status'];
                    break;
                case 'change-status-main':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['content_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->Content_video->loadById($inId);
                    $inBlogs['content_status_main'] = ($inBlogs['content_status_main']==0)?1:0;
                    $this->Content_video->save($inBlogs);
                    echo $inBlogs['content_status_main'];
                    break;
            }
        }
        
    }
    //
    public function delete() {
        if (!empty($_POST['content_id'])&&$_POST['Process']=='delete-content') {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['content_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Content_video->DeleteById($inMcId);
        }
    }
    //
    public function save() {   
        //var_dump($_POST,$_FILES); die();
        $inArg = func_get_args();
        if (!empty($_POST)) {
            //var_dump($_POST); die();
            $inDecode=$_POST;
            
            if(!empty($inDecode['content_status'])&&$inDecode['content_status']=='on') {
                $inDecode['content_status']=1;
            }
            if(!empty($inDecode['content_status_top'])&&$inDecode['content_status_top']=='on') {
                $inDecode['content_status_top']=1;
            }
            if(!empty($inDecode['content_status_main'])&&$inDecode['content_status_main']=='on') {
                $inDecode['content_status_main']=1;
            }
            //echo "<pre>"; var_dump($_FILES['content_file']); die();
            //$inDecode['content_file'] = 'video/empty.mp4';
            
            if (!empty($_FILES['content_file']['tmp_name'])) {
                $inFName = $_FILES['content_file']['tmp_name'];
                $inExt = pathinfo($_FILES['content_file']['name']); $inExt = $inExt['extension'];
                $inGenName = 'video_'.md5(time()).".$inExt";
                $outFName = 'video/'.$inGenName;
                $inDecode['content_file'] = $inGenName;
                move_uploaded_file($inFName, $outFName);
            }
            //echo "<pre>"; var_dump($inDecode); die();
            $inDecode['images_id'] = (trim($inDecode['images_id'])=='')?null:$inDecode['images_id'];
            
            $this->Content_video->save($inDecode);
        }
        Goto_Page('/administration/section/admin_content_video');
    }
    public function save_category() {   
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }
        }
        //$this->Content_video->Debug();
        echo $this->Content_video->save_category($inDecode);
    }
}

