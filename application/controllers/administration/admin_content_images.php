<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_picture extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
        //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model("Content_picture","act_model");
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
    }
    //
    public function index()
    {   
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Новости',
            'picture'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        if (empty($inArg)) {
            $inData['output'] = $this->act_model->getOutput();
            $inData['tabs_run'] = $this->Tabs_model->loadContentPicture();
        }
        echo $this->twig->render("{$inData['tabs_run']['property']['template']}", $inData);
    }
    public function load() {
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        $inData['output'] = $this->act_model->getOutput($inProces);
        //
        //echo "<pre>"; var_dump($inData); die();
        $inData['data'] = $this->act_model->loadTree(
                array(
                    'category'=>array('fields'=>array('cpicture_status','cpicture_id as value', 'cpicture_title as title')),
                    'picture'=>array('fields'=>array('picture_status','picture_status_main','picture_id as value', 'picture_title as title'))    
                )
        );
        //echo "<pre>"; var_dump($inData['data']); die();
        foreach ($inData['data'] as $outKey => $outData) {
            $outData['action'] =    "<a href='#' id='ref-cpicture-edit-{$outData['value']}' class='action-base action-edit'></a>".
                                    "<a href='#' id='ref-cpicture-delete-{$outData['value']}' class='action-base action-delete'></a>";
            $outName = "ref-cpicture-status-{$outData['value']}";                        
            $outChecked = ($outData['cpicture_status']==1)?'checked':'un-checked';
            //$outData['status'] = "<input name='{$outName}' type='checkbox' id='{$outName}' $outChecked />";
            //
            $outData['action'] =    "<a href='#' id='ref-cpicture-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                    "<a href='#' id='ref-cpicture-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                    "<a href='#' id='ref-cpicture-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>"        
                                    ;
            if (!empty($outData['sub_tree'])) {
                foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                    $outChecked = ($outSData['picture_status']==1)?'checked':'un-checked';
                    $outMain = ($outSData['picture_status_main']==1)?'checked':'un-checked';
                    $outSData['action'] =   "<a href='#' id='ref-picture-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                            "<a href='#' id='ref-picture-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                            "<a href='#' id='ref-picture-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>".
                                            "<a href='#' id='ref-picture-main-{$outSData['value']}' class='action-base action-main-{$outMain}' title='Отключить/Включить'></a>".
                                            "<a href='#' id='ref-picture-video-{$outSData['value']}' class='action-base action-video' title='Код встраивания'></a>"        
                                            ;
                    $outData['sub_tree'][$outSKey] = $outSData;
                }
            }
            $inData['data'][$outKey] = $outData;
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        $inCategory = $this->act_model->loadCategory(array('cpicture_status'=>1,'fields'=>array('cpicture_title as title','cpicture_id as value')));
        $inData['form'] = $this->act_model->getForm(
                $inCategory,
                null,
                array(),
                PROCESS_CONTENT_SECTION
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inCategory = $this->act_model->loadCategory(array('cpicture_status'=>1,'fields'=>array('cpicture_title as title','cpicture_id as value')));
        $inData['form'] = $this->act_model->getForm(
                $inCategory,
                null
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        if (!empty($_POST['picture_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['picture_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->act_model->loadById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inMenu = null;
            $inCategory = $this->act_model->loadCategory(array('cpicture_status'=>1,'fields'=>array('cpicture_title as title','cpicture_id as value')));
            $inData['form'] = $this->act_model->getForm(
                $inCategory,
                null,
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function edit_section() {
        if (!empty($_POST['cpicture_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['cpicture_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->act_model->loadCategoryById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inMenu = null;
            $inCategory = null;
            $inData['form'] = $this->act_model->getForm(
                $inCategory,
                null,
                $inData,
                PROCESS_CONTENT_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function picture_code() {
        $inProcess = filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS);
        if ($inProcess=='picture-code') {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $_POST['picture_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            $outObj = (object)$this->act_model->loadById($inId);
            echo $outObj->picture_code;
        }
    }
    public function change_status() {
        if (!empty($_POST)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['picture_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->act_model->loadById($inId);
                    $inBlogs['picture_status'] = ($inBlogs['picture_status']==0)?1:0;
                    $this->act_model->save($inBlogs);
                    echo $inBlogs['picture_status'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'cpicture_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inCBlogs = $this->act_model->loadCategoryById($inId);
                    $inCBlogs['cpicture_status'] = ($inCBlogs['cpicture_status']==0)?1:0;
                    $this->act_model->save_category($inCBlogs);
                    echo $inCBlogs['cpicture_status'];
                    break;
                case 'change-status-main':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['picture_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->act_model->loadById($inId);
                    $inBlogs['picture_status_main'] = ($inBlogs['picture_status_main']==0)?1:0;
                    $this->act_model->save($inBlogs);
                    echo $inBlogs['picture_status_main'];
                    break;
            }
        }
        
    }
    public function save() {   
        //var_dump($_POST,$_FILES); die();
        $inArg = func_get_args();
        if (!empty($_POST)) {
            //var_dump($_POST); die();
            $inDecode=$_POST;
            
            if(!empty($inDecode['picture_status'])&&$inDecode['picture_status']=='on') {
                $inDecode['picture_status']=1;
            }
            if(!empty($inDecode['picture_status_top'])&&$inDecode['picture_status_top']=='on') {
                $inDecode['picture_status_top']=1;
            }
            if(!empty($inDecode['picture_status_main'])&&$inDecode['picture_status_main']=='on') {
                $inDecode['picture_status_main']=1;
            }
            //echo "<pre>"; var_dump($inDecode); die();
            $inDecode['picture_file'] = 'video/empty.mp4';
            if (!empty($_FILES)) {
                $inFName = $_FILES['picture_file']['tmp_name'];
                $inExt = pathinfo($_FILES['picture_file']['name']); $inExt = $inExt['extension'];
                $inGenName = 'video_'.md5(time()).".$inExt";
                $outFName = 'video/'.$inGenName;
                $inDecode['picture_file'] = $inGenName;
                move_uploaded_file($inFName, $outFName);
            }
            //echo "<pre>"; var_dump($inDecode); die();
            $this->act_model->save($inDecode);
        }
        Goto_Page('/administration/section');
    }
    public function save_category() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }
        }
        echo $this->act_model->save_category($inDecode);
    }
}

