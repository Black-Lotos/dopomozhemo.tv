<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Section extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    private $inOutRecord = 15;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
        //
     private function startUp() {
        $this->Menu_model->setRecursive(true);
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        if (!$this->Users_model->isLogin()) {
            CI_goto("/administration/users/login");
        }
        if ($this->Users_model->getCheckRoles($this->inUser,array('Администратор','Главный администратор'))) {
            $this->inPermition = $this->inUser->user_login;
        }
        else {
            $inData['message'] = getMessage('error',true);
            echo $this->twig->render("administration/administration_empty.twig", $inData);
        }
    }
    //
    //
    public function index()
    {   
        Goto_Page('/administration/section/admin_news/');
    }
    //
    public function admin_expert_help() {
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $this->load->model('Expert_help_model');
        $inData['output'] = $this->Expert_help_model->getOutput();
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadExpertHelp($inPage);
        }
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        //'left'=>$this->Menu_model->loadParentMenuByLink(array('name'=>'admin_menu','mi_url'=>'/'.$this->uri->uri_string())),
        $inData = array('title'=>'Административная панель Ссылки',
            'content'=>array(
                'left'=>'',
                'right'=>$this->twig->render("{$inData['tabs_run']['property']['template']}", $inData)),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
                //echo "<pre>"; var_dump($inData['content']['left']);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    //
    public function admin_news() {
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Новости',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadNews($inPage);
        }
        //var_dump($inData['tabs_run']['property']['template']);
        $inData['content']['right'] = $this->twig->render("{$inData['tabs_run']['property']['template']}", $inData);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function admin_blogs() {
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Блоги',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        $inData['output'] = $this->Blogs_model->getOutput();
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadBlogs($inPage);
        }
        $inData['content']['right'] = $this->twig->render("{$inData['tabs_run']['property']['template']}", $inData);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function admin_content_video() {
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Новости',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        if (empty($inArg)) {
            $inData['output'] = $this->Content_video->getOutput();
            $inData['tabs_run'] = $this->Tabs_model->loadContentVideo($inPage);
        }
        $inData['content']['right'] = $this->twig->render("{$inData['tabs_run']['property']['template']}", $inData);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function admin_picture() {
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $this->load->model("Picture_model");
        if (empty($inArg)) {
            $inData['output'] = $this->Picture_model->getOutput();
            $inData['tabs_run'] = $this->Tabs_model->loadContentPicture();
        }
        
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        //echo "<pre>"; var_dump($inData); die();
        $inData = array('title'=>'Административная панель Новости',
                'content'=>array(
                    'left'=>'',
                    'right'=>$this->twig->render($inData['tabs_run']['property']['template'], $inData)),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function admin_proposal() {
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $this->load->model("Proposal_model");
         $inData['output'] = $this->Proposal_model->getOutput();
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadProposal();
        }
        
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Блоги',
            'content'=>array(
                'left'=>'',
                'right'=>$this->twig->render("{$inData['tabs_run']['property']['template']}", $inData)),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function admin_question() {
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $this->load->model("Question_model");
        $inData['output'] = $this->Question_model->getOutput();
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadQuestion();
        }
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Блоги',
            'content'=>array(
                'left'=>'',
                'right'=>$this->twig->render("{$inData['tabs_run']['property']['template']}", $inData)),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function admin_asktocontact() {
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $this->load->model("ask_to_contact_model");
        $inData['output'] = $this->ask_to_contact_model->getOutput();
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadAsktocontact($inPage);
        }
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Блоги',
            'content'=>array(
                'left'=>'',
                'right'=>$this->twig->render("{$inData['tabs_run']['property']['template']}", $inData)),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    //
    public function admin_static() {
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $this->load->model("Static_model");
        $inData['output'] = $this->Static_model->getOutput();
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadStaticPage($inPage);
        }
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Статические страницы',
            'content'=>array(
                'left'=>'',
                'right'=>$this->twig->render("{$inData['tabs_run']['property']['template']}", $inData)),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
}

