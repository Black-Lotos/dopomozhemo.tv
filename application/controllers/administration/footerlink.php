<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Footerlink extends CI_Controller {    
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output){
        echo $output;  
    }
        //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model('Footerlink_model');
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
    }
    //
    public function index() {   
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Блоги',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        
        $inData['output'] = $this->Footerlink_model->getOutput();
        
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadFooterLink();
        }
        //echo "<pre>"; var_dump($inData['tabs_run']);
        echo $this->twig->render("{$inData['tabs_run']['property']['template']}", $inData);
    }
    //
    public function load() {
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        $inData['output'] = $this->Footerlink_model->getOutput($inProces);
        //
        //echo "<pre>"; var_dump($inProces); die();
        $inData['data'] = $this->Footerlink_model->loadTree(
                array(
                    'category'=>array('fields'=>array('cfooterlink_status','cfooterlink_id as value', 'cfooterlink_title as title')),
                    'footerlink'=>array('fields'=>array('footerlink_status','footerlink_id as value', 'footerlink_title as title'))    
                )
        );
        //echo "<pre>====================>"; var_dump($inData['data']); die();
        foreach ($inData['data'] as $outKey => $outData) {
            $outData['action'] =    "<a href='#' id='ref-сfooterlink-edit-{$outData['value']}' class='action-base action-edit'></a>".
                                    "<a href='#' id='ref-сfooterlink-delete-{$outData['value']}' class='action-base action-delete'></a>";
            $outName = "ref-сfooterlink-status-{$outData['value']}";                        
            $outChecked = ($outData['cfooterlink_status']==1)?'checked':'un-checked';
            //$outData['status'] = "<input name='{$outName}' type='checkbox' id='{$outName}' $outChecked />";
            //
            $outData['action'] =    "<a href='#' id='ref-сfooterlink-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                    "<a href='#' id='ref-сfooterlink-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                    "<a href='#' id='ref-сfooterlink-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>"        
                                    ;
            if (!empty($outData['sub_tree'])) {
                foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                    $outChecked = ($outSData['footerlink_status']==1)?'checked':'un-checked';
                    //$outMain = ($outSData['footerlink_main']==1)?'checked':'un-checked';
                    $outSData['action'] =    "<a href='#' id='ref-footerlink-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                            "<a href='#' id='ref-footerlink-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                            "<a href='#' id='ref-footerlink-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>"
                    //                        "<a href='#' id='ref-footerlink-main-{$outSData['value']}' class='action-base action-main-{$outMain}' title='На главной(включить отключить)'></a>"                
                                            ;
                    $outData['sub_tree'][$outSKey] = $outSData;
                }
            }
            $inData['data'][$outKey] = $outData;
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        $inCategory = $this->Footerlink_model->loadCategory(array('cfooterlink_status'=>1,'fields'=>array('cfooterlink_title as title','cfooterlink_id as value')));
        $inData['form'] = $this->Footerlink_model->getForm(
                $inCategory,
                null,
                array(),
                PROCESS_FOOTERLINK_SECTION
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inCategory = $this->Footerlink_model->loadCategory(array('cfooterlink_status'=>1,'fields'=>array('cfooterlink_title as title','cfooterlink_id as value')));
        $inData['form'] = $this->Footerlink_model->getForm(
                $inCategory,
                null
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        if (!empty($_POST['footerlink_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['footerlink_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Footerlink_model->loadById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inMenu = null;
            $inCategory = $this->Footerlink_model->loadCategory(array('cfooterlink_status'=>1,'fields'=>array('cfooterlink_title as title','cfooterlink_id as value')));
            $inData['form'] = $this->Footerlink_model->getForm(
                $inCategory,
                null,
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function edit_section() {
        if (!empty($_POST['cfooterlink_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['cfooterlink_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Footerlink_model->loadCategoryById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inMenu = null;
            $inCategory = null;
            $inData['form'] = $this->Footerlink_model->getForm(
                $inCategory,
                null,
                $inData,
                PROCESS_FOOTERLINK_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function change_status() {
        if (!empty($_POST)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['footerlink_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inFooterlink = $this->Footerlink_model->loadById($inId);
                    $inFooterlink['footerlink_status'] = ($inFooterlink['footerlink_status']==0)?1:0;
                    $this->Footerlink_model->save($inFooterlink);
                    echo $inFooterlink['footerlink_status'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'cfooterlink_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inCFooterlink = $this->Footerlink_model->loadCategoryById($inId);
                    $inCFooterlink['cfooterlink_status'] = ($inCFooterlink['cfooterlink_status']==0)?1:0;
                    $this->Footerlink_model->save_category($inCFooterlink);
                    echo $inCFooterlink['cfooterlink_status'];
                    break;
                case 'change-main':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['footerlink_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inFooterlink = $this->Footerlink_model->loadById($inId);
                    $inFooterlink['footerlink_main'] = ($inFooterlink['footerlink_main']==0)?1:0;
                    $this->Footerlink_model->save($inFooterlink);
                    echo $inFooterlink['footerlink_main'];
                    break;
            }
        }
        
    }
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            if(!empty($inDecode['footerlink_status'])&&$inDecode['footerlink_status']=='on') {
                $inDecode['footerlink_status']=1;
            }
            if(!empty($inDecode['footerlink_top'])&&$inDecode['footerlink_top']=='on') {
                $inDecode['footerlink_top']=1;
            }
            if(!empty($inDecode['footerlink_main'])&&$inDecode['footerlink_main']=='on') {
                $inDecode['footerlink_main']=1;
            }
            
            /*if ($_FILES['images_id']['size']>0) {
                
                $inFName = $_FILES['images_id']['tmp_name'];
                $inExt = pathinfo($_FILES['images_id']['name']); $inExt = $inExt['extension'];
                $inGenName = 'footerlink_'.md5(time()).".$inExt";
                $outFName = 'uploads/'.$inGenName;
                $inDecode['images_id'] = $inGenName;
                
                $outResultMove = move_uploaded_file($inFName, $outFName);
                if($outResultMove) {
                    $writeImages = array(
                        'images_created'=>date('Y-m-d H:i:s',time()),
                        'images_folder'=>'uploads/',
                        'images_name'=>$inGenName,
                        'images_type'=>T_IMAGE);
                    $this->load->model('Images_model','images');
                    $inDecode['images_id'] = $this->images->save($writeImages);
                    $inDecode['images_id'] = $inDecode['images_id']['rec-no'];
                    //$inDecode['images_id'] = $inDecode['images_id']['rec_no'];
                }
            } else {unset($inDecode['images_id']);}*/
            
            $this->Footerlink_model->save($inDecode);
            //Goto_Page('/administration/section');
        }
    }
    public function save_category() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }
        }
        echo $this->Footerlink_model->save_category($inDecode);
    }
    public function loadForSite() {
        //echo "test";
        $inData['output'] = $this->Footerlink_model->getOutput();
        $inData['data'] = $this->Footerlink_model->load();
        $outResult = $inData;
    }
}
