<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_slider extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
        //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model("Slider_model","act_model");
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
    }
    //
    //
    public function index() {   
        if (empty($inArg)) {
            $inData['output'] = $this->act_model->getOutput();
            $inData['tabs_run'] = $this->Tabs_model->loadSlider();
        }
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Новости',
            'content'=>array(
                'left'=>'',
                'right'=>$this->twig->render("{$inData['tabs_run']['property']['template']}", $inData)),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function load() {
        $inData['output'] = $this->act_model->getOutput();
        //echo "<pre>"; var_dump($inData); die();
        //
        $inData['data'] = $this->act_model->load();
        //
        $inPrefix = $this->act_model->getPrefix();
        if ($inData['data']) {
            foreach ($inData['data'] as $outKey => $outData) {
                $outKey = $this->act_model->getPrefix().'id';
                $outData['action'] =    "<a href='#' id='ref-{$inPrefix}edit-{$outData[$outKey]}' class='action-base action-edit'></a>".
                                        "<a href='#' id='ref-{$inPrefix}delete-{$outData[$outKey]}' class='action-base action-delete'></a>";
                $outName = "ref-{$inPrefix}status-{$outData[$outKey]}";                        
                $outChecked = ($outData["{$inPrefix}status"]==1)?"checked='checked'":'';
                $outData["{$inPrefix}status"] = "<input name='{$outName}' type='checkbox' id='{$outName}' $outChecked />";
                $inData['data'][$outKey] = $outData;
            }
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-img.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inData['form'] = $this->act_model->getForm(
            null
        );
        //var_dump($inData['form']);
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        if (!empty($_POST["{$this->act_model->getPrefix()}id"])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST["{$this->act_model->getPrefix()}id"], $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->act_model->loadById($inMcId);
            $inMenu = null;
            $inData['form'] = $this->act_model->getForm(
                $inData    
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value']))
                    $inDecode[$outData['name']]=$outData['value'];
            }
            if(!empty($inDecode["{$this->act_model->getPrefix()}status"])&&$inDecode["{$this->act_model->getPrefix()}status"]=='on') {
                $inDecode["{$this->act_model->getPrefix()}status"]=1;
            }
            if(!empty($inDecode["{$this->act_model->getPrefix()}top"])&&$inDecode["{$this->act_model->getPrefix()}top"]=='on') {
                $inDecode["{$this->act_model->getPrefix()}top"]=1;
            }
            if(!empty($inDecode["{$this->act_model->getPrefix()}main"])&&$inDecode["{$this->act_model->getPrefix()}main"]=='on') {
                $inDecode["{$this->act_model->getPrefix()}main"]=1;
            }
            //echo "<pre>"; var_dump($inDecode); die();
            echo $this->act_model->save($inDecode);
        }
    }
}

