<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        if (!$this->Users_model->isLogin()) {
            CI_goto("/administration/users/login");
        }
        if ($this->Users_model->getCheckRoles($this->inUser,array('Администратор','Главный администратор'))) {
            $this->inPermition = $this->inUser->user_login;
        }
        else {
            $inData['message'] = getMessage('error',true);
            echo $this->twig->render("administration/administration_empty.twig", $inData);
        }
    }
    //
    public function index()
    {   
        Goto_Page('/administration/settings/users/');
    }
    public function users() {
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Пользователи',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        if (empty($inArg)) {
            $inData['output'] = $this->Users_model->getOutput();
            $inData['tabs_run'] = $this->Tabs_model->loadUsers($inPage);
        }
        $inData['content']['right'] = $this->twig->render("{$inData['tabs_run']['property']['template']}", $inData);
        $inData['content']['page_message']['success'] = getMessage('status',true,'status');
        $inData['content']['page_message']['error'] = getMessage('error',true,'error');
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function admin_settings() {
        //echo "gfgfgfg";
        $inData = array();
        //var_dump($inData); die("YYYYYY");
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель «Настройки сайта»',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        
        if (empty($inArg)) {
            $inData['output'] = $this->Site_model->getOutput();
            $inData['tabs_run'] = $this->Tabs_model->loadSettingsSite($inPage);
        }
        //var_dump($inData['output']); die();
        $inData['content']['right'] = $this->twig->render("{$inData['tabs_run']['property']['template']}", $inData);
        $inData['content']['page_message']['success'] = getMessage('status',true,'status');
        $inData['content']['page_message']['error'] = getMessage('error',true,'error');
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function admin_roles() {
        $inData = array();
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель «Роли пользователя»',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        if (empty($inArg)) {
            $inData['output'] = $this->Users_model->getOutput(array(),PROCESS_ROLES);
            $inData['tabs_run'] = $this->Tabs_model->loadUsersRoles($inPage);
        }
        $inData['content']['right'] = $this->twig->render("{$inData['tabs_run']['property']['template']}", $inData);
        $inData['content']['page_message']['success'] = getMessage('status',true,'status');
        $inData['content']['page_message']['error'] = getMessage('error',true,'error');
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
}

