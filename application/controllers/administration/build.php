<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Build extends CI_Controller {
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/administration/login");
        }
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
    public function index()
    {   
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Строение',
            'content'=>array('left'=>'Левый контент','right'=>'Правый контент'),
            'menu'=>$inMenu);
        //$this->load->view('administration_start', $data);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
}

