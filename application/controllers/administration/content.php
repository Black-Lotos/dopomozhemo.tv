<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        //print_r($inArg); die();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
    private function startUp() {
        //__init();
        $this->Menu_model->setRecursive(true);
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        //echo "<pre>"; var_dump($this->inUriString);die();
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        //echo "<pre>"; var_dump($this->Users_model->isLogin(),"ggggg"); die("gggg");
        if (!$this->Users_model->isLogin()) {
            CI_goto("/administration/users/login");
        }
        if ($this->Users_model->getCheckRoles($this->inUser,array('Администратор','Главный администратор'))) {
            $this->inPermition = $this->inUser->user_login;
        }
        else {
            $inData['message'] = getMessage('error',true);
            echo $this->twig->render("administration/administration_empty.twig", $inData);
            die();
        }
    }
    //
    public function index() {   //var_dump("/{$this->inUriString}administration/login"); die();
        Goto_Page("/administration/content/admin_menu");
    }
    public function admin_footerlink() {
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $this->load->model('Footerlink_model');
        $inData['output'] = $this->Footerlink_model->getOutput();
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadFooterLink($inPage);
        }
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        //'left'=>$this->Menu_model->loadParentMenuByLink(array('name'=>'admin_menu','mi_url'=>'/'.$this->uri->uri_string())),
        $inData = array('title'=>'Административная панель Ссылки',
            'content'=>array(
                'left'=>'',
                'right'=>$this->twig->render("{$inData['tabs_run']['property']['template']}", $inData)),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
                //echo "<pre>"; var_dump($inData['content']['left']);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function admin_language() {
        $inPage = isset($_GET['page'])?$_GET['page']:1;
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Языки',
            'content'=>array('left'=>'','right'=>''),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadLanguages($inPage);
        }
        $inData['content']['right'] = $this->twig->render($inData['tabs_run']['property']['template'], $inData);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function admin_menu() {
        $inData = array();
        $inArg = func_get_args();
        $inPage = (filter_input(INPUT_GET,'page'))?filter_input(INPUT_GET,'page'):1;
        $inSubTree = (filter_input(INPUT_GET,'select_menu'))?filter_input(INPUT_GET,'select_menu'):null;
        //var_dump($inPage,$inSubTree); die();
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        //echo "<pre>"; var_dump($inMenu); die();
        $inData = array('title'=>'Административная панель Меню',
            'content'=>array('left'=>'','right'=>'dsfsf'),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        if (empty($inSubTree)) {
            $inData['tabs_run'] = $this->Tabs_model->loadMenu($inPage);
        } else {
            $inData['tabs_run'] = $this->Tabs_model->loadMenuItem($inPage,$inSubTree);
        }
        //var_dump($inMenu);
        //echo ($inData['tabs_run']['property']['template']);
        $inData['content']['right'] = $this->twig->render($inData['tabs_run']['property']['template'], $inData);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
}

