<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_proposal extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    private $inPage=1;
    private $inOutRecord = 26;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
        //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model("Proposal_model");
        if (!$this->Users_model->isLogin()) {
            Goto_Page("/administration/users/login");
        }
        $this->lang->load('form_items','ru');
        
    }
    //
    //
    public function index()
    {   $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inData['output'] = $this->Proposal_model->getOutput();
        if (empty($inArg)) {
            $inData['tabs_run'] = $this->Tabs_model->loadProposal($this->inPage);
        }
        
        $inMenu = $this->Menu_model->load(array('name'=>'admin_menu'));
        $inData = array('title'=>'Административная панель Блоги',
            'content'=>array(
                'left'=>'',
                'right'=>$this->twig->render("{$inData['tabs_run']['property']['template']}", $inData)),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //$this->load->view('administration_start', $data);
        echo $this->twig->render('administration/administration_master.twig', $inData);
    }
    public function load() {
        
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        $inData['output'] = $this->Proposal_model->getOutput($inProces);
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Proposal_model->loadCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'administration/section/admin_proposal';
        //echo "<pre>"; var_dump($inProces); die();
        $inData['data'] = $this->Proposal_model->load(array('fields'=>array('proposal_status','proposal_id as value', 'proposal_title as title')),false,$this->inPage,$this->inOutRecord);
        
        //echo "<pre>"; var_dump($inData['data']); die();
        if(!empty($inData['data'])) {
            foreach ($inData['data'] as $outKey => $outData) {
                $outChecked = ($outData['proposal_status']==1)?'checked':'un-checked';
                $outData['action'] =    "<a href='#' id='ref-proposal-edit-{$outData['value']}' class='action-base action-edit'></a>".
                                        "<a href='#' id='ref-proposal-delete-{$outData['value']}' class='action-base action-delete'></a>".
                                        "<a href='#' id='ref-proposal-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";        
                $inData['data'][$outKey] = $outData;
            }
        } else {
            $inData['data'][] = array('title'=>"Заявок нет. Подайте заявку.",
                'action'=> "<a href='#' id='ref-proposal-edit-0' class='action-base action-edit'></a>".
                "<a href='#' id='ref-proposal-delete-0' class='action-base action-delete'></a>".
                "<a href='#' id='ref-proposal-check-0' class='action-base action-checked' title='Активировать/Деактивировать'></a>");        
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        setLanguage($inData);
        $inData['form'] = $this->Proposal_model->getForm(
                $inData,
                PROCESS_BLOGS_SECTION
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inData['language_id'] = 1;
        setLanguage($inData);
        $inData['form'] = $this->Proposal_model->getForm(
                $inData
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        if (!empty($_POST['proposal_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['proposal_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Proposal_model->loadById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inData['language_id'] = 1;
            setLanguage($inData,TRUE);
            $inData['form'] = $this->Proposal_model->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function edit_section() {
        if (!empty($_POST['cproposal_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['cproposal_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Proposal_model->loadCategoryById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            setLanguage($inData,TRUE);
            $inData['form'] = $this->Proposal_model->getForm(
                $inData,
                PROCESS_BLOGS_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function change_status() {
        if (!empty($_POST)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['proposal_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inProposal = $this->Proposal_model->loadById($inId);
                    $inProposal['proposal_status'] = ($inProposal['proposal_status']==0)?1:0;
                    $this->Proposal_model->save($inProposal);
                    echo $inProposal['proposal_status'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'cproposal_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inCProposal = $this->Proposal_model->loadCategoryById($inId);
                    $inCProposal['cproposal_status'] = ($inCProposal['cproposal_status']==0)?1:0;
                    $this->Proposal_model->save_category($inCProposal);
                    echo $inCProposal['cproposal_status'];
                    break;
            }
        }
        
    }
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            /*foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }*/
            if(!empty($inDecode['proposal_status'])&&$inDecode['proposal_status']=='on') {
                $inDecode['proposal_status']=1;
            }
            if(!empty($inDecode['proposal_top'])&&$inDecode['proposal_top']=='on') {
                $inDecode['proposal_top']=1;
            }
            if(!empty($inDecode['proposal_main'])&&$inDecode['proposal_main']=='on') {
                $inDecode['proposal_main']=1;
            }
            //echo "<pre>"; var_dump($inDecode); die();
            $inDecode['user_id']=1;
            $this->Proposal_model->save($inDecode);
            Goto_Page("/administration/section/admin_proposal");
        }
    }
    public function save_category() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value'])) {
                    $inDecode[$outData['name']]=$outData['value'];
                }    
            }
        }
        echo $this->Proposal_model->save_category($inDecode);
    }
    //
    public function delete() {
        if ((filter_input(INPUT_POST, 'proposal_id')) && filter_input(INPUT_POST, 'process')=='delete-proposal') {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'proposal_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $this->Proposal_model->DeleteById($inId);
        }
    }
}

