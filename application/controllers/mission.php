<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mission extends MY_Controller {
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
    public function index() {   
        $inData = $this->includeUp();
        $inData['sub_page'] = "<h2>".getCaptionInput('msg_mission_dtv')."</h2>";
        echo $this->twig->render('site-master-page.twig', $inData);
    }
}