<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
    //
    public function index() {   
        //
        //echo $this->captcha->draw();
        //$outCV = $this->Content_video->loadById(10);
        //$this->load->model('Images_model');
        //$outImage = getImagesForElement($outCV['images_id']);
        //echo "<pre>"; var_dump($outImage); die();
        $inData = $this->includeUp();
        $inData['property']['isMain'] = true;
        $inData['pieses']['outContentVideo'] = $this->Content_video->getOutput();
        $outCV = $this->Content_video->load(array('content_status_main'=>1));
        $inData['pieses']['outContentVideo']['data'] = (!$outCV)?array('content_title'=>'Видео нет'):array('data'=>$outCV[0]); 
        //
        $this->load->model('Slider_model');
        $inData['pieses']['outSlider'] = $this->Slider_model->getOutput();
        $inData['pieses']['outSlider']['data'] = $this->Slider_model->load(array('picture_status'=>1, 'picture_main'=>1),false);
        $inData['pieses']['outSlider']['property']['isRun'] = $inData['pieses']['outSlider']['data']?count($inData['pieses']['outSlider']['data']):0;
        //echo "<pre>"; var_dump($inData['pieses']['outSlider']['data']); die();
        $inData['pieses']['outSlider']['property']['data_path'] = $this->inPicPath;
        $this->load->model('Picture_model');
        $outHeaderCollection = $this->Picture_model->loadCategory(array('cpicture_alias'=>'main_gallery'));
        $outHeaderCollection = $outHeaderCollection[0];
        $inData['collection_title'] = $outHeaderCollection['cpicture_title']; 
        //= (count($inData['pieses']['outSlider']['data']>0));
        //echo "<pre>"; var_dump($inData['pieses']['outSlider']['data']); die();
        //$inData['pieses']['outLinkDown']['data'] = (!$outCV)?array('content_title'=>'Видео нет'):array('data'=>$outCV[0]);
        $this->load->model('Proposal_model');
        $inData['pieses']['outProposal'] = $proposals = $this->Proposal_model->load_index();
        $inData['pieses']['outProposal']['property']['isRun'] = true;
        $inData['pieses']['outProposal']['property']['template'] ='proposal/proposal_index.twig';
        //echo "<pre>"; var_dump($inData); die();
        //$inData['new_bd'] = $outHeaderCollection['cpicture_title'];
        
        
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    public function geography_to_help() {
        //echo "Пункт добавлен из админчасти. Верстка подсистемы в разработке. Поэтому выводится это.";
        echo $this->twig->render('site-master-page-empty.twig');
    }
    public function ask_for_help() {
        //echo "Пункт добавлен из админчасти. Верстка подсистемы в разработке. Поэтому выводится это.";
        echo $this->twig->render('site-master-page-empty.twig');
    }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */