<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cvideo_controller extends MY_Controller {
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
    public function index() {   
        $inData = $this->includeUp(true);
        //die("TYT");
        $this->Content_video->setCountRecord(0);
        $inData['pieses']['listVideo'] = $this->Content_video->getOutput(OUTPUT_LIST);
        //$this->Content_video->Debug();
        $inData['pieses']['listVideo']['data'] = 
                $this->Content_video->load(array('content_status'=>1,'language_id'=>$this->inLang),false,$this->inPage);
        //
        //var_dump($inData['pieses']['listVideo']['data']); die();
        if ($inData['pieses']['listVideo']['data']) {
            foreach ($inData['pieses']['listVideo']['data'] as $outKey => $outData) {
                //$outData['news_count'] = $this->Comments->CountByNewsId($outData['news_id']);
                $inCategory = $this->Content_video->loadCategoryById($outData['ccontent_id']);
                $outData['ccontent_title'] = $inCategory['ccontent_title'];
                $inData['pieses']['listVideo']['data'][$outKey] = $outData;
            }
        };
        
        $inData['sub_page'] = $this->twig->render('video/video-out-list.twig', $inData);
        //$inData['sub_page']['info'] = $inData['pieses']['listVideo']['info']['count_record']; 
        $inData['pieses']['outVideo']['property']['isRun'] = false;
        //echo "<pre>"; var_dump($inData['pieses']['listVideo']['info']); die();
        
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    public function detail($aId=null) {
        $inData = $this->includeUp(true);
        $inData['pieses']['outVideo']['property']['isRun'] = false;
        $inArg = func_get_args();
        if (empty($inArg[0])) {
            $this->index();
        }
        $inVideo = $this->Content_video->loadById($inArg[0]);
        //echo "<pre>"; var_dump($inVideo); die();
        /*if ($inVideo['content_link']) {
            //$inTest = '<iframe width="200" height="300" src="//www.youtube.com/embed/41M34rnUqq8" frameborder="0" allowfullscreen></iframe>';
            $inPatternGetIframe = '/<iframe[^>]+src=([\'"])?((?(1).+?|[^\s>]+))(?(1)\1)/';
            preg_match_all($inPatternGetIframe, $inVideo['content_link'], $matches);
            $inPatternGetWidth = '/(width|height)(=[\'"])(\d+)([\'"])/';
            preg_match_all($inPatternGetWidth, $inVideo['content_link'], $matchesWidth);
            $matchesWidth[3][0] = (int)$matchesWidth[3][0]+15;
            $matchesWidth[3][1] = (int)$matchesWidth[3][1]+15;
            $inVideo['content_link'] = "<iframe {$matchesWidth[1][0]}='{$matchesWidth[3][0]}' {$matchesWidth[1][1]}='{$matchesWidth[3][1]}' src='{$matches[2][0]}' frameborder='0' allowfullscreen></iframe>";
            //echo "<pre>"; var_dump($inVideo['content_link'],$matchesWidth);
        };*/
        //die();
        $inCategory = $this->Content_video->loadCategoryById($inVideo['ccontent_id']);
        $inVideo['ccontent_title'] = $inCategory['ccontent_title'];
        //echo "<pre>"; var_dump($inVideo['ccontent_title']); die();
        $inData['sub_page'] = $this->twig->render('video/video-one-record-list-detail.twig', array("data"=>$inVideo));
        //echo "<pre>"; var_dump($inSData); die();
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    public function save_comments() {
        if (!empty($_POST)) {
            $this->Comments->save($_POST);
        }
        Goto_Page('/Cvideo_controller/');
    }
    public function CategoryDetail() {
        $inArg = func_get_args();
        $inCategory = isset($inArg[0])?$inArg[0]:1;
        $inData = $this->includeUp(true);
        $inData['pieses']['listNews'] = $this->News_model->getOutput(OUTPUT_LIST);
        $this->News_model->setCountRecord(0);
        $inData['pieses']['listNews']['data'] = (empty($inCategory))?
                $this->News_model->load(array('news_status_main'=>1,'language_id'=>$this->inLang),false,$this->inPage):
                $this->News_model->load(array('news_status_main'=>1,'language_id'=>$this->inLang,'nc_id'=>$inCategory),false,$this->inPage);
        //
        if ($inData['pieses']['listNews']['data']) {
            foreach ($inData['pieses']['listNews']['data'] as $outKey => $outData) {
                $outData['news_count'] = $this->Comments->CountByNewsId($outData['news_id']);
                $inCategory = $this->News_model->loadCategoryById($outData['nc_id']);
                $outData['nc_title'] = $inCategory['nc_title'];
                $inData['pieses']['listNews']['data'][$outKey] = $outData;
            }
        }
        //$inData = $this->includeUp(array('SET-NEWS-CATEGORY'=>$inCategory));
        $inData['rss'] = "/rss/NewsCategoryDetail/{$inCategory}";
        //echo "<pre>"; var_dump($inData); die();
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    protected function goSearch() {
        $this->startUp(true);
        $inData = $this->includeUp();
        $inData['sub_page']['messages']['error'] = '';
        $inData['sub_page']['messages']['status'] = '';
        $inData['search_menu'] = $this->Menu_model->load(array('name'=>'search_menu','status'=>1));
        $inStrSearch = filter_input(INPUT_GET, 'inSearch');
        if (empty($inStrSearch)) { 
            $inData['sub_page']['messages']['error'] .= sprintf(getCaptionInput('error_empty'),getCaptionInput('caption_search'));
        }
        else {
            $this->PropertySearch=$inStrSearch;
            $inData['sub_page']['insearch'] = $this->PropertySearch;
            $inData['sub_page']['search_list'][] = "<a href='/news/goSearch?inSearch={$inStrSearch}'>".getCaptionInput('msg_search_news')."</a>";
            $inData['sub_page']['search_list'][] = "<a href='/blogs/goSearch?inSearch={$inStrSearch}'>".getCaptionInput('msg_search_blogs')."</a>";
            //$inData['sub_page']['search_list'][] = "<a href='/cvideo_controller/goSearch?inSearch={$inStrSearch}'>".getCaptionInput('msg_search_video')."</a>";
            //$inData['sub_page']['search_result']['news'] = 
            //    $this->News_model->loadTreeSearch(array('news_status'=>1,'language_id'=>$this->inLang,'fields'=>array('news_id, news_title, nc_id as category')),false,$this->inPage);
            //$inData['sub_page']['messages']['status'] .= getCaptionInput('msg_search_news')." ". sprintf(getCaptionInput('msg_search_result'),count($inData['sub_page']['search_result']['news']));
            //
            //$inData['sub_page']['search_result']['blogs'] = 
            //    $this->Blogs_model->loadTreeSearch(array('blogs_status'=>1,'language_id'=>$this->inLang,'fields'=>array('blogs_id, blogs_title, cblogs_id as category')),false,$this->inPage);
            //$inData['sub_page']['messages']['status'] .= getCaptionInput('msg_search_blogs')." ".sprintf(getCaptionInput('msg_search_result'),count($inData['sub_page']['search_result']['blogs']));
            //
            $inData['sub_page']['search_result']['video'] = 
                $this->Content_video->loadTreeSearch(array('content_status'=>1,'language_id'=>$this->inLang,'content_text'=>"like '%{$inStrSearch}%'",'fields'=>array('content_id, content_title, ccontent_id as category')),false,$this->inPage);
            $inData['sub_page']['messages']['status']['video'] = getCaptionInput('msg_search_video')." ".sprintf(getCaptionInput('msg_search_result'),count($inData['sub_page']['search_result']['video']));
            //
        }
        //echo "<pre>"; var_dump($inData['sub_page']['search_result']); die();
        //$inData['sub_page']['messages']['error'] = getMessage('error');
        //$inData['sub_page']['messages']['status'] = getMessage('status');
        $inData['sub_page'] = $this->twig->render('common/search/collection-serach-site.twig',$inData);//$this->uri->uri_string();
        //$inData['sub_page'] = $this->twig->render('common/search/collection-search-start-up.twig',$inData);
        echo $this->twig->render('site-master-page.twig', $inData);
    }
}