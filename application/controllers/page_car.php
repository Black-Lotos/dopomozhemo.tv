<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page_car extends MY_Controller {
    //
    public function index() {   
        $inData = array();
        $inData = $this->includeUp('site_menu',true);
        //$this->Language_model->load(array('lan_status'=>1,'fields'=>array('lan_title as title','language_id as value')));
        //$this->load_page('main_info',$inData);
        
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function load($aParam) {
        $inData = $this->includeUp('site_menu',true);
        $inCar = $this->Auto_model->loadCategoryById($aParam,array("fields"=>array('car_name as title','images_id','car_description as note','car_id as value')));
        
        $inAvto = $this->Auto_model->load(
            array("{$this->Auto_model->getCategoryId()}"=>$inCar['value'],'fields'=>array("avto_id as value","images_id","avto_name as title","avto_year as year","avto_description as text","avto_url as url")),
            false        
        );
        //    
        //
        $inBloks = array();
        // Данные для марок автомобилей
        if($inCar===false || ($inCar && $inAvto===false)) {
            $dataCar=array('title'=>'Выбранная марка автомобилей пуста','text'=>'','value'=>'','images_id'=>$inCar['images_id']);
        } else  {
                    $dataCar=array('title'=>$inCar['title'],'text'=>$inCar['note'],'value'=>$inCar['value'],'images_id'=>$inCar['images_id']);
                }
        // Данные для автомобилей
        if($inAvto===false) {
            $dataAvto=array('title'=>'Автомобилей не найдено','text'=>'','value'=>'','images_id'=>$inCar['images_id']);
        } else  {
                    $dataAvto=$inAvto;
                }        
        // Формируем представление для марок автомобилей
        //var_dump(array('data'=>$dataAvto)); die();
        $inBloks[] = $this->twig->render('blocks/block-car-start-up.twig',array('data'=>$dataCar));
        // Собираем подчиненные автомобили
        //echo "<pre>"; var_dump(array('data'=>$inAvto)); die();
        $inBloks[] = $this->twig->render('blocks/block-avto-start-up.twig',array('data'=>$dataAvto));
        // загоняем в общий вывод всех блоков
        $inData['page']['content']['right'] = $this->twig->render('blocks/block-build.twig',array("data"=>$inBloks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function detail_avto($aParam) {
        $inData = $this->includeUp('site_menu',true);
        $inAvto = $this->Auto_model->loadById($aParam,
                array('fields'=>array("avto_id as value","images_id","avto_name as title","avto_year as year","avto_description as text","avto_url as url"))
        );
        //var_dump($inAvto);
        $inBloks = array();
        $inBloks[] = $this->twig->render('avto/avto-detail.twig',array("data"=>$inAvto));
        $inData['page']['content']['right'] = $this->twig->render('blocks/block-build.twig',array("data"=>$inBloks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
}

