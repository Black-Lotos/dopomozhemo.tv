<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Visitors extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'site_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model("Content_video");
        $this->load->model("Comments_model","Comments");
    }
    //
    public function index() {   
        $inMenu = $this->Menu_model->load(array('name'=>'site_menu','status'=>1));
        $inData = array('title'=>'Главная страница','content'=>array('header'=>'Главная страница'),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //
        $inData['pieses']['outNews'] = $this->News_model->getOutput();
        $outNews = $this->News_model->loadNews(array('news_status_main'=>1));
        $inData['pieses']['outNews']['data'] = (!$outNews)?array('news_title'=>'Новостей нет'):array('data'=>$outNews[0]);
        //
        $inData['pieses']['outBlogs'] = $this->Blogs_model->getOutput();
        $outBlogs = $this->Blogs_model->load(array('blogs_main'=>1));
        $inData['pieses']['outBlogs']['data'] = (!$outBlogs)?array('blogs_title'=>'Блогов нет'):array('data'=>$outBlogs);
        //
        $inData['pieses']['listNews'] = $this->News_model->getOutput(OUTPUT_LIST);
        foreach ($inData['pieses']['listNews']['data'] as $outKey => $outData) {
            $outData['news_count'] = $this->Comments->CountByNewsId($outData['news_id']);
            $inData['pieses']['listNews']['data'][$outKey] = $outData;
        }
        $this->load->model("Footerlink_model");
        $inData['pieses']['outLinkDown'] = $this->Footerlink_model->getOutput();
        $inData['pieses']['outLinkDown']['data'] = $this->Footerlink_model->load(array('footerlink_status'=>1));
        $inData['pieses']['outVideo'] = $this->Content_video->getOutput();
        $inData['pieses']['outVideo']['data'] = $this->Content_video->loadCategoryElemntByAlias('alias_down_slider_video',array('content_status'=>1));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    public function detail() {
        $inNewsId = filter_input(INPUT_POST, 'news_id', FILTER_SANITIZE_SPECIAL_CHARS);
        if (!empty($inNewsId)) {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $inNewsId, $outMatches)>0)?(int)$outMatches[0]:0;
            $inData=array('comments'=>array('title'=>getCaptionInput('caption_comments'),'counts'=>$this->Comments->CountByNewsId($inId)));
            $inData['comments']['data'] = $this->Comments->loadByNewsId($inId);
            $inData['news']['data'] = $this->News_model->loadById($inId);
            $inData['news']['data']['category'] = $this->News_model->loadCategoryById($inData['news']['data']['nc_id']);
            //echo "<pre>"; var_dump($inData); die();
        }
        echo $this->twig->render('news/news-one-record-list.twig', $inData);
    }
    public function save_comments() {
        if (!empty($_POST)) {
            $this->Comments->save($_POST);
        }
        Goto_Page('/visitors/');
    }
}