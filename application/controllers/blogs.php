<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blogs extends MY_Controller {
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
    public function index() {   
        //echo $this->inPage;
        $inData = $this->includeUp(true);
        $this->Blogs_model->setCountRecord(0);
        $inData['pieses']['listBlogs'] = $this->Blogs_model->getOutput(OUTPUT_LIST);
        $inData['pieses']['listBlogs']['data'] = $this->Blogs_model->load(array('blogs_status'=>1,'language_id'=>$this->inLang),false,$this->inPage);
        //var_dump($inData['pieses']['listBlogs']['info']); die();
        if ($inData['pieses']['listBlogs']['data']) {
            foreach ($inData['pieses']['listBlogs']['data'] as $outKey => $outData) {
                $outData['blogs_count'] = $this->Comments->CountByBlogId($outData['blogs_id']);
                $inCategory = $this->Blogs_model->loadCategoryById($outData['cblogs_id']);
                //var_dump($inCategory); die();
                $outData['cblogs_title'] = $inCategory['cblogs_title'];
                $inData['pieses']['listBlogs']['data'][$outKey] = $outData;
            }
        }
        $inData['sub_page'] = $this->twig->render('blogs/blogs-out-list.twig', $inData);
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function LoadTop() {
        echo "TopBlogs";
    }
    //
    public function detail($aId=null) {
        $inData = $this->includeUp(true); $inId = 0;
        //$inData['sub_page_title'] = getMessage('status', true, 'status').getMessage('error', true, 'error');
        if ($_POST) {
            $inNewsId = filter_input(INPUT_POST, 'blogs_id', FILTER_SANITIZE_SPECIAL_CHARS);
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $inNewsId, $outMatches)>0)?(int)$outMatches[0]:0;
        } else $inId  = $aId;
        if (!empty($inId)) {
            $inSData=array('comments'=>array('title'=>getCaptionInput('caption_comments'),'counts'=>$this->Comments->CountByBlogId($inId)));
            $inSData['comments']['data'] = $this->Comments->loadByBlogId($inId);
            $inSData['blogs']['data'] = $this->Blogs_model->loadById($inId);
            $inSData['blogs']['data']['category'] = $this->Blogs_model->loadCategoryById($inSData['blogs']['data']['cblogs_id']);
        }
        $inData['sub_page'] = $this->twig->render('blogs/blogs-one-record-list.twig', $inSData);
        //echo "<pre>"; var_dump($inSData); die();
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function save_comments() {
        if (!empty($_POST)) {
            $inCheckComments = $this->CheckComments();
            //var_dump($inCheckComments,$_POST);
            //echo "<pre>"; var_dump($_POST); die();
            unset($_POST['edtCaptcha']);
            if ($inCheckComments)
                $this->Comments->save($_POST);
            //echo "<pre>"; var_dump($inCheckComments,$_POST);
                //
            //echo getMessage('status', true, 'status');
            //cho getMessage('error', false, 'error');
        //Goto_Page("/blogs/detail/{$_POST['blogs_id']}");    
        }
    }
    //
    public function setPage() {
        $inId = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
        $inProcess = filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS);
        if (($inProcess=='set-page')&&!empty($inId)) {
            $inPattern = '/([0-9]+)$/';
            $this->inPage = (preg_match($inPattern, $inId, $outMatches)>0)?(int)$outMatches[0]:0;
            echo $this->inPage;
        }
    }
    //
    protected function goSearch() {
        $this->startUp(true);
        $inData = $this->includeUp();
        $inData['sub_page']['messages']['error'] = '';
        $inData['sub_page']['messages']['status'] = '';
        $inData['search_menu'] = $this->Menu_model->load(array('name'=>'search_menu','status'=>1));
        $inStrSearch = filter_input(INPUT_GET, 'inSearch');
        if (empty($inStrSearch)) { 
            $inData['sub_page']['messages']['error'] .= sprintf(getCaptionInput('error_empty'),getCaptionInput('caption_search'));
        }
        else {
            $this->PropertySearch=$inStrSearch;
            $inData['sub_page']['insearch'] = $this->PropertySearch;
            $inData['sub_page']['search_list'][] = "<a href='/news/goSearch?inSearch={$inStrSearch}'>".getCaptionInput('msg_search_news')."</a>";
            $inData['sub_page']['search_list'][] = "<a href='/cvideo_controller/goSearch?inSearch={$inStrSearch}'>".getCaptionInput('msg_search_video')."</a>";
            $inData['sub_page']['search_result']['blogs'] = 
                $this->Blogs_model->loadTreeSearch(array('blogs_status'=>1,'language_id'=>$this->inLang,'blogs_text'=>"like '%{$inStrSearch}%'",'fields'=>array('blogs_id, blogs_title, cblogs_id as category')),false,$this->inPage);
            $inData['sub_page']['messages']['status']['blogs'] = getCaptionInput('msg_search_blogs')." ".sprintf(getCaptionInput('msg_search_result'),count($inData['sub_page']['search_result']['blogs']));
        }
        $inData['sub_page'] = $this->twig->render('common/search/collection-serach-site.twig',$inData);//$this->uri->uri_string();
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    public function CategoryDetail() {
        $inArg = func_get_args();
        $inCategory = isset($inArg[0])?$inArg[0]:1;
        $inData = $this->includeUp();
        $inData['pieses']['listBlogs'] = $this->Blogs_model->getOutput(OUTPUT_LIST);
        $this->Blogs_model->setCountRecord(0);
        $inData['pieses']['listBlogs']['data'] = (empty($inCategory))?
                $this->Blogs_model->load(array('blogs_status'=>1,'language_id'=>$this->inLang),false,$this->inPage):
                $this->Blogs_model->load(array('blogs_status'=>1,'language_id'=>$this->inLang,'cblogs_id'=>$inCategory),false,$this->inPage);
        //
        if ($inData['pieses']['listBlogs']['data']) {
            foreach ($inData['pieses']['listBlogs']['data'] as $outKey => $outData) {
                $outData['blogs_count'] = $this->Comments->CountByNewsId($outData['blogs_id']);
                $inCategory = $this->Blogs_model->loadCategoryById($outData['cblogs_id']);
                $outData['cblogs_title'] = $inCategory['cblogs_title'];
                $inData['pieses']['listBlogs']['property']['title'] = $inCategory['cblogs_title'];
                $inData['pieses']['listBlogs']['data'][$outKey] = $outData;
            }
        }
        //$inData = $this->includeUp(array('SET-NEWS-CATEGORY'=>$inCategory));
        //$inData['rss'] = @"/rss/NewsCategoryDetail/{$inCategory}";
        
        $inData['sub_page'] = $this->twig->render('blogs/blogs-out-list.twig', $inData);
        //echo "<pre>"; var_dump($inData); die();
        echo $this->twig->render('site-master-page.twig', $inData);
    }
}