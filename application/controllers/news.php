<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends MY_Controller {
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
    public function index() {   
        $inData = $this->includeUp();
        $this->News_model->setOrder('news_creates');
        $this->News_model->setCountRecord(0);
        $inData['pieses']['listNews'] = $this->News_model->getOutput(OUTPUT_LIST);
        $inData['pieses']['listNews']['data'] = 
                $this->News_model->load(array('news_status'=>1,'language_id'=>$this->inLang),false,$this->inPage);
        //echo "<pre>"; var_dump($inData['pieses']['listNews']); die();
        //
        //
        if ($inData['pieses']['listNews']['data']) {
            foreach ($inData['pieses']['listNews']['data'] as $outKey => $outData) {
                $outData['news_count'] = $this->Comments->CountByNewsId($outData['news_id']);
                $inCategory = $this->News_model->loadCategoryById($outData['nc_id']);
                $outData['nc_title'] = $inCategory['nc_title'];
                $inData['pieses']['listNews']['data'][$outKey] = $outData;
            }
        };
        $inData['sub_page'] = $this->twig->render('news/news-out-list.twig', $inData);
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    public function CategoryDetail() {
        $inData = $this->includeUp();
        $this->News_model->setOrder('news_creates');
        $this->News_model->setCountRecord(0);
        $inArg = func_get_args();
        $inCategory = isset($inArg[0])?$inArg[0]:1;
        //$inData['pieses']['listNews'] = $this->News_model->getOutput(OUTPUT_LIST);
        //$this->News_model->debug();
        //$this->News_model->Debug();
        //$this->News_model->Debug();
        $inData['pieses']['listNews'] = (empty($inCategory))?
                $this->News_model->getOutput(OUTPUT_LIST,array('news_status'=>1,'language_id'=>$this->inLang),false,$this->inPage):
                $this->News_model->getOutput(OUTPUT_LIST,array('news_status'=>1,'language_id'=>$this->inLang,'nc_id'=>$inCategory),false,$this->inPage);
                /*$this->News_model->load(array('news_status'=>1,'language_id'=>$this->inLang),false,$this->inPage):
                $this->News_model->load(array('news_status'=>1,'language_id'=>$this->inLang,'nc_id'=>$inCategory),false,$this->inPage);*/
        //
        if ($inData['pieses']['listNews']['data']) {
            foreach ($inData['pieses']['listNews']['data'] as $outKey => $outData) {
                $outData['news_count'] = $this->Comments->CountByNewsId($outData['news_id']);
                $inCategory = $this->News_model->loadCategoryById($outData['nc_id']);
                $outData['nc_title'] = $inCategory['nc_title'];
                $inData['pieses']['listNews']['data'][$outKey] = $outData;
            }
        };
        //$inData = $this->includeUp(array('SET-NEWS-CATEGORY'=>$inCategory));
        //$inData['rss'] = @"/rss/NewsCategoryDetail/{$inCategory}";
        
        $inData['sub_page'] = $this->twig->render('news/news-out-list.twig', $inData);
        //echo "<pre>"; var_dump($inData); die();
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    public function detail($aId=null) {
        $inData = $this->includeUp(); $inId = 0;
        //$inData['sub_page_title'] = getMessage('status', true, 'status').getMessage('error', true, 'error');
        if ($_POST) {
            $inNewsId = filter_input(INPUT_POST, 'news_id', FILTER_SANITIZE_SPECIAL_CHARS);
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $inNewsId, $outMatches)>0)?(int)$outMatches[0]:0;
        } else $inId  = $aId;
        if (!empty($inId)) {
            $inSData=array('comments'=>array('title'=>getCaptionInput('caption_comments'),'counts'=>$this->Comments->CountByNewsId($inId)));
            $inSData['comments']['data'] = $this->Comments->loadByNewsId($inId);
            $inSData['news']['data'] = $this->News_model->loadById($inId);
            $inSData['news']['data']['category'] = $this->News_model->loadCategoryById($inSData['news']['data']['nc_id']);
            if($inSData['news']['data']['cpicture_id']) {
                $this->load->model('Picture_model');
                $inCollection = $this->Picture_model->load(array('picture_status'=>1,'cpicture_id'=>$inSData['news']['data']['cpicture_id'],
                    'fields'=>array('picture_code as code, picture_id')));
                $inCollection = array('collection'=>$inCollection);
                //echo "<pre>"; var_dump($inCollection); die();
                $inSData['news']['data']['collection'] = $this->twig->render('common/collection-include-min.twig',$inCollection);
                //echo "<pre>"; var_dump($inSData['news']['data']['collection']); die();
            }
            //$inSData['collection']['data'] = 
            
        }
        $inData['sub_page'] = $this->twig->render('news/news-one-record-list.twig', $inSData);
        //echo "<pre>"; var_dump($inSData); die();
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    public function save_comments() {
        if (!empty($_POST)) {
            $inCheckComments = $this->CheckComments();
            //var_dump($inCheckComments);
            //echo "<pre>"; var_dump($_POST); die();
		//var_dump($inCheckComments, $_POST);
            unset($_POST['edtCaptcha']);
            if ($inCheckComments) {
                $this->Comments->save($_POST);
        	
	    }
	    Goto_Page("/news/detail/{$_POST['news_id']}");    	
        }
    }
    protected function goSearch() {
        $this->startUp();
        $inData = $this->includeUp();
        $inData['sub_page']['messages']['error'] = '';
        $inData['sub_page']['messages']['status'] = '';
        $inData['search_menu'] = $this->Menu_model->load(array('name'=>'search_menu','status'=>1));
        $inStrSearch = filter_input(INPUT_GET, 'inSearch');
        if (empty($inStrSearch)) { 
            $inData['sub_page']['messages']['error'] .= sprintf(getCaptionInput('error_empty'),getCaptionInput('caption_search'));
        }
        else {
            $this->PropertySearch=$inStrSearch;
            $inData['sub_page']['insearch'] = $this->PropertySearch;
            $inData['sub_page']['search_list'][] = "<a href='/blogs/goSearch?inSearch={$inStrSearch}'>".getCaptionInput('msg_search_blogs')."</a>";
            $inData['sub_page']['search_list'][] = "<a href='/cvideo_controller/goSearch?inSearch={$inStrSearch}'>".getCaptionInput('msg_search_video')."</a>";
            $inData['sub_page']['search_result']['news'] = 
                $this->News_model->loadTreeSearch(array('language_id'=>$this->inLang,'news_text'=>"like '%{$inStrSearch}%'"),false,$this->inPage);
            $inData['sub_page']['messages']['status']['news'] = getCaptionInput('msg_search_news')." ". sprintf(getCaptionInput('msg_search_result'),count($inData['sub_page']['search_result']['news']));
        }
        $inData['sub_page'] = $this->twig->render('common/search/collection-serach-site.twig',$inData);//$this->uri->uri_string();
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    
}