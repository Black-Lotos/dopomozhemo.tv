<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class api extends CI_Controller {
    private $outResult = array('method'=>array('status'=>-1,'message'=>'не существует'));
    function __construct() {
        // Construct our parent class
        parent::__construct();
        $this->methods['newsCategory']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['newsList']['limit'] = 100; //100 requests per hour per user/key
        $this->load->model("News_model");
        $this->load->model("Language_model");
        $this->load->model("Images_model");
    }
    public function _remap($aMethod=null){
        //echo $aMethod;
        $inArg = func_get_args();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'error';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function index() {
        echo header('Accept: text/plain, text/html');
        echo header('Content-type: text/plain; charset=UTF-8');
        echo header('Access-Control-Allow-Origin: *');
        if(!filter_input(INPUT_GET, 'method', FILTER_SANITIZE_SPECIAL_CHARS)) {
            echo jdecoder(json_encode($this->outResult));
        } else {
            if (method_exists($this, filter_input(INPUT_GET, 'method', FILTER_SANITIZE_SPECIAL_CHARS))) {
                echo call_user_func_array(array($this, filter_input(INPUT_GET, 'method', FILTER_SANITIZE_SPECIAL_CHARS)), $_GET);
            } else {
                $this->outResult['method'] = array('status'=>0,'message'=>'не описан');
                echo jdecoder(json_encode($this->outResult));
            }
        }
    }
    private function newsCategory() {
        $this->outResult = $_GET;
        $inLan = isset($this->outResult['lang'])?
                $this->Language_model->loadLanguageByCode($this->outResult['lang']):$this->Language_model->loadLanguageByCode($this->outResult['ru']);
        $inResultCategory = $this->News_model->loadCategory(
                array(  'status'=>1,
                        'language_id'=>$inLan,
                        'fields'=>array('nc_id as id','nc_title as title')
                    )
        );
        if($inResultCategory) {
            foreach ($inResultCategory as $outKey=>$outData) {
                //echo $outData['id'];
                $outData["countNewsPosts"]=$this->News_model->CountByCategoryId($outData['id']);
                $inResultCategory[$outKey] = $outData;
            }
            $this->outResult = array('categories'=>$inResultCategory);
        } else {
            $this->outResult = array('categories'=>array('status'=>100,'message'=>'данных нет'));
        }
        echo jdecoder(json_encode($this->outResult));
    }
    private function newsList() {
        $this->outResult = $_GET;
        $inLan = isset($this->outResult['lang'])?
                $this->Language_model->loadLanguageByCode($this->outResult['lang']):$this->Language_model->loadLanguageByCode($this->outResult['ru']);
        $inCategory = isset($this->outResult['cat'])?$this->outResult['cat']:0;
        $inOffset = isset($this->outResult['offset'])?$this->outResult['offset']:1;
        $inCount = isset($this->outResult['count'])?$this->outResult['count']:10;
        $this->News_model->setCountRecord($inCount);
        $inResult = $this->News_model->load(
                array(  'status'=>1,
                        'language_id'=>$inLan,
                        'nc_id'=>$inCategory,
                        'fields'=>array('news_id as id','news_title as title','news_creates as date','news_text as excerpt','images_id as img')
                    ),
                false,
                $inOffset,
                $inCount
        );
        /*
        id: '1',
  title: "КАК ГОВОРИТЬ С ДЕТЬМИ О ВОЙНЕ. СОВЕТЫ ПСИХОЛОГОВ",
  author: { name: "d2h" },
  date: '24.12.2014, 13:35:42',
  img:
         */
        if($inResult) {
            foreach ($inResult as $outKey=>$outData) {
                //echo $outData['id'];
                //$outData["countNewsPosts"]=$this->News_model->CountByCategoryId($outData['id']);
                $outData['body']    = $outData['excerpt'];
                $outData['excerpt'] = mb_substr($outData['excerpt'], 0, OUT_CUT_STRING);
                $inImage = $this->Images_model->loadById($outData['img']);
                $outData['img'] = array('tbm'=>base_url().$inImage['images_folder'].$inImage['images_name'],'full'=>base_url().$inImage['images_folder'].$inImage['images_name']);
                $inResult[$outKey] = $outData;
            }
            $this->outResult = array('posts'=>$inResult);
        } else {
            $this->outResult = array('posts'=>array('status'=>100,'message'=>'данных нет'));
        }
        echo jdecoder(json_encode($this->outResult));
    }
    public function error() {
        echo "No called method";
    }
}
