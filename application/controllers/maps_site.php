<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maps_site extends MY_Controller {
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        //echo "admin"; die();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    public function index() {   
        $inData = $this->includeUp();
        echo $this->twig->render('site-master-page.twig', $inData);
    }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */