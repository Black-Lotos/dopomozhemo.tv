<?php
class Expert_help extends MY_Controller {
    public function startUp() {
        $this->load->model('Expert_help_model');
        $this->load->model('Professional_question');
        parent::startUp();
    }
    public function index() {   
        $inData = $this->includeUp();
        //echo "<pre>"; var_dump($this->Expert_help_model->loadTree());
        $inData['pieses']['listExpert'] = $this->Expert_help_model->getOutput();
        $inTree = array($this->Expert_help_model->getCategoryStatus()=>1,'fields'=>array("{$this->Expert_help_model->getCategoryId()} as value, {$this->Expert_help_model->getCategoryName()} as title, {$this->Expert_help_model->getCategoryStatus()}"));
        $inItem = array($this->Expert_help_model->getStatus()=>1,'fields'=>array("{$this->Expert_help_model->getSelfId()} as value, {$this->Expert_help_model->getSelfName()} as title, {$this->Expert_help_model->getStatus()}, images_id"));
        
        $inData['pieses']['listExpert']['data'] = $this->Expert_help_model->loadTree(array('tree'=>$inTree,'item'=>$inItem));
        //echo "<pre>"; var_dump($inData['pieses']['listExpert']['data']);
        $inData['pieses']['listExpert']['property']['title'] = '';
        $inData['sub_page'] = $this->twig->render('expert-help/expert-help-start-up.twig',$inData['pieses']['listExpert']);
        echo $this->twig->render('site-master-page.twig',$inData);
    }
    public function detail() {
        $inData = $this->includeUp();
        $inArg = func_get_args();
        $inId  = isset($inArg[0])?$inArg[0]:0;
        if($inId==0) $this->index();
        //$inData['answer']['data'] = $this->Comments->loadByBlogId($inId);
        $inExpert['expert_help']['data'] = $this->Expert_help_model->loadById($inId,
                array("fields"=>array(
                    "{$this->Expert_help_model->getSelfId()} as value",
                    "{$this->Expert_help_model->getSelfName()} as title",
                    "{$this->Expert_help_model->getPrefix()}_text as text",
                    "{$this->Expert_help_model->getPrefix()}_date as date",
                    $this->Expert_help_model->getCategoryId(),
                    "images_id"
                )));
        $inExpert['expert_help']['answer'] = 0;
        //echo "<pre>"; var_dump($inExpert); die();
        //$inData['sub_page'] = 
        //
        $inExpert['expert_help']['data']['sub_tree'] = 
            $this->Professional_question_model->load(array($this->Expert_help_model->getSelfId()=>$inId),false);
        $inExpert['expert_help']['answer'] = count($inExpert['expert_help']['data']['sub_tree']);
        //
        $inData['sub_page_title'] = $this->Expert_help_model->loadCategoryById($inExpert['expert_help']['data'][$this->Expert_help_model->getCategoryId()],
            array('fields'=>array("{$this->Expert_help_model->getCategoryName()} as title")));
            
        $inData['sub_page_title'] = "<h2>".$inData['sub_page_title']['title']."</h2>";
        $inData['sub_page'] = $this->twig->render('expert-help/expert-help-detail.twig',$inExpert['expert_help']);
        echo $this->twig->render('site-master-page.twig',$inData);
    }
    public function add($aExpert=0) {
        $inData = $this->includeUp();
        $inId  = $aExpert;
        if($inId==0) $this->index();
        //$inData['answer']['data'] = $this->Comments->loadByBlogId($inId);
        $inExpert['expert_help']['data'] = $this->Expert_help_model->loadById($inId,
                array("fields"=>array(
                    "{$this->Expert_help_model->getSelfId()} as value",
                    "{$this->Expert_help_model->getSelfName()} as title",
                    "{$this->Expert_help_model->getPrefix()}_text as text",
                    "{$this->Expert_help_model->getPrefix()}_date as date",
                    $this->Expert_help_model->getCategoryId(),
                    "images_id"
                )));
        $inExpert['expert_help']['answer'] = 0;
        //echo "<pre>"; var_dump($inExpert); die();
        //$inData['sub_page'] = 
        $inData['sub_page_title'] = $this->Expert_help_model->loadCategoryById($inExpert['expert_help']['data'][$this->Expert_help_model->getCategoryId()],
            array('fields'=>array("{$this->Expert_help_model->getCategoryName()} as title")));
        $inData['sub_page_title'] = "<h2>".$inData['sub_page_title']['title']."</h2>";
        
        $inFormData = array();
        //setLanguage($inExpert);
        //var_dump($inFormData);
        $inFormData['proffessional_id'] = $inId;
        $inFormData['language_id'] = $this->inLang;
        $inExpert['expert_help']['form'] = $this->Professional_question_model->getForm($inFormData,PROCESS_USER_APPEND);
        $inExpert['expert_help']['form']['form_property']['action'] = '/expert_help/save_question';
        //echo "<pre>"; var_dump($inExpert['expert_help']['form']); die();
        
        $inExpert['expert_help']['form'] = $this->twig->render("administration/common/form-users.twig", array('form'=>$inExpert['expert_help']['form']));
        //echo "<pre>"; var_dump($inExpert['expert_help']['form']); die();
        $inData['sub_page'] = $this->twig->render('expert-help/expert-help-detail.twig',$inExpert['expert_help']);
        echo $this->twig->render('site-master-page.twig',$inData);
    }
    public function save_question() {
        //var_dump($_POST);
        //$this->Professional_question_model->debug();
        $this->Professional_question_model->save($_POST);
        setMessage('Вопрос записан. Отправлен на обработку.');
        Goto_Page("expert_help/detail/{$_POST['proffessional_id']}");
    }
} 
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

