<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
•	Имя того, кто оставляет заявку
•	Телефон
•	E-mail
•	Подробное описание
•	Дата заявки
•	Ответ
•	Дата ответа
•	Статус «Опубликовано» / «Не опубликовано»
•proposal_	Статус «Решена» / «Не решена»

 *  */
class Proposal extends MY_Controller {
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        $this->load->model("Proposal_model");
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
    //
    protected function afterInclude($aData) {
        $inData = $aData;
        $inData['pieses']['listProposal'] = $this->Proposal_model->getOutput(OUTPUT_LIST);
        $this->Proposal_model->setCountRecord(0);
        $inData['pieses']['listProposal']['data'] = $this->Proposal_model->load(array('proposal_status'=>1,'language_id'=>$this->inLang),false,$this->inPage);
        //var_dump($inData['pieses']['listProposal']['data']);
        if ($inData['pieses']['listProposal']['data']) {
            foreach ($inData['pieses']['listProposal']['data'] as $outKey => $outData) {
                $outData['proposal_count'] = $this->Comments->CountByProposalId($outData['proposal_id']);
                $inData['pieses']['listProposal']['data'][$outKey] = $outData;
            }
        } 
        return $inData;
    }
    //
    public function index() {   
        $inData = $this->includeUp();
        //$inData = $this->afterInclude($inData);
        $inData['sub_page_title'] = getMessage('status', true, 'status');
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function add() {
        $inData = $this->includeUp();
        $inData = $this->afterInclude($inData);
        $inDataAdd = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inDataAdd['language_id'] = $this->inLang; 
        //setLanguage($inDataAdd,true);
        //echo "<pre>"; var_dump($inDataAdd);
        $inData['pieses']['proposal']['form'] = $this->Proposal_model->getForm(
                $inDataAdd,
                PROCESS_USER_APPEND
        );
        //echo "<pre>";var_dump($inData['pieses']['proposal']['form']['form_property']['action']); die();
        $inData['pieses']['proposal']['form']['form_property']['action'] = '/proposal/save';
        $inData['pieses']['proposal']['add']['form'] = $this->twig->render("administration/common/form-users.twig", array('form'=>$inData['pieses']['proposal']['form']));
        //echo "<pre>";var_dump($inData['pieses']['proposal']['add']['form']); die();
        $inData['pieses']['proposal']['add']['isRun'] = true;
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function detail($aId=null) {
        $inData = $this->includeUp(); $inId = 0;
        $inData['pieses']['outVideo']['property']['isRun'] = false;
        if ($_POST) {
            $inNewsId = filter_input(INPUT_POST, 'proposal_id', FILTER_SANITIZE_SPECIAL_CHARS);
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $inNewsId, $outMatches)>0)?(int)$outMatches[0]:0;
        } else $inId  = $aId;
        if (!empty($inId)) {
            $inSData=array('comments'=>array('title'=>getCaptionInput('caption_comments'),'counts'=>$this->Comments->CountByProposalId($inId)));
            $inSData['comments']['data'] = $this->Comments->loadByProposalId($inId);
            $inSData['proposal']['data'] = $this->Proposal_model->loadById($inId);
            
        }
        $inData['sub_page'] = $this->twig->render('proposal/proposal-one-record-list.twig', $inSData);
        $inData['sub_page_title'] = "<hr/><h2>".  getVariableLanguageByName("caption_proposal_sub_title",$this->inLangName)."</h2>";
        //echo "<pre>"; var_dump($inSData); die();
        echo $this->twig->render('proposal/proposal-page-detail.twig', $inData);
        //echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function save_comments() {
        if (!empty($_POST)) {
            $this->Comments->save($_POST);
        }
        Goto_Page('/proposal/');
    }
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            if(!empty($inDecode['proposal_status'])&&$inDecode['proposal_status']=='on') {
                $inDecode['proposal_status']=1;
            }
            if(!empty($inDecode['proposal_top'])&&$inDecode['proposal_top']=='on') {
                $inDecode['proposal_top']=1;
            }
            if(!empty($inDecode['proposal_main'])&&$inDecode['proposal_main']=='on') {
                $inDecode['proposal_main']=1;
            }
            //echo "<pre>"; var_dump($inDecode); die();
            //if($this->inUser->user_id)
            $inDecode['user_id']=(!empty($this->inUser->user_id)?$this->inUser->user_id:$this->Users_model->getRoleId('Анонимный пользователь'));
            echo $this->Proposal_model->save($inDecode);
            setMessage(getCaptionInput("msg_proposal_save"));
            Goto_Page('/proposal/');
        }
    }
}