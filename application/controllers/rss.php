<?php
class Rss extends CI_Controller {
    var $Title;          // заголовок канала
    var $Link;           // ссылка на главную страницу
    var $Copyright;      // копирайт
    var $Description;    // описание канала
    var $LastBuildDate;  // дата последнего документа (по умолчанию текущая)
    var $Language;        // язык
    var $PubDate;        // дата публикации
    var $ManagingEditor;  // E-mail редактора
    var $WebMaster;      // E-mail webmaster
    var $Category;       // категория
    var $Query;          // содержимое запроса
    var $Connect;           // для соединения с базой данных
    var $Result;         // для хранения результата
    private $RN = "\r\n";
    function Translate($text) {
        $trans = array("<" => "&lt;", ">" => "&gt;",'"' => "&quot;","&" => "&amp;");
        $text=strtr($text,$trans);
        $array=explode("<br>",$text);
        $count=count($array);
        return $text;
    }

    function Query() {
        $this->Result = mysql_query($this->Query) or die("Query failed");
    }
    function Open($Server,$DataBase,$Login,$Password) {
        $this->Connect = mysql_connect($Server, $Login, $Password ) or die("Could not connect");
        mysql_select_db($DataBase) or die("Could not select database");

    }
    function Close() {
        mysql_free_result($this->Result);
        mysql_close($this->Connect);
    }


    function PrintHeader() {
        header("Content-Type: application/xml ");
        $End="?";
        $Date=date("r");   // дата в формате Mon, 25 Dec 2006 10:23:37 +0400
        print "<$End";
        print "xml version=\"1.0\" encoding=\"utf-8\" $End> {$this->RN}";
        print "<rss version=\"2.0\">{$this->RN}";
        print "   <channel>{$this->RN}";
        print "       <title>$this->Title</title>{$this->RN}";
        print "       <category>$this->Category</category>{$this->RN}";
        print "       <link>$this->Link</link>{$this->RN}";
        print "       <copyright>$this->Copyright</copyright>{$this->RN}";
        print "       <description>$this->Description</description>{$this->RN}";
        print "       <lastBuildDate>$this->LastBuildDate</lastBuildDate>{$this->RN}";
        print "       <language>$this->Language</language>{$this->RN}";
        print "       <pubDate>$this->PubDate</pubDate>{$this->RN}";
        print "       <docs>http://blogs.law.harvard.edu/tech/rss</docs>{$this->RN}";
        print "       <managingEditor>$this->ManagingEditor</managingEditor>{$this->RN}";
        print "       <webMaster>$this->WebMaster</webMaster>{$this->RN}";
    }
    function PrintBody($Title,$Link,$Description,$Category,$PubDate) {
        //$Title =$this->Translate($Title);
        //$Link =$this->Translate($Link);
        $Description =$this->Translate($Description);
        print "              <item>{$this->RN}";
        print "                <title>$Title</title>{$this->RN}";
        print "                 <link>$Link</link>{$this->RN}";
        print "                 <description>$Description</description>{$this->RN}";
        print "                 <category>$Category</category>{$this->RN}";
        print "                 <pubDate>$PubDate</pubDate>{$this->RN}";
        print "                 <guid>$Link</guid>{$this->RN}";
        print "              </item>{$this->RN}";
    }
    function PrintFooter() {
        print "   </channel>{$this->RN}";
        print "</rss>{$this->RN}";
    }
    public function index() {
        $this->Title="Допоможемо ТВ";
        $this->Link=base_url();
        $this->Copyright="© 2014 Допоможемо.tv";
        $this->Description="Помощь в зоне АТО";
        $this->Category = "Информационный сайт";
        $this->Language="ru";
        $this->ManagingEditor="rybak.evgen@gmail.com";
        $this->WebMaster="rybak.evgen@gmail.com";
        $this->LastBuildDate=date("r");
        //
        $outNews = $this->News_model->load(array('news_status_main'=>1));
        $Date =date("r",strtotime($outNews[0]['news_creates']));
        $this->LastBuildDate=$Date;
        $this->PubDate=$this->LastBuildDate;
        $this->PrintHeader();
        //$Rss->Query();
        foreach ($outNews as $outKey => $outData) {
            $Title = $outData['news_title'];
            $Description = $outData['news_text'];
            $Link=base_url()."news/detail/".$outData['news_id'];
            $PubDate=date("r",strtotime($outData['news_creates']));
            $Category=$this->News_model->loadCategoryById($outData['nc_id']);
            $Category = $Category['nc_title'];
            $this->PrintBody($Title,$Link,$Description,$Category,$PubDate);
        }
        $this->PrintFooter();
        //$Rss->Close();
    }
    public function NewsCategoryDetail() {
        $inArg = func_get_args();
        $this->Title="Допоможемо ТВ";
        $this->Link=base_url();
        $this->Copyright="© 2014 Допоможемо.tv";
        $this->Description="Помощь в зоне АТО";
        $this->Category = "Информационный сайт";
        $this->Language="ru";
        $this->ManagingEditor="rybak.evgen@gmail.com";
        $this->WebMaster="rybak.evgen@gmail.com";
        $this->LastBuildDate=date("r");
        //
        $outNews = $this->News_model->load(array('nc_id'=>$inArg[0],'news_status'=>1));
        //var_dump($outNews);
        $Date =date("r",strtotime($outNews[0]['news_creates']));
        $this->LastBuildDate=$Date;
        $this->PubDate=$this->LastBuildDate;
        $this->PrintHeader();
        //$Rss->Query();
        foreach ($outNews as $outKey => $outData) {
            $Title = $outData['news_title'];
            $Description = $outData['news_text'];
            $Link=base_url()."news/detail/".$outData['news_id'];
            $PubDate=date("r",strtotime($outData['news_creates']));
            $Category=$this->News_model->loadCategoryById($outData['nc_id']);
            $Category = $Category['nc_title'];
            $this->PrintBody($Title,$Link,$Description,$Category,$PubDate);
        }
        $this->PrintFooter();
    }
}