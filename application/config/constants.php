<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/**/
define('OUT_CUT_STRING',150);
/**/
define ('PROCESS_USER',0100);
define ('PROCESS_ROLES',0101);
define ('PROCESS_LOGIN',0102);
define ('PROCESS_BLOGS',0103);
define ('PROCESS_BLOGS_SECTION',0104);
define ('PROCESS_NEWS_SECTION',0105);
define ('PROCESS_CONTENT_SECTION',0106);
define ('PROCESS_FOOTERLINK_SECTION',0107);
define ('PROCESS_MENU_ADD_ITEM',0108);
define ('PROCESS_MENU_ADD_SECTION',0109);
define ('PROCESS_STATIC_ADD_SECTION',0110);
define ('PROCESS_NEWS_SECTION_ADD',0111);
define ('PROCESS_BLOGS_SECTION_ADD',0112);
define ('PROCESS_SECTION_ADD',0113);
define ('PROCESS_USER_APPEND',0114);
// image type
define ('T_IMAGE',0200);
define ('T_CV',0201);
define ('T_VIDEO',0202);
//
define ('OUTPUT_LIST',0300);
define ('OUTPUT_STARTUP',0301);
//
/* End of file constants.php */
/* Location: ./application/config/constants.php */