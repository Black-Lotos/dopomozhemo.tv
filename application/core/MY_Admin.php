<?php
class MY_Admin extends CI_Controller {
private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    protected $inUser;
    private $inPermition;
    protected $inPage=1;
    protected $inLang=0;
    protected $inLangName='';
    protected $inFooterMenu=false;
    protected $inModule;
    protected $PropertySearch = "";
    protected $inPicPath = "picture/";
    protected $inImgPath = "uploads/";
    protected $inVideoPath = "video/";

    function __construct() {
        parent::__construct();
    }
    //
    public function _remap($aMethod=null){
        //die("STEP");
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    
    public function _output($output) {
        echo $output;  
    }
    //
    protected function startUp() {
        
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->load->library('Captcha');
        $this->inUser = $this->session->userdata('user');
        $inData = array(); 
        $this->inPermition = 'start';
        if (!$this->Users_model->isLogin()) {
            //CI_goto("/administration/users/login");
        }
        
        if ($this->Users_model->getCheckRoles($this->inUser,array('Администратор','Главный администратор'))) {
            $this->inPermition = $this->inUser->user_login;
        }
        else {
            $inData['message'] = getMessage('error');
            //echo $this->twig->render("administration/administration_empty.twig", $inData);
            //die("TYT");
        }
        //$this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
    }
    //
    protected function includeUp($aMenuName,$aMenuRecursive=false) {
        $this->Menu_model->setRecursive($aMenuRecursive);
        
        $this->inMenu = $this->Menu_model->load(array('name'=>$aMenuName,'status'=>1));
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        
        $inData = array(
            'page' => array(
                'menu'=>$this->inMenu,
                'property'=>array(
                    'title'=>'Администрирование',
                    'copyright'=>"© 2015 <a href='".base_url()."'></a>. Все права защищены"
                ),
                'content'=>array(
                    'left'=>"hkdhgfdkf",
                    'right'=>'sdfsdjflsfls'
                ),
                'user'=>array (
                    'status'=>'skhfdkjshfkjsh'
                )
            )
        );
        /*array('title'=>'Главная страница','content'=>array('header'=>'Главная страница'),
        'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);*/
        $inData = $this->afterInclude($inData);
        return $inData;
    }
    //
    protected function afterInclude($aData=array()) {
        //echo "after";
        return $aData;
    }
    //
    public function index() {   
        $inData = $this->includeUp('admin_menu');
        //Goto_Page("/administration/content/admin_menu");
        echo $this->twig->render("admin/admin-master-page.twig", $inData);
        
    }
    //
    public function login() {
        
    }
}