<?php
class MY_Controller extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    protected $inUser;
    private $inPermition;
    protected $inPage=1;
    protected $inLang=0;
    protected $inLangName='';
    protected $inFooterMenu=false;
    protected $inModule;
    protected $PropertySearch = "";
    protected $inPicPath = "picture/";
    protected $inImgPath = "uploads/";
    protected $inVideoPath = "video/";

    function __construct() {
        parent::__construct();
    }
    public function getImages($aImageId,$aImageWidth, $aImageHeight, $aSpectRatio=true){
        
    }

    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    
    public function _output($output)
    {
        echo $output;  
    }
    
    protected function getLang() {
        return $this->inLang;
    }
    protected function startUp() {
        //
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model("Content_video");
        $this->load->model("Comments_model","Comments");
        $this->load->library('Captcha');
        if (filter_input(INPUT_GET, 'lan')) {
            $this->session->set_userdata(array('lan' => filter_input(INPUT_GET, 'lan')));
            $this->lang->load('form_items',$this->session->userdata('lan'));
        } else {
            if ($this->session->userdata('lan'))
                $this->lang->load('form_items',$this->session->userdata('lan'));
            else {
                $this->session->set_userdata(array('lan'=>'ru'));
                $this->lang->load('form_items',$this->session->userdata('lan'));
            }
        }
        $this->inLang = $this->Language_model->loadLanguageByCode($this->session->userdata('lan'));
        $this->inLangName = $this->session->userdata('lan');
        //echo "<pre>"; var_dump($this->config); die();
        //$this->inMenu = $this->Menu_model->load(array('name'=>'site_menu','status'=>1));
        
        //$this->lang->load('form_items','ua');
    }
    protected function includeUp($aMenuRecursive=false) {
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $this->Menu_model->setRecursive($aMenuRecursive);
        $this->inMenu = $this->Menu_model->load(array('name'=>'site_menu','status'=>1));
        if ($this->inMenu) {
            foreach ($this->inMenu as $fKey=>$fData) {
                if(!empty($fData['sub_tree'])) {
                    //echo "<pre>"; var_dump($fData['sub_tree']); die();
                    foreach ($fData['sub_tree'] as $fSKey=>$fSData) {
                        //echo "<pre>"; var_dump($fSData); die();
                        $fSData["mi_title"]=$fSData["mi_title_{$this->inLangName}"];
                        $fData["sub_tree"][$fSKey] = $fSData;
                    }
                }
                $fData["mi_title"]=$fData["mi_title_{$this->inLangName}"];
                unset($fData["mi_title_{$this->inLangName}"]);
                $this->inMenu[$fKey]=$fData;
            }
        }
        //echo "<pre>"; var_dump($this->inMenu); die();
        $inData = array('title'=>'Главная страница',
            'property'=>array('isMain'=>false),
            'content'=>array('header'=>'Главная страница'),
            'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);
        //
        $inData['pieses']['outNews'] = $this->News_model->getOutput();
        $this->News_model->setCountRecord(3);
        $outNews = $this->News_model->load(array('news_status_main'=>1,'language_id'=>$this->inLang),false);
        $inData['pieses']['outNews']['data'] = (!$outNews)?
                array('data'=>array(array('news_title'=>getCaptionInput('msg_nodata')))):array('data'=>$outNews);
        //
        //echo "<pre>"; var_dump($inData['pieses']['outNews']['data']); die();
        $inData['pieses']['outBlogs'] = $this->Blogs_model->getOutput();
        $this->Blogs_model->setCountRecord(3);
        $outBlogs = $this->Blogs_model->load(array('blogs_main'=>1,'language_id'=>$this->inLang),false);
        $inData['pieses']['outBlogs']['data'] = (!$outBlogs)?
                array('data'=>array(array('blogs_title'=>getCaptionInput('msg_nodata')))):array('data'=>$outBlogs);
        //
        $inData['page']['active'] = $this->inPage;
        $inData['lan']['active'] = $this->inLangName;
        //
        $this->load->model("Footerlink_model");
        $inData['pieses']['outLinkDown'] = $this->Footerlink_model->getOutput();
        $inData['pieses']['outLinkDown']['data'] = $this->Footerlink_model->load(array('footerlink_status'=>1,'language_id'=>$this->inLang));
        //
        $inData['pieses']['outVideo'] = $this->Content_video->getOutput();
        $inData['pieses']['outVideo']['data'] = $this->Content_video->loadCategoryElemntByAlias('alias_down_slider_video',array('content_status'=>1));
        //echo "<pre>"; var_dump($inData['pieses']['outVideo']['data']); die();
        $inData = $this->afterInclude($inData);
        //
        return $inData;
    }
    protected function afterInclude($aData=array()) {
        //echo "after";
        return $aData;
    }
    //abstract function goSearch();
    protected function goSearch() {
        $this->startUp();
        $inData = $this->includeUp();
        $inData['sub_page']['messages']['error'] = '';
        $inData['sub_page']['messages']['status'] = '';
        $inData['search_menu'] = $this->Menu_model->load(array('name'=>'search_menu','status'=>1));
        $inStrSearch = filter_input(INPUT_GET, 'inSearch');
        if (empty($inStrSearch)) { 
            $inData['sub_page']['messages']['error'] .= sprintf(getCaptionInput('error_empty'),getCaptionInput('caption_search'));
        }
        else {
            $this->PropertySearch=$inStrSearch;
            $inData['sub_page']['insearch'] = $this->PropertySearch; 
            //$inData['sub_page']['search_list'][] = "<a href='/news/goSearch?inSearch={$inStrSearch}'>".getCaptionInput('msg_search_news')."</a>";
            $inData['sub_page']['search_list'][] = "<a href='/blogs/goSearch?inSearch={$inStrSearch}'>".getCaptionInput('msg_search_blogs')."</a>";
            $inData['sub_page']['search_list'][] = "<a href='/cvideo_controller/goSearch?inSearch={$inStrSearch}'>".getCaptionInput('msg_search_video')."</a>";
            //$this->News_model->Debug();
            $inData['sub_page']['search_result']['news'] = 
                $this->News_model->loadTreeSearch(array('language_id'=>$this->inLang,'news_text'=>"like '%{$inStrSearch}%'"),false,$this->inPage);
            $inData['sub_page']['messages']['status']['news'] = getCaptionInput('msg_search_news')." ". sprintf(getCaptionInput('msg_search_result'),count($inData['sub_page']['search_result']['news']));
            //
            //$inData['sub_page']['search_result']['blogs'] = 
            //    $this->Blogs_model->loadTreeSearch(array('blogs_status'=>1,'language_id'=>$this->inLang,'fields'=>array('blogs_id, blogs_title, cblogs_id as category')),false,$this->inPage);
            //$inData['sub_page']['messages']['status'] .= getCaptionInput('msg_search_blogs')." ".sprintf(getCaptionInput('msg_search_result'),count($inData['sub_page']['search_result']['blogs']));
            //
            //$inData['sub_page']['search_result']['video'] = 
            //    $this->Content_video->loadTreeSearch(array('content_status'=>1,'language_id'=>$this->inLang,'fields'=>array('content_id, content_title, ccontent_id as category')),false,$this->inPage);
            //$inData['sub_page']['messages']['statgoSearch?inSearch=fdfdfus'] .= getCaptionInput('msg_search_video')." ".sprintf(getCaptionInput('msg_search_result'),count($inData['sub_page']['search_result']['video']));
            //
        }
        //echo "<pre>"; var_dump($inData['sub_page']['search_result']); die();
        //$inData['sub_page']['messages']['error'] = getMessage('error');
        //$inData['sub_page']['messages']['status'] = getMessage('status');
        $inData['sub_page'] = $this->twig->render('common/search/collection-serach-site.twig',$inData);//$this->uri->uri_string();
        //$inData['sub_page'] = $this->twig->render('common/search/collection-search-start-up.twig',$inData);
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    public function CheckComments() {
        $outResult = true;
        //var_dump($this->Comments_model);
        if (!isset($this->Comments)) $this->load->model("Comments_model","Comments");
        //var_dump($this->session->userdata('captcha'));
        //echo json_encode($_POST);
        //if (filter_input(INPUT_POST,'process')=='check-comments') {
        $inData = $_POST;
        //var_dump(trim($inData['edtCaptcha'],trim($this->session->userdata('captcha'))));
        if (trim($inData['edtCaptcha'])!=trim($this->session->userdata('captcha'))) {
            setMessage(getCaptionInput('error_captcha'),'error');
            //setMessage(trim($inData['edtCaptcha'])."    ".trim($this->session->userdata('captcha')));
            $outResult = false;
        }
        if (empty($inData['comments_name'])) {
            setMessage(sprintf(getCaptionInput('error_empty'),getCaptionInput('ph_inputname')),'error');
            $outResult = false;
        }
        if ($outResult)
            setMessage(getCaptionInput('comments_save'),'status');
            /*
            if (empty(trim($inData['comments_text']))) {
                setMessage(sprintf(getCaptionInput('error_empty')),getCaptionInput('ph_inputcomments'),ERRORS_LIST);
            }
             * 
             */
            //print_r(getMessage('error'));
            //echo json_encode(jdecoder(getMessage(ERRORS_LIST)));
        //}
        return $outResult;
    }
} 
