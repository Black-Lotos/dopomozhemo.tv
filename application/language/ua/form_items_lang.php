<?php
$inLangDatabase = getVariableLanguage('ua');
if ($inLangDatabase) {
    foreach ($inLangDatabase as $keyLan=>$dataLan) {
       $lang[$dataLan['variable_name']] =  $dataLan['variable_value_ua'];
    }
}
/*
$lang['answer_id']      = 'Ідентифікатор відповіді';
$lang['question_id']    = 'Ідентифікатор питання';
$lang['user_id']        = 'Ідентифікатор користувача';
$lang['answer_message'] = 'Текст відповіді';
$lang['answer_date']    = 'Дата відповіді';
//
$lang ['blogs_id']      = 'Ідентифікатор блогу';
$lang ['cblogs_id']     = 'Категорія блогу';
$lang ['language_id']   = 'Вибрати мову';
$lang ['images_id']     = 'Завантажити зображення';
$lang ['blogs_name']    = 'П.І.Б. відповідального ';
$lang ['blogs_title']   = 'Заголовок блогу';
$lang ['blogs_text']    = 'Текст блогу';
$lang ['blogs_status']  = 'Активувати блог';
$lang ['blogs_main']    = 'Блог на головній';
$lang ['blogs_top']     = 'Блог в заголовку';
$lang ['blogs_owner']   = 'Батько для блогу';
$lang ['blogs_creates'] = 'Дата створення';
//
//
$lang ['ask_to_contact_id'] = 'Ідентифікатор звернення';    
$lang ['ask_to_contact_title'] = 'Заголовок звернення'; 
$lang ['ask_to_contact_text'] = 'Текс звернення';  
$lang ['ask_to_contact_name'] = 'П.І.Б.';  
$lang ['ask_to_contact_phone'] = 'Ваш № телефону'; 
$lang ['ask_to_contact_email'] = 'Ваш e-mail'; 
$lang ['ask_to_contact_date'] = 'Дата реєстрації взернення';  
$lang ['ask_to_contact_status'] = 'Статус звернення';
$lang ['ask_to_contact_resolved'] = 'Статус відповіді';
//
$lang['proposal_id']                = 'Ідентифікатор заявки';      
$lang['proposal_title']             = 'Заголовок заявки';
$lang['proposal_text']              = 'Текст заявки';
$lang['proposal_name']              = 'Введіть Ваше ім\'я';
//$lang['proposal_phone']             = '';
//$lang['proposal_email']             = '';
$lang['proposal_date']              = 'Дата заявки';
$lang['proposal_status']            = 'Статус заявки';
$lang['proposal_resolved']          = 'Заявка вирішена';
$lang['proposal_answer']            = 'Відповідь';
$lang['msg_proposal_save']          = 'Вашу заявку записано. Відправлена на модерацію.';
//
$lang ['question_id'] = 'Идентификатор питання';    
$lang ['question_title'] = 'Питання'; 
$lang ['question_text'] = 'Відповід';  
$lang ['question_name'] = 'П.І.Б.';  
//$lang ['question_phone'] = 'Ваш телефон'; 
//$lang ['question_email'] = 'Ваш e-mail'; 
$lang ['question_date'] = 'Дата реєстрації';  
$lang ['question_status'] = 'Статус питання';
$lang ['question_resolved'] = 'Статус обработки';
//
$lang['contacts_hotline_title'] = 'Горяча ліния Гуманітарного штабу Ріната Ахметова:';
$lang['contacts_presscenter_title'] = 'Телефон прес–служби Гуманітарного штабу Ріната Ахметова:';
//
$lang['caption_description'] = 'Опис:';
$lang['caption_phone'] = 'Телефон:';
$lang['caption_email'] = 'Електронна адреса:';
$lang['button_save']            = "Зберегти";
$lang['caption_comments']       = "Коментарі";
$lang['caption_blogs']          = "Блоги";
$lang['caption_news']           = "Новини";
$lang['caption_links']          = "Посилання";
$lang['caption_callme']         = "Допоможіть зв'язатися";
$lang['caption_callme_subtitle']= "Ми допоможемо розмістити Ваше оголошення на нашому сайті і оповістимо про нього наших партнерів.";
$lang['caption_all_matherial']  = "усі матеріали";
$lang['caption_search']         = "пошук";
$lang['caption_video']          = "Відео";
$lang['caption_text_maps']      = "Гуманітарна мапа <br> ситуації та потреб <br> Донецької і Луганської областей";
$lang['caption_proposal']       = "База даних";
$lang['button_proposal_add']    = "Додати заявку";
$lang['caption_question']       = "Питання і відповіді";
$lang['button_question_add']    = "Додати питання";
$lang['button_registartion']    = "Реєстрація";
$lang['button_reminder']        = "Забули пароль?";
//
$lang['msg_nodata']             = "Дані відсутні!";
$lang['msg_mission_dtv']        = '<strong>Місія ДТВ</strong> - через мультимедійні спеціальні та інформаційні проекти, історії конкретних людей, що потрапили в біду і потребують допомоги, - сприяти вирішенню гуманітарних проблем країни.';
$lang['msg_search_news']        = 'Пошук в Новинах';
$lang['msg_search_blogs']       = 'Пошук в Блогах';
$lang['msg_search_video']       = 'Пошук в Відео'; 
$lang['msg_search_result']      = 'Знайдено <strong>«%s»</strong> записів.'; 
//
$lang['ph_inputname']           = 'Введіть Ваше и\'мя';
$lang['ph_inputcomments']       = 'Введіть Ваш коментар';
$lang['ph_inputcaptcha']        = 'Введіть код з зображення';
$lang['ph_putcomments']         = 'Залишити коментар';
$lang['ph_putsearch']           = 'Провести пошук';
//
$lang['error_captcha']          = 'Невірно введено код з зображення.';
$lang['error_empty']            = 'Поле "%s" не може бути порожнім';
$lang['error_no_permitison']    = 'Вы не маєта прав доступу до цього елементу';
 * 
 */