<?php
$inLangDatabase = getVariableLanguage('en');
if ($inLangDatabase) {
    foreach ($inLangDatabase as $keyLan=>$dataLan) {
       $lang[$dataLan['variable_name']] =  $dataLan['variable_value_en'];
    }
}
/*
$lang['answer_id']      = 'Identifier of the answer';
$lang['question_id']    = 'Identifier of the question';
$lang['user_id']        = 'Identifier of the user';
$lang['answer_message'] = 'Answer text';
$lang['answer_date']    = 'Date of answer';
//
$lang ['blogs_id']      = 'Identifier of the blog';
$lang ['cblogs_id']     = 'Category blog';
$lang ['language_id']   = 'Select language';
$lang ['images_id']     = 'Upload image';
$lang ['blogs_name']    = 'Name responsible';
$lang ['blogs_title']   = 'Title of the blog';
$lang ['blogs_text']    = 'Text of the blog';
$lang ['blogs_status']  = 'Blog is the active';
$lang ['blogs_main']    = 'Blog on the main page';
$lang ['blogs_top']     = 'Blog on the top';
$lang ['blogs_owner']   = 'Parent Blog';
$lang ['blogs_creates'] = 'Date created';
//
$lang ['ask_to_contact_id'] = 'Identifier of the treatment';    
$lang ['ask_to_contact_title'] = 'Title of the treatment'; 
$lang ['ask_to_contact_text'] = 'Text of the treatment';  
$lang ['ask_to_contact_name'] = 'Your name';  
//$lang ['ask_to_contact_phone'] = 'Your phone number'; 
//$lang ['ask_to_contact_email'] = 'Your e-mail'; 
$lang ['ask_to_contact_date'] = 'Date of the treatment';  
$lang ['ask_to_contact_status'] = 'Status of the treatment';
$lang ['ask_to_contact_resolved'] = 'Status of the resolved treatment';
//
$lang['proposal_id']                = 'Identifier of proposal';      
$lang['proposal_title']             = 'Title of the proposal';
$lang['proposal_text']              = 'Text of the proposal';
$lang['proposal_name']              = 'Input Your name';
//$lang['proposal_phone']             = '';
//$lang['proposal_email']             = '';
$lang['proposal_date']              = 'Date of the proposal';
$lang['proposal_status']            = 'Status of the proposal';
$lang['proposal_resolved']          = 'Proposal is resolved';
$lang['proposal_answer']            = 'Answer';
$lang['msg_proposal_save']          = 'Your proposal is save. Send to moderation.';
//
$lang ['question_id'] = 'Identifier of the question';    
$lang ['question_title'] = 'Question'; 
$lang ['question_text'] = 'Text of the question';  
$lang ['question_name'] = 'Your name';  
//$lang ['question_phone'] = 'Your phone number'; 
//$lang ['question_email'] = 'Your e-mail'; 
$lang ['question_date'] = 'Date of the question';  
$lang ['question_status'] = 'Status of the question';
$lang ['question_resolved'] = 'Status of the resolved question';
//
$lang['contacts_hotline_title'] = 'Hotline Humanitarian Staff Rinat Akhmetov:';
$lang['contacts_presscenter_title'] = 'Phone pres-service staff Gumanіtarnogo Rіnata Akhmetov:';
//
$lang['caption_phone'] = 'Phone:';
$lang['caption_email'] = 'e-mail:'; 
$lang['button_save']            = "Save";
//
$lang['caption_description']    = 'Description:';
$lang['caption_comments']       = "Comments";
$lang['caption_blogs']          = "Blogs";
$lang['caption_news']           = "News";
$lang['caption_links']          = "Links";
$lang['caption_callme']         = "Help contact";
$lang['caption_callme_subtitle']= "We will help you to place your ad on our site and notify him of our partners.";
$lang['caption_all_matherial']  = "all matherials";
$lang['caption_search']         = "search";
$lang['caption_video']          = "Video";
$lang['caption_text_maps']      = "The humanitarian <br> situation and needs card <br> Donetsk and Lugansk regions";
$lang['caption_proposal']       = "Database";
$lang['button_proposal_add']    = "Add proposal";
$lang['caption_question']       = "Questions and Answers";
$lang['button_question_add']    = "Add question";
$lang['button_registartion']    = "Registration";
$lang['button_reminder']        = "Lost password?";

$lang['msg_nodata']             = "Data is not exist!";
$lang['msg_mission_dtv']        = '<strong>Mission DTV</strong> - through special multimedia and information projects, stories of specific people in distress and in need of help - help solve humanitarian problems of the country.';
$lang['msg_search_news']        = 'Search from News';
$lang['msg_search_blogs']       = 'Search from Blogs';
$lang['msg_search_video']       = 'Search from Video'; 
$lang['msg_search_result']      = 'System find <strong>«%s»</strong> records.'; 
//
$lang['ph_inputname']           = 'Please enter your name';
$lang['ph_inputcomments']       = 'Please enter your comment';
$lang['ph_inputcaptcha']        = 'Enter code from image';
$lang['ph_putcomments']         = 'Leave a Comment';
$lang['ph_putsearch']           = 'Go search';
//
$lang['error_captcha']          = 'The incorrect code from the picture.';
$lang['error_empty']            = 'The field "%s" can not be empty';
$lang['error_no_permitison']    = 'Do not have permition to access this element';
 * 
 */