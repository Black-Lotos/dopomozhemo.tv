<?php
$inLangDatabase = getVariableLanguage();
if ($inLangDatabase) {
    foreach ($inLangDatabase as $keyLan=>$dataLan) {
       $lang[$dataLan['variable_name']] =  $dataLan['variable_value'];
    }
}
/*
//$this->load->model('Variables_model');
$lang['answer_id']      = 'Идентификатор ответа';
$lang['question_id']    = 'Идентификатор вопроса';
$lang['user_id']        = 'Идентификатор пользователя';
$lang['answer_message'] = 'Текст ответа';
$lang['answer_date']    = 'Дата ответа';
//
$lang['blogs_id']       = 'Идентификатор блога';
$lang['cblogs_id']      = 'Категория блога';        
$lang['language_id']    = 'Выбрать язык';      
$lang['images_id']      = 'Загрузить изображение';        
$lang['blogs_name']     = 'Ф.И.О. ответственного';      
$lang['blogs_title']    = 'Заголовок блога';      
$lang['blogs_text']     = 'Текст блога';       
$lang['blogs_status']   = 'Активировать блог';     
$lang['blogs_main']     = 'Блог на главной';       
$lang['blogs_top']      = 'Блог в заголовке';        
$lang['blogs_owner']    = 'Родитель для блога';   
$lang['blogs_creates']  = 'Дата создания';   
//
$lang ['ask_to_contact_id'] = 'Идентификатор обращения';    
$lang ['ask_to_contact_title'] = 'Заголовок обращения'; 
$lang ['ask_to_contact_text'] = 'Текст обращения';  
$lang ['ask_to_contact_name'] = 'Ф.И.О.';  
//$lang ['ask_to_contact_phone'] = 'Ваш телефон'; 
//$lang ['ask_to_contact_email'] = 'Ваш e-mail'; 
$lang ['ask_to_contact_date'] = 'Дата регистрации';  
$lang ['ask_to_contact_status'] = 'Статус обращения';
$lang ['ask_to_contact_resolved'] = 'Статус обработки';
//
$lang['proposal_id']                = 'Идентификатор заявки';      
$lang['proposal_title']             = 'Заголовок заявки';
$lang['proposal_text']              = 'Текст заявки';
$lang['proposal_name']              = 'Введите Ваше имя';
//$lang['proposal_phone']             = '';
//$lang['proposal_email']             = '';
$lang['proposal_date']              = 'Дата заявки';
$lang['proposal_status']            = 'Статус заявки';
$lang['proposal_resolved']          = 'Заявка решена';
$lang['proposal_answer']            = 'Ответ';
$lang['msg_proposal_save']          = 'Ваша заявка записана. Отправлена на модерацию.';

//
$lang ['question_id'] = 'Идентификатор вопроса';    
$lang ['question_title'] = 'Вопрос'; 
$lang ['question_text'] = 'Ответ';  
$lang ['question_name'] = 'Ф.И.О.';  
//$lang ['question_phone'] = 'Ваш телефон'; 
//$lang ['question_email'] = 'Ваш e-mail'; 
$lang ['question_date'] = 'Дата регистрации';  
$lang ['question_status'] = 'Статус вопроса';
$lang ['question_resolved'] = 'Статус обработки';
//
$lang['contacts_hotline_title'] = 'Горячая линия Гуманитарного штаба Рината Ахметова:';
$lang['contacts_presscenter_title'] = 'Телефон пресс–служби Гуманитарного штаба Рината Ахметова:';
//
$lang['caption_description']    = 'Описание:';
$lang['caption_phone']          = 'Телефон:';
$lang['caption_email']          = 'Электронный адрес:';
$lang['button_save']            = "Сохранить";
$lang['caption_comments']       = "Комментарии";
$lang['caption_blogs']          = "Блоги";
$lang['caption_news']           = "Новости";
$lang['caption_links']          = "Ссылки";
$lang['caption_callme']         = "Помогите связаться";
$lang['caption_callme_subtitle']= "Мы поможем разместить Ваше объявление на нашем сайте и оповестим о нем наших партнеров.";
$lang['caption_all_matherial']  = "все материалы";
$lang['caption_search']         = "поиск";
$lang['caption_video']          = "Видео";
$lang['caption_text_maps']      = "Гуманитарная карта <br> ситуации и потребностей <br> Донецкой и Луганской областей";
$lang['caption_proposal']       = "База данных";
$lang['button_proposal_add']    = "Добавить заявку";
$lang['caption_question']       = "Вопросы и ответы";
$lang['button_question_add']    = "Добавить вопрос";
$lang['button_registartion']    = "Регистрация";
$lang['button_reminder']        = "Забыли пароль?";
//
$lang['msg_nodata']             = "Данные отсутствуют!";
$lang['msg_mission_dtv']        = '<strong>Миссия ДТВ</strong> – через мультимедийные специальные и информационные проекты, истории конкретных людей, попавших в беду и нуждающихся в помощи, – содействовать решению гуманитарных проблем страны.';
$lang['msg_search_news']        = 'Поиск в Новостях';
$lang['msg_search_blogs']       = 'Поиск в Блогах';
$lang['msg_search_video']       = 'Поиск в Видео'; 
$lang['msg_search_result']      = 'Найдено <strong>«%s»</strong> записей.'; 
//
$lang['ph_inputname']           = 'Введите Ваше имя';
$lang['ph_inputcomments']       = 'Введите Ваш комментарий';
$lang['ph_inputcaptcha']        = 'Введите код с картинки';
$lang['ph_putcomments']         = 'Оставить комментарий';
$lang['ph_putsearch']           = 'Провести поиск';
//
$lang['error_captcha']          = 'Неверно введенный код с картинки.';
$lang['error_empty']            = 'Поле "%s" не может быть пустым';
$lang['error_no_permitison']    = 'Вы не имеете прав доступа к этому элементу';
 * 
 */