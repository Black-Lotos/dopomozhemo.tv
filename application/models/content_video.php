<?php
    class Content_video extends MY_Model {
        function __construct(){
            parent::__construct();
        }
        //
        function StartUp() {
            $this->inTblName            = 'tb_content';
            $this->inTblCategoryName    = 'tb_content_category';
            $this->inTblUnion           = 'v_category_to_content';
            $this->inCountRec           = 0;
            $this->inOrderFields        = 'content_id';
            $this->inDebug              = false;
            $this->inStatus             = 'content_status'; 
            $this->inStatusMain         = 'content_status_main'; 
            $this->inStatusTop          = 'content_status_top';
            $this->inCategoryStatus     = 'ccontent_status';
            $this->inAlias              = '';
            $this->inSelfId             = 'content_id'; 
            $this->inSelfName           = 'content_title'; 
            $this->inCategoryId         = 'ccontent_id';
            $this->inCategoryName       = 'ccontent_title';
            $this->inSufix              = 'content'; 
            $this->inScriptUrl          = "/admin/admin_content_video";
        }
        //
        public function getTabs($aPage=1) {
            $outResult = $this->inTabs;
            $outResult['property'] = array('template' => 'administration/common/list-start-up-02.twig','title'=>'Работа с видео','include_js'=>'info.content.js');
            $outResult['data']['list'] = array('title' => 'Список видео','url'=> "{$this->inScriptUrl}/load?page={$aPage}");
            $outResult['data']['add'] = array('title' => 'Добавить видео','url'=> "{$this->inScriptUrl}/add");
            $outResult['data']['add_section'] = array('title' => 'Добавить раздел видео','url'=> "{$this->inScriptUrl}/add_section");
            $outResult['data']['default'] = $outResult['data']['list']['url'];//file_get_contents(base_url().$outResult['data']['list']['url']);
            return $outResult;
        }
        //
        public function loadCategoryElemntByAlias($aAlias='alias_down_slider_video',$aFilter=array()) {
            $outWhere = "and ccontent_alias = '{$aAlias}'"; $outFields = '*';
            if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
                $outFields = implode(',', $aFilter['fields']);
                unset($aFilter['fields']);
            }
            
            if (isset($aFilter['status'])) {
                $outWhere .= " and ccontent_status = '" . $aFilter['status'] . "'";
            } else {
                if (!empty($aFilter) &&  is_array($aFilter)) {
                    foreach ($aFilter as $outKey=>$outData) {
                        $outWhere .= " and  {$outKey} = '{$outData}'";
                    }
                }
            }
            $inSql = "SELECT {$outFields} FROM {$this->inTblUnion} where 1 {$outWhere}";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData;
            }
            return false;
        }
        //
        public function getForm($aData=array(), $aProcess=null) {
            
            $outResult = array (    
                'form_property'=>array('name'=>"frm{$this->inSufix}-add",'method'=>'post','action'=>"/admin/admin_content_video/save",'include_js'=>"info.{$this->inSufix}.js"),
                'form_data'=>array(
                    array('caption'=>'Идентификатор видео','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>"{$this->inSufix}_id",
                        'set_value'=>(!empty($aData["{$this->inSufix}_id"])?$aData["{$this->inSufix}_id"]:''))),
                    array('caption'=>'Категория видео','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select','set_name'=>'ccontent_id',
                        'set_value'=>(!empty($aData['ccontent_id'])?$aData['ccontent_id']:''))),
                    array('caption'=>'Язык','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                                    
                    array('caption'=>'Заголовок видео','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_title",
                        'set_value'=>(!empty($aData["{$this->inSufix}_title"])?$aData["{$this->inSufix}_title"]:''))),
                    array('caption'=>'','to_control'=>
                        array(  'set_type'=>'include_image',
                                'set_css'=>'image_preview','set_value'=>(!empty($aData["images_id"])?$aData["images_id"]:0),
                                'set_for'=>"content_id_".(!empty($aData['content_id'])?$aData['content_id']:0)
                                )),
                    array('caption'=>'Текст видео','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'textarea','set_name'=>"{$this->inSufix}_text",
                        'set_value'=>(!empty($aData["{$this->inSufix}_text"])?$aData["{$this->inSufix}_text"]:''))),
                    array('caption'=>'Видео файл','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'file','set_name'=>"{$this->inSufix}_file",
                        'set_value'=>(!empty($aData["{$this->inSufix}_file"])?$aData["{$this->inSufix}_file"]:''))),            
                    array('caption'=>'Видео файл ссылка','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text','set_name'=>"{$this->inSufix}_link",
                        'set_value'=>(!empty($aData["{$this->inSufix}_link"])?$aData["{$this->inSufix}_link"]:''))),                        
                    /*array('caption'=>'Ширина видео','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_width",
                        'set_value'=>(!empty($aData["{$this->inSufix}_width"])?$aData["{$this->inSufix}_width"]:''))),            
                    array('caption'=>'Высота видео','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_height",
                        'set_value'=>(!empty($aData["{$this->inSufix}_height"])?$aData["{$this->inSufix}_height"]:''))),           */
                    /*array('caption'=>'Код втраивания','to_control'=>array('set_readonly'=>'*','set_require'=>'','set_type'=>'text','set_name'=>"{$this->inSufix}_code",
                        'set_value'=>(!empty($aData["{$this->inSufix}_code"])?$aData["{$this->inSufix}_code"]:''))),                                    */
                    array('caption'=>'Активировать видео','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'content_status',
                        'set_value'=>(!empty($aData["{$this->inSufix}_status"])?$aData["{$this->inSufix}_status"]:''))),
                    array('caption'=>'На главной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inSufix}_status_main",
                        'set_value'=>(!empty($aData["{$this->inSufix}_status_main"])?$aData["{$this->inSufix}_status_main"]:''))),
                    array('caption'=>'В заголовке','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inSufix}_status_top",
                        'set_value'=>(!empty($aData["{$this->inSufix}_status_top"])?$aData["{$this->inSufix}_status_top"]:''))),
                    array('caption'=>'Дата создания','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'content_creates','set_value'=>(!empty($aData['content_creates'])?$aData['content_creates']:date('Y-m-d H:i:s',time())))),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'button-base green','set_url'=>'')),        
                    //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
                )
            );
            if ($aProcess==PROCESS_CONTENT_SECTION) {
                $outResult['form_property']=array('name'=>"frm{$this->inSufix}-add",'method'=>'post','action'=>"/admin/admin_content_video/save",'include_js'=>"info.content.js");
                $outResult['form_data'] = array(
                    array(  'caption'=>'Идентификатор видео','set_css'=>"text-pos-left text-cl-black",
                            'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"c{$this->inSufix}_id",
                                                'set_value'=>(!empty($aData["c{$this->inSufix}_id"])?$aData["c{$this->inSufix}_id"]:''))),
                    array(  'caption'=>'Название категории','set_css'=>"text-pos-left text-cl-black",
                            'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"c{$this->inSufix}_title",
                            'set_value'=>(!empty($aData["c{$this->inSufix}_title"])?$aData["c{$this->inSufix}_title"]:''))),
                    array('caption'=>'Язык','to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                                        
                    array(  'caption'=>'Алиас категории','set_css'=>"text-pos-left text-cl-black",
                            'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"c{$this->inSufix}_alias",
                            'set_value'=>(!empty($aData["c{$this->inSufix}_alias"])?$aData["c{$this->inSufix}_alias"]:''))),                
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'button-base green','set_url'=>'')),                        
                );
            }
            
            return  $outResult;
        }
        //
        //
         public function getOutput($aProcess=null) {
            $outResult = array (    
                'property'=>array('title'=>  getCaptionInput('caption_video'),'isRun'=>true,'include_js'=>"info-{$this->inSufix}.js",'template'=>'pieses/pieses-content-video.twig'),
                'info'=>array('count_record'=>$this->Content_video->loadCountRecord()),        
                'titles'=>array(),
                'data'=>array()
            );
            return  $outResult;
        }
        //
    }