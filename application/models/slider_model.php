<?php
    class Slider_model extends MY_Model {
        function __construct(){
            parent::__construct();
        }
        //
        protected function StartUp() {
            $this->inTblName = 'tb_pictures';       
            $this->inTblCategoryName = '';
            $this->inTblUnion = ''; 
            $this->inCountRec = 0;
            $this->inStatus = 'picture_status';    
            $this->inStatusMain='picture_main';    
            $this->inStatusTop='picture_top';    
            $this->inCategoryStatus = '';
            $this->inSelfId = '';        
            $this->inSelfName = '';
            $this->inCategoryId='';        
            $this->inCategoryName = ''; 
            $this->inSufix='picture_';              
            $this->inPrefix='picture_';              
            $this->inOrderFields='news_creates';
            $this->inDebug              = false;
            $this->inAlias              = '';
        }
        //
        public function getForm($aData=array()){
            $outResult = array (    
                'form_property'=>array('name'=>"frm{$aData}add",'method'=>'post','action'=>"/administration/admin_content_video/save",'include_js'=>'info-images.js'),
                'form_data'=>array(
                    array('caption'=>'Идентификатор картинки','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"{$this->inIdPrefix}id",
                        'set_value'=>(!empty($aData["{$this->inPrefix}id"])?$aData["{$this->inIdPrefix}id"]:''))),
                    array('caption'=>'Заголовок картинки','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inIdPrefix}title",
                        'set_value'=>(!empty($aData["{$this->inIdPrefix}title"])?$aData["{$this->inIdPrefix}title"]:''))),
                    array('caption'=>'URL картинки','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inIdPrefix}url",
                        'set_value'=>(!empty($aData["{$this->inIdPrefix}url"])?$aData["{$this->inIdPrefix}url"]:''))),            
                    array('caption'=>'Активировать картинку','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inIdPrefix}status",
                        'set_value'=>(!empty($aData["{$this->inIdPrefix}status"])?$aData["{$this->inIdPrefix}status"]:''))),
                    array('caption'=>'На главной','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inIdPrefix}main",
                        'set_value'=>(!empty($aData["{$this->inIdPrefix}main"])?$aData["{$this->inIdPrefix}main"]:''))),
                )
            );
            return $outResult;
        }
        //
        public function getOutput($aData=array()) {
            $outResult = array (    
                'property'=>array('isRun'=>true,'include_js'=>'info-images.js','template'=>'common/top-carusel-pictures.twig'),
                'titles'=>array(
                    array('title'=>'ID','size'=>20),
                    array('title'=>'Заголовок картинки','size'=>0),
                    array('title'=>"Активно",'size'=>80),
                    array('title'=>"На главной",'size'=>80),
                    array('title'=>'Действия','size'=>80)
                ),
                'data'=>array("{$this->inPrefix}id","{$this->inPrefix}title","{$this->inPrefix}status","{$this->inPrefix}main","action")        
            );
            return  $outResult;
        }
    }