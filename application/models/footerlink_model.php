<?php
class Footerlink_model extends CI_Model {
    private $inTblName = 'tb_footerlink';
    private $inStatus = 'footerlink_status', $inStatusMain='footerlink_main', $inStatusTop='footerlink_top';
    private $inSelfId = 'footerlink_id', $inCategoryId='cfooterlink_id';
    private $inSufix='footerlink';
    private $inOrderFields='cfooterlink_id, footerlink_id';
    private $inTblCatName = 'tb_footerlink_category';
    private $inTblUnion = 'v_category_to_footerlink';
    private $inCountRec = 0;
    function __construct(){
        parent::__construct();
    }
    //
    public function setCountRecord($aValue) {
        $this->inCountRec = $aValue;
    }
    //
    public function loadCountPage($aRecordByPage) {
            $outResult = $this->load();
            $outResult = count($outResult);
            return ceil($outResult/$aRecordByPage);
            //(($outResult % $aRecordByPage)==0)?1:($outResult % $aRecordByPage);
        }
    //
    public function load($aFilter=array(),$aUnion=false, $aPage=0, $aCountRec=26) {
        //echo "<pre>"; var_dump($aFilter); die();
        $outWhere = ''; $outFields = '*'; $inOrder='';
        if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
            $outFields = implode(',', $aFilter['fields']);
            unset($aFilter['fields']);
        }

        if (isset($aFilter['status'])) {
            $outWhere .= " and footerlink_status = '" . $aFilter['status'] . "'";
        } else {
            if (!empty($aFilter) &&  is_array($aFilter)) {
                foreach ($aFilter as $outKey=>$outData) {
                    $outWhere .= " and  {$outKey} = '{$outData}'";
                }
            }
        }
        $inTable = (($aUnion)?$this->inTblUnion:$this->inTblName);
        if ($this->inCountRec>0) {
            $inOrder = "order by {$this->inOrderFields} DESC limit {$this->inCountRec}";
        } 
        if ($aPage>0) {
            $inOrder = "order by {$this->inOrderFields} limit ".(($aPage-1)*$aCountRec).",{$aCountRec}";
        }

        $inSql = "SELECT {$outFields} FROM {$inTable} where 1 {$outWhere} {$inOrder}";
        //var_dump($inSql); die();
        $outData = $this->db->query($inSql)->result_array();
        $this->inCountRec=0;
        if (!empty($outData)) {
            return $outData;
        }
        return false;
    }
    //
    public function loadById($aId,$aFilter=array()) {
        $outWhere = ''; $outFields = '*';
        if (!empty($aId)) {
            $outWhere .= " and {$this->inSelfId} = '" . $aId . "'";
        }
        $inSql = "SELECT {$outFields} FROM {$this->inTblName} where 1 {$outWhere}";
        //var_dump($inSql); die();
        $outData = $this->db->query($inSql)->result_array();
        if (!empty($outData)) {
            return $outData[0];
        }
        return false;
    }
    //
    public function loadCategory($aFilter=array()) {
        $outWhere = ''; $outFields = '*';
        if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
            $outFields = implode(',', $aFilter['fields']);
            unset($aFilter['fields']);
        }

        if (isset($aFilter['status'])) {
            $outWhere .= " and cfooterlink_status = '" . $aFilter['status'] . "'";
        } else {
            if (!empty($aFilter) &&  is_array($aFilter)) {
                foreach ($aFilter as $outKey=>$outData) {
                    $outWhere .= " and  {$outKey} = '{$outData}'";
                }
            }
        }

        $inSql = "SELECT {$outFields} FROM {$this->inTblCatName} where 1 {$outWhere}";
        //var_dump($inSql); die();
        $outData = $this->db->query($inSql)->result_array();
        if (!empty($outData)) {
            return $outData;
        }
        return false;
    }
    //
    public function loadCategoryById($aId, $aFilter=array()) {
        $outWhere = ''; $outFields = '*';
        if (!empty($aId)) {
            $outWhere .= " and {$this->inCategoryId} = '" . $aId . "'";
        }
        $inSql = "SELECT {$outFields} FROM {$this->inTblCatName} where 1 {$outWhere}";
        //var_dump($inSql); die();
        $outData = $this->db->query($inSql)->result_array();
        if (!empty($outData)) {
            return $outData[0];
        }
        return false;
    }
    //
    public function loadTree($aFilter=array(),$aUnion=false,$aPage=0,$aCountRec=12) {
        //echo "<pre>"; var_dump($aFilter); die();
        $inFieldsCategory = $aFilter['category'];
        $inFieldsfooterkink = $aFilter['footerlink'];
        
        $outCategory = $this->loadCategory($inFieldsCategory);
        //echo "<pre>"; var_dump($outCategory); die();
        
        foreach ($outCategory as $outKey=>$outData) {
            $outData['sub_tree'] = $this->load(array('cfooterlink_id'=>$outData['value'],'fields'=>$inFieldsfooterkink['fields']),$aUnion,$aPage,$aCountRec);
            $outCategory[$outKey] = $outData;
        }
        //echo "<pre>"; var_dump($outCategory); die();
        return $outCategory;
    }
    //public function getForm(){}
    //
    public function getForm($aData=array(), $aProcess=null) {
        $outResult = array (    
            'form_property'=>array('name'=>"frm{$this->inSufix}-add",'method'=>'post','action'=>"/administration/admin_footerlink/save",'include_js'=>"info-{$this->inSufix}.js"),
            'form_data'=>array(
                array('caption'=>'Идентификатор ссылки','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"{$this->inSufix}_id",
                    'set_value'=>(!empty($aData["{$this->inSufix}_id"])?$aData["{$this->inSufix}_id"]:''))),
                array('caption'=>'Категория ссылок','to_control'=>array('set_type'=>'select','set_name'=>'cfooterlink_id',
                    'set_value'=>(!empty($aData["cfooterlink_id"])?$aData["cfooterlink_id"]:''))),
                array('caption'=>'Язык','to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                    'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),            
                array('caption'=>'Заголовок ссылки','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_title",
                    'set_value'=>(!empty($aData["{$this->inSufix}_title"])?$aData["{$this->inSufix}_title"]:''))),
                array('caption'=>'Текст ссылки','to_control'=>array('set_require'=>'*','set_type'=>'textarea','set_name'=>"{$this->inSufix}_text",
                    'set_value'=>(!empty($aData["{$this->inSufix}_text"])?$aData["{$this->inSufix}_text"]:''))),
                array('caption'=>'Url ссылки','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_url",
                    'set_value'=>(!empty($aData["{$this->inSufix}_url"])?$aData["{$this->inSufix}_url"]:''))),            
                array('caption'=>'Активировать ссылку','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'footerlink_status',
                    'set_value'=>(!empty($aData["{$this->inSufix}_status"])?$aData["{$this->inSufix}_status"]:''))),
                /*array('caption'=>'Дата создания',
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'footerlink_creates','set_value'=>(!empty($aData['footerlink_creates'])?$aData['footerlink_creates']:date('Y-m-d H:i:s',time())))),*/
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSave{$this->inSufix}",'set_css'=>'button-base green','set_url'=>'#')),        
                //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
            )
        );
        if ($aProcess==PROCESS_FOOTERLINK_SECTION) {
            $outResult['form_property']['action']="/administration/admin_footerlink/save_category";
            $outResult['form_data'] = array(
                array(  'caption'=>'Идентификатор блога',
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"c{$this->inSufix}_id",
                                            'set_value'=>(!empty($aData["c{$this->inSufix}_id"])?$aData["c{$this->inSufix}_id"]:''))),
                array('caption'=>'Язык','to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                    'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                                                
                array(  'caption'=>'Название категории',
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"c{$this->inSufix}_title",
                        'set_value'=>(!empty($aData["c{$this->inSufix}_title"])?$aData["c{$this->inSufix}_title"]:''))),
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSaveСfooterlink",'set_css'=>'button-base green','set_url'=>'#')),                        
            );
        }
        return  $outResult;
    }
    //
    public function getOutput($aProcess=null) {
        $outResult = array (    
            'property'=>array('title'=>getCaptionInput('caption_links'),'isRun'=>true,'include_js'=>"info-{$this->inSufix}.js",'template'=>'footerlink/footerlink-start-up.twig','to_site'=>'footerlink/footerlink-to-site.twig'),
            'titles'=>array(),
            'data'=>array()
        );
        switch ($aProcess) {
            case OUTPUT_LIST:
                $outResult['data']= $this->load(array(), true);
                $outResult['info']['count_record']=count($outResult['data']);
                foreach ($outResult['data'] as $inKey=>$inData) {
                    if (empty($inData['footerlink_creates'])) {
                        unset($outResult['data'][$inKey]);
                    }
                    else {
                        $inData['footerlink_creates'] = getDecodeDate($inData['footerlink_creates']);
                        if (mb_strlen($inData['footerlink_text'])>OUT_CUT_STRING) {
                            //die("yes");
                            $inData['cut_text'] =  mb_substr($inData['footerlink_text'], 0, OUT_CUT_STRING).'...';
                            $inData['detail'] = true;
                        }
                        $outResult['data'][$inKey] = $inData;
                    }
                }
                break;
            default :
                $outResult['titles'] = array (    
                    array(
                        array('title'=>'ID','size'=>20),
                        array('title'=>'Заголовок блога','size'=>0),
                        array('title'=>"Статус",'size'=>80),
                        array('title'=>'Действия','size'=>80)
                    ),
                    'data'=>array($this->inSelfId,"{$this->inSufix}_title","{$this->inSufix}_status",'action')        
                );
        }
        return  $outResult;
    }
    //
    public function save($aData) {
        $outRes = false;
        if (empty($aData[$this->inSelfId])) {
            if (isset($aData[$this->inSelfId])) unset($aData[$this->inSelfId]);
            $inSql = "insert into {$this->inTblName}";
            $inField = array(); $inOutData = array();
            foreach ($aData as $inKey => $inData) {
                $inField[] = $inKey;
                $inOutData[] = "'".$inData."'";
            }
            $inSql .= "(".implode(',', $inField).") values (".implode(',', $inOutData).")";
            //var_dump($inSql); //die();
            $outRes = $this->db->insert($this->inTblName, $aData); 
        } else  {
                    $this->db->where($this->inSelfId, $aData[$this->inSelfId]);
                    $outRes = $this->db->update($this->inTblName, $aData); 
                }
        return  array('rec-no'=>$outRes);       
    }
    //
    public function save_category($aData) {
        $outRes = false;
        if (empty($aData[$this->inCategoryId])) {
            $inSql = "insert into {$this->inTblCatName}";
            $inField = array(); $inOutData = array();
            foreach ($aData as $inKey => $inData) {
                $inField[] = $inKey;
                $inOutData[] = "'".$inData."'";
            }
            $inSql .= "(".implode(',', $inField).") values (".implode(',', $inOutData).")";
            var_dump($inSql); //die();
            $outRes = $this->db->insert($this->inTblCatName, $aData); 
        } else  {
                    $this->db->where($this->inCategoryId, $aData[$this->inCategoryId]);
                    $outRes = $this->db->update($this->inTblCatName, $aData); 
                }
        return  array('rec-no'=>$outRes);       
    }
    //
    public function outOneRecord($aData) {

    }
    //
    public function DeleteById($aId,$aFilter=array()) {
        $outWhere = ''; $outFields = '*';
        if (!empty($aId)) {
            $outWhere .= " and {$this->inSelfId} = '" . $aId . "'";
            $inSql = "delete FROM {$this->inTblName} where 1 {$outWhere}";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql);
            var_dump($outData);
            if (!empty($outData)) {
                return $outData;
            }
        }
        return false;
    }
}