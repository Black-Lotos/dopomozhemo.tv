<?php
    class Professional_question_model extends MY_Model {
        public function StartUp() {
            $this->inTblName            = 'tb_professional_question';
            $this->inTblCategoryName    = 'tb_professional';
            $this->inTblUnion           = '';
            $this->inOrderFields        = 'question_date';
            $this->inStatus             = 'question_status'; 
            $this->inSelfId             = 'question_id'; 
            $this->inSelfName           = 'question_text';
            $this->inCategoryId         = 'proffessional_id';
            $this->inCategoryName       = 'proffessional_title';
            $this->inCategoryStatus     = 'proffessional_status';
            $this->inSufix              = 'question';
            $this->inPrefix             = 'question';
            $this->inCategorySufix      = 'proffessional';
            $this->inCategoryPrefix     = 'proffessional';
            $this->inOrderType          = 'DESC';
        }
        public function getForm($aData=array(), $aProcess=null) {
            $outResult = array (    
                'form_property'=>array('name'=>"frm{$this->inSufix}-add",'method'=>'post','action'=>"/administration/admin_question/save",'include_js'=>"info-{$this->inSufix}.js"),
                'form_data'=>array(
                    array('caption'=>getCaptionInput("{$this->inSufix}_id"),'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"{$this->inSufix}_id",
                        'set_value'=>(!empty($aData["{$this->inSufix}_id"])?$aData["{$this->inSufix}_id"]:''))),
                    array('caption'=>getCaptionInput("professional_id"),'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"professional_id",
                        'set_value'=>(!empty($aData["professional_id"])?$aData["professional_id"]:''))),            
                    array('caption'=>getCaptionInput("language_id"),'to_control'=>array('set_type'=>'text','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                                    
                    array('caption'=>getCaptionInput("{$this->inSufix}_title"),'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_title",
                        'set_value'=>(!empty($aData["{$this->inSufix}_title"])?$aData["{$this->inSufix}_title"]:''))),
                    array('caption'=>getCaptionInput("{$this->inSufix}_text"),'to_control'=>array('set_require'=>'*','set_type'=>'textarea','set_name'=>"{$this->inSufix}_text",
                        'set_value'=>(!empty($aData["{$this->inSufix}_text"])?$aData["{$this->inSufix}_text"]:''))),
                    /*array('caption'=>'Аватар заявки','to_control'=>array('set_require'=>'*','set_type'=>'file','set_name'=>"{$this->inSufix}_avatar",
                        'set_value'=>(!empty($aData["{$this->inSufix}_file"])?$aData["{$this->inSufix}_avatar"]:''))),                        */
                    array('caption'=>getCaptionInput("{$this->inSufix}_name"),'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_name",
                        'set_value'=>(!empty($aData["{$this->inSufix}_name"])?$aData["{$this->inSufix}_name"]:''))),
                    array('caption'=>getCaptionInput("caption_phone"),'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_phone",
                        'set_value'=>(!empty($aData["caption_phone"])?$aData["{$this->inSufix}_phone"]:''))),            
                    array('caption'=>getCaptionInput("caption_email"),'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_email",
                        'set_value'=>(!empty($aData["{$this->inSufix}_email"])?$aData["{$this->inSufix}_email"]:''))),                        
                    /*array('caption'=>'Активировать заявку','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'question_status',
                        'set_value'=>(!empty($aData["{$this->inSufix}_status"])?$aData["{$this->inSufix}_status"]:''))),            */
                    array('caption'=>getCaptionInput("{$this->inSufix}_date"),
                        'to_control'=>array('set_readonly'=>'*','set_require'=>'*','set_type'=>'text','set_name'=>'question_date','set_value'=>(!empty($aData['question_date'])?$aData['question_date']:date('Y-m-d H:i:s',time()))))        
                    )
            );
            switch($aProcess) {
                case PROCESS_USER_APPEND:
                    $outResult['form_data']=array(
                    array('caption'=>getCaptionInput("{$this->inSufix}_id"),'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"{$this->inSufix}_id",
                        'set_value'=>(!empty($aData["{$this->inSufix}_id"])?$aData["{$this->inSufix}_id"]:''))),
                    array('caption'=>getCaptionInput("proffessional_id"),'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"proffessional_id",
                        'set_value'=>(!empty($aData["proffessional_id"])?$aData["proffessional_id"]:''))),            
                    array('caption'=>getCaptionInput("language_id"),'to_control'=>array('set_type'=>'hidden','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                                    
                    array('caption'=>getCaptionInput("{$this->inSufix}_title"),'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_title",
                        'set_value'=>(!empty($aData["{$this->inSufix}_title"])?$aData["{$this->inSufix}_title"]:''))),
                    /*array('caption'=>getCaptionInput("{$this->inSufix}_text"),'to_control'=>array('set_require'=>'*','set_type'=>'textarea','set_name'=>"{$this->inSufix}_text",
                        'set_value'=>(!empty($aData["{$this->inSufix}_text"])?$aData["{$this->inSufix}_text"]:''))),
                    /*array('caption'=>'Аватар заявки','to_control'=>array('set_require'=>'*','set_type'=>'file','set_name'=>"{$this->inSufix}_avatar",
                        'set_value'=>(!empty($aData["{$this->inSufix}_file"])?$aData["{$this->inSufix}_avatar"]:''))),                        */
                    array('caption'=>getCaptionInput("{$this->inSufix}_name"),'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_name",
                        'set_value'=>(!empty($aData["{$this->inSufix}_name"])?$aData["{$this->inSufix}_name"]:''))),
                    array('caption'=>getCaptionInput("caption_phone"),'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_phone",
                        'set_value'=>(!empty($aData["caption_phone"])?$aData["{$this->inSufix}_phone"]:''))),            
                    array('caption'=>getCaptionInput("caption_email"),'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_email",
                        'set_value'=>(!empty($aData["{$this->inSufix}_email"])?$aData["{$this->inSufix}_email"]:''))),                        
                    /*array('caption'=>'Активировать заявку','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'question_status',
                        'set_value'=>(!empty($aData["{$this->inSufix}_status"])?$aData["{$this->inSufix}_status"]:''))),            */
                    array('caption'=>getCaptionInput("{$this->inSufix}_date"),
                        'to_control'=>array('set_readonly'=>'*','set_require'=>'*','set_type'=>'hidden','set_name'=>'question_date','set_value'=>(!empty($aData['question_date'])?$aData['question_date']:date('Y-m-d H:i:s',time())))),
                        array('caption'=>getCaptionInput("button_save"),'to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'add-article-link pull-left','set_url'=>'#')),        
                    );
                break;
                case PROCESS_BLOGS_SECTION:
                    $outResult['form_property']['action']="/administration/admin_question/save_category";
                    $outResult['form_data'] = array(
                    array(  'caption'=>'Идентификатор заявки',
                            'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"c{$this->inSufix}_id",
                                                'set_value'=>(!empty($aData["c{$this->inSufix}_id"])?$aData["c{$this->inSufix}_id"]:''))),
                    array(  'caption'=>'Название категории',
                            'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"c{$this->inSufix}_title",
                            'set_value'=>(!empty($aData["c{$this->inSufix}_title"])?$aData["c{$this->inSufix}_title"]:''))),
                    array('caption'=>'Язык','to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                                        
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSaveСProposal",'set_css'=>'button-base green','set_url'=>'#')),                        
                );
                break;
                default:
                    $outResult['form_data'][] = 
                        array('caption'=>"Вопрос решен",'to_control'=>array('set_type'=>'checkbox','set_name'=>"{$this->inSufix}_resolved"));        
                        
                    $outResult['form_data'][] = 
                        array('caption'=>getCaptionInput("button_save"),'to_control'=>array('set_type'=>'a','set_name'=>"btnSave{$this->inSufix}",
                            'set_css'=>'button-base green','set_url'=>'#'));        
                        
                break;
            }    
            return  $outResult;
        }
        //
        public function getOutput($aProcess=null) {
            $outResult = array (    
                'property'=>array('title'=>getCaptionInput('caption_question'),'isRun'=>true,'include_js'=>"info-{$this->inSufix}.js",'template'=>'question/question-start-up.twig'),
                'titles'=>array(),
                'data'=>array()
            );
            switch ($aProcess) {
                case OUTPUT_LIST:
                    $outResult['data']= $this->load(array());
                    $outResult['info']['count_record']=count($outResult['data']);
                    if($outResult['data']) {
                        foreach ($outResult['data'] as $inKey=>$inData) {
                            if (empty($inData['question_date'])) {
                                unset($outResult['data'][$inKey]);
                            }
                            else {
                                $inData['question_date'] = getDecodeDate($inData['question_date']);
                                if (mb_strlen($inData['question_text'])>OUT_CUT_STRING) {
                                    //die("yes");
                                    $inData['cut_text'] =  mb_substr($inData['question_text'], 0, OUT_CUT_STRING).'...';
                                    $inData['detail'] = true;
                                }
                                $outResult['data'][$inKey] = $inData;
                            }
                        }
                    }
                    break;
                default :
                    $outResult['titles'] = array (    
                        array(
                            array('title'=>'ID','size'=>20),
                            array('title'=>'Заголовок заявки','size'=>0),
                            array('title'=>"Статус",'size'=>80),
                            array('title'=>'Действия','size'=>80)
                        ),
                        'data'=>array($this->inSelfId,"{$this->inSufix}_title","{$this->inSufix}_status",'action')        
                    );
            }
            return  $outResult;
        }
    }