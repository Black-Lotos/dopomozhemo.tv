<?php
class Expert_help_model extends MY_Model {
    function __construct(){
            parent::__construct();
        }
    //
    protected function StartUp() {
        $this->inTblName            = 'tb_professional';
        $this->inTblCategoryName    = 'tb_professional_category';
        $this->inTblUnion           = '';
        $this->inStatus             = 'proffessional_status'; 
        $this->inStatusMain         = ''; 
        $this->inStatusTop          = '';
        $this->inAlias              = '';
        $this->inSelfId             = 'proffessional_id'; 
        $this->inSelfName           = 'proffessional_title';
        $this->inCategoryId         = 'p_category_id';
        $this->inCategoryName       = 'p_category_title';
        $this->inCategoryStatus     = 'p_category_status';
        $this->inSufix              = 'proffessional';
        $this->inPrefix             = 'proffessional';
        $this->inCategorySufix      = 'p_category';
        $this->inCategoryPrefix     = 'p_category';
        $this->inTblUnion           = 'v_category_to_proffessional';
        $this->inCountRec           = 0;
        $this->inOrderFields        = "$this->inCategoryId, $this->inSelfId";
        $this->inScriptUrl          = '/admin/admin_expert_help';
    }
    //
    public function getTabs($aPage=1,$aParam=null) {
        $outResult = $this->inTabs;
        //var_dump($aParam); 
        $outMenu = !empty($aParam)?"/$aParam/":"";
        $outResult['property']= array('template'=>'administration/common/list-start-up-02.twig','title'=>'Список специалистов по категориям','include_js'=>'info.menu.js');
        if (empty($aParam)) {
            $outResult['data']['list'] = array('title' => 'Список категорий','target'=>'pnl-list','url'=> "{$this->inScriptUrl}/load{$outMenu}?page={$aPage}");
            //$outResult['data']['add'] = array('title' => 'Добавить специалиста','target'=>'pnl-item','url'=> "{$this->inScriptUrl}/add");
            $outResult['data']['add_section'] = array('title' => 'Добавить категорию','target'=>'pnl-item','url'=> "{$this->inScriptUrl}/add_section");
            
        } else  {
             //die("TYT");
                //$this->Debug();
                $inCategory = $this->loadCategoryById($aParam, array('fields'=>array("$this->inCategoryName as title")));
                //var_dump($inCategory); die();
                $outResult['property']['title'] = $inCategory['title'];
                $outResult['data']['list'] = array('title' => $inCategory['title'],'target'=>'pnl-list','url'=> "{$this->inScriptUrl}/load{$outMenu}?page={$aPage}");
                $outResult['data']['add'] = array('title' => 'Добавить специалиста','target'=>'pnl-item','url'=>"{$this->inScriptUrl}/add/{$aParam}");
                }
        //var_dump("/administration/admin_menu/load{$outMenu}?page={$aPage}"); die();
        $outResult['data']['default'] = $outResult['data']['list']['url'];
        return $outResult;
    }
    //
    public function getOutput($aProcess=null) {
        $outResult = array (    
            'property'=>array('title'=>'Помощь специалиста','isRun'=>true,'include_js'=>"info.expert.js",'template'=>'expert_help/start-expert-help.twig'),
            'titles'=>array(),
            'data'=>array()
        );
        return $outResult;
    }
    //
    public function getForm($aData=array(), $aProcess=null) {
        $outResult = array (    
                'form_property'=>array('name'=>"frm{$this->inPrefix}",'method'=>'post','action'=>"/admin/admin_expert_help/save",'include_js'=>"info.expert.js"),
                'form_data'=>array(
                    array('caption'=>'Идентификатор помощника','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"{$this->inPrefix}_id",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_id"])?$aData["{$this->inPrefix}_id"]:''))),
                    array('caption'=>'Категория помощника','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>$this->inCategoryId,
                        'set_value'=>!empty($aData[$this->inCategoryId])?$aData[$this->inCategoryId]:'')),
                    array('caption'=>'Язык','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                        
                    array('caption'=>'Ф.И.О. помощника','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inPrefix}_title",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_title"])?$aData["{$this->inPrefix}_title"]:''))),
                    array('caption'=>'Описание помощника','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'textarea',"set_css"=>"input-skin-01",'set_name'=>"{$this->inPrefix}_text",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_text"])?$aData["{$this->inPrefix}_text"]:''))),
                    array('caption'=>'Фотография помощника','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'include_image',"set_css"=>"input-skin-01",'set_name'=>"images_id",
                        'set_value'=>(!empty($aData["images_id"])?$aData["images_id"]:0))),            
                    array('caption'=>'Активировать помощника','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox',"set_css"=>"input-skin-01",'set_name'=>"{$this->inPrefix}_status",
                        'set_value'=>(!empty($aData[$this->inStatus])?$aData[$this->inStatus]:''))),
                    array('caption'=>'Дата регистрации','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"{$this->inPrefix}_date",'set_value'=>(!empty($aData["{$this->inPrefix}_date"])?$aData["{$this->inPrefix}_date"]:date('Y-m-d H:i:s',time())))),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'button-base green','set_url'=>'')),        
                    //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
                )
            );
            if ($aProcess==PROCESS_SECTION_ADD) {
                $outResult['form_property']['action']="/admin/admin_expert_help/save_section";
                $outResult['form_data'] = array(
                    array(  'caption'=>'Идентификатор категории помощников','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"{$this->inCategoryId}",
                        'set_value'=>(!empty($aData[$this->inCategoryId])?$aData[$this->inCategoryId]:''))),
                    array('caption'=>'Язык категории помощников','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                            
                    array(  'caption'=>'Название категории помощников','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inCategoryName}",
                        'set_value'=>(!empty($aData[$this->inCategoryName]))?$aData[$this->inCategoryName]:'')),
                    array(  'caption'=>'Описание категории помощников','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'textarea',"set_css"=>"input-skin-01",'set_name'=>"{$this->inCategoryPrefix}_text",
                        'set_value'=>(!empty($aData["{$this->inCategoryPrefix}_text"]))?$aData["{$this->inCategoryPrefix}_text"]:'')),            
                    array('caption'=>'Сохранить','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'button-base red','set_url'=>'')),                        
                );
            }
            return  $outResult;
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

