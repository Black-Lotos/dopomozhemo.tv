<?php
class Blogs_model extends MY_Model {
    function __construct(){
        parent::__construct();
    }
    protected function StartUp() {
        $this->inTblName = 'tb_blogs';       
        $this->inTblCategoryName = 'tb_blogs_category';
        $this->inStatus = 'blogs_status';    
        $this->inStatusMain='blogs_main';    
        $this->inStatusTop='blogs_top';    
        $this->inCategoryStatus = 'cblogs_status';
        $this->inSelfId = 'blogs_id';       
        $this->inSelfName = 'blogs_title';
        $this->inCategoryId='cblogs_id';    
        $this->inCategoryName = 'cblogs_title'; 
        $this->inSufix='blogs';              
        $this->inTblUnion = 'v_category_to_blogs'; 
        $this->inOrderFields='blogs_creates';
        $this->inScriptUrl = '/admin/admin_blogs';
    }
    //
    public function getTabs($aPage=1) {
        $outResult = $this->inTabs;
        $outResult['property'] = array('template' => 'administration/common/list-start-up-02.twig','title'=>'Работа с Блогами','include_js'=>'info.blogs.js');
        $outResult['data']['list'] = array('title' => 'Список блогов','url'=> "{$this->inScriptUrl}/load?page={$aPage}");
        $outResult['data']['add'] = array('title' => 'Добавить блог','url'=> "{$this->inScriptUrl}/add");
        $outResult['data']['add_section'] = array('title' => 'Добавить раздел блогов','url'=> "{$this->inScriptUrl}/add_section");
        $outResult['data']['default'] = $outResult['data']['list']['url'];//file_get_contents(base_url().$outResult['data']['list']['url']);
        return $outResult;
    }
    //
    public function getForm($aData=array(), $aProcess=null) {
        $outResult = array (    
            'form_property'=>array('name'=>"frm{$this->inSufix}-add",'method'=>'post','action'=>"/admin/admin_blogs/save",'include_js'=>"info.{$this->inSufix}.js"),
            'form_data'=>array(
                array('caption'=>getVariable('blogs_id'),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"{$this->inSufix}_id",
                    'set_value'=>(!empty($aData["{$this->inSufix}_id"])?$aData["{$this->inSufix}_id"]:''))),
                array('caption'=>getVariable('cblogs_id'),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'select','set_name'=>'cblogs_id',
                    'set_value'=>(!empty($aData['cblogs_id'])?$aData['cblogs_id']:''))),
                array('caption'=>getVariable('language_id'),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                    'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                        
                array('caption'=>getVariable("{$this->inSufix}_name"),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_name",
                    'set_value'=>(!empty($aData["{$this->inSufix}_name"])?$aData["{$this->inSufix}_name"]:''))),            
                array('caption'=>getVariable("{$this->inSufix}_title"),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_length'=>'100','set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_title",
                    'set_value'=>(!empty($aData["{$this->inSufix}_title"])?$aData["{$this->inSufix}_title"]:''))),
                array('caption'=>'','to_control'=>
                        array(  'set_type'=>'include_image',
                                'set_css'=>'image_preview','set_value'=>(!empty($aData["images_id"])?$aData["images_id"]:0),
                                'set_for'=>"blogs_id_".(!empty($aData['blogs_id'])?$aData['blogs_id']:0)
                                )),
                array('caption'=>getVariable("{$this->inSufix}_text"),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'textarea','set_name'=>"{$this->inSufix}_text",
                    'set_value'=>(!empty($aData["{$this->inSufix}_text"])?$aData["{$this->inSufix}_text"]:''))),
                array('caption'=>getVariable("{$this->inSufix}_status"),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'blogs_status',
                    'set_value'=>(!empty($aData["{$this->inSufix}_status"])?$aData["{$this->inSufix}_status"]:''))),
                array('caption'=>getVariable("{$this->inSufix}_main"),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inSufix}_main",
                    'set_value'=>(!empty($aData["{$this->inSufix}_main"])?$aData["{$this->inSufix}_main"]:''))),
                array('caption'=>getVariable("{$this->inSufix}_top"),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inSufix}_top",
                    'set_value'=>(!empty($aData["{$this->inSufix}_top"])?$aData["{$this->inSufix}_top"]:''))),
                array('caption'=>getVariable('blogs_creates'),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'datepicker','set_name'=>'blogs_creates',
                    'set_extended'=>"input-date-time=true",
                    'set_value'=>(!empty($aData['blogs_creates'])?$aData['blogs_creates']:date('Y-m-d H:i:s',time())))),
                array('caption'=>"Сохранить",'to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'','set_url'=>'')),        
                //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
            )
        );
        if ($aProcess==PROCESS_BLOGS_SECTION) {
            $outResult['form_property']['action']="/admin/admin_blogs/save_category";
            $outResult['form_data'] = array(
                array(  'caption'=>'Идентификатор блога','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_readonly'=>'*','set_require'=>'*','set_type'=>'text','set_name'=>"c{$this->inSufix}_id",
                    'set_value'=>(!empty($aData["c{$this->inSufix}_id"])?$aData["c{$this->inSufix}_id"]:''))),
                array('caption'=>'Язык','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                    'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),            
                array('caption'=>'Название категории','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"c{$this->inSufix}_title",
                    'set_value'=>(!empty($aData["c{$this->inSufix}_title"])?$aData["c{$this->inSufix}_title"]:''))),
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'','set_url'=>'')),                        
            );
        };
        if ($aProcess==PROCESS_BLOGS_SECTION_ADD) {
            $outResult['form_property']['action']="/admin/admin_blogs/save_category";
            $outResult['form_data'] = array(
                array('caption'=>'Язык','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                    'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),            
                array('caption'=>'Название категории','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"c{$this->inSufix}_title",
                    'set_value'=>(!empty($aData["c{$this->inSufix}_title"])?$aData["c{$this->inSufix}_title"]:''))),
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'','set_url'=>'')),                        
            );
        }
        return  $outResult;
    }
    //
    public function getOutput($aProcess=null,$aData=array(),$aUnion=true,$aPage=1) {
        $outResult = array (    
            'property'=>array('title'=>getCaptionInput('caption_blogs'),'isRun'=>true,'include_js'=>"info.{$this->inSufix}.js",'template'=>'blogs/blogs-start-up.twig'),
            'info'=>array('count_record'=>$this->loadCountRecord()),                
            'titles'=>array(),
            'data'=>array()
        );
        return  $outResult;
    }
}