<?php
    class News_comments_model extends MY_Model {
        function __construct(){
            parent::__construct();
        }
        //
        protected function StartUp() {
            $this->inTblName = 'tb_comments';       
            $this->inTblCategoryName = 'tb_news';
            $this->inTblUnion = 'v_news_to_comments'; 
            $this->inCountRec           = 0;
            $this->inStatus = 'comments_status';    
            $this->inStatusMain='';    
            $this->inStatusTop='';    
            $this->inCategoryStatus = 'news_status';
            $this->inSelfId = 'comments_id';        
            $this->inSelfName = 'comments_text';
            $this->inCategoryId='news_id';        
            $this->inCategoryName = 'news_title'; 
            $this->inSufix='comments';              
            $this->inOrderFields='comments_created';
            $this->inDebug              = false;
            $this->inAlias              = '';
        }
        //
        public function getForm($aData=array(), $aProcess=null){
            $outResult = array (    
                'form_property'=>array('name'=>"frmNews-add{$this->inSufix}",'method'=>'post','action'=>"/administration/admin_news/save",'include_js'=>'info-news.js'),
                'form_data'=>array(
                    array('caption'=>'Идентификатор новости',
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'news_id','set_value'=>(!empty($aData['news_id'])?$aData['news_id']:''))),
                    array('caption'=>'Категория новости','to_control'=>array('set_type'=>'select','set_name'=>'nc_id',
                        'set_value'=>(!empty($aData['nc_id'])?$aData['nc_id']:''))),
                    array('caption'=>'Язык','to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),            
                    array('caption'=>'Заголовок новости',
                        'to_control'=>array('set_length'=>100,'set_require'=>'*','set_type'=>'text','set_name'=>'news_title','set_value'=>(!empty($aData['news_title'])?$aData['news_title']:''))),
                    array('caption'=>'','to_control'=>
                        array('set_type'=>'include_image',
                                'set_css'=>'image_preview','set_value'=>(!empty($aData["images_id"])?$aData["images_id"]:0),
                                'set_for'=>"news_id_".(!empty($aData['news_id'])?$aData['news_id']:0)
                                )),
                    array('caption'=>'Текст новости',
                        'to_control'=>array('set_require'=>'*','set_type'=>'textarea','set_name'=>'news_text','set_value'=>(!empty($aData['news_text'])?$aData['news_text']:''))),
                    array('caption'=>'Присоединить коллекцию','to_control'=>array('set_type'=>'select','set_name'=>'cpicture_id',
                        'set_value'=>(!empty($aData['cpicture_id'])?$aData['cpicture_id']:''))),
                    array('caption'=>'Активировать новость',
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'news_status','set_value'=>(!empty($aData['news_status'])?$aData['news_status']:''))),
                    array('caption'=>'На главной',
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'news_status_main','set_value'=>(!empty($aData['news_status_main'])?$aData['news_status_main']:''))),
                    array('caption'=>'В заголовке',
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'news_status_top','set_value'=>(!empty($aData['news_status_top'])?$aData['news_status_top']:''))),
                    array('caption'=>'Дата создания',
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>'news_creates','set_value'=>(!empty($aData['news_creates'])?$aData['news_creates']:date('Y-m-d H:i:s',time())))),
                    array('caption'=>'Сохранить',
                        'to_control'=>array('set_type'=>'a','set_name'=>'btnSaveNews','set_css'=>'button-base green','set_url'=>'#')),        
                )
            );
            if ($aProcess==PROCESS_NEWS_SECTION) {
                $outResult['form_property']['action']="/administration/admin_news/save_category";
                $outResult['form_data'] = array(
                    array(  'caption'=>'Идентификатор категории',
                        'to_control'=>array('set_readonly'=>'*','set_require'=>'*','set_type'=>'text','set_name'=>"nc_id",
                                            'set_value'=>(!empty($aData["nc_id"])?$aData["nc_id"]:''))),
                    array(  'caption'=>'Название категории',
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"nc_title",
                        'set_value'=>(!empty($aData["nc_title"])?$aData["nc_title"]:''))),
                    array('caption'=>'Язык','to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),            
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSaveCNews",'set_css'=>'button-base green','set_url'=>'#')),                        
                );
            }
            if ($aProcess==PROCESS_NEWS_SECTION_ADD) {
                $outResult['form_property']['action']="/administration/admin_news/save_category";
                $outResult['form_data'] = array(
                    array(  'caption'=>'Название категории',
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"nc_title",
                        'set_value'=>(!empty($aData["nc_title"])?$aData["nc_title"]:''))),
                    array('caption'=>'Язык','to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),            
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSaveCNews",'set_css'=>'button-base green','set_url'=>'#')),                        
                );
            }
            return  $outResult;
        }
        //
        public function getOutput($aProcess=null,$aData=array(),$aUnion=true,$aPage=1) {
            $outResult = array (    
                'property'=>array('title'=>getCaptionInput('caption_news'),'isRun'=>true,'include_js'=>"info.{$this->inSufix}.js",'template'=>'news/news-start-up.twig'),
                'titles'=>array(),
                'data'=>array(),
                'info'=>array(),
            );
            switch ($aProcess) {
                case OUTPUT_LIST:
                    $outResult['data']= $this->load($aData,$aUnion,$aPage);
                    $outResult['info']['count_record']=count($outResult['data']);
                    
                    foreach ($outResult['data'] as $inKey=>$inData) {
                        if (empty($inData['news_creates'])) {
                            unset($outResult['data'][$inKey]);
                        }
                        else {
                            //echo "<pre>"; var_dump($inData['news_creates']); die();
                            //$inData['news_creates'] = getDecodeDate($inData['news_creates']);
                            if (mb_strlen($inData['news_text'])>OUT_CUT_STRING) {
                                //die("yes");
                                $inData['cut_text'] =  mb_substr($inData['news_text'], 0, OUT_CUT_STRING).'...';
                                $inData['detail'] = true;
                            }
                            $outResult['data'][$inKey] = $inData;
                        }
                    }
                    break;
                default :
                    $outResult['titles'] = array (    
                        array(
                            array('title'=>'ID','size'=>20),
                            array('title'=>'Заголовок блога','size'=>0),
                            array('title'=>"Статус",'size'=>80),
                            array('title'=>'Действия','size'=>80)
                        ),
                        'data'=>array($this->inSelfId,"{$this->inSufix}_title","{$this->inSufix}_status",'action')        
                    );
            }
            return  $outResult;
        }
        //
    }