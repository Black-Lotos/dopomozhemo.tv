<?php
    class Language_model extends CI_Model {
        private $inTblName = 'tb_language';
        private $inTblUnion = 'tb_language';
        private $inCountRec = 0;
        private $inOrderFields='language_id';
        /*private $inTblCategoryName = 'tb_menu_category';
        private $inTblUnion = 'v_menu_category_to_menu';*/
        function __construct(){
            parent::__construct();
        }
        //
        public function load($aFilter=array(),$aUnion=false,$aPage=0,$aCountRec = 12) {
            //echo "<pre>"; var_dump($aFilter); die();
            $outWhere = ''; $outFields = '*'; $inOrder='';
            if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
                $outFields = implode(',', $aFilter['fields']);
                unset($aFilter['fields']);
            }
            
            if (isset($aFilter['status'])) {
                $outWhere .= " and lan_status = '" . $aFilter['status'] . "'";
            } else {
                if (!empty($aFilter) &&  is_array($aFilter)) {
                    foreach ($aFilter as $outKey=>$outData) {
                        $outWhere .= " and  {$outKey} = '{$outData}'";
                    }
                }
            }
            $inTable = (($aUnion)?$this->inTblUnion:$this->inTblName);
            if ($this->inCountRec>0) {
                $inOrder = "order by {$this->inOrderFields} DESC limit {$this->inCountRec}";
            } 
            if ($aPage>0) {
                $inOrder = "order by {$this->inOrderFields} limit ".(($aPage-1)*$aCountRec).",{$aCountRec}";
            }
            $inSql = "SELECT {$outFields} FROM {$inTable} where 1 {$outWhere} {$inOrder}";
            //var_dump($inSql);
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData;
            }
            return false;
        }
        //
        public function loadLanguage($aFilter=array()) {
            $outWhere = ''; $outFields = '*';
            if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
                $outFields = implode(',', $aFilter['fields']);
                unset($aFilter['fields']);
            }
            if (isset($aFilter['status'])) {
                $outWhere .= " and lan_status = '" . $aFilter['status'] . "'";
            } else {
                foreach ($aFilter as $outKey => $outData) {
                    $outWhere .= " and {$outKey} = '{$outData}'";
                }
            }
            $inSql = "SELECT {$outFields} FROM {$this->inTblName} where 1 {$outWhere} order by lan_weight";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData;
            }
            return false;
        }
        //
        public function loadLanguageById($aId,$aFilter=array()) {
            $outWhere = ''; $outFields = '*';
            if (!empty($aId)) {
                $outWhere .= " and language_id = '" . $aId . "'";
            }
            $inSql = "SELECT {$outFields} FROM {$this->inTblName} where 1 {$outWhere} order by lan_weight";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData[0];
            }
            return false;
        }
        public function loadLanguageByCode($aId,$aFilter=array()) {
            $outWhere = ''; $outFields = '*';
            if (!empty($aId) &&  is_string($aId)) {
                $outWhere .= " and lan_code = '" .  strtoupper($aId) . "'";
            } else return false;
            $inSql = "SELECT {$outFields} FROM {$this->inTblName} where 1 {$outWhere} order by lan_weight";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData[0]['language_id'];
            }
            return false;
        }
        public function getForm(){}
        //
        public function getFormLanguage($aSufix, $aCategory, $aLanguage,$aMenu, $aData=array()) {
            $outResult = array (    
                'form_property'=>array('name'=>"frmlan-add{$aSufix}",'method'=>'post','action'=>"/administration/language/save",'include_js'=>'info-language.js'),
                'form_data'=>array(
                    array('caption'=>'Идентификатор языка','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'language_id','set_value'=>(!empty($aData['language_id'])?$aData['language_id']:''))),
                    array('caption'=>'Наименование языка (рус.)','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>'lan_title','set_value'=>(!empty($aData['lan_title'])?$aData['lan_title']:''))),
                    array('caption'=>'Код языка','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>'lan_code','set_value'=>(!empty($aData['lan_code'])?$aData['lan_code']:''))),
                    array('caption'=>'Активировать язык','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'lan_status','set_value'=>(!empty($aData['lan_status'])?$aData['lan_status']:''))),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>'btnSaveLanguage','set_css'=>'button-base green','set_url'=>'#')),        
                    //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
                )
            );
            return  $outResult;
        }
        //
        public function getOutput($aData=array()) {
            $outResult['output'] = array (    
                'property'=>array(  'include_js'=>'info-language.js'),
                'titles'=>array(
                    array('title'=>'ID','size'=>20),
                    array('title'=>'Наименование языка','size'=>0),
                    array('title'=>'Код языка','size'=>80),
                    array('title'=>"Статус",'size'=>80),
                    array('title'=>'Действия','size'=>80)
                ),
                'data'=>array('language_id','lan_title','lan_code','lan_status','action')        
            );
            return  $outResult;
        }
        //
        public function save($aData) {
            $outRes = false;
            if (empty($aData['language_id'])) {
                $inSql = "insert into {$this->inTblName}";
                $inField = array(); $inOutData = array();
                foreach ($aData as $inKey => $inData) {
                    $inField[] = $inKey;
                    $inOutData[] = "'".$inData."'";
                }
                $inSql .= "(".implode(',', $inField).") values (".implode(',', $inOutData).")";
                $outRes = $this->db->insert($this->inTblName, $aData); 
            } else  {
                        $this->db->where('language_id', $aData['language_id']);
                        $outRes = $this->db->update($this->inTblName, $aData); 
                    }
            return  array('rec-no'=>$outRes);       
        }
    }