<?php
    class Picture_model extends CI_Model {
        private $inTblName = 'tb_pictures';
        private $inStatus = 'picture_status', $inStatusMain='picture_main', $inStatusTop='picture_top';
        private $inSelfId = 'picture_id', $inCategoryId='cpicture_id';
        private $inSufix='picture';
        private $inTblCatName = 'tb_category_pictures';
        private $inTblUnion = 'v_category_to_picture';
        private $inCountRec = 0;
        private $inOrderFields='cpicture_id, picture_id';
        //
        function __construct(){
            parent::__construct();
        }
        //
        public function deleteCategoryById($aId) {
        $outWhere = ''; 
        if (!empty($aId)) {
            $outWhere .= "{$this->inCategoryId} = '".$aId."'";
            $inSql = "delete FROM {$this->inTblCatName} where {$outWhere}";
            $outData = $this->db->query($inSql);
            if (!empty($outData)) {
                return $outData;
            }
        }
        return false;
        }
        //
        public function loadCountPage($aRecordByPage) {
            $outResult = $this->load();
            $outResult = count($outResult);
            return ceil($outResult/$aRecordByPage);
            //(($outResult % $aRecordByPage)==0)?1:($outResult % $aRecordByPage);
        }
        //
        public function load($aFilter=array(),$aUnion=false,$aPage=0,$aCountRec=12) {
            $outWhere = ''; $outFields = '*'; $inOrder='';
            if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
                $outFields = implode(',', $aFilter['fields']);
                unset($aFilter['fields']);
            }
            
            if (isset($aFilter['status'])) {
                $outWhere .= " and picture_status = '" . $aFilter['status'] . "'";
            } else {
                if (!empty($aFilter) &&  is_array($aFilter)) {
                    foreach ($aFilter as $outKey=>$outData) {
                        $outWhere .= " and  {$outKey} = '{$outData}'";
                    }
                }
            }
            $inTable = (($aUnion)?$this->inTblUnion:$this->inTblName);
            if ($this->inCountRec>0) {
                $inOrder = "order by {$this->inOrderFields} DESC limit {$this->inCountRec}";
            } 
            if ($aPage>0) {
                $inOrder = "order by {$this->inOrderFields} limit ".(($aPage-1)*$aCountRec).",{$aCountRec}";
            }
            $inSql = "SELECT {$outFields} FROM {$inTable} where 1 {$outWhere} {$inOrder}";
            //var_dump($inSql); die("gggg");
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData;
            }
            return false;
        }
        //
        public function loadById($aId,$aFilter=array()) {
            $outWhere = ''; $outFields = '*';
            if (!empty($aId)) {
                $outWhere .= " and {$this->inSelfId} = '" . $aId . "'";
            }
            $inSql = "SELECT {$outFields} FROM {$this->inTblName} where 1 {$outWhere}";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData[0];
            }
            return false;
        }
        //
        public function loadCategory($aFilter=array()) {
            $outWhere = ''; $outFields = '*';
            if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
                $outFields = implode(',', $aFilter['fields']);
                unset($aFilter['fields']);
            }
            
            if (isset($aFilter['status'])) {
                $outWhere .= " and cpicture_status = '" . $aFilter['status'] . "'";
            } else {
                if (!empty($aFilter) &&  is_array($aFilter)) {
                    foreach ($aFilter as $outKey=>$outData) {
                        $outWhere .= " and  {$outKey} = '{$outData}'";
                    }
                }
            }
            
            $inSql = "SELECT {$outFields} FROM {$this->inTblCatName} where 1 {$outWhere}";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            
            if (!empty($outData)) {
                return $outData;
            }
            return false;
        }
        //
        public function loadCategoryById($aId, $aFilter=array()) {
            $outWhere = ''; $outFields = '*';
            if (!empty($aId)) {
                $outWhere .= " and {$this->inCategoryId} = '" . $aId . "'";
            }
            $inSql = "SELECT {$outFields} FROM {$this->inTblCatName} where 1 {$outWhere}";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData[0];
            }
            return false;
        }
        //
        public function loadCategoryElemntByAlias($aAlias='alias_down_slider_video',$aFilter=array()) {
            $outWhere = "cpicture_alias = '{$aAlias}' and "; $outFields = '*';
            if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
                $outFields = implode(',', $aFilter['fields']);
                unset($aFilter['fields']);
            }
            
            if (isset($aFilter['status'])) {
                $outWhere .= " and cpicture_status = '" . $aFilter['status'] . "'";
            } else {
                if (!empty($aFilter) &&  is_array($aFilter)) {
                    foreach ($aFilter as $outKey=>$outData) {
                        $outWhere .= " and  {$outKey} = '{$outData}'";
                    }
                }
            }
            $inSql = "SELECT {$outFields} FROM {$this->inTblUnion} where 1 {$outWhere}";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData[0];
            }
            return false;
        }
        //
        public function loadTree($aFilter=array(),$aUnion=false,$aPage=0,$aCountRec=12) {
            //echo "<pre>"; var_dump($aFilter); die();
            $inFieldsCategory = $aFilter['category'];
            $inFieldsBlogs = $aFilter['picture'];
            $outCategory = $this->loadCategory($inFieldsCategory);
            //echo "<pre>"; var_dump($outCategory); die();
            if(($outCategory)) {
                foreach ($outCategory as $outKey=>$outData) {
                    $outData['sub_tree'] = $this->load(array('cpicture_id'=>$outData['value'],'fields'=>$inFieldsBlogs['fields']),$aUnion,$aPage,$aCountRec);
                    $outCategory[$outKey] = $outData;
                }
            }
            return $outCategory;
        }
        //
        public function getForm($aData=array(), $aProcess=null) {
            $outResult = array (    
                'form_property'=>array('name'=>"frm{$this->inSufix}-add",'method'=>'post','action'=>"/administration/admin_picture/save",'include_js'=>"info-{$this->inSufix}.js"),
                'form_data'=>array(
                    array('caption'=>'Идентификатор картинки','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"{$this->inSufix}_id",
                        'set_value'=>(!empty($aData["{$this->inSufix}_id"])?$aData["{$this->inSufix}_id"]:''))),
                    array('caption'=>'Коллекция','to_control'=>array('set_type'=>'select','set_name'=>'cpicture_id',
                        'set_value'=>!empty($aData['cpicture_id'])?$aData['cpicture_id']:'')),
                    array('caption'=>'Язык','to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                        
                    array('caption'=>'Заголовок картинки','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_title",
                        'set_value'=>(!empty($aData["{$this->inSufix}_title"])?$aData["{$this->inSufix}_title"]:''))),
                    array('caption'=>'Текст картинки','to_control'=>array('set_require'=>'*','set_type'=>'textarea','set_name'=>"{$this->inSufix}_text",
                        'set_value'=>(!empty($aData["{$this->inSufix}_text"])?$aData["{$this->inSufix}_text"]:''))),
                    /*array('caption'=>'<a class="button-base red" href="#" id="btnClearImagePreview" url="#'.'preview-'.
                            (!empty($aData["images_id"])?$aData["images_id"]:0).
                        '" for="preview-'.(!empty($aData["news_id"])?$aData["news_id"]:0).'">Удалить картинку</a>',
                        'to_control'=>array('set_type'=>'include_image','set_name'=>'preview-'.(!empty($aData["images_id"])?$aData["images_id"]:0),'set_css'=>'image_preview',
                            'set_callback'=>array('function'=>'getImageNews','param'=>(!empty($aData["images_id"])?$aData["images_id"]:'')))),*/
                    array('caption'=>'Файл картинки','to_control'=>array('set_require'=>'*','set_type'=>'file','set_name'=>"{$this->inSufix}_file",
                        'set_value'=>(!empty($aData["{$this->inSufix}_file"])?$aData["{$this->inSufix}_file"]:''))),            
                    array('caption'=>'Ширина картинки','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_width",
                        'set_value'=>(!empty($aData["{$this->inSufix}_width"])?$aData["{$this->inSufix}_width"]:''))),            
                    array('caption'=>'Высота картинки','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_height",
                        'set_value'=>(!empty($aData["{$this->inSufix}_height"])?$aData["{$this->inSufix}_height"]:''))),           
                    array('caption'=>'Код втраивания','to_control'=>array('set_readonly'=>'*','set_require'=>'','set_type'=>'hidden','set_name'=>"{$this->inSufix}_code",
                        'set_value'=>(!empty($aData["{$this->inSufix}_code"])?$aData["{$this->inSufix}_code"]:''))),                                    
                    array('caption'=>'Активировать картинку','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'picture_status',
                        'set_value'=>(!empty($aData["{$this->inSufix}_status"])?$aData["{$this->inSufix}_status"]:''))),
                    /*array('caption'=>'На главной','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inSufix}_status_main",
                        'set_value'=>(!empty($aData["{$this->inSufix}_status_main"])?$aData["{$this->inSufix}_status_main"]:''))),
                    array('caption'=>'В заголовке','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inSufix}_status_top",
                        'set_value'=>(!empty($aData["{$this->inSufix}_status_top"])?$aData["{$this->inSufix}_status_top"]:''))),*/
                    array('caption'=>'Дата создания',
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'picture_creates','set_value'=>(!empty($aData['picture_creates'])?$aData['picture_creates']:date('Y-m-d H:i:s',time())))),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSave{$this->inSufix}",'set_css'=>'button-base green','set_url'=>'#')),        
                    //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
                )
            );
            if ($aProcess==PROCESS_CONTENT_SECTION) {
                $outResult['form_property']['action']="/administration/admin_picture/save_category";
                $outResult['form_data'] = array(
                    array(  'caption'=>'Идентификатор видео',
                            'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"c{$this->inSufix}_id",
                                                'set_value'=>(!empty($aData["c{$this->inSufix}_id"])?$aData["c{$this->inSufix}_id"]:''))),
                    array(  'caption'=>'Название коллеции',
                            'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"c{$this->inSufix}_title",
                            'set_value'=>(!empty($aData["c{$this->inSufix}_title"])?$aData["c{$this->inSufix}_title"]:''))),
                    array('caption'=>'Язык','to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                            
                    array(  'caption'=>'Алиас коллекции',
                            'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"c{$this->inSufix}_alias",
                            'set_value'=>(!empty($aData["c{$this->inSufix}_alias"])?$aData["c{$this->inSufix}_alias"]:''))),                
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSaveСContent",'set_css'=>'button-base green','set_url'=>'#')),                        
                );
            }
            return  $outResult;
        }
        //
        //
         public function getOutput($aProcess=null) {
            $outResult = array (    
                'property'=>array('title'=>'Контентное видео','isRun'=>true,'include_js'=>"info-{$this->inSufix}.js",'template'=>'pieses/pieses-picture.twig'),
                'titles'=>array(),
                'data'=>array()
            );
            switch ($aProcess) {
                case OUTPUT_LIST:
                    $outResult['data']= $this->load(array(), true);
                    $outResult['info']['count_record']=count($outResult['data']);
                    foreach ($outResult['data'] as $inKey=>$inData) {
                        if (empty($inData['picture_creates'])) {
                            unset($outResult['data'][$inKey]);
                        }
                        else {
                            $inData['picture_creates'] = getDecodeDate($inData['picture_creates']);
                            if (mb_strlen($inData['picture_text'])>OUT_CUT_STRING) {
                                //die("yes");
                                $inData['cut_text'] =  mb_substr($inData['picture_text'], 0, OUT_CUT_STRING).'...';
                                $inData['detail'] = true;
                            }
                            $outResult['data'][$inKey] = $inData;
                        }
                    }
                    break;
                default :
                    $outResult['titles'] = array (    
                        array(
                            array('title'=>'ID','size'=>20),
                            array('title'=>'Заголовок блога','size'=>0),
                            array('title'=>"Статус",'size'=>80),
                            array('title'=>'Действия','size'=>80)
                        ),
                        'data'=>array($this->inSelfId,"{$this->inSufix}_title","{$this->inSufix}_status",'action')        
                    );
            }
            return  $outResult;
        }
        //
        public function save($aData) {
            $outRes = false; $inId = 0;
            if (empty($aData[$this->inSelfId])) {
                if (isset($aData[$this->inSelfId])) unset($aData[$this->inSelfId]);
                $inSql = "insert into {$this->inTblName}";
                $inField = array(); $inOutData = array();
                foreach ($aData as $inKey => $inData) {
                    $inField[] = $inKey;
                    $inOutData[] = "'".$inData."'";
                }
                $inSql .= "(".implode(',', $inField).") values (".implode(',', $inOutData).")";
                //var_dump($inSql); die();
                $outRes = $this->db->insert($this->inTblName, $aData); 
                $inId = $this->db->insert_id();
            } else  {
                        $this->db->where($this->inSelfId, $aData[$this->inSelfId]);
                        $outRes = $this->db->update($this->inTblName, $aData); 
                        $inId = $aData[$this->inSelfId];
                    }
            // формируем строку кода
            $aData[$this->inSelfId] = $inId;
            if(!empty($aData['picture_file'])) {
                $outCode = "<img width='".$aData['picture_width']."' height='".$aData['picture_height']."' src='/picture/".$aData['picture_file']."'></img>";        
                $aData['picture_code'] = $outCode;
                //var_dump($aData); die();
                $this->db->where($this->inSelfId, $aData[$this->inSelfId]);
                $outRes = $this->db->update($this->inTblName, $aData); 
            } else {
                $inTmpData = $this->loadById($aData[$this->inSelfId]);
                $inTmpData['picture_width'] = $aData['picture_width'];
                $inTmpData['picture_height'] = $aData['picture_height'];
                //$inTmpData['picture_file'] = $aData['picture_file'];
                $aData = $inTmpData;
                $outCode = "<img width='".$aData['picture_width']."' height='".$aData['picture_height']."' src='/picture/".$aData['picture_file']."'></img>";        
                $aData['picture_code'] = $outCode;
                //var_dump($aData); die();
                //var_dump($aData); die();
                $this->db->where($this->inSelfId, $aData[$this->inSelfId]);
                $outRes = $this->db->update($this->inTblName, $aData); 
            }
            return  array('rec-no'=>$outRes);       
        }
        //
        public function save_category($aData) {
            $outRes = false;
            if (empty($aData[$this->inCategoryId])) {
                unset($aData[$this->inCategoryId]);
                $inSql = "insert into {$this->inTblCatName}";
                $inField = array(); $inOutData = array();
                foreach ($aData as $inKey => $inData) {
                    $inField[] = $inKey;
                    $inOutData[] = "'".$inData."'";
                }
                $inSql .= "(".implode(',', $inField).") values (".implode(',', $inOutData).")";
                //var_dump($inSql); die();
                $outRes = $this->db->insert($this->inTblCatName, $aData); 
            } else  {
                        $this->db->where($this->inCategoryId, $aData[$this->inCategoryId]);
                        $outRes = $this->db->update($this->inTblCatName, $aData); 
                    }
            return  array('rec-no'=>$outRes);       
        }
        //
        public function getPrefix() {
        }
        //
        public function DeleteById($aId,$aFilter=array()) {
            $outWhere = ''; $outFields = '*';
            if (!empty($aId)) {
                $outWhere .= " and {$this->inSelfId} = '" . $aId . "'";
                $inSql = "delete FROM {$this->inTblName} where 1 {$outWhere}";
                //var_dump($inSql); die();
                $outData = $this->db->query($inSql);
                //var_dump($outData);
                if (!empty($outData)) {
                    return $outData;
                }
            }
            return false;
        }
    }