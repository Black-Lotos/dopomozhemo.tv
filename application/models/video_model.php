<?php
    class Video_model extends CI_Model {
        private $inTblName = 'tb_images';
        private $inIdPrefix = 'img_';
        /*private $inTblCategoryName = 'tb_menu_category';
        private $inTblUnion = 'v_menu_category_to_menu';*/
        function __construct(){
            parent::__construct();
        }
        //
        public function load($aFilter=array()) {
            $outWhere = " and {$this->inIdPrefix}type='".T_VIDEO."'"; $outFields = '*';
            if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
                $outFields = implode(',', $aFilter['fields']);
                unset($aFilter['fields']);
            }
            
            if (isset($aFilter['status'])) {
                $outWhere .= " and {$this->inIdPrefix}status = '" . $aFilter['status'] . "'";
            } else {
                if (!empty($aFilter) &&  is_array($aFilter)) {
                    foreach ($aFilter as $outKey=>$outData) {
                        $outWhere .= " and  {$outKey} = '{$outData}'";
                    }
                }
            }
            
            $inSql = "SELECT {$outFields} FROM {$this->inTblName} where 1 {$outWhere}";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData;
            }
            return false;
        }
        //
        public function loadById($aId,$aFilter=array()) {
            $outWhere = ''; $outFields = '*';
            if (!empty($aId)) {
                $outWhere .= " and {$this->inIdPrefix}id = '" . $aId . "'";
            }
            $inSql = "SELECT {$outFields} FROM {$this->inTblName} where 1 {$outWhere}";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData[0];
            }
            return false;
        }
        public function getForm($aData=array()){
            $outResult = array (    
                'form_property'=>array('name'=>"frm{$aData}add",'method'=>'post','action'=>"/administration/admin_content_video/save",'include_js'=>'info-images.js'),
                'form_data'=>array(
                    array('caption'=>'Идентификатор видео','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"{$this->inIdPrefix}id",
                        'set_value'=>(!empty($aData["{$this->inIdPrefix}id"])?$aData["{$this->inIdPrefix}id"]:''))),
                    array('caption'=>'Заголовок видео','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inIdPrefix}title",
                        'set_value'=>(!empty($aData["{$this->inIdPrefix}title"])?$aData["{$this->inIdPrefix}title"]:''))),
                    array('caption'=>'URL видео','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inIdPrefix}url",
                        'set_value'=>(!empty($aData["{$this->inIdPrefix}url"])?$aData["{$this->inIdPrefix}url"]:''))),            
                    array('caption'=>'Активировать видео','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inIdPrefix}status",
                        'set_value'=>(!empty($aData["{$this->inIdPrefix}status"])?$aData["{$this->inIdPrefix}status"]:''))),
                    array('caption'=>'На главной','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inIdPrefix}main",
                        'set_value'=>(!empty($aData["{$this->inIdPrefix}main"])?$aData["{$this->inIdPrefix}main"]:''))),
                )
            );
            return $outResult;
        }
        //
        public function getOutput($aData=array()) {
            $outResult = array (    
                'property'=>array('isRun'=>true,'include_js'=>'info-images.js','template'=>'pieses/pieses-video.twig'),
                'titles'=>array(
                    array('title'=>'ID','size'=>20),
                    array('title'=>'Заголовок видео','size'=>0),
                    array('title'=>"Активно",'size'=>80),
                    array('title'=>"На главной",'size'=>80),
                    array('title'=>'Действия','size'=>80)
                ),
                'data'=>array("{$this->inIdPrefix}id","{$this->inIdPrefix}title","{$this->inIdPrefix}status","{$this->inIdPrefix}main","action")        
            );
            return  $outResult;
        }
        //
        public function save($aData) {
            $outRes = false;
            if (empty($aData["{$this->inIdPrefix}id"])) {
                $inSql = "insert into {$this->inTblName}";
                $inField = array(); $inOutData = array();
                foreach ($aData as $inKey => $inData) {
                    $inField[] = $inKey;
                    $inOutData[] = "'".$inData."'";
                }
                $inSql .= "(".implode(',', $inField).") values (".implode(',', $inOutData).")";
                //var_dump($inSql); die();
                $outRes = $this->db->insert($this->inTblName, $aData); 
            } else  {
                        $this->db->where("{$this->inIdPrefix}id", $aData["{$this->inIdPrefix}id"]);
                        $outRes = $this->db->update($this->inTblName, $aData); 
                    }
            return  array('rec-no'=>$outRes);       
        }
        //
        public function getPrefix() {
            return $this->inIdPrefix;
        }
    }