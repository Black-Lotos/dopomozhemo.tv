<?php
class Variables_model extends MY_Model {
    function __construct(){
        parent::__construct();
    }
    function StartUp() {
        $this->inTblName            = 'tb_variables';
        $this->inTblCategoryName    = 'tb_site_settings';
        $this->inTblUnion           = 'v_site_to_variable';
        $this->inCountRec           = 0;
        $this->inOrderFields        = 'variable_name';
        $this->inDebug              = false;
        $this->inStatus             = 'variable_active'; 
        $this->inStatusMain         = ''; 
        $this->inStatusTop          = '';
        $this->inAlias              = 'variable_name';
        $this->inSelfId             = 'variable_id'; 
        $this->inSelfName           = 'variable_name';
        $this->inCategoryId         = 'ss_id';
        $this->inCategoryName       = 'ss_title';
        $this->inCategoryStatus     = 'ss_active';
        $this->inSufix              = '';
        $this->inPrefix             = 'variable';
        $this->inRecursive          = true;
        $this->setObjectResult      = false;
        $this->inCategorySufix      = '';
        $this->inCategoryPrefix     = '';
    }
    public function loadCountPage($aRecordByPage,$aData=array()) {
        $outResult = $this->load($aData, false);
        $outResult = count($outResult);
        return ceil($outResult/$aRecordByPage);
    }
    public function getOutput($aProcess=null){
        $outResult = array(
            'property'=>array('include_js'=>"info.site.js",'template'=>'site/variable-start-up.twig'),
        );
        return $outResult;
    }
    public function getForm($aData = array(), $aProcess = null) {
        $outResult = array (    
            'form_property'=>array('name'=>"frmVariableAdd",'method'=>'post','action'=>"/administration/admin_settings/save_variable",'include_js'=>"info.site.js"));
        switch ($aProcess) {
            case VARIABLE_STRING:
                $outResult['form_data']=array(
                    array('caption'=>'Сайт','to_control'=>array('set_type'=>'select','set_name'=>"{$this->inCategoryId}",
                            'set_value'=>!empty($aData[$this->inCategoryId])?$aData[$this->inCategoryId]:'')),
                    array('caption'=>'Ідентификатор переменной','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"{$this->inPrefix}_id",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_id"])?$aData["{$this->inPrefix}_id"]:''))),                        
                    array('caption'=>'Наименоввание переменной','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inPrefix}_name",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_name"])?$aData["{$this->inPrefix}_name"]:''))),                        
                    array('caption'=>'Значение переменной (Рус)','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inPrefix}_value",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_value"])?$aData["{$this->inPrefix}_value"]:''))),
                    array('caption'=>'Значення змінної (Укр)','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inPrefix}_value_ua",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_value_ua"])?$aData["{$this->inPrefix}_value_ua"]:''))),
                    array('caption'=>'Variable (Eng)','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inPrefix}_value_en",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_value_en"])?$aData["{$this->inPrefix}_value_en"]:''))),            
                    array('caption'=>'Описание переменной','to_control'=>array('set_require'=>'','set_type'=>'text','set_name'=>"{$this->inPrefix}_description",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_description"])?$aData["{$this->inPrefix}_description"]:''))),            
                    array('caption'=>'Превикс группы переменных','to_control'=>array('set_require'=>'','set_type'=>'text','set_name'=>"{$this->inPrefix}_prefix",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_prefix"])?$aData["{$this->inPrefix}_prefix"]:''))),            
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')),            
                    array('caption'=>'Функция обратного вызова для переменной','to_control'=>array('set_require'=>'','set_type'=>'text','set_name'=>"{$this->inPrefix}_callback",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_callback"])?$aData["{$this->inPrefix}_callback"]:''))),            
                    array('caption'=>'Тип переменной','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>"{$this->inPrefix}_type",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_type"])?$aData["{$this->inPrefix}_type"]:''))),                        
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')),            
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSaveVariable",
                        'set_css'=>'button-base green whide','set_url'=>'#')),        
                    //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
                );
                break;
            case VARIABLE_DIGIT:
                $outResult['form_data']=array(
                    array('caption'=>'Сайт','to_control'=>array('set_type'=>'select','set_name'=>"{$this->inCategoryId}",
                            'set_value'=>!empty($aData[$this->inCategoryId])?$aData[$this->inCategoryId]:'')),
                    array('caption'=>'Ідентификатор переменной','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"{$this->inPrefix}_id",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_id"])?$aData["{$this->inPrefix}_id"]:''))),                        
                    array('caption'=>'Наименоввание переменной','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inPrefix}_name",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_name"])?$aData["{$this->inPrefix}_name"]:''))),                        
                    array('caption'=>'Значение переменной','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inPrefix}_value",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_value"])?$aData["{$this->inPrefix}_value"]:''))),
                    array('caption'=>'Описание переменной','to_control'=>array('set_require'=>'','set_type'=>'text','set_name'=>"{$this->inPrefix}_description",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_description"])?$aData["{$this->inPrefix}_description"]:''))),                        
                    array('caption'=>'Превикс группы переменных','to_control'=>array('set_require'=>'','set_type'=>'text','set_name'=>"{$this->inPrefix}_prefix",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_prefix"])?$aData["{$this->inPrefix}_prefix"]:''))),            
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')),            
                    array('caption'=>'Функция обратного вызова для переменной','to_control'=>array('set_require'=>'','set_type'=>'text','set_name'=>"{$this->inPrefix}_callback",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_callback"])?$aData["{$this->inPrefix}_callback"]:''))),            
                    array('caption'=>'Тип переменной','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>"{$this->inPrefix}_type",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_type"])?$aData["{$this->inPrefix}_type"]:''))),                                    
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')),            
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSaveVariable",
                        'set_css'=>'button-base green whide','set_url'=>'#')),        
                    //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
                );
                break;
        }
        return $outResult;
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

