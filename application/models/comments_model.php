<?php
    class Comments_model extends MY_Model {
        private $inTblNameRate = 'tb_comments_rate';
        private $inTblNameView = 'tb_comments_view';
        /*private $inTblCategoryName = 'tb_menu_category';
        private $inTblUnion = 'v_menu_category_to_menu';*/
        public function StartUp() {
            $this->inTblName            = 'tb_comments';
            $this->inTblCategoryName    = '';
            $this->inTblUnion           = 'tb_comments';
            $this->inCountRec           = 0;
            $this->inOrderFields        = 'comments_created';
            $this->inDebug              = false;
            $this->inStatus             = 'comments_status'; 
            $this->inStatusMain         = ''; 
            $this->inStatusTop          = '';
            $this->inAlias              = '';
            $this->inSelfId             = 'comments_id'; 
            $this->inSelfName           = 'comments_text';
            $this->inCategoryId         = '';
            $this->inCategoryName       = '';
            $this->inCategoryStatus     = '';
            $this->inSufix              = 'comments';
            $this->inPrefix             = 'comments';
            $this->inCategorySufix      = '';
            $this->inCategoryPrefix     = '';
            $this->inOrderType          = 'DESC';
        }
        //
        public function loadByProposalId($aId=null,$aFilter=array()) {
            if (empty($aId)) {
                return false;
            }
            //
            $outData =  $this->load(array('proposal_id'=>$aId,'comments_status'=>1),false); 
            //var_dump($outData); die();
            return $outData; 
        }
        public function CountByProposalId($aId=null,$aFilter=array()) {
            if (empty($aId)) {
                return 0;
            }
            //
            $outData= $this->loadByProposalId($aId); 
            $outData = ($outData)?count($outData):0;
            //var_dump($outData); die();
            return $outData;
        }
        //
        public function loadByBlogId($aId=null,$aFilter=array()) {
            if (empty($aId)) {
                return false;
            }
            //
            $outData =  $this->load(array('blogs_id'=>$aId,'comments_status'=>1),false); 
            //var_dump($outData); die();
            return $outData; 
        }
        public function CountByBlogId($aId=null,$aFilter=array()) {
            if (empty($aId)) {
                return 0;
            }
            //
            $outData= $this->loadByBlogId($aId); 
            $outData = ($outData)?count($outData):0;
            //var_dump($outData); die();
            return $outData;
        }
        //
        public function loadByNewsId($aId=null,$aFilter=array()) {
            if (empty($aId)) {
                return false;
            }
            //
            $outData =  $this->load(array('news_id'=>$aId,'comments_status'=>1),false); 
            //var_dump($outData); die();
            return $outData; 
        }
        public function CountByNewsId($aId=null,$aFilter=array()) {
            if (empty($aId)) {
                return 0;
            }
            //
            $outData= $this->loadByNewsId($aId); 
            $outData = ($outData)?count($outData):0;
            //var_dump($outData); die();
            return $outData;
        }
        //
        public function getForm(){
            
        }
        //
        public function getOutput($aData=array()) {
            $outResult['output'] = array (    
                'property'=>array('include_js'=>'info.comments.js'),
                'data'=>array()        
            );
            return  $outResult;
        }
        //
    }