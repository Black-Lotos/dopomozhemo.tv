<?php
    class News_model extends MY_Model {
        
        function __construct(){
            parent::__construct();
        }
        //
        protected function StartUp() {
            $this->inTblName = 'tb_news';       
            $this->inTblCategoryName = 'tb_news_category';
            $this->inTblUnion = 'v_category_to_news'; 
            $this->inCountRec           = 0;
            $this->inStatus = 'news_status';    
            $this->inStatusMain='news_main';    
            $this->inStatusTop='news_top';    
            $this->inCategoryStatus = 'nc_status';
            $this->inSelfId = 'news_id';        
            $this->inSelfName = 'news_title';
            $this->inCategoryId='nc_id';        
            $this->inCategoryName = 'nc_title'; 
            $this->inSufix='news';              
            $this->inOrderFields='news_creates';
            $this->inDebug              = false;
            $this->inAlias              = '';
            $this->inScriptUrl = '/admin/admin_news';
        }
        //
        public function getTabs($aPage=1) {
            $outResult = $this->inTabs;
            $outResult['property'] = array('template' => 'administration/common/list-start-up-02.twig','title'=>'Работа с новостями','include_js'=>'info.news.js');
            $outResult['data']['list'] = array('title' => 'Список новостей','url'=> "{$this->inScriptUrl}/load?page={$aPage}");
            $outResult['data']['add'] = array('title' => 'Добавить новость','url'=> "{$this->inScriptUrl}/add");
            $outResult['data']['add_section'] = array('title' => 'Добавить раздел новостей','url'=> "{$this->inScriptUrl}/add_section");
            $outResult['data']['default'] = $outResult['data']['list']['url'];//file_get_contents(base_url().$outResult['data']['list']['url']);
            return $outResult;
        }
        public function getForm($aData=array(), $aProcess=null){
            $outResult = array (    
                'form_property'=>array('name'=>"frmNews-add{$this->inSufix}",'method'=>'post','action'=>"{$this->inScriptUrl}/save",'include_js'=>'info.news.js'),
                'form_data'=>array(
                    array('caption'=>'Идентификатор новости','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'news_id','set_value'=>(!empty($aData['news_id'])?$aData['news_id']:''))),
                    array('caption'=>'Категория новости','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select','set_name'=>'nc_id',
                        'set_value'=>(!empty($aData['nc_id'])?$aData['nc_id']:''))),
                    array('caption'=>'Язык','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),            
                    array('caption'=>'Заголовок новости','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_length'=>100,'set_require'=>'*','set_type'=>'text','set_name'=>'news_title','set_value'=>(!empty($aData['news_title'])?$aData['news_title']:''))),
                    array('caption'=>'','to_control'=>
                        array('set_type'=>'include_image',
                                'set_css'=>'image_preview','set_value'=>(!empty($aData["images_id"])?$aData["images_id"]:0),
                                'set_for'=>"news_id_".(!empty($aData['news_id'])?$aData['news_id']:0)
                                )),
                    array('caption'=>'Текст новости','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'textarea','set_name'=>'news_text','set_value'=>(!empty($aData['news_text'])?$aData['news_text']:''))),
                    array('caption'=>'Присоединить коллекцию','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select','set_name'=>'cpicture_id',
                        'set_value'=>(!empty($aData['cpicture_id'])?$aData['cpicture_id']:''))),
                    array('caption'=>'Активировать новость','set_css'=>"text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'news_status','set_value'=>(!empty($aData['news_status'])?$aData['news_status']:''))),
                    /*array('caption'=>'На главной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'news_status_main','set_value'=>(!empty($aData['news_status_main'])?$aData['news_status_main']:''))),*/
                    array('caption'=>'Дата создания','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>'news_creates','set_value'=>(!empty($aData['news_creates'])?$aData['news_creates']:date('Y-m-d H:i:s',time())))),
                    array('caption'=>'Сохранить',
                        'to_control'=>array('set_type'=>'submit','set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'')),        
                )
            );
            if ($aProcess==PROCESS_NEWS_SECTION) {
                $outResult['form_property']['action']="{$this->inScriptUrl}/save_category";
                $outResult['form_data'] = array(
                    array(  'caption'=>'Идентификатор категории','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_readonly'=>'*','set_require'=>'*','set_type'=>'text','set_name'=>"nc_id",
                                            'set_value'=>(!empty($aData["nc_id"])?$aData["nc_id"]:''))),
                    array(  'caption'=>'Название категории','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"nc_title",
                        'set_value'=>(!empty($aData["nc_title"])?$aData["nc_title"]:''))),
                    array('caption'=>'Язык','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),            
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'bottom-margin-1','set_url'=>'#')),                        
                );
            }
            if ($aProcess==PROCESS_NEWS_SECTION_ADD) {
                $outResult['form_property']['action']="{$this->inScriptUrl}/save_category";
                $outResult['form_data'] = array(
                    array(  'caption'=>'Название категории','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"nc_title",
                        'set_value'=>(!empty($aData["nc_title"])?$aData["nc_title"]:''))),
                    array('caption'=>'Язык','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select','set_name'=>'language_id',
                        'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),            
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'bottom-margin-1','set_url'=>'#')),                        
                );
            }
            return  $outResult;
        }
        //
        public function getOutput($aProcess=null,$aData=array(),$aUnion=true,$aPage=1) {
            $outResult = array (    
                'property'=>array('title'=>getCaptionInput('caption_news'),'isRun'=>true,'include_js'=>"info-{$this->inSufix}.js",'template'=>'news/news-start-up.twig'),
                'titles'=>array(),
                'data'=>array(),
                'info'=>array(),
            );
            switch ($aProcess) {
                case OUTPUT_LIST:
                    $inDataClon = $aData;
                    $outResult['data']= $this->load($aData,$aUnion,$aPage);
                    //$this->Debug();
                    $outResult['info']['count_record']=$this->loadCountRecord($inDataClon,$aUnion);
                    //var_dump($outResult['info']['count_record']);
                    if($outResult['data']) {
                        foreach ($outResult['data'] as $inKey=>$inData) {
                            if (empty($inData['news_creates'])) {
                                unset($outResult['data'][$inKey]);
                            }
                            else {
                                //echo "<pre>"; var_dump($inData['news_creates']); die();
                                //$inData['news_creates'] = getDecodeDate($inData['news_creates']);
                                if (mb_strlen($inData['news_text'])>OUT_CUT_STRING) {
                                    //die("yes");
                                    $inData['cut_text'] =  mb_substr($inData['news_text'], 0, OUT_CUT_STRING).'...';
                                    $inData['detail'] = true;
                                }
                                $outResult['data'][$inKey] = $inData;
                            }
                        }
                    }
                    break;
                default :
                    $outResult['titles'] = array (    
                        array(
                            array('title'=>'ID','size'=>20),
                            array('title'=>'Заголовок блога','size'=>0),
                            array('title'=>"Статус",'size'=>80),
                            array('title'=>'Действия','size'=>80)
                        ),
                        'data'=>array($this->inSelfId,"{$this->inSufix}_title","{$this->inSufix}_status",'action')        
                    );
            }
            return  $outResult;
        }
        //
    }