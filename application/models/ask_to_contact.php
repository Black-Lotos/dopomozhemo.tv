<?php
    class Ask_to_contact extends CI_Model {
        private $inTblName = 'tb_ask_to_contact';
        private $inStatus = 'ask_to_contact_status', $inStatusMain='ask_to_contact_main', $inStatusTop='ask_to_contact_top';
        private $inSelfId = 'ask_to_contact_id', $inCategoryId='cask_to_contact_id';
        private $inSufix='ask_to_contact';
        private $inTblCatName = 'tb_ask_to_contact_category';
        //private $inTblUnion = 'v_category_to_ask_to_contact';
        private $inCountRec = 0;
        private $inOrderFields='ask_to_contact_date';
        function __construct(){
            parent::__construct();
        }
        //
        public function setCountRecord($aValue) {
            $this->inCountRec = $aValue;
        }
        //
        public function load($aFilter=array(),$aUnion=false,$aPage=0) {
            echo "<pre>"; var_dump($aFilter); die();
            $outWhere = ''; $outFields = '*'; $inOrder='';
            if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
                $outFields = implode(',', $aFilter['fields']);
                unset($aFilter['fields']);
            }
            
            if (isset($aFilter['status'])) {
                $outWhere .= " and ask_to_contact_status = '" . $aFilter['status'] . "'";
            } else {
                if (!empty($aFilter) &&  is_array($aFilter)) {
                    foreach ($aFilter as $outKey=>$outData) {
                        $outWhere .= " and  {$outKey} = '{$outData}'";
                    }
                }
            }
            $inTable = (($aUnion)?$this->inTblUnion:$this->inTblName);
            if ($this->inCountRec>0) {
                $inOrder = "order by {$this->inOrderFields} DESC limit {$this->inCountRec}";
            } 
            if ($aPage>0) {
                $inOrder = "order by {$this->inOrderFields} limit ".(($aPage-1)*12).",12";
            }
            $inSql = "SELECT {$outFields} FROM {$inTable} where 1 {$outWhere} {$inOrder}";
            var_dump($inSql); die("gggg");
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData;
            }
            return false;
        }
        //
        public function loadById($aId,$aFilter=array()) {
            $outWhere = ''; $outFields = '*';
            if (!empty($aId)) {
                $outWhere .= " and {$this->inSelfId} = '" . $aId . "'";
            }
            $inSql = "SELECT {$outFields} FROM {$this->inTblName} where 1 {$outWhere}";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData[0];
            }
            return false;
        }
        //
        public function loadCategory($aFilter=array()) {
            $outWhere = ''; $outFields = '*';
            if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
                $outFields = implode(',', $aFilter['fields']);
                unset($aFilter['fields']);
            }
            
            if (isset($aFilter['status'])) {
                $outWhere .= " and ask_to_contact_status = '" . $aFilter['status'] . "'";
            } else {
                if (!empty($aFilter) &&  is_array($aFilter)) {
                    foreach ($aFilter as $outKey=>$outData) {
                        $outWhere .= " and  {$outKey} = '{$outData}'";
                    }
                }
            }
            
            $inSql = "SELECT {$outFields} FROM {$this->inTblCatName} where 1 {$outWhere}";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData;
            }
            return false;
        }
        //
        public function loadCategoryById($aId, $aFilter=array()) {
            $outWhere = ''; $outFields = '*';
            if (!empty($aId)) {
                $outWhere .= " and {$this->inCategoryId} = '" . $aId . "'";
            }
            $inSql = "SELECT {$outFields} FROM {$this->inTblCatName} where 1 {$outWhere}";
            //var_dump($inSql); die();
            $outData = $this->db->query($inSql)->result_array();
            if (!empty($outData)) {
                return $outData[0];
            }
            return false;
        }
        //
        public function loadTree($aFilter=array()) {
            //echo "<pre>"; var_dump($aFilter); die();
            $inFieldsCategory = $aFilter['category'];
            $inFieldsProposal = $aFilter['ask_to_contact'];
            $outCategory = $this->loadCategory($inFieldsCategory);
            //echo "<pre>"; var_dump($outCategory); die();
            foreach ($outCategory as $outKey=>$outData) {
                $outData['sub_tree'] = $this->load(array('cask_to_contact_id'=>$outData['value'],'ask_to_contact_owner'=>0,'fields'=>$inFieldsProposal['fields']));
                $outCategory[$outKey] = $outData;
            }
            return $outCategory;
        }
        //public function getForm(){}
        //
        public function getForm($aCategory, $aLanguage,$aData=array(), $aProcess=null) {
            $outResult = array (    
                'form_property'=>array('name'=>"frm{$this->inSufix}-add",'method'=>'post','action'=>"/administration/ask_to_contact/save",'include_js'=>"info-{$this->inSufix}.js"),
                'form_data'=>array(
                    array('caption'=>'Идентификатор заявки','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"{$this->inSufix}_id",
                        'set_value'=>(!empty($aData["{$this->inSufix}_id"])?$aData["{$this->inSufix}_id"]:''))),
                    array('caption'=>'Заголовок заявки','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_title",
                        'set_value'=>(!empty($aData["{$this->inSufix}_title"])?$aData["{$this->inSufix}_title"]:''))),
                    array('caption'=>'Текст заявки','to_control'=>array('set_require'=>'*','set_type'=>'textarea','set_name'=>"{$this->inSufix}_text",
                        'set_value'=>(!empty($aData["{$this->inSufix}_text"])?$aData["{$this->inSufix}_text"]:''))),
                    /*array('caption'=>'Аватар заявки','to_control'=>array('set_require'=>'*','set_type'=>'file','set_name'=>"{$this->inSufix}_avatar",
                        'set_value'=>(!empty($aData["{$this->inSufix}_file"])?$aData["{$this->inSufix}_avatar"]:''))),                        */
                    array('caption'=>'Представтесь','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_name",
                        'set_value'=>(!empty($aData["{$this->inSufix}_name"])?$aData["{$this->inSufix}_name"]:''))),
                    array('caption'=>'Телефон','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_phone",
                        'set_value'=>(!empty($aData["{$this->inSufix}_phone"])?$aData["{$this->inSufix}_phone"]:''))),            
                    array('caption'=>'e-mail','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_email",
                        'set_value'=>(!empty($aData["{$this->inSufix}_email"])?$aData["{$this->inSufix}_email"]:''))),                        
                    array('caption'=>'Активировать заявку','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'ask_to_contact_status',
                        'set_value'=>(!empty($aData["{$this->inSufix}_status"])?$aData["{$this->inSufix}_status"]:''))),            
                    array('caption'=>'Дата создания',
                        'to_control'=>array('set_readonly'=>'*','set_require'=>'*','set_type'=>'text','set_name'=>'ask_to_contact_date','set_value'=>(!empty($aData['ask_to_contact_date'])?$aData['ask_to_contact_date']:date('Y-m-d H:i:s',time())))),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSave{$this->inSufix}",'set_css'=>'button-base green','set_url'=>'#')),        
                    //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
                )
            );
            if ($aProcess==PROCESS_BLOGS_SECTION) {
                $outResult['form_property']['action']="/administration/ask_to_contact/save_category";
                $outResult['form_data'] = array(
                    array(  'caption'=>'Идентификатор заявки',
                            'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"c{$this->inSufix}_id",
                                                'set_value'=>(!empty($aData["c{$this->inSufix}_id"])?$aData["c{$this->inSufix}_id"]:''))),
                    array(  'caption'=>'Название категории',
                            'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"c{$this->inSufix}_title",
                            'set_value'=>(!empty($aData["c{$this->inSufix}_title"])?$aData["c{$this->inSufix}_title"]:''))),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSaveСProposal",'set_css'=>'button-base green','set_url'=>'#')),                        
                );
            }
            return  $outResult;
        }
        //
        public function getOutput($aProcess=null) {
            $outResult = array (    
                'property'=>array('title'=>'База данных','isRun'=>true,'include_js'=>"info-{$this->inSufix}.js",'template'=>'ask_to_contact/ask_to_contact-start-up.twig'),
                'titles'=>array(),
                'data'=>array()
            );
            switch ($aProcess) {
                case OUTPUT_LIST:
                    $outResult['data']= $this->load(array());
                    $outResult['info']['count_record']=count($outResult['data']);
                    if($outResult['data']) {
                        foreach ($outResult['data'] as $inKey=>$inData) {
                            if (empty($inData['ask_to_contact_date'])) {
                                unset($outResult['data'][$inKey]);
                            }
                            else {
                                $inData['ask_to_contact_date'] = getDecodeDate($inData['ask_to_contact_date']);
                                if (mb_strlen($inData['ask_to_contact_text'])>OUT_CUT_STRING) {
                                    //die("yes");
                                    $inData['cut_text'] =  mb_substr($inData['ask_to_contact_text'], 0, OUT_CUT_STRING).'...';
                                    $inData['detail'] = true;
                                }
                                $outResult['data'][$inKey] = $inData;
                            }
                        }
                    }
                    break;
                default :
                    $outResult['titles'] = array (    
                        array(
                            array('title'=>'ID','size'=>20),
                            array('title'=>'Заголовок заявки','size'=>0),
                            array('title'=>"Статус",'size'=>80),
                            array('title'=>'Действия','size'=>80)
                        ),
                        'data'=>array($this->inSelfId,"{$this->inSufix}_title","{$this->inSufix}_status",'action')        
                    );
            }
            return  $outResult;
        }
        //
        public function save($aData) {
            //echo "<pre>"; print_r($aData); die();
            $outRes = false;
            if (empty($aData[$this->inSelfId])) {
                $inSql = "insert into {$this->inTblName}";
                $inField = array(); $inOutData = array();
                
                foreach ($aData as $inKey => $inData) {
                    $inField[] = $inKey;
                    $inOutData[] = "'".$inData."'";
                }
                $inSql .= "(".implode(',', $inField).") values (".implode(',', $inOutData).")";
                //echo "<pre>"; print_r($inSql); die();
                $outRes = $this->db->insert($this->inTblName, $aData); 
            } else  {
                        $this->db->where($this->inSelfId, $aData[$this->inSelfId]);
                        $outRes = $this->db->update($this->inTblName, $aData); 
                    }
            return  array('rec-no'=>$outRes);       
        }
        //
        public function save_category($aData) {
            $outRes = false;
            if (empty($aData[$this->inCategoryId])) {
                $inSql = "insert into {$this->inTblCatName}";
                $inField = array(); $inOutData = array();
                foreach ($aData as $inKey => $inData) {
                    $inField[] = $inKey;
                    $inOutData[] = "'".$inData."'";
                }
                $inSql .= "(".implode(',', $inField).") values (".implode(',', $inOutData).")";
                //var_dump($inSql); die();
                $outRes = $this->db->insert($this->inTblCatName, $aData); 
            } else  {
                        $this->db->where($this->inCategoryId, $aData[$this->inCategoryId]);
                        $outRes = $this->db->update($this->inTblCatName, $aData); 
                    }
            return  array('rec-no'=>$outRes);       
        }
        //
        private function createTree($aRecords) {
        $outResult = array();
        try {
            
        } 
        catch (Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }
        return $outResult;
        }
        //
        public function outOneRecord($aData) {
            
        }
    }