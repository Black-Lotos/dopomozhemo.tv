<?php
class Site_model extends MY_Model {
    //private $inTblName = 'tb_site_settings';
    //private $inTblCategoryName = 'tb_menu_category';
    //private $inTblUnion = 'v_menu_category_to_menu';
    function __construct(){
        parent::__construct();
    }
    protected function StartUp() {
        $this->inTblName            = 'tb_site_settings';
        $this->inStatus             = ''; 
        $this->inStatusMain         = ''; 
        $this->inStatusTop          = '';
        $this->inSelfId             = 'ss_id'; 
        $this->inCategoryId         = '';
        $this->inSufix              = '';
        $this->inTblCategoryName    = '';
        $this->inTblUnion           = '';
        $this->inCountRec           = 0;
        $this->inOrderFields        = '';
    }
    public function loadSettings($aSiteId=1) {
        //$this->Debug();
        $resOut = $this->load(array($this->inSelfId=>$aSiteId),false);
        return $resOut;
    }
    public function getForm() {
        /* предпологается единая функция для вызова форм работа с базой. комплекс форм */
    }
    public function getOutput($aProcess=null,$aData=array()) {
        $outResult = array (    
            'property'=>array('include_js'=>"info.site.js",'template'=>'site/site-start-up.twig'),
        );
        switch ($aProcess) {
            case PROCESS_USER:
                $outResult['titles'] = array(
                    array('title'=>'ID','size'=>20),
                    array('title'=>'Логин пользователя','size'=>0),
                    array('title'=>'e-mail пользователя','size'=>0),
                    array('title'=>"Статус",'size'=>80),
                    array('title'=>'Действия','size'=>80)
                );
                $outResult['data']=array('user_id',"user_login","user_email","user_active",'action');
                break;
            case PROCESS_ROLES:
                $outResult['titles'] = array(
                    array('title'=>'ID','size'=>20),
                    array('title'=>'Роль пользователя','size'=>0),
                    array('title'=>'Действия','size'=>80)
                );
                $outResult['data']=array('role_id','role_title','&nbsp');
                break;
        }
        return  $outResult;
    }
}