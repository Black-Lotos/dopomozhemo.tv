<?php
    class Blogs_comments_model extends MY_Model {
        function __construct(){
            parent::__construct();
        }
        //
        protected function StartUp() {
            $this->inTblName = 'tb_comments';       
            $this->inTblCategoryName = 'tb_blogs';
            $this->inTblUnion = 'v_blogs_to_comments'; 
            $this->inCountRec           = 0;
            $this->inStatus = 'comments_status';    
            $this->inStatusMain='';    
            $this->inStatusTop='';    
            $this->inCategoryStatus = 'blogs_status';
            $this->inSelfId = 'comments_id';        
            $this->inSelfName = 'comments_text';
            $this->inCategoryId='blogs_id';        
            $this->inCategoryName = 'blogs_title'; 
            $this->inSufix='comments';              
            $this->inOrderFields='comments_created';
            $this->inDebug              = false;
            $this->inAlias              = '';
        }
        //
        public function getForm($aData=array(), $aProcess=null){
            $outResult = array (    
            );
            if ($aProcess==PROCESS_NEWS_SECTION) {
            }
            if ($aProcess==PROCESS_NEWS_SECTION_ADD) {
            }
            return  $outResult;
        }
        //
        public function getOutput($aProcess=null,$aData=array(),$aUnion=true,$aPage=1) {
            $outResult = array (    
                'property'=>array('title'=>getCaptionInput('caption_news'),'isRun'=>true,'include_js'=>"info.{$this->inSufix}.js",'template'=>'blogs/blogs-start-up.twig'),
                'titles'=>array(),
                'data'=>array(),
                'info'=>array(),
            );
            switch ($aProcess) {
                case OUTPUT_LIST:
                    $outResult['data']= $this->load($aData,$aUnion,$aPage);
                    $outResult['info']['count_record']=count($outResult['data']);
                    
                    foreach ($outResult['data'] as $inKey=>$inData) {
                        if (empty($inData['news_creates'])) {
                            unset($outResult['data'][$inKey]);
                        }
                        else {
                            //echo "<pre>"; var_dump($inData['news_creates']); die();
                            //$inData['news_creates'] = getDecodeDate($inData['news_creates']);
                            if (mb_strlen($inData['news_text'])>OUT_CUT_STRING) {
                                //die("yes");
                                $inData['cut_text'] =  mb_substr($inData['news_text'], 0, OUT_CUT_STRING).'...';
                                $inData['detail'] = true;
                            }
                            $outResult['data'][$inKey] = $inData;
                        }
                    }
                    break;
                default :
                    $outResult['titles'] = array (    
                        array(
                            array('title'=>'ID','size'=>20),
                            array('title'=>'Заголовок блога','size'=>0),
                            array('title'=>"Статус",'size'=>80),
                            array('title'=>'Действия','size'=>80)
                        ),
                        'data'=>array($this->inSelfId,"{$this->inSufix}_title","{$this->inSufix}_status",'action')        
                    );
            }
            return  $outResult;
        }
        //
    }