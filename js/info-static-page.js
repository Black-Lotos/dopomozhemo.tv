$(document).ready(function(){ 
    //
    $("[type=text], textarea").on({
        blur: function() {
            var $outCheck;
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                $(this).attr('verify','false');
                //$outControl = $(this).attr('id');
                } else {
                    $(this).removeClass('error');
                    $(this).attr('verify','true');
                }
        },
    })
    //
    function checkTextEmpty() {
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
        return  $outCheck;   
    }
    //
    $("#btnSaveStaticPageCategory").on ({
        click : function() {
            //alert(checkTextEmpty());
            if (checkTextEmpty()) {
                $.ajax({
                        type: 'POST',
                        url: '/administration/admin_static/save_category',
                        //dataType: 'JSON',
                        data:$(this).parent().parent().serialize()+"&process=category-save",
                        success: function(Responce) {
                            location.reload();
                            //alert(Responce);
                        }
                });     
            }
            return false;
        }
    });
    $("#btnSaveStaticPage").on ({
        click : function() {
            if (checkTextEmpty()) {
                $.ajax({
                        type: 'POST',
                        url: '/administration/admin_static/save',
                        //dataType: 'JSON',
                        data:$(this).parent().parent().serialize()+"&process=item-save",
                        success: function(Responce) {
                            //alert(Responce);
                            location.reload();
                        }
                });     
            }
            return false;
        }
    });
    $("[id^='ref-spc-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_static/edit_section',
                //dataType: 'JSON',
                data:{process: 'edit-category', spc_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                }
            });     
        }
    });
    $("[id^='ref-sp-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_static/edit',
                //dataType: 'JSON',
                data:{process: 'edit-item', sp_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                }
            });     
        }
    });
    $("[id^='ref-sp-delete-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_static/delete',
                //dataType: 'JSON',
                data:{process: 'delete-item', sp_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
        }
    });
    $("[id^='ref-sp-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_static/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', sp_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
        }
    });
    $("[id^='ref-spc-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_static/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-category', spc_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
        }
    });
})