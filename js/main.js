$(document).ready(function(){
    $('#carousel-top').owlCarousel({
      loop:true,
      nav:true,
      items:2,
      slideSpeed: 200,
      autoPlay: true
    }),
     $('#carousel-video').owlCarousel({
      loop:true,
      margin:1,
      nav:true,
      items:4
      //autoHeight : true
    });
    $('#carousel-small-images').owlCarousel({
            loop:true,
            margin:1,
            autowidth:true,
            nav:true,
            items:4
    });
    $('#btnAutorizationSubmit').click(function(){
        $.ajax({
            type: "post",
            url: "/users/login",
            data: {user_login:$('#edtLogin').val(),user_password:$('#edtPassword').val()},
            success: function(res){
                location.reload();
            }
        });
        return false;
    });
    $('#btnUserLogOut').click(function(){
        $.ajax({
            type: "post",
            url: "/users/logout",
            data: {user_action:'LogOut'},
            success: function(res){
                location.reload();
              //alert(res);
            }
      });
      return false;
    });
})