$(document).ready(function(){ 
    $("#divPictureTabs").tabs();
    $("[type=text], textarea").each(function(){
        $(this).on('keypress keydown keyup',function(){
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                //$outResult = ($outResult && false); 
            } else {
                $(this).removeClass('error');
            }
        });
       
    })
    //
    $("#btnSaveblogs").on ({
        click : function() {
            if (checkTextEmpty()) {
                $.ajax({
                        type: 'POST',
                        url: '/administration/admin_content_video/save',
                        //dataType: 'JSON',
                        data:{data_form: $(this).parent().parent().serializeArray()},
                        success: function(Responce) {
                            location.reload();
                        }
                });     
            }
            return false;
        }
    });
    //
    $("[type=text], textarea").on({
        blur: function() {
            var $outCheck;
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                $(this).attr('verify','false');
                //$outControl = $(this).attr('id');
                } else {
                    $(this).removeClass('error');
                    $(this).attr('verify','true');
                }
        },
    })
    //
    $("[id^='ref-img_-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_content_video/edit',
                //dataType: 'JSON',
                data:{Process: 'edit', blogs_id:$(this).attr('id')},
                success: function(Responce) {
                    $(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-img_-status-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_content_video/change_status',
                //dataType: 'JSON',
                data:{Process: 'change-status', language_id:$(this).attr('id')},
                success: function(Responce) {
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    function checkTextEmpty() {
        //alert('checkTextEmpty');
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
        return  $outCheck;   
    }
})


