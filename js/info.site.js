$(document).ready(function(){ 
    if ($("#divSettingsTabs").length>0)
        $("#divSettingsTabs").tabs();
    
    if ($("#btnSaveVariable").length>0) {
        $("#btnSaveVariable").on({
            click:function() {
                if (checkTextEmpty()) {
                    $(this).parent().parent().submit();
                };
                return false;
            } 
        });
    }
    $("[id^='ref-variable-edit-']").on({
        click : function(){
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_settings/edit',
                //dataType: 'JSON',
                data:{process: 'edit-variable', variable_id:$inId},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
            console.log("info.site => edit => variable => "+$inId);
            return false;
        }
    });
    $("[id^='ref-variable-delete-']").on({
        click : function(){
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_settings/delete_variable',
                //dataType: 'JSON',
                data:{process: 'delete-variable', variable_id:$inId},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            console.log("info.site => edit => variable => "+$inId);
            return false;
        }
    });
    function checkTextEmpty() {
        //alert('checkTextEmpty');
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
        return  $outCheck;   
    }
});