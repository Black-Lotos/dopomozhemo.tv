$(document).ready(function(){ 
    console.log("info-content"); // string 1 rad baz tubular
    if ($("#content_text").length>0) {
        {$('#content_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};   
    }
    //
    $("#btnClearImagePreview").on({
        click:function() {
            var $inIdDiv = $(this).attr('url');
            var $inSelf = "#"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_content_video/clear_image',
                //dataType: 'JSON',
                data:{process: 'clear-image-content', content_id:$(this).attr('for')},
                success: function(Responce) {
                    $($inIdDiv).hide();
                    $($inSelf).hide();
                    //$("#form-ajax").html(Responce);
                }
            });     
            //alert();
            return false;
        }
    })
    //
    $("#btnSavecontent").on ({
        click : function() {
            if (checkTextEmpty()) {
                $("#frmcontent-add").submit();
                /*$.ajax({
                        type: 'POST',
                        url: '/administration/admin_content_video/save',
                        //dataType: 'JSON',
                        data:{data_form: $("#frmcontent-add").serializeArray()},
                        success: function(Responce) {
                            $("#content_text").val(Responce);
                            //location.reload('/administration/admin_content_video');
                        }
                });*/
            }
            return false;
        }
    });
    //
    $("#btnSaveSection").on ({
        click : function() {
            //alert(checkTextEmpty());
            if (checkTextEmpty()) {
                $.ajax({
                        type: 'POST',
                        url: '/administration/admin_content_video/save_category',
                        //dataType: 'JSON',
                        data:{data_form: $(this).parent().parent().serializeArray()},
                        success: function(Responce) {
                            //$("#form-ajax").html(Responce);
                            location.reload();
                        }
                });     
            }
            return false;
        }
    });
    //
    $("[type=text], textarea").on({
        blur: function() {
            var $outCheck;
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                $(this).attr('verify','false');
                //$outControl = $(this).attr('id');
                } else {
                    $(this).removeClass('error');
                    $(this).attr('verify','true');
                }
        },
    })
    //
    
    
    //
    
    $("[id^='detail-']").on({
        click:function() {
            //alert('click');
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin_content_video/detail',
                //dataType: 'JSON',
                data:{process: 'content-detail', blogs_id:$inId},
                success: function(Responce) {
                    $('#PagingRecord').html(Responce);
                }
            });     
        }
    });
    //
    function checkTextEmpty() {
        //alert('checkTextEmpty');
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
            //alert($outCheck);
        return  $outCheck;   
    }
    //
    function checkTextEmpty01() {
        //alert('checkTextEmpty');
        var $outCheck = true;
        $("#frmSaveComments").find("input,select,textarea").not('[type="submit"]').not('[type="hidden"]').each(function(){
            var $inVal = $.trim($(this).val());
                if ($inVal<1) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
                        
        });
        return  $outCheck;   
    }
    //
    $("#frmSaveComments").submit(function(){
        var goSubmit = true;
        goSubmit = checkTextEmpty01();
        //alert(goSubmit);
        return goSubmit;
    });
    //
   $("[id^='ref-content-video-']").on({
        click:function() {
            
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_content_video/content_code',
                //dataType: 'JSON',
                data:{process: 'content-code', content_id:$inId},
                success: function(Responce) {
                    window.prompt ('Ваша ссылка',Responce);
                }
            });
            
        }
    });
})


