$(document).ready(function(){ 
    
    /**/
    $("#list").on({
        click:function() {
            $.ajax({
                type: 'get',
                url: $(this).attr('url'),
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                }
           });
           return false;
        } 
    });
    $("#add").on({
       click:function() {
            $.ajax({
                 type: 'get',
                 url: $(this).attr('url'),
                 success: function(Responce) {
                     $("#form-ajax").html(Responce);
                 }
            });
            return false;
       } 
    });
    
    $("#add_section").on({
       click:function() {
            $.ajax({
                 type: 'get',
                 url: $(this).attr('url'),
                 success: function(Responce) {
                     $("#form-ajax").html(Responce);
                 }
            });
            return false;
       } 
    });
    
    $("#add_multi_variable").on({
       click:function() {
            $.ajax({
                 type: 'get',
                 url: $(this).attr('url'),
                 success: function(Responce) {
                     $("#form-ajax").html(Responce);
                 }
            });
            return false;
       } 
    });
    /**/
    $("[id^='ref-spc-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_static/edit_section',
                //dataType: 'JSON',
                data:{process: 'edit-category', spc_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                }
            });
            return false;
        }
    });
    
    $("[id^='ref-sp-edit-']").on({
        click:function() {
            $.ajax({
                type: 'POST',
                url: '/admin/admin_static/edit',
                //dataType: 'JSON',
                data:{process: 'edit-item', sp_id:$(this).attr('id')},
                success: function(Responce) {
                    //alert(Responce);
                    $("#form-ajax").html(Responce);
                }
            });
            return false;
        }
    });
    /**/
    /* РАБОТА с ЭКСПЕРТАМИ */
    $("[id^='ref-node-edit-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            console.log("category-edit: "+$inId);
            
            $.ajax({
                url:"/admin/admin_expert_help/edit_section",
                method:'post',
                //dataType:'json',
                data:{process: 'category-edit', category_id:$inId},
                success:function(Responce){
                    $("#form-ajax").html(Responce);
                }
            })
        }
    });
    $("[id^='ref-items-edit-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            console.log("items-edit: "+$inId);
            $.ajax({
                url:"/admin/admin_expert_help/edit",
                method:'post',
                //dataType:'json',
                data:{process: 'items-edit', items_id:$inId},
                success:function(Responce){
                    $("#form-ajax").html(Responce);
                }
            })
        }
    });
    $("[id^='ref-node-delete-']").on({
        click:function() {
            //alert($(this).attr('id'));
        }
    });
    $("[id^='ref-items-delete-']").on({
        click:function() {
            //alert($(this).attr('id'));
        }
    });
    $("[id^='ref-items-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                url:"/admin/admin_expert_help/change_status",
                method:'post',
                //dataType:'json',
                data:{process: 'change-status', items_id:$inId},
                success:function(Responce){
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            })
        }
    });
    $("[id^='ref-node-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                url:"/admin/admin_expert_help/change_status",
                method:'post',
                //dataType:'json',
                data:{process: 'change-status-category', category_id:$inId},
                success:function(Responce){
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            })
        }
    });
    /* конец РАБОТА с ЭКСПЕРТАМИ*/
    /* РАБОТА С БЛОГАМИ */
    $("[id^='ref-blogs-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/admin/admin_blogs/edit',
                //dataType: 'JSON',
                data:{Process: 'edit-blogs', blogs_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-blogs-delete-']").on({
        click:function() {
            //alert($(this).attr('id'));
            if(confirm("Внимание! Будет удален блог и коментарии к нему. Продолжить")) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/admin_blogs/delete',
                    //dataType: 'JSON',
                    data:{process: 'delete-blogs', blogs_id:$(this).attr('id')},
                    success: function(Responce) {
                        //$("#form-ajax").html(Responce);
                        location.reload();
                    }
                });     
            }
        }
    });
    $("[id^='ref-cblogs-delete-']").on({
        click:function() {
            //alert($(this).attr('id'));
            if(confirm("Внимание! Будет удалена категория блогов и все блоги в ней. Продолжить?")) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/admin_blogs/delete_section',
                    //dataType: 'JSON',
                    data:{process: 'delete-cblogs', cblogs_id:$(this).attr('id')},
                    success: function(Responce) {
                        //$("#form-ajax").html(Responce);
                        location.reload();
                    }
                });     
            }
        }
    });
    $("[id^='ref-cblogs-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/admin/admin_blogs/edit_section',
                //dataType: 'JSON',
                data:{process: 'edit-cblogs', cblogs_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-blogs-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_blogs/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', blogs_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
        }
    });
    //
    $("[id^='ref-cblogs-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_blogs/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-category', cblogs_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
        }
    });
    //
    $("[id^='ref-blogs-main-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_blogs/change_status',
                //dataType: 'JSON',
                data:{process: 'change-main', blogs_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-main-un-checked');
                        $("#"+$inId).addClass('action-base action-main-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-main-checked');
                        $("#"+$inId).addClass('action-base action-main-un-checked');
                    }
                }
            });     
        }
    });
    /* КОНЕЦ РАБОТА С БЛОГАМИ */
    /* РАБОТА С ВИДЕО */
    $("[id^='ref-content-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/admin/admin_content_video/edit',
                //dataType: 'JSON',
                data:{Process: 'edit-content', content_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-content-delete-']").on({
        click:function() {
            //alert($(this).attr('id'));
            if (confirm('Внимание! Вы запросили удаление. Продолжить?')) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/admin_content_video/delete',
                    //dataType: 'JSON',
                    data:{Process: 'delete-content', content_id:$(this).attr('id')},
                    success: function(Responce) {
                        //$("#form-ajax").html(Responce);
                        location.reload();
                    }
                });     
            }
        }
    });
    //
    $("[id^='ref-content-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_content_video/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', content_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-ccontent-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/admin/admin_content_video/edit_section',
                //dataType: 'JSON',
                data:{process: 'edit-category', ccontent_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                }
            });     
        }
    });
    //
    $("[id^='ref-ccontent-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_content_video/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-category', ccontent_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
        }
    });
    $("[id^='ref-content-main-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_content_video/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-main', content_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-main-un-checked');
                        $("#"+$inId).addClass('action-base action-main-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-main-checked');
                        $("#"+$inId).addClass('action-base action-main-un-checked');
                    }
                }
            });     
        }
    });
    /* КОНЕЦ РАБОТЫ С ВИДЕО*/
    /* НОВОСТИ */
    // редактировать
    $("[id^='ref-news-edit-']").each(function(){
        $(this).on({
            click:function() {
                $.ajax({
                type: 'POST',
                url: '/admin/admin_news/edit',
                //dataType: 'JSON',
                data:{process: 'edit-news', news_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
                });  
                return false;
            }
        })
    });
    // удалить
    $("[id^='ref-news-delete-']").on({
        click:function() {
            if(confirm("Внимание! Будет удалена новость и коментарии к ней. Продолжить?")) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/admin_news/delete',
                    //dataType: 'JSON',
                    data:{process: 'delete-news', news_id:$(this).attr('id')},
                    success: function(Responce) {
                        //$("#form-ajax").html(Responce);
                        location.reload();
                    }
                });     
            }
            return false;
        }
    });
    // сменить статус
    $("[id^='ref-news-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_news/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', news_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
            return false;
        }
    });
    // 
    $("[id^='ref-snews-edit-']").on({
        click:function() {
            //alert('edit');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_news/edit_section',
                //dataType: 'JSON',
                data:{process: 'edit-snews', nc_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
            return false;
        }
    });
    $("[id^='ref-snews-delete-']").on({
        click:function() {
            if(confirm("Внимание! Будет удалена категория и все новости в ней. Продолжить?")) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/admin_news/delete_category',
                    //dataType: 'JSON',
                    data:{process: 'delete-snews', nc_id:$(this).attr('id')},
                    success: function(Responce) {
                        //alert(Responce);
                        location.reload();
                    }
                });     
            }
            return false;
        }
    });
    $("[id^='ref-snews-check-']").on({
        click:function() {
            //alert('click');
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_news/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-category', nc_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
            return false;
        }
    });
    $("[id^='ref-news-main-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_news/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-main', news_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-main-un-checked');
                        $("#"+$inId).addClass('action-base action-main-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-main-checked');
                        $("#"+$inId).addClass('action-base action-main-un-checked');
                    }
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    /* КОНЕЦ РАБОТЫ С НОВОСТЯМИ*/
    /**/
    $("[id^='ref-mc-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_menu/edit_section',
                //dataType: 'JSON',
                data:{process: 'menu-edit', menu_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
            //alert($inDivId);
            return false;
        }
    });
    //
    $("[id^='ref-mc-delete-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_menu/delete_section',
                //dataType: 'JSON',
                data:{process: 'menu-delete', menu_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            //alert($inDivId);
            return false;
        }
    });
    //
    $("[id^='ref-mc-delete-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_menu/delete_section',
                //dataType: 'JSON',
                data:{process: 'menu-delete', menu_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            //alert($inDivId);
            return false;
        }
    });
    //
    $("[id^='ref-mc-check-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_menu/ChangeStatus',
                //dataType: 'JSON',
                data:{process: 'menu-check', menu_item_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            //alert($inDivId);
            return false;
        }
    });
    $("[id^='ref-menu-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_menu/edit',
                //dataType: 'JSON',
                data:{process: 'menu-item-edit', menu_item_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-menu-delete-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_menu/delete',
                //dataType: 'JSON',
                data:{process: 'menu-item-delete', menu_item_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            //alert($inDivId);
            return false;
        }
    });
    //
    //
    $("[id^='ref-menu-check-']").on({
        click:function() {
            var $inId = "#div-"+$(this).attr('id');
            //alert('click');
            $.ajax({
                type: 'POST',
                url: '/admin/admin_menu/ChangeStatus',
                //dataType: 'JSON',
                data:{process: 'menu-item-check', menu_item_id:$(this).attr('id')},
                success: function(Responce) {
                    location.reload();
                }
            });     
            //alert($inDivId);
            return false;
        }
    });
    /**/
    /**/
    if ($('#spc_text').length>0)            
        {$('#spc_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript | ",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#sp_text').length>0)             
        {$('#sp_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#blogs_text').length>0)          
        {$('#blogs_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#news_text').length>0)           
        {$('#news_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#content_text').length>0)        
        {$('#content_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
            extended_valid_elements : "iframe[name|src|framespacing|border|frameborder|scrolling|title|height|width]", 
            media_strict: false,
        })};
    if ($('#question_text').length>0)       
        {$('#question_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#proposal_text').length>0)       
        {$('#proposal_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#footerlink_text').length>0)     
        {$('#footerlink_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#ask_to_contact_text').length>0) 
        {$('#ask_to_contact_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#proposal_answer').length>0)     
        {$('#proposal_answer').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};    
    if ($('#proffessional_text').length>0)  
        {$('#proffessional_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};    
    if ($('#picture_text').length>0)        
        {$('#picture_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};    
    if ($('#variable_value').length>0)        
        {$('#variable_value').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};    
    if ($('#variable_value_ua').length>0)        
        {$('#variable_value_ua').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#variable_value_en').length>0)        
        {$('#variable_value_en').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    //
    
    function checkTextEmpty() {
        //alert('checkTextEmpty');
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
        return  $outCheck;   
    }
    /*$("[type=text], textarea").each(function(){
        $(this).on('keypress keydown keyup',function(){
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                //alert('error');
                //$outResult = ($outResult && false); 
            } else {
                $(this).removeClass('error');
            }
            var $inSpan = "#span_"+$(this).attr('id');
            var $inSpanValue = parseInt($($inSpan).attr('base'), 10); 
            var $inDigit = $inSpanValue-$inVal.length;
            //alert($inVal+" "+$inSpanValue);
            if ($inDigit>=0) {
                $($inSpan).html($inSpanValue-$inVal.length);
            }
        });
    });
    */
    if($('#output-system-success').length>0) {
        setTimeout(function(){$('#output-system-success').fadeOut()}, 5000);
    };
});

