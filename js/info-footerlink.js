$(document).ready(function(){ 
    if ($("#divFLTabs").length>0) {
        $("#divFLTabs").tabs();
    }
    console.log("info-footerlink"); // string 1 rad baz tubular
    $("[type=text], textarea").each(function(){
        $(this).on('keypress keydown keyup',function(){
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                //$outResult = ($outResult && false); 
            } else {
                $(this).removeClass('error');
            }
        });
       
    })
    //
    $("#btnSavefooterlink").on ({
        click : function() {
            if (checkTextEmpty()) {
                $("#frmfooterlink-add").submit();
            }
            return false;
        }
    });
    //
    $("#btnSaveСfooterlink").on ({
        click : function() {
            //alert('click');
            if (checkTextEmpty()) {
                $.ajax({
                        type: 'POST',
                        url: '/administration/admin_footerlink/save_category',
                        //dataType: 'JSON',
                        data:{data_form: $(this).parent().parent().serializeArray()},
                        success: function(Responce) {
                            location.reload('/administration/footerlink');
                            //$("#cfooterlink_title").html(Responce);
                        }
                });     
            }
            return false;
        }
    });
    //
    $("[type=text], textarea").on({
        blur: function() {
            var $outCheck;
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                $(this).attr('verify','false');
                //$outControl = $(this).attr('id');
                } else {
                    $(this).removeClass('error');
                    $(this).attr('verify','true');
                }
        },
    })
    //
    $("[id^='ref-footerlink-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_footerlink/edit',
                //dataType: 'JSON',
                data:{Process: 'edit-footerlink', footerlink_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-footerlink-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_footerlink/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', footerlink_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-footerlink-delete-']").on({
        click:function() {
            $.ajax({
                type: 'POST',
                url: '/administration/admin_footerlink/delete',
                //dataType: 'JSON',
                data:{process: 'delete-footerlink', footerlink_id:$(this).attr('id')},
                success: function(Responce) {
                    //$(".divOutputBox").html(Responce);
                    location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-сfooterlink-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_footerlink/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-category', cfooterlink_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
        }
    });
    //
    $("[id^='ref-footerlink-main-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_footerlink/change_status',
                //dataType: 'JSON',
                data:{process: 'change-main', footerlink_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-main-un-checked');
                        $("#"+$inId).addClass('action-base action-main-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-main-checked');
                        $("#"+$inId).addClass('action-base action-main-un-checked');
                    }
                }
            });     
        }
    });
    //
    $(".tree").on({
        click:function() {
            
        }
    })
    //
    function checkTextEmpty() {
        //alert('checkTextEmpty');
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
            alert($outCheck);
        return  $outCheck;   
    }
    //
    function checkTextEmpty01() {
        //alert('checkTextEmpty');
        var $outCheck = true;
        $("#frmSaveComments").find("input,select,textarea").not('[type="submit"]').not('[type="hidden"]').each(function(){
            var $inVal = $.trim($(this).val());
                if ($inVal<1) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
                        
        });
            /*// дополнительная проверка на пустоту
            $("#frmSaveComments:input").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                if ($inVal<1) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });*/
        return  $outCheck;   
    }
    //
    $("#frmSaveComments").submit(function(){
        var goSubmit = true;
        goSubmit = checkTextEmpty01();
        //alert(goSubmit);
        return goSubmit;
    });
    
})


