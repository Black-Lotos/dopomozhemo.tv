$(document).ready(function(){ 
    if ($("#divNewsTabs").length>0) {
        $("#divNewsTabs").tabs();
    }
    console.log("info-proposal"); // string 1 rad baz tubular
    $("[type=text], textarea").each(function(){
        $(this).on('keypress keydown keyup',function(){
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                //$outResult = ($outResult && false); 
            } else {
                $(this).removeClass('error');
            }
        });
       
    })
    //
    $("#btnSaveproposal").on ({
        click : function() {
            //alert(checkTextEmpty());
            if (checkTextEmpty()) {
                /*$.ajax({
                        type: 'POST',
                        url: '/administration/proposal/save',
                        //dataType: 'JSON',
                        data:{data_form: $(this).parent().parent().serializeArray()},
                        success: function(Responce) {
                            location.reload(Responce);
                        }
                });     */
                $("#frmproposal-add").submit();
            }
            return false;
        }
    });
    //
    $("[type=text], textarea").on({
        blur: function() {
            var $outCheck;
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                $(this).attr('verify','false');
                //$outControl = $(this).attr('id');
                } else {
                    $(this).removeClass('error');
                    $(this).attr('verify','true');
                }
        },
    })
    //
    $("[id^='ref-proposal-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_proposal/edit',
                //dataType: 'JSON',
                data:{Process: 'edit-proposal', proposal_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-proposal-delete-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_proposal/delete',
                //dataType: 'JSON',
                data:{process: 'delete-proposal', proposal_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
        }
    });
    $("[id^='ref-proposal-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_proposal/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', proposal_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-сproposal-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_proposal/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-category', cproposal_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
        }
    });
    //
    
    $("[id^='detail-']").on({
        click:function() {
            //alert('click');
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/proposal/detail',
                //dataType: 'JSON',
                data:{process: 'proposal-detail', proposal_id:$inId},
                success: function(Responce) {
                    $('#PagingRecord').html(Responce);
                }
            });     
        }
    });
    //
    $(".tree").on({
        click:function() {
            
        }
    })
    //
    function checkTextEmpty() {
        //alert('checkTextEmpty');
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
        return  $outCheck;   
    }
    //
    function checkTextEmpty01() {
        //alert('checkTextEmpty');
        var $outCheck = true;
        $("#frmSaveComments").find("input,select,textarea").not('[type="submit"]').not('[type="hidden"]').each(function(){
            var $inVal = $.trim($(this).val());
                if ($inVal<1) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
                        
        });
        return  $outCheck;   
    }
    //
    $("#frmSaveComments").submit(function(){
        var goSubmit = true;
        goSubmit = checkTextEmpty01();
        //alert(goSubmit);
        return goSubmit;
    });
    //
    $("#btnAddProposal").on({
        click:function() {
            /*$.ajax({
                type: 'POST',
                url: '/proposal/add',
                //dataType: 'JSON',
                data:{process: 'proposal-add'},
                success: function(Responce) {
                    $('#container_proposal').html(Responce);
                }
            });     */
        }
    });
    
})


