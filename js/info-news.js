$(document).ready(function(){ 
    if ($("#divNewsTabs").length>0) {
        $("#divNewsTabs").tabs();
    }
    console.log("info-news work"); // string 1 rad baz tubular
    /*if ($('#news_text').length>0){
        $('#news_text').ckeditor()
    };*/
        
    $("[id^='picture-to-div-']").on({
        click:function() {
            var $inImg = $(this).attr('id');
            var $regPattern = /(\d)+$/;
            var $regRes = $regPattern.exec($inImg);
            //alert($regRes[0]);
            $("#div-preview-img").html("<img src='/images_module/getPicture/"+$regRes[0]+"/400/400'/>");
        }
    });
    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);
    
    $("#news_creates").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        onSelect: function(datetext){
            var d = new Date(); // for now
            datetext=datetext+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
            $('#news_creates').val(datetext);
        },
    });
    $("[type=text], textarea").each(function(){
        $(this).on('keypress keydown keyup',function(){
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                //$outResult = ($outResult && false); 
            } else {
                $(this).removeClass('error');
            }
            var $inSpan = "#span_"+$(this).attr('id');
            var $inSpanValue = parseInt($($inSpan).attr('base'), 10); 
            var $inDigit = $inSpanValue-$inVal.length;
            //alert($inVal+" "+$inSpanValue);
            if ($inDigit>=0) {
                $($inSpan).html($inSpanValue-$inVal.length);
            }
        });
       
    })
    //
    $("[type=text], textarea").on({
        blur: function() {
            var $outCheck;
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                $(this).attr('verify','false');
                //$outControl = $(this).attr('id');
                } else {
                    $(this).removeClass('error');
                    $(this).attr('verify','true');
                }
        },
    })
    //
    $("#btnSaveNews").on ({
        click : function() {
            if (checkTextEmpty()) {
                $("#frmNews-addnews").submit();
                //location.reload();
            }
            return false;
        }
    });
    //
    $("#btnSaveCNews").on ({
        click : function() {
            //alert('click');
            if (checkTextEmpty()) {
                $.ajax({
                        type: 'POST',
                        url: '/administration/admin_news/save_category',
                        //dataType: 'JSON',
                        data:{data_form: $(this).parent().parent().serializeArray()},
                        success: function(Responce) {
                            location.reload();
                        }
                });     
            }
            return false;
        }
    });
    //
    $("[id^='ref-news-edit-']").on({
        click:function() {
            //alert('edit');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/edit',
                //dataType: 'JSON',
                data:{process: 'edit-news', news_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-news-delete-']").on({
        click:function() {
            if(confirm("Внимание! Будет удалена новость и коментарии к ней. Продолжить?")) {
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_news/delete',
                    //dataType: 'JSON',
                    data:{process: 'delete-news', news_id:$(this).attr('id')},
                    success: function(Responce) {
                        //$("#form-ajax").html(Responce);
                        location.reload();
                    }
                });     
            }
        }
    });
    $("[id^='ref-cnews-delete-']").on({
        click:function() {
            if(confirm("Внимание! Будет удалена категория и все новости в ней. Продолжить?")) {
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_news/delete_category',
                    //dataType: 'JSON',
                    data:{process: 'delete-cnews', nc_id:$(this).attr('id')},
                    success: function(Responce) {
                        //alert(Responce);
                        location.reload();
                    }
                });     
            }
        }
    });
    $("[id^='ref-cnews-edit-']").on({
        click:function() {
            //alert('edit');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/edit_section',
                //dataType: 'JSON',
                data:{process: 'edit-cnews', nc_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-news-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', news_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-news-main-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-main', news_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-main-un-checked');
                        $("#"+$inId).addClass('action-base action-main-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-main-checked');
                        $("#"+$inId).addClass('action-base action-main-un-checked');
                    }
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-cnews-check-']").on({
        click:function() {
            //alert('click');
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-category', nc_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
        }
    });
    $("[id^='ref-news-delete-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/delete',
                //dataType: 'JSON',
                data:{process: 'delete-news', news_id:$(this).attr('id')},
                success: function(Responce) {
                    
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("#btnClearImageElement").on({
        click:function() {
            //alert('click');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/clear_image',
                dataType: 'json',
                data:{process: 'clear-image-news', news_id:$(this).attr('for')},
                success: function(Responce) {
                    $.each(Responce,function(inKey, inData){
                        //alert(inKey+" "+inData);
                        console.log(inKey+":"+inData);
                        if(inKey=='rec-no') {
                            $("#images_id").val(inData);
                        };
                        if(inKey=='rec-name') {
                            $("[id^='preview-image-']").attr('src',"/"+inData);
                        }
                    });
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
            //alert();
            return false;
        }
    })
    //
    function checkTextEmpty() {
        //alert('checkTextEmpty');
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
        return  $outCheck;   
    }
    //
    function checkTextEmpty01() {
        //alert('checkTextEmpty');
        var $outCheck = true;
        $("#frmSaveComments").find("input,select,textarea").not('[type="submit"]').not('[type="hidden"]').each(function(){
            var $inVal = $.trim($(this).val());
                if ($inVal<1) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
                        
        });
        return  $outCheck;   
    }
    //
    
    //
    $("#frmSaveComments").submit(function(){
        var goSubmit = true;
        goSubmit = checkTextEmpty01();
        //alert(goSubmit);
        return goSubmit;
    });
    //
    
    //
    /*$("[id^='detail-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin_news/detail',
                //dataType: 'JSON',
                data:{process: 'news-detail', news_id:$inId},
                success: function(Responce) {
                    $('#PagingRecord').html(Responce);
                }
            });     
        }
    });*/
})


