$(document).ready(function(){ 
    //alert('User');
    $("#divUsersTabs").tabs();
    $("#divUsersRolesTabs").tabs();
    $("#btnLogin").on({
        click:function() {
            var inUrl = $(this).attr('url');
            //alert(inUrl);
            if (checkTextEmpty()) {
                /*$.ajax({
                    type: 'POST',
                    url: inUrl,
                    //dataType: 'JSON',
                    data:{user_login:$('#user_login').val(),user_pass:$('#user_pass').val()},
                    success: function(Responce) {
                        location.reload();
                    }
                });     */
                $("#frmUserLogin").submit();
            }
            return false;
        }
    })
    //
    $("#btnSaveUser").on ({
        click : function() {
            //alert('click user save');
            if (checkTextEmpty()) {
                $.ajax({
                        type: 'POST',
                        url: '/administration/users/save',
                        //dataType: 'JSON',
                        data:{process:'user-save',data_form: $(this).parent().parent().serializeArray()},
                        success: function(Responce) {
                            $(".divOutputForm").html(Responce);
                            //alert(Responce);
                            //location.reload();
                    }
                });     
            }
            return false;
        }
    });
    //
    $("#btnSaveRoles").on ({
        click : function() {
            //alert(checkTextEmpty());
            if (checkTextEmpty()) {
                $.ajax({
                        type: 'POST',
                        url: '/administration/users/save/roles',
                        //dataType: 'JSON',
                        data:{process:'role-save',data_form: $(this).parent().parent().serializeArray()},
                        success: function(Responce) {
                            $(".divOutputForm").html(Responce);
                            //alert(Responce);
                            //location.reload();
                    }
                });     
            }
            return false;
        }
    });
    //
    $("[type=text], textarea").on({
        blur: function() {
            var $outCheck;
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                $(this).attr('verify','false');
                //$outControl = $(this).attr('id');
                } else {
                    $(this).removeClass('error');
                    $(this).attr('verify','true');
                }
        },
    })
    //
    $("[id^='ref-user-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/users/edit',
                //dataType: 'JSON',
                data:{Process: 'edit-user', user_id:$(this).attr('id')},
                success: function(Responce) {
                    $(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    function checkTextEmpty() {
        //alert('checkTextEmpty');
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
        return  $outCheck;   
    }
})


