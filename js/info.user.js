$(document).ready(function(){ 
    if ($("#divUsersTabs").length>0) {
        $("#divUsersTabs").tabs();
    }
    if ($("#divUsersRolesTabs").length>0) {
        $("#divUsersRolesTabs").tabs();
    }
    console.log("info.user"); // string 1 rad baz tubular
    /* roles */
    $("[id^='ref-role-edit-']").on({
        click : function(){
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/users/edit_roles',
                //dataType: 'JSON',
                data:{process: 'edit-role', role_id:$inId},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
            console.log("info.user => edit => role => "+$inId);
            return false;
        }
    });
    $("[id^='ref-role-delete-']").on ({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/users/delete_roles',
                //dataType: 'JSON',
                data:{process: 'delete-role', role_id:$inId},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            console.log("info.user => delete => role => "+$inId);
            return false;
        }
    });
    $("#btnSaveRoles").on({
        click:function() {
            console.log("info.user => save => role => "+$(this).parent().parent().attr('id'));
            if(checkTextEmpty()) {
                $(this).parent().parent().submit();
                console.log("info.user => save => role => submit");
            }    
            return false;
        }
    });
    /* end roles*/
    
    $("[id^='ref-user-edit-']").on({
        click : function(){
            var $inId = $(this).attr('id');
            console.log("info.user => edit => user => "+$inId);
            $.ajax({
                type: 'POST',
                url: '/administration/users/edit',
                //dataType: 'JSON',
                data:{process: 'edit-user', user_id:$inId},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
            return false;
        }
    });
    $("[id^='ref-user-delete-']").on ({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/users/delete',
                //dataType: 'JSON',
                data:{process: 'delete-user', user_id:$inId},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            console.log("info.user => delete => role => "+$inId);
            return false;
        }
    });
    $("[id^='ref-user-check-']").on ({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/users/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', user_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                    location.reload();
                }
            });     
            return false;
        }
    });
    //
    $("#btnSaveUser").on ({
        click : function() {
            $(this).parent().parent().submit();
            return false;
        }
    });
    $("#btnSaveUserAjax").on ({
        click : function() {
            if (checkTextEmpty()) {
                $(this).parent().parent().submit();
            }
            return false;
        }
    });
    $("#frmUserAddAjax").on({
        submit:function() {
            var $inUrl      = $(this).attr('action');
            var outData = $(this).serialize();
            //alert($inUrl);
            $.ajax({
                type: 'POST',
                url: $inUrl,
                //dataType: 'JSON',
                data:outData,
                success: function(Responce) {
                    $("#form-ajax-system").html(Responce);
                }
            });     
            return false;
        }
    });
            
    $("#btnLoginUser").on ({
        click : function() {
            $(this).parent().parent().submit();
            return false;
        }
    });
    //
    //alert("YGU");
    
    //
    $("[type=text], textarea").on({
        blur: function() {
            var $outCheck;
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                $(this).attr('verify','false');
                //$outControl = $(this).attr('id');
                } else {
                    $(this).removeClass('error');
                    $(this).attr('verify','true');
                }
        },
    })
    //
    function checkTextEmpty() {
        //alert('checkTextEmpty');
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
        return  $outCheck;   
    }
})


