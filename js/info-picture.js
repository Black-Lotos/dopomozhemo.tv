$(document).ready(function(){ 
    if ($("#divPictureTabs").length>0) {
        $("#divPictureTabs").tabs();
    }
    console.log("info-picture"); // string 1 rad baz tubular
    $("[type=text], textarea").each(function(){
        $(this).on('keypress keydown keyup',function(){
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                //$outResult = ($outResult && false); 
            } else {
                $(this).removeClass('error');
            }
        });
       
    })
    //
    $("#btnSavepicture").on ({
        click : function() {
            if (checkTextEmpty()) {
                $("#frmpicture-add").submit();
                /*$.ajax({
                        type: 'POST',
                        url: '/administration/admin_picture/save',
                        //dataType: 'JSON',
                        data:{data_form: $("#frmpicture-add").serializeArray()},
                        success: function(Responce) {
                            $("#picture_text").val(Responce);
                            //location.reload('/administration/admin_picture');
                        }
                });*/
            }
            return false;
        }
    });
    //
    $("#btnSaveCContent").on ({
        click : function() {
            //alert('click');
            if (checkTextEmpty()) {
                $("#frmpicture-add").submit();
            }
            return false;
        }
    });
    //
    $("[type=text], textarea").on({
        blur: function() {
            var $outCheck;
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                $(this).attr('verify','false');
                //$outControl = $(this).attr('id');
                } else {
                    $(this).removeClass('error');
                    $(this).attr('verify','true');
                }
        },
    })
    //
    $("[id^='ref-picture-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/edit',
                //dataType: 'JSON',
                data:{Process: 'edit-picture', picture_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-picture-delete-']").on({
        click:function() {
            if (confirm('Удалить картинку из коллекции?')) {
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_picture/delete',
                    //dataType: 'JSON',
                    data:{process: 'delete-picture', picture_id:$(this).attr('id')},
                    success: function(Responce) {
                        //$("#form-ajax").html(Responce);
                        location.reload();
                    }
                });     
            }
        }
    });
    //
    $("[id^='ref-picture-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', picture_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-cpicture-delete-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/delete_section',
                //dataType: 'JSON',
                data:{process: 'delete-category', cpicture_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-cpicture-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/edit_section',
                //dataType: 'JSON',
                data:{process: 'edit-category', cpicture_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-cpicture-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            //alert('change category'+$inId);
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-category', cpicture_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
        }
    });
    $("[id^='ref-picture-main-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-main', picture_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-main-un-checked');
                        $("#"+$inId).addClass('action-base action-main-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-main-checked');
                        $("#"+$inId).addClass('action-base action-main-un-checked');
                    }
                }
            });     
        }
    });
    
    //
    
    $("[id^='detail-']").on({
        click:function() {
            //alert('click');
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin_picture/detail',
                //dataType: 'JSON',
                data:{process: 'picture-detail', blogs_id:$inId},
                success: function(Responce) {
                    $('#PagingRecord').html(Responce);
                }
            });     
        }
    });
    //
    function checkTextEmpty() {
        //alert('checkTextEmpty');
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
            //alert($outCheck);
        return  $outCheck;   
    }
    //
    function checkTextEmpty01() {
        //alert('checkTextEmpty');
        var $outCheck = true;
        $("#frmSaveComments").find("input,select,textarea").not('[type="submit"]').not('[type="hidden"]').each(function(){
            var $inVal = $.trim($(this).val());
                if ($inVal<1) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
                        
        });
        return  $outCheck;   
    }
    //
    $("#frmSaveComments").submit(function(){
        var goSubmit = true;
        goSubmit = checkTextEmpty01();
        //alert(goSubmit);
        return goSubmit;
    });
    //
   $("[id^='ref-picture-video-']").on({
        click:function() {
            
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/picture_code',
                //dataType: 'JSON',
                data:{process: 'picture-code', picture_id:$inId},
                success: function(Responce) {
                    window.prompt ('Ваша ссылка',Responce);
                }
            });
        }
    });
})


