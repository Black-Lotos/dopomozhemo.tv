$(document).ready(function(){ 
    console.log("info-blogs"); // string 1 rad baz tubular
    //
    $("#btnClearImageElement").on({
        click:function() {
            //alert('click');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_blogs/clear_image',
                dataType: 'json',
                data:{process: 'clear-image-blogs', blogs_id:$(this).attr('for')},
                success: function(Responce) {
                    $.each(Responce,function(inKey, inData){
                        alert(inKey+" "+inData);
                        console.log(inKey+":"+inData);
                        if(inKey=='rec-no') {
                            $("#images_id").val(inData);
                        };
                        if(inKey=='rec-name') {
                            $("[id^='preview-image-']").attr('src',"/"+inData);
                        }
                    });
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
            //alert();
            return false;
        }
    })
    //
    $("#frmSaveComments").submit(function(){
        var goSubmit = true;
        goSubmit = checkTextEmpty01();
        //alert(goSubmit);
        return goSubmit;
    });
    //
    if ($('#blogs_text').length>0)             
        {$('#blogs_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
});


