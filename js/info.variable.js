$(document).ready(function(){ 
    $("[id^='ref-variable-edit-']").on({
        click : function(){
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_variable/edit',
                //dataType: 'JSON',
                data:{process: 'edit-variable', variable_id:$inId},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
            console.log("info.site => edit => variable => "+$inId);
            return false;
        }
    });
    $("[id^='ref-variable-delete-']").on({
        click : function(){
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_variable/delete_variable',
                //dataType: 'JSON',
                data:{process: 'delete-variable', variable_id:$inId},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            console.log("info.site => edit => variable => "+$inId);
            return false;
        }
    });
});