$(document).ready(function(){ 
    console.log("info-news work"); // string 1 rad baz tubular
    $("[id^='picture-to-div-']").on({
        click:function() {
            var $inImg = $(this).attr('id');
            var $regPattern = /(\d)+$/;
            var $regRes = $regPattern.exec($inImg);
            //alert($regRes[0]);
            $("#div-preview-img").html("<img src='/images_module/getPicture/"+$regRes[0]+"/400/400'/>");
        }
    });
    //
    $("[id^='ref-news-edit-']").on({
        click:function() {
            //alert('edit');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/edit',
                //dataType: 'JSON',
                data:{process: 'edit-news', news_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-news-delete-']").on({
        click:function() {
            if(confirm("Внимание! Будет удалена новость и коментарии к ней. Продолжить?")) {
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_news/delete',
                    //dataType: 'JSON',
                    data:{process: 'delete-news', news_id:$(this).attr('id')},
                    success: function(Responce) {
                        //$("#form-ajax").html(Responce);
                        location.reload();
                    }
                });     
            }
        }
    });
    $("[id^='ref-snews-delete-']").on({
        click:function() {
            if(confirm("Внимание! Будет удалена категория и все новости в ней. Продолжить?")) {
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_news/delete_category',
                    //dataType: 'JSON',
                    data:{process: 'delete-snews', nc_id:$(this).attr('id')},
                    success: function(Responce) {
                        //alert(Responce);
                        location.reload();
                    }
                });     
            }
        }
    });
    $("[id^='ref-snews-edit-']").on({
        click:function() {
            //alert('edit');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/edit_section',
                //dataType: 'JSON',
                data:{process: 'edit-snews', nc_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-news-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', news_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-news-main-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-main', news_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-main-un-checked');
                        $("#"+$inId).addClass('action-base action-main-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-main-checked');
                        $("#"+$inId).addClass('action-base action-main-un-checked');
                    }
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-snews-check-']").on({
        click:function() {
            //alert('click');
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-category', nc_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
        }
    });
    $("[id^='ref-news-delete-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/delete',
                //dataType: 'JSON',
                data:{process: 'delete-news', news_id:$(this).attr('id')},
                success: function(Responce) {
                    
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("#btnClearImageElement").on({
        click:function() {
            //alert('click');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/clear_image',
                dataType: 'json',
                data:{process: 'clear-image-news', news_id:$(this).attr('for')},
                success: function(Responce) {
                    $.each(Responce,function(inKey, inData){
                        //alert(inKey+" "+inData);
                        console.log(inKey+":"+inData);
                        if(inKey=='rec-no') {
                            $("#images_id").val(inData);
                        };
                        if(inKey=='rec-name') {
                            $("[id^='preview-image-']").attr('src',"/"+inData);
                        }
                    });
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
            //alert();
            return false;
        }
    })
    //
    if ($('#news_text').length>0)             
        {$('#news_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste fullpage textcolor colorpicker textpattern"
            ],

            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
})


