$(document).ready(function(){ 
    $("#divLanguageTabs").tabs();
    //btnSaveLanguage
    $("[type=text], textarea").on({
        blur: function() {
            var $outCheck;
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                $(this).attr('verify','false');
                //$outControl = $(this).attr('id');
                } else {
                    $(this).removeClass('error');
                    $(this).attr('verify','true');
                }
        },
    })
    //
    $("#btnSaveLanguage").on ({
        click : function() {
            if (checkTextEmpty()) {
                $.ajax({
                        type: 'POST',
                        url: '/administration/admin_language/save',
                        //dataType: 'JSON',
                        data:{data_form: $(this).parent().parent().serializeArray()},
                        success: function(Responce) {
                            //$(".parent").html(Responce);
                            location.reload();
                    }
                });     
            }
            return false;
        }
    });
    //
    $("[id^='ref-lan-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_language/edit',
                //dataType: 'JSON',
                data:{Process: 'edit-language', language_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-lan-status-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_language/change_status',
                //dataType: 'JSON',
                data:{Process: 'change-status', language_id:$(this).attr('id')},
                success: function(Responce) {
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-lan-delete-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            /*$.ajax({
                type: 'POST',
                url: '/administration/admin_language/delete',
                //dataType: 'JSON',
                data:{Process: 'edit-language', language_id:$(this).attr('id')},
                success: function(Responce) {
                    $(".parent").html(Responce);
                    //location.reload();
                }
            });     */
        }
    });
    //
    function checkTextEmpty() {
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
        return  $outCheck;   
    }
})


