$(document).ready(function(){ 
    $("#divNewsTabs").tabs();
    $("[type=text], textarea").on({
        blur: function() {
            var $outCheck;
            var $inVal = $.trim($(this).val());
            var $inRequire = $(this).attr('require');
            if ($inVal<1 && $inRequire) { 
                $(this).addClass('error');
                $(this).attr('verify','false');
                //$outControl = $(this).attr('id');
                } else {
                    $(this).removeClass('error');
                    $(this).attr('verify','true');
                }
        },
    })
    //
    function checkTextEmpty() {
        //alert('checkTextEmpty');
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
        return  $outCheck;   
    }
})


