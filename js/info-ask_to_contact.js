$(document).ready(function(){ 
    if ($("#divNewsTabs").length>0) {
        $("#divNewsTabs").tabs();
    }
    
    $("#btnSaveask_to_contact").on ({
        click : function() {
            //alert('click');
            if (checkTextEmpty()) {
                $("#frmask_to_contact-add").submit();
            }
            return false;
        }
    });
    //
    $("[id^='ref-ask_to_contact-edit-']").on({
        click:function() {
            $.ajax({
                type: 'POST',
                url: '/administration/admin_asktocontact/edit',
                //dataType: 'JSON',
                data:{process: 'edit-asktocontact', ask_to_contact_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-ask_to_contact-delete-']").on({
        click:function() {
            if (confirm('Удалить запрос?')) {
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_asktocontact/delete',
                    //dataType: 'JSON',
                    data:{process: 'delete-asktocontact', ask_to_contact_id:$(this).attr('id')},
                    success: function(Responce) {
                        //$("#form-ajax").html(Responce);
                        location.reload();
                    }
                });     
            }
        }
    });
    //
    function checkTextEmpty() {
        //alert('checkTextEmpty');
        var $outCheck = true;
            // дополнительная проверка на пустоту
            $("[type=text], textarea").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                var $inRequire = $(this).attr('require');
                if ($inVal<1 && $inRequire) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });
            
        return  $outCheck;   
    }
    //
    function checkTextEmpty01() {
        //alert('checkTextEmpty');
        var $outCheck = true;
        $("#frmSaveComments").find("input,select,textarea").not('[type="submit"]').not('[type="hidden"]').each(function(){
            var $inVal = $.trim($(this).val());
                if ($inVal<1) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
                        
        });
            /*// дополнительная проверка на пустоту
            $("#frmSaveComments:input").each(function(){
                //var $outCheck=true;
                var $inVal = $.trim($(this).val());
                if ($inVal<1) { 
                    $(this).addClass('error');
                    $(this).attr('verify','false');
                    $outCheck = false;
                    $(this).focus();
                    return false;
                } else  {
                            $(this).removeClass('error');
                            $(this).attr('verify','true');
                        }
            });*/
        return  $outCheck;   
    }
    //
    $("#frmSaveComments").submit(function(){
        var goSubmit = true;
        goSubmit = checkTextEmpty01();
        //alert(goSubmit);
        return goSubmit;
    });
    
})


